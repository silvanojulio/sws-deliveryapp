package waterservice.mobile.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.ComprobanteFisico;
import waterservice.mobile.clases.ComprobanteFisicoUtilizado;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.services.Service_ComprobantesFisicos;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;

public class ActivityComprobanteFisico extends AppCompatActivity {

    private EditText txt_numero;
    private EditText txt_prefijo;
    private TextView lbl_tipoComrprobanteFisico;
    private int tipoDeComprobanteId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comprobante_fisico);

        init();
    }

    private void init() {

        txt_numero = (EditText) this.findViewById(R.id.txt_numero);
        txt_prefijo = (EditText) this.findViewById(R.id.txt_prefijo);
        lbl_tipoComrprobanteFisico = (TextView) this.findViewById(R.id.lbl_tipoComrprobanteFisico);

        tipoDeComprobanteId = (Integer) SessionVars.Get_Session_Var(SessionKeys.TIPO_COMPROBANTE_FISICO_ID);

        if(tipoDeComprobanteId == Constantes.TIPO_COMPROBANTE_RECIBO)
            lbl_tipoComrprobanteFisico.setText("Recibo");
        else if(tipoDeComprobanteId== Constantes.TIPO_COMPROBANTE_REMITO)
            lbl_tipoComrprobanteFisico.setText("Remito");
    }


    public void btn_confirmarOnClick(View view){

        try{
            confirmar();
        }catch (Exception ex){
            Dialog.error("Error", ex.getMessage(), this);
        }
    }

    private void confirmar() throws Exception{

        ComprobanteFisicoUtilizado comprobanteFisico = new ComprobanteFisicoUtilizado();
        comprobanteFisico.tipoDeComprobanteFisico_ids = tipoDeComprobanteId;

        try{
            comprobanteFisico.nroPrefijo = Long.valueOf(txt_prefijo.getText().toString());
        }catch (Exception ex){
            throw new Exception("Prefijo inválido");
        }

        try{
            comprobanteFisico.nroComprobante = Long.valueOf(txt_numero.getText().toString());
        }catch (Exception ex){
            throw new Exception("Número de comprobante inválido");
        }

        new Service_ComprobantesFisicos(this).validarComprobanteFisico(
                comprobanteFisico.tipoDeComprobanteFisico_ids,
                comprobanteFisico.nroPrefijo,
                comprobanteFisico.nroComprobante);

        SessionVars.Add_Session_Var(SessionKeys.COMPROBANTE_FISICO_CREADO, comprobanteFisico);
        this.setResult(RESULT_OK);
        this.finish();
    }

}
