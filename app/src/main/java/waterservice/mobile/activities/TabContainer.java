package waterservice.mobile.activities;

import java.util.ArrayList;
import java.util.List;

public class TabContainer {

    public List<ITabViewStub> tabs;

    public TabContainer(){
        tabs = new ArrayList<>();
    }

    public void addTab(ITabViewStub tab){
        tabs.add(tab);
        tab.setSelected(tabs.size()==1);
    }

    public void onButtonClick(ITabViewStub tab){

        for (ITabViewStub t: tabs) {
            t.setSelected(false);
        }
        tab.setSelected(true);
    }
}
