package waterservice.mobile.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.Usuario;
import waterservice.mobile.services.Service_ControlDePlayaOnLine;
import waterservice.mobile.utiles.AsyncWork;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivityLoginControlador extends Activity {

    private EditText loginUsuario_txt_password;
    private EditText loginUsuario_txt_usuario;

    private Service_ControlDePlayaOnLine _srvControlDePlaya;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_controlador);

        init();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login_interno, menu);
        return true;
    }

    private void init(){

        loginUsuario_txt_password = (EditText) this.findViewById(R.id.loginUsuario_txt_password);
        loginUsuario_txt_usuario = (EditText) this.findViewById(R.id.loginUsuario_txt_usuario);

        _srvControlDePlaya=new Service_ControlDePlayaOnLine(this);
    }

    public void confirmarLogin(View view){

        AsyncWork.run("confirmar", this, "Comprobando usuario...");

    }

    public void confirmar(){

        try{

            String username=loginUsuario_txt_usuario.getText().toString();
            String password = loginUsuario_txt_password.getText().toString();

            Usuario usuario = _srvControlDePlaya.obtenerUsuarioControlador(username, password);
            SessionVars.Add_Session_Var(SessionKeys.USUARIO_LOGUEADO,usuario);

            Utiles.Mensaje_En_Pantalla("Bienvenido "+usuario.nombreApellido, true, true, this);
            this.setResult(RESULT_OK);
            this.finish();

        }catch (Exception ex){
            ex.printStackTrace();
            Utiles.Mensaje_En_Pantalla("Error: " + ex.getMessage(), true, true, this);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.mainMenu_configuracion:
                configOnClick(null);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void configOnClick(View v) {
        try {

            Intent myIntent=new Intent(this,Configuracion_Activity.class);
            startActivity(myIntent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
