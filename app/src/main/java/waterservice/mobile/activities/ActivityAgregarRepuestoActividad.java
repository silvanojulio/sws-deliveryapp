package waterservice.mobile.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.ArticuloDeLista;
import waterservice.mobile.clases.RepuestoActividad;
import waterservice.mobile.services.Service_Articulos;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivityAgregarRepuestoActividad extends AppCompatActivity {

    private EditText txt_cantidad;
    private Spinner spn_articulo;
    private List<RepuestoActividad> repuestosActividades;
    private List<ArticuloDeLista> articulos;
    private Service_Articulos _srvArticulos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_repuesto_actividad);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {

        _srvArticulos = new Service_Articulos(this);

        txt_cantidad = (EditText) this.findViewById(R.id.txt_cantidad);
        spn_articulo = (Spinner) this.findViewById(R.id.spn_articulo);

        repuestosActividades = (List<RepuestoActividad>) SessionVars.Get_Session_Var(SessionKeys.REPUESTOSACTIVIDADES);

        boolean esRepuesto = (boolean) SessionVars.Get_Session_Var(SessionKeys.ES_REPUESTO);

        txt_cantidad.setText("1");

        if(esRepuesto){
            articulos = _srvArticulos.obtenerArticulosRepuestos();

        }else{
            articulos = _srvArticulos.obtenerArticulosActividadesDeMantenimiento();
            txt_cantidad.setFocusable(false);
        }

        Utiles.SetItemsToSpinner(spn_articulo,articulos,"nombreArticulo",this);
    }

    public void btn_agregarOnClick(View view){

        int cantidad=0;

        try {
            cantidad = Integer.valueOf(txt_cantidad.getText().toString());
        }catch (Exception ex){
            Utiles.Mensaje_En_Pantalla("Error en la cantidad",true, false, this);
            return;
        }

        ArticuloDeLista articuloSeleccionado = articulos.get(spn_articulo.getSelectedItemPosition());

        RepuestoActividad item = new RepuestoActividad();
        item.cantidad = cantidad;
        item.articulo_id = articuloSeleccionado.id;
        item.tipo_id = articuloSeleccionado.tipoArticulo_ids;
        item.nombreDelItem = articuloSeleccionado.nombreArticulo;
        repuestosActividades.add(item);

        this.finish();
    }
}
