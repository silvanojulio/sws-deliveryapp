package waterservice.mobile.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.GastoDeReparto;
import waterservice.mobile.clases.ValorSatelite;
import waterservice.mobile.services.Service_General;
import waterservice.mobile.services.Service_HojasDeRutas;
import waterservice.mobile.utiles.AsyncWork;
import waterservice.mobile.utiles.Utiles;

public class ActivityGasto extends Activity {

    private EditText gasto_txt_descripcion;
    private EditText gasto_txt_importe;
    private Spinner gasto_spn_tipo;

    Service_HojasDeRutas _srvHojaDeRuta;
    Service_General _srvGeneral;

    List<ValorSatelite> tipoGastoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gasto);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {

        gasto_txt_descripcion = (EditText) this.findViewById(R.id.gasto_txt_descripcion);
        gasto_txt_importe = (EditText) this.findViewById(R.id.gasto_txt_importe);
        gasto_spn_tipo = (Spinner) this.findViewById(R.id.gasto_spn_tipo);

        _srvHojaDeRuta=new Service_HojasDeRutas(this);
        _srvGeneral = new Service_General(this);

        tipoGastoList = _srvGeneral.obtenerValoresSatelites(Constantes.T_TIPOSDEGASTOS);

        Utiles.SetItemsToSpinner(gasto_spn_tipo, tipoGastoList, "valor_texto", this);

    }

    public void gasto_btn_confirmarOnClick(View view){

        Utiles.MostrarConfirmacion("Confirmacion","Confirma registrar este gasto?",
                "respuestaDialogoConfirmacion",this, this);
    }

    public void respuestaDialogoConfirmacion(){
        AsyncWork.run("confirmarGuardarGasto",this,"Registrando gasto...");
    }

    public void confirmarGuardarGasto()throws Exception{

        GastoDeReparto gasto = new GastoDeReparto();
        gasto.descripcion = gasto_txt_descripcion.getText().toString();
        gasto.montoGasto = Utiles.getDouble(gasto_txt_importe, 0);
        gasto.tipoGasto_id = tipoGastoList.get(gasto_spn_tipo.getSelectedItemPosition()).valor_id;

        _srvHojaDeRuta.guardarGasto(gasto);

        this.finish();
    }
}
