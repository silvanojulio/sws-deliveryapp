package waterservice.mobile.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.OrdenDeTrabajo;
import waterservice.mobile.services.Service_Articulos;
import waterservice.mobile.services.Service_Clientes;
import waterservice.mobile.services.Service_ServicioTecnico;
import waterservice.mobile.services.Service_Ventas;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivityMenuCliente  extends Activity implements PopupMenu.OnMenuItemClickListener {

    TextView menuCliente_lbl_nombreCliente;
    TextView menuCliente_lbl_direccion;
    Cliente _cliente;
    Service_Clientes _srvClientes;
    Service_Ventas _srvVentas;
    private Service_Articulos _srvArticulos;
    private Button menuCliente_btn_atencionCliente;
    private Button menuCliente_btn_ausente;
    private Button menuCliente_btn_devoluciones;
    private Button menuCliente_btn_envases;
    private Button menuCliente_btn_facturas;
    private Button menuCliente_btn_fichaCliente;
    private Button menuCliente_btn_noGestiona;
    private Button menuCliente_btn_transacciones;
    private Button menuCliente_btn_ventasEntregas;
    private Button menuCliente_btn_remito;
    private Button menuCliente_btn_venta_web;
    AppSession appSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_cliente);

        init();
    }

    private void init(){

        this.setFinishOnTouchOutside(false);

        appSession = new AppSession(this);

        menuCliente_lbl_direccion=(TextView) findViewById(R.id.menuCliente_lbl_direccion);
        menuCliente_lbl_nombreCliente=(TextView) findViewById(R.id.menuCliente_lbl_nombreCliente);

        menuCliente_btn_ausente = (Button) this.findViewById(R.id.menuCliente_btn_ausente);
        menuCliente_btn_devoluciones = (Button) this.findViewById(R.id.menuCliente_btn_devoluciones);
        menuCliente_btn_fichaCliente = (Button) this.findViewById(R.id.menuCliente_btn_fichaCliente);
        menuCliente_btn_noGestiona = (Button) this.findViewById(R.id.menuCliente_btn_noGestiona);
        menuCliente_btn_transacciones = (Button) this.findViewById(R.id.menuCliente_btn_transacciones);
        menuCliente_btn_ventasEntregas = (Button) this.findViewById(R.id.menuCliente_btn_ventasEntregas);
        menuCliente_btn_venta_web = (Button) this.findViewById(R.id.menuCliente_btn_venta_web);

        _srvArticulos = new Service_Articulos(this);
        _srvClientes = new Service_Clientes(this);

        try {
            _cliente = appSession.getClienteActual();
            _cliente.articulosAbonos=_srvArticulos.obtenerArticulosDeAbonos(_cliente.cliente_id);
            _cliente.articulosComodatos=_srvArticulos.obtenerArticulosDeComodatos(_cliente.cliente_id);
            _cliente.articulosLista=_srvArticulos.obtenerArticulosDeLista();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try{
            _srvArticulos.asignarPreciosSegunPrioridad(_cliente);
        }catch (Exception e){
            e.printStackTrace();
        }

        menuCliente_lbl_nombreCliente.setText(_cliente.nombreCliente);
        menuCliente_lbl_direccion.setText(_cliente.domicilioCompleto);

        _srvVentas=new Service_Ventas(this);

        configurarBotones();
    }

    public void btn_ventasEntregas_onClick(View view){

        try {

            if(_cliente.ausente || (_cliente.visitado && _cliente.cerrado && !_cliente.ventaEntrega))
                return;

            if(_cliente.ventaEntrega){

                Dialog.confirm("Cliente con entregas",
                        "Desea imprimir el comprobante de entregas?",
                        new Dialog.OnConfirm() {
                            @Override
                            public void confirm() {

                                try {
                                    new Service_Ventas(getApplicationContext()).imprimirComprobante(_cliente, ActivityMenuCliente.this);
                                } catch (Exception e) {
                                    Utiles.Mensaje_En_Pantalla(e.getMessage(), true,false,ActivityMenuCliente.this);
                                }
                            }
                        }, this);

                return;
            }

            if(_cliente.tieneAlertas && !_cliente.haVistoAlertas)
            {
                btn_alertas_onClick(view);
                return;
            }

            if(appSession.getBloquearCuenta() &&
                    _cliente.esCuentaBloqueada && !(_cliente.desbloqueado != null && _cliente.desbloqueado))
            {
                onVentaWebClick(null);
                return;
            }

            Intent intent=new Intent(this, ActivityVenta.class);
            startActivity(intent);

        } catch (Exception e) {
            Dialog.error("No permitido", e.getMessage(),this);
        }
    }

    public void btn_devoluciones_onClick(View view){
        try {
            Intent intent=new Intent(this, ActivityDevoluciones.class);
            startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void btn_transacciones_onClick(View view){
        try {
            Intent intent=new Intent(this, ActivityTransacciones.class);
            startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void btn_fichaCliente_onClick(View view){

        try {
            Intent intent=new Intent(this, ActivityFichaCliente.class);
            startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void btn_alertas_onClick(View view){

        Intent intent=new Intent(this, ActivityAlertasDelCliente.class);
        startActivity(intent);
    }

    public void btn_clienteAusente_onClick(View view){

        if(_cliente.ausente){
            imprimirComprobanteDeVisita();
            return;
        }

        if(_cliente.tieneAlertas && !_cliente.haVistoAlertas)
        {
            btn_alertas_onClick(view);
            return;
        }

        new AlertDialog.Builder(this)
                //.setIcon(android.R.drawable.ic_dialog_alert)
                .setIcon(R.drawable.question)
                .setTitle("Cliente sin gestiones")
                .setMessage("Confirma cliente ausente?" )
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {

                    //@Override
                    public void onClick(DialogInterface dialog, int which) {

                        ActivityMenuCliente.this.confirmarAusente();
                    }
                })
                .setNegativeButton("NO", null)
                .show();
    }

    public void btn_clienteNoGestion_onClick(View view){

        if(_cliente.tieneAlertas && !_cliente.haVistoAlertas)
        {
            btn_alertas_onClick(view);
            return;
        }

        new AlertDialog.Builder(this)
                //.setIcon(android.R.drawable.ic_dialog_alert)
                .setIcon(R.drawable.question)
                .setTitle("Cliente sin gestiones")
                .setMessage("Confirma que el cliente no tenga gestiones?" )
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {

                    //@Override
                    public void onClick(DialogInterface dialog, int which) {

                        ActivityMenuCliente.this.confirmarNoCompra();
                    }
                })
                .setNegativeButton("NO", null)
                .show();
    }

    public void btn_Cobros_onClick(View v){

        if(_cliente.ausente)
        {
            return;
        }

        Intent intent = new Intent(this, ActivityRecibo.class);
        startActivityForResult(intent, Constantes.ACTIVITY_RECIBO);
    }

    public void btn_tomarCoordinadas_onClick(View view){

        Intent intent = new Intent(this, ActivitySeleccionarUbicacionCliente.class);
        startActivity(intent);

        //Utiles.MostrarConfirmacion("Registrar coordenadas del cliente",
                //"Confirma el registro de las coordenadas del cliente?",
                //"registrarCoordenadas", this, this);
    }

    public void btn_servicioTecnico_onClick(View view){

        try {

            Service_ServicioTecnico srvServicioTecnico=new Service_ServicioTecnico(this);
            OrdenDeTrabajo ot = srvServicioTecnico.ObtenerOrdenDeTrabajoDeCliente(this._cliente.cliente_id);

            if(ot==null){

                throw new Exception("El cliente no posee ninguna orden de trabajo");

            }else{
                SessionVars.Add_Session_Var(SessionKeys.ORDEN_DE_TRABAJO_ACTUAL, ot);
                Intent intent=new Intent(this, ActivityOrdenDeTrabajo.class);
                startActivity(intent);
            }

        } catch (Exception e) {
            Utiles.Mensaje_En_Pantalla(e.getMessage(), true, false, this);
        }
    }

    void confirmarAusente(){

        try{
            _srvVentas.GuardarAusente(_cliente);

            Toast.makeText(getApplicationContext(),
                    "El cliente se ha registrado como ausente", Toast.LENGTH_LONG).show();

            imprimirComprobanteDeVisita();

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),
                    e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    void imprimirComprobanteDeVisita(){
        Dialog.confirm("Imprimir comprobante", "Desea imprimir el comprobante de visita?",
                new Dialog.OnConfirm() {
                    @Override
                    public void confirm() {
                        try {
                            _srvClientes.imprimirComprobanteDeVisita(_cliente, ActivityMenuCliente.this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                ,this);
    }

    void confirmarNoCompra(){

        try{
            _srvVentas.GuardarNoCompra(_cliente);

            Toast.makeText(getApplicationContext(),
                    "Registro exitoso", Toast.LENGTH_LONG).show();
            this.finish();

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),
                    e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    void configurarBotones(){

        //menuCliente_btn_ausente.setVisibility(_cliente.visitado?View.GONE:View.VISIBLE);
        menuCliente_btn_noGestiona.setVisibility(_cliente.visitado?View.GONE:View.VISIBLE);
    }

    @Override
    protected void onPostResume() {

        configurarBotones();
        super.onPostResume();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0 && _cliente.visitado && !_cliente.cerrado) {

            Utiles.MostrarConfirmacion("Salir",
                    "Confirma salir de esta pantalla, no podra volver a ingresar al menú del cliente.",
                    "confirmarSalir",this,this);

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    public void confirmarSalir(){

        _srvClientes.confirmarClienteCerrado(_cliente);
        this.finish();
    }

    public void btn_barriles_onClick(View view) {
        //Intent intent = new Intent(this, ActivityEntregaEnvases.class);
        //intent.putExtra("esDevolucion", false);
        //startActivityForResult(intent, Constantes.ACTIVITY_RECIBO);
    }

    private void onCobrosResult(int resultCode){

        try {
            this._cliente = new Service_Clientes(this).obtenerCliente(this._cliente.cliente_id);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch(requestCode) {
            case Constantes.ACTIVITY_RECIBO:
                onCobrosResult(resultCode);
                break;
        }

    }

    public void btn_incidentesOnClick(View v){
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(this);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.incidentes, popup.getMenu());
        popup.show();

    }

    void onIncidentesCreate(){
        String urlSistema = getUrlSistema()+"/Incidentes/Create?cliente_id="+
                String.valueOf(_cliente.cliente_id);
        Utiles.openUrl(urlSistema, this);
    }

    void onIncidentesList(){
        String urlSistema = getUrlSistema()+"/Incidentes/Historial?cliente_id="+
                String.valueOf(_cliente.cliente_id);
        Utiles.openUrl(urlSistema, this);
    }


    public void onVentaWebClick(View v) throws Exception {
        String urlSistema = getUrlSistema()+"/TransaccionesTemporales/Create?clienteId="+
                String.valueOf(_cliente.cliente_id)+"&hojaDeRutaId="+String.valueOf(appSession.getHojaDeRuta().id);
        Utiles.openUrl(urlSistema, this);
    }

    String getUrlSistema(){
        final String serverRemoto= appSession.getValueString("servidor_web");
        return serverRemoto;
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_incidentes_create:
                onIncidentesCreate();
                return true;
            case R.id.menu_incidentes_list:
                onIncidentesList();
                return true;
            default:
                return false;
        }
    }
}
