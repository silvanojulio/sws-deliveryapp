package waterservice.mobile.activities.formasDePago;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.Cheque;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.services.Service_Recibos;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;

public class ActivityFormaDePagoEfectivo extends ActivityFormaDePagoBase {

    private Button btn_confirmar;
    private EditText txt_importe;
    private Cliente cliente;
    private boolean enPantallaVentas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forma_de_pago_efectivo);
        try
        {
            enPantallaVentas = getIntent().getExtras()!=null?
                    getIntent().getExtras().getBoolean("enPantallaVentas", false):
                    false;

            init();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void init() throws Exception {

        btn_confirmar = (Button) this.findViewById(R.id.btn_confirmar);
        txt_importe = (EditText) this.findViewById(R.id.txt_importe);

        cliente = appSession.getClienteActual();
    }

    public void btn_confirmarOnClick(View v){

        if(guardando) return;
        guardando = true;

        try{

            guardarEfectivo();

        }catch (Exception ex)
        {
            Dialog.error("Error", ex.getMessage(), this);
        }

        guardando = false;

    }

    private void guardarEfectivo() throws Exception {

        double importe;

        try{
            importe = Double.valueOf( txt_importe.getText().toString());
        }catch (Exception ex){
            if(enPantallaVentas)
            {
                importe = 0;
            }else{
                throw new Exception("Importe no válido");
            }
        }

        if(enPantallaVentas)
        {
            Intent intent = new Intent();
            intent.putExtra("importe", importe);
            this.setResult(RESULT_OK, intent);
            this.finish();
        }else{
            new Service_Recibos(this).guardarItemReciboEfectivo(importe, cliente);
            this.setResult(RESULT_OK);
            this.finish();
        }

    }
}
