package waterservice.mobile.activities.formasDePago;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.Retencion;
import waterservice.mobile.clases.ValorSatelite;
import waterservice.mobile.services.Service_General;
import waterservice.mobile.services.Service_Recibos;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.Utiles;
import waterservice.mobile.views.UIFechaSimple;

public class ActivityFormaDePagoRetencion extends ActivityFormaDePagoBase {

    private EditText txt_descripcion;
    private EditText txt_importe;
    private Spinner spn_tipoDeRetencion;
    private UIFechaSimple ctr_fecha;

    private Retencion retencion;
    private List<ValorSatelite> tiposDeRetenciones;

    private Cliente cliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forma_de_pago_retencion);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {

        txt_descripcion = (EditText) this.findViewById(R.id.txt_descripcion);
        txt_importe = (EditText) this.findViewById(R.id.txt_importe);
        spn_tipoDeRetencion = (Spinner) this.findViewById(R.id.spn_tipoDeRetencion);
        ctr_fecha = (UIFechaSimple) this.findViewById(R.id.ctr_fecha);

        cliente = appSession.getClienteActual();

        tiposDeRetenciones = new Service_General(this).obtenerValoresSatelites(Constantes.T_TIPOS_RETENCIONES);
        Utiles.SetItemsToSpinner(spn_tipoDeRetencion, tiposDeRetenciones, "valor_texto", this);
    }

    public void btn_confirmarOnClick(View v){

        if(guardando) return;
        guardando = true;

        try{

            guardarRetencion();

        }catch (Exception ex)
        {
            Dialog.error("Error", ex.getMessage(), this);
        }

        guardando = false;
    }

    private void guardarRetencion() throws Exception{

        if(retencion==null)
            retencion = new Retencion();

        ValorSatelite tipoSelected = tiposDeRetenciones.get(spn_tipoDeRetencion.getSelectedItemPosition());

        retencion.tipoRetencionId = tipoSelected.valor_id;

        try{
            retencion.importe = Double.valueOf( txt_importe.getText().toString());
        }catch (Exception ex){
            throw new Exception("Importe no válido");
        }

        retencion.descripcion = txt_descripcion.getText().toString();
        retencion.fecha = Utiles.Get_Fecha(ctr_fecha.obtenerFechaSeleccionada().getFechaDate());
        retencion.descripcionResumen = obtenerResumen(tipoSelected, retencion);

         new Service_Recibos(this).guardarItemReciboRetencion(retencion, cliente);

        this.setResult(RESULT_OK);
        this.finish();
    }

    private String obtenerResumen(ValorSatelite tipo, Retencion retencion) {

        return "Tipo "+ tipo.valor_texto+ ". "+ retencion.descripcion;
    }

}
