package waterservice.mobile.activities.formasDePago;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;

public class ActivitySeleccionarFormaDePago extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccionar_forma_de_pago);
    }

    public void efectivoOnClick(View v){
        seleccionarFormaDePago(Constantes.FORMADEPAGO_EFECTIVO);
    }

    public void chequeOnClick(View v){
        seleccionarFormaDePago(Constantes.FORMADEPAGO_CHEQUE);
    }

    public void retencionOnClick(View v){
        seleccionarFormaDePago(Constantes.FORMADEPAGO_RETENCIONES);
    }

    public void tarjetaDeDebitoOnClick(View v){
        seleccionarFormaDePago(Constantes.FORMADEPAGO_TARJETADEBITO);
    }

    public void tarjetaDeCreditoOnClick(View v){
        seleccionarFormaDePago(Constantes.FORMADEPAGO_TARJETACREDITO);
    }

    private void seleccionarFormaDePago(int formaDePagoId){

        SessionVars.Add_Session_Var(SessionKeys.FORMA_DE_PAGO_SELECTED, formaDePagoId);
        setResult(RESULT_OK);
        this.finish();
    }
}
