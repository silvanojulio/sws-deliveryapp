package waterservice.mobile.activities.formasDePago;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.Cheque;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.TarjetaDeCredito;
import waterservice.mobile.clases.ValorSatelite;
import waterservice.mobile.services.Service_General;
import waterservice.mobile.services.Service_Recibos;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivityFormaDePagoTarjetaDeCredito extends ActivityFormaDePagoBase {

    private Service_General srvGeneral;
    private Service_Recibos srvRecibos;
    private List<ValorSatelite> bancos, marcas;
    private TarjetaDeCredito tarjetaActual;
    private Cliente clienteActual;

    private Spinner spn_marcaTarjeta;
    private Spinner spn_banco;
    private EditText txt_codigoAutorizacion;
    private EditText txt_cuotas;
    private EditText txt_cupon;
    private EditText txt_importe;
    private EditText txt_lote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forma_de_pago_tarjeta_de_credito);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {

        srvRecibos = new Service_Recibos(this);
        srvGeneral = new Service_General(this);
        bancos = srvGeneral.obtenerValoresSatelites(Constantes.T_BANCOS);
        marcas = srvGeneral.obtenerValoresSatelites(Constantes.T_MARCAS_TARJETAS);

        clienteActual = appSession.getClienteActual();

        spn_marcaTarjeta = (Spinner) this.findViewById(R.id.spn_marcaTarjeta);
        spn_banco = (Spinner) this.findViewById(R.id.spn_banco);

        txt_codigoAutorizacion = (EditText) this.findViewById(R.id.txt_codigoAutorizacion);
        txt_cuotas = (EditText) this.findViewById(R.id.txt_cuotas);
        txt_cupon = (EditText) this.findViewById(R.id.txt_cupon);
        txt_importe = (EditText) this.findViewById(R.id.txt_importe);
        txt_lote = (EditText) this.findViewById(R.id.txt_lote);

        bancos = srvGeneral.obtenerValoresSatelites(Constantes.T_BANCOS);
        marcas = srvGeneral.obtenerValoresSatelites(Constantes.T_MARCAS_TARJETAS);

        Utiles.SetItemsToSpinner(spn_banco, bancos, "valor_texto", this);
        Utiles.SetItemsToSpinner(spn_marcaTarjeta, marcas, "valor_texto", this);
    }


    public void btn_confirmarOnClick(View v){

        if(guardando) return;
        guardando = true;

        try{

            guardarTarjetaDeCredito();

        }catch (Exception ex)
        {
            Dialog.error("Error", ex.getMessage(), this);
        }

        guardando = false;
    }

    private void guardarTarjetaDeCredito() throws Exception{

        if(tarjetaActual==null)
            tarjetaActual = new TarjetaDeCredito();

        ValorSatelite bancoSelected = bancos.get(spn_banco.getSelectedItemPosition());
        ValorSatelite marcaSelected = marcas.get(spn_marcaTarjeta.getSelectedItemPosition());

        tarjetaActual.banco_id = bancoSelected.valor_id;
        tarjetaActual.marcaTarjeta_id = marcaSelected.valor_id;

        try{
            tarjetaActual.importe = Double.valueOf( txt_importe.getText().toString());
        }catch (Exception ex){
            throw new Exception("Importe no válido");
        }

        try{
            tarjetaActual.cuotas = Integer.valueOf( txt_cuotas.getText().toString());
        }catch (Exception ex){
            throw new Exception("Cantidad de cuotas no válido");
        }

        tarjetaActual.lote = txt_lote.getText().toString();
        tarjetaActual.codigoAutorizacion = txt_codigoAutorizacion.getText().toString();
        tarjetaActual.cupon = txt_cupon.getText().toString();
        tarjetaActual.descripcionResumen = obtenerResumen(bancoSelected, marcaSelected, tarjetaActual);

        srvRecibos.guardarItemReciboTarjetaDeCredito(tarjetaActual, clienteActual);

        this.setResult(RESULT_OK);
        this.finish();
    }

    private String obtenerResumen(ValorSatelite bancoSelected, ValorSatelite marca, TarjetaDeCredito tarjetaActual) {

        return marca.valor_texto + " banco "+ bancoSelected.valor_texto +
                ". Cupón "+ tarjetaActual.cupon + ". Lote "+ tarjetaActual.lote + ". Cod aut "+
                tarjetaActual.codigoAutorizacion+ ". ";
    }
}
