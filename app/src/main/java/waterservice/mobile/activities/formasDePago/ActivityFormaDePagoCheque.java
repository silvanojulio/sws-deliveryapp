package waterservice.mobile.activities.formasDePago;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.Cheque;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.ValorSatelite;
import waterservice.mobile.services.Service_General;
import waterservice.mobile.services.Service_Recibos;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.Utiles;
import waterservice.mobile.views.UIFechaSimple;

public class ActivityFormaDePagoCheque extends ActivityFormaDePagoBase {

    private Service_General srvGeneral;
    private Service_Recibos srvRecibos;
    private List<ValorSatelite> bancos;
    private Cheque chequeActual;
    private Cliente clienteActual;

    private Spinner spn_banco;
    private EditText txt_importe;
    private EditText txt_librador;
    private EditText txt_nroDeCheque;
    private EditText txt_sucursal;

    private UIFechaSimple ctr_fechaDeCobro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forma_de_pago_cheque);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void init() throws Exception {

        srvRecibos = new Service_Recibos(this);
        srvGeneral = new Service_General(this);
        bancos = srvGeneral.obtenerValoresSatelites(Constantes.T_BANCOS);

        clienteActual = appSession.getClienteActual();

        spn_banco = (Spinner) this.findViewById(R.id.spn_banco);
        Utiles.SetItemsToSpinner(spn_banco, bancos, "valor_texto", this);

        txt_importe = (EditText) this.findViewById(R.id.txt_importe);
        txt_librador = (EditText) this.findViewById(R.id.txt_librador);
        txt_nroDeCheque = (EditText) this.findViewById(R.id.txt_nroDeCheque);
        txt_sucursal = (EditText) this.findViewById(R.id.txt_sucursal);
        ctr_fechaDeCobro = (UIFechaSimple) this.findViewById(R.id.ctr_fechaDeCobro);
    }

    public void btn_confirmarOnClick(View v){

        if(guardando) return;
        guardando = true;

        try{

            guardarCheque();

        }catch (Exception ex)
        {
            Dialog.error("Error", ex.getMessage(), this);
        }

        guardando = false;
    }

    private void guardarCheque() throws Exception{

        if(chequeActual==null)
            chequeActual = new Cheque();

        ValorSatelite bancoSelected = bancos.get(spn_banco.getSelectedItemPosition());

        chequeActual.banco_id = bancoSelected.valor_id;

        try{
            chequeActual.importe = Double.valueOf( txt_importe.getText().toString());
        }catch (Exception ex){
            throw new Exception("Importe no válido");
        }

        chequeActual.librador = txt_librador.getText().toString();
        chequeActual.nroCheque = txt_nroDeCheque.getText().toString();
        chequeActual.nroSucursalBanco = txt_sucursal.getText().toString();
        chequeActual.fechaCobro = Utiles.Get_Fecha(ctr_fechaDeCobro.obtenerFechaSeleccionada().getFechaDate());
        chequeActual.descripcionResumen = obtenerResumen(bancoSelected, chequeActual);

        srvRecibos.guardarItemReciboCheque(chequeActual, clienteActual);

        this.setResult(RESULT_OK);
        this.finish();
    }

    private String obtenerResumen(ValorSatelite bancoSelected, Cheque cheque) {

        return "Cheque Nro "+ cheque.nroCheque + ". Banco "+ bancoSelected.valor_texto;
    }

}
