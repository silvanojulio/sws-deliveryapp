package waterservice.mobile.activities.formasDePago;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;

public class ActivityFormaDePagoBase extends AppCompatActivity {
    AppSession appSession;
    boolean guardando=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appSession = new AppSession(this);
    }
}
