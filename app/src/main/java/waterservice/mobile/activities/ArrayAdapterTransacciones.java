package waterservice.mobile.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.Transaccion;
import waterservice.mobile.utiles.Utiles;

public class ArrayAdapterTransacciones extends ArrayAdapter<Transaccion> {

    List<Transaccion> _values;
    Context _context;

    private TextView rowTran_evento;
    private TextView rowTran_texto1;
    private TextView rowTran_texto2;
    private TextView rowTran_texto3;
    private ImageView rowTran_img;

    public ArrayAdapterTransacciones(Context context, List<Transaccion> values) {

        super(context, R.layout.row_transaccion ,values);
        this._context = context;
        this._values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) _context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Transaccion transaccion=_values.get(position);

        View rowView = inflater.inflate(R.layout.row_transaccion, parent, false);

        rowTran_evento = (TextView) rowView.findViewById(R.id.rowTran_evento);
        rowTran_texto1 = (TextView) rowView.findViewById(R.id.rowTran_texto1);
        rowTran_texto2 = (TextView) rowView.findViewById(R.id.rowTran_texto2);
        rowTran_texto3 = (TextView) rowView.findViewById(R.id.rowTran_texto3);
        rowTran_img = (ImageView) rowView.findViewById(R.id.rowTran_img);

        rowTran_texto1.setText(transaccion.cliente+ " (" + String.valueOf(transaccion.cliente_id) + ")");

        switch (transaccion.tipoTransaccion){
            case Transaccion.Tipos.VENTA: //Venta
                rowTran_img.setImageResource(R.drawable.cart);
                rowTran_evento.setText("Venta");
                rowTran_texto2.setText(transaccion.articulo +" (X"+ String.valueOf(transaccion.cantidad)+")");
                rowTran_texto3.setText("Subtotal $"+ Utiles.Round(transaccion.precio) );

                break;
        case Transaccion.Tipos.COBRO: //Cobro facturas
                rowTran_img.setImageResource(R.drawable.cobro);
                rowTran_evento.setText("Cobro");
                rowTran_texto2.setText("Forma de pago: "+transaccion.articulo);
                rowTran_texto3.setText("Cobro $"+ Utiles.Round( transaccion.precio) );

                break;
            case Transaccion.Tipos.ARTICULO_DEVOLUCION: //Devoluciones articulos
                rowTran_img.setImageResource(R.drawable.product_return);
                rowTran_evento.setText("Dev art");
                rowTran_texto2.setText(transaccion.articulo);
                rowTran_texto3.setText("X "+ String.valueOf(transaccion.cantidad) );
                break;
            case Transaccion.Tipos.ENVASES_PRESTAMO: //Prestamos envases
                rowTran_img.setImageResource(R.drawable.star);

                rowTran_evento.setText("Pres env");
                rowTran_texto2.setText(transaccion.articulo);
                rowTran_texto3.setText("X "+ String.valueOf(transaccion.cantidad) );

                break;
            case Transaccion.Tipos.ENVASES_DEVOLUCION: //Devoluciones envases
                rowTran_img.setImageResource(R.drawable.star);

                rowTran_evento.setText("Dev env");
                rowTran_texto2.setText(transaccion.articulo);
                rowTran_texto3.setText("X "+ String.valueOf(transaccion.cantidad) );

                break;
            case Transaccion.Tipos.ENVASES_RELEVAMIENTO: //Relvamiento
                rowTran_img.setImageResource(R.drawable.star);

                rowTran_evento.setText("Relevamiento");
                rowTran_texto2.setText(transaccion.articulo);
                rowTran_texto3.setText("X "+ String.valueOf(transaccion.cantidad) );

                break;
        }

        return rowView;

    }
}
