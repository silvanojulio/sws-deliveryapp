package waterservice.mobile.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.ArticuloControlDePlaya;

public class ArrayAdapterArticulosControlPlaya extends ArrayAdapter<ArticuloControlDePlaya> {

    List<ArticuloControlDePlaya> _values;
    Context _context;

    public ArrayAdapterArticulosControlPlaya(Context context, List<ArticuloControlDePlaya> values) {

        super(context, R.layout.row_articulo_venta ,values);
        this._context = context;
        this._values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView rowArtControl_lbl_articulo;
        TextView rowArtControl_lbl_cantFalladosPack;
        TextView rowArtControl_lbl_cantFalladosUni;
        TextView rowArtControl_lbl_cantLlenosPack;
        TextView rowArtControl_lbl_cantLlenosUni;
        TextView rowArtControl_lbl_cantVaciosPack;
        TextView rowArtControl_lbl_cantVaciosUni;
        TextView rowArtControl_lbl_cantRepuesto;

        TextView lbl_dispenser_l1;
        TextView lbl_dispenser_l2;

        LinearLayout pnl_producto;
        LinearLayout pnl_repuesto;
        LinearLayout pnl_dispenser;

        LayoutInflater inflater = (LayoutInflater) _context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row = inflater.inflate(R.layout.row_articulo_control_playa, parent, false);

        rowArtControl_lbl_articulo = (TextView) row.findViewById(R.id.rowArtControl_lbl_articulo);

        pnl_producto = (LinearLayout) row.findViewById(R.id.pnl_producto);
        pnl_repuesto = (LinearLayout) row.findViewById(R.id.pnl_repuesto);
        pnl_dispenser = (LinearLayout) row.findViewById(R.id.pnl_dispenser);

        rowArtControl_lbl_cantFalladosPack = (TextView) row.findViewById(R.id.rowArtControl_lbl_cantFalladosPack);
        rowArtControl_lbl_cantFalladosUni = (TextView) row.findViewById(R.id.rowArtControl_lbl_cantFalladosUni);

        rowArtControl_lbl_cantRepuesto = (TextView) row.findViewById(R.id.rowArtControl_lbl_cantRepuesto);

        rowArtControl_lbl_cantLlenosPack = (TextView) row.findViewById(R.id.rowArtControl_lbl_cantLlenosPack);
        rowArtControl_lbl_cantLlenosUni = (TextView) row.findViewById(R.id.rowArtControl_lbl_cantLlenosUni);

        rowArtControl_lbl_cantVaciosPack = (TextView) row.findViewById(R.id.rowArtControl_lbl_cantVaciosPack);
        rowArtControl_lbl_cantVaciosUni = (TextView) row.findViewById(R.id.rowArtControl_lbl_cantVaciosUni);

        lbl_dispenser_l1 = (TextView) row.findViewById(R.id.lbl_dispenser_l1);
        lbl_dispenser_l2 = (TextView) row.findViewById(R.id.lbl_dispenser_l2);

        ArticuloControlDePlaya articulo=_values.get(position);

        if(articulo.articulo!=null){

            rowArtControl_lbl_articulo.setText(articulo.articulo.nombreArticulo);
            pnl_dispenser.setVisibility(View.GONE);

            if(articulo.articulo.tipoArticulo_ids==1){

                pnl_repuesto.setVisibility(View.GONE);
                rowArtControl_lbl_cantFalladosPack.setText(String.valueOf(articulo.cantidadFalladosPack));
                rowArtControl_lbl_cantFalladosUni.setText(String.valueOf(articulo.cantidadFalladosUni));

                rowArtControl_lbl_cantLlenosPack.setText(String.valueOf(articulo.cantidadLlenosPack));
                rowArtControl_lbl_cantLlenosUni.setText(String.valueOf(articulo.cantidadLlenosUni));

                rowArtControl_lbl_cantVaciosPack.setText(String.valueOf(articulo.cantidadVaciosPack));
                rowArtControl_lbl_cantVaciosUni.setText(String.valueOf(articulo.cantidadVaciosUni));

            }else if (articulo.articulo.tipoArticulo_ids ==3){

                pnl_producto.setVisibility(View.GONE);
                rowArtControl_lbl_cantRepuesto.setText(String.valueOf(articulo.cantidadLlenosUni));
            }

        }else{
            rowArtControl_lbl_articulo.setVisibility(View.GONE);
            pnl_repuesto.setVisibility(View.GONE);
            pnl_producto.setVisibility(View.GONE);
            pnl_dispenser.setVisibility(View.VISIBLE);

            lbl_dispenser_l1.setText("Nro: "+ articulo.dispenser.nroDispenser + "("+articulo.dispenser.marca+")");
            lbl_dispenser_l2.setText("Color: "+ articulo.dispenser.tipo + ". Color:"+ articulo.dispenser.color);
        }

        return row;
    }

}
