package waterservice.mobile.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.sql.SQLException;
import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.FirmaRemito;
import waterservice.mobile.clases.OrdenDeTrabajo;
import waterservice.mobile.clases.RepuestoActividad;
import waterservice.mobile.services.Service_Clientes;
import waterservice.mobile.services.Service_ServicioTecnico;
import waterservice.mobile.services.Service_Ventas;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivityRemitoServicioTecnico extends AppCompatActivity {

    private Button btn_firmarRemito;
    private ListView lst_repuestosActividadesRemito;
    private OrdenDeTrabajo ordenDeTrabajo;
    private List<RepuestoActividad> itemsRemito;
    private Cliente clienteActual;

    private Service_Clientes _srvCliente;
    private Service_ServicioTecnico srvServicioTecnico;

    private EditText remito_txt_nroRemito;
    private EditText remito_txt_prefijoRemito;
    private AppSession appSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remito_servicio_tecnico);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {

        appSession = new AppSession(this);
        srvServicioTecnico = new Service_ServicioTecnico(this);
        _srvCliente = new Service_Clientes(this);

        btn_firmarRemito = (Button) findViewById(R.id.btn_firmarRemito);
        lst_repuestosActividadesRemito = (ListView) findViewById(R.id.lst_repuestosActividadesRemito);
        remito_txt_nroRemito = (EditText) this.findViewById(R.id.remito_txt_nroRemito);
        remito_txt_prefijoRemito = (EditText) this.findViewById(R.id.remito_txt_prefijoRemito);

        ordenDeTrabajo = (OrdenDeTrabajo) SessionVars.Get_Session_Var(SessionKeys.ORDEN_DE_TRABAJO_ACTUAL);
        clienteActual = appSession.getClienteActual();

        if(ordenDeTrabajo.nroRemitoFisico!=null)
        {
            remito_txt_prefijoRemito.setEnabled(false);
            remito_txt_nroRemito.setEnabled(false);
        }

        try {
            itemsRemito = srvServicioTecnico.obtenerRepuestosActividadesDeOrdenDeTrabajo(ordenDeTrabajo);

            ArrayAdapterActividadRepuesto ad = new ArrayAdapterActividadRepuesto(this, itemsRemito);
            lst_repuestosActividadesRemito.setAdapter(ad);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean validarFirmaRemito(){
        try {

            if(ordenDeTrabajo.nroRemitoFisico != null)
            {
                Utiles.Mensaje_En_Pantalla("El servicio técnico tiene un remito físico", true, false, this);
                return false;
            }

            FirmaRemito firmaRemito = _srvCliente.obtenerFirmaRemito(clienteActual.cliente_id, true);

            if(firmaRemito!=null)
            {
                Utiles.Mensaje_En_Pantalla("El remito ya ha sido firmado por "+firmaRemito.firmante, true, false, this);
                return false;
            }

        } catch (SQLException e) {
            return false;
        }

        return true;
    }

    public void remito_btn_guardarRemitoFisicoOnClick(View view){

        if(!validarFirmaRemito())return;

        try {
            srvServicioTecnico.asignarRemitoFisico(
                    ordenDeTrabajo,
                    Long.valueOf(remito_txt_nroRemito.getText().toString()),
                    Long.valueOf(remito_txt_prefijoRemito.getText().toString()));

            Utiles.Mensaje_En_Pantalla("El remito físico se ha asignado correctamente", true, false, this);

            this.finish();

        } catch (Exception e) {
            Utiles.Mensaje_En_Pantalla("Error al asignar remito físico. Error: "+e.getMessage(), true, false, this);
        }
    }

    public void btn_firmarRemitoOnClick(View view){

        if(!validarFirmaRemito())return;

        Intent intent = new Intent(this, ActivityRemitoFirma.class);
        startActivityForResult(intent, Constantes.ACTIVITY_FIRMA_REMITO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch(requestCode) {
            case Constantes.ACTIVITY_FIRMA_REMITO:
                onFirmaClienteResult(resultCode);
                break;
        }

    }

    private void onFirmaClienteResult(int resultCode) {

        if(resultCode != RESULT_OK) return;

        FirmaRemito firmaRemito = (FirmaRemito) SessionVars.Get_Session_Var(SessionKeys.FIRMA_CLIENTE);

        Service_Ventas _srvVentas=new Service_Ventas(this);
        _srvVentas.registrarFirmaRemito(clienteActual, firmaRemito.firmaRemito, firmaRemito.firmante, true);

        this.finish();
    }
}
