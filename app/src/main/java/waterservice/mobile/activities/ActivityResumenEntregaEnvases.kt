package waterservice.mobile.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import ar.com.waterservice.newapp.R
import kotlinx.android.synthetic.main.activity_resumen_entrega_envases.*
import waterservice.mobile.adapters.ResumenEnvaseEntregadoAdapter
import waterservice.mobile.clases.Constantes
import waterservice.mobile.clases.EnvaseDisponible
import waterservice.mobile.clases.EnvaseDisponible.ResumenEnvaseEntregado
import waterservice.mobile.utiles.SessionKeys
import waterservice.mobile.utiles.SessionVars
import waterservice.mobile.utiles.Utiles

class ActivityResumenEntregaEnvases : AppCompatActivity() {

    private var envasesEntregados: ArrayList<EnvaseDisponible>? = null
    private var resumenContenidosEntregados: ArrayList<ResumenEnvaseEntregado>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resumen_entrega_envases)

        init()
    }

    private fun init() {

        envasesEntregados = SessionVars.Get_Session_Var(SessionKeys.ENVASES_REGISTRABLES_ENTREGADOS)
                            as ArrayList<EnvaseDisponible>

        Utiles.prepareRecyclerView(lts_contenidoEntregado, this)

        generarResumenEntregado()
    }

    fun generarResumenEntregado(){

        resumenContenidosEntregados = ArrayList()

        envasesEntregados!!.forEach { env ->

            var itemDeResumeExistente = resumenContenidosEntregados!!.find {it.articuloContenidoId == env.articuloContenidoId}

            if(itemDeResumeExistente!=null) {

                itemDeResumeExistente.cantidad += env.cantidadDeCarga

            }else{

                var item:ResumenEnvaseEntregado = EnvaseDisponible().ResumenEnvaseEntregado()
                item.articuloContenidoId = env.articuloContenidoId
                item.articuloContenidoNombre = env.articuloContenidoNombre
                item.cantidad = env.cantidadDeCarga
                item.unidadCantidad = env.unidadDeCarga
                item.precio = 65.0

                resumenContenidosEntregados!!.add(item)
            }

        }

        calcularTotales()

        lts_contenidoEntregado.adapter = ResumenEnvaseEntregadoAdapter(resumenContenidosEntregados, this)
    }

    fun calcularTotales(){

        var total:Double = 0.0
        resumenContenidosEntregados!!.forEach {

            var subtotal = it.precio * it.cantidad

            it.subtotal = (subtotal) - (subtotal/100 * it.descuento)

            total+=it.subtotal
        }

        lbl_total.text = total.toString()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED, Intent())
        super.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            Constantes.ACTIVITY_ENTREGA_ENVASES -> onContinuarResult(resultCode)
        }
    }

    private fun onContinuarResult(resultCode: Int) {

        if (resultCode != RESULT_OK) return

        this.setResult(Activity.RESULT_OK, Intent())
        this.finish()
    }

    fun onContinuar(view : View){

        SessionVars.Add_Session_Var(SessionKeys.ENVASES_REGISTRABLES_ENTREGADOS, envasesEntregados);
        val intent = Intent(this, ActivityEntregaEnvases::class.java)
        intent.putExtra("esDevolucion", true)
        startActivityForResult(intent, Constantes.ACTIVITY_ENTREGA_ENVASES)
    }
}
