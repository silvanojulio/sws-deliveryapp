package waterservice.mobile.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;

import java.util.ArrayList;
import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.ArticuloControlDePlaya;
import waterservice.mobile.clases.ArticuloDeLista;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.ControlDePlaya;
import waterservice.mobile.clases.Dispenser;
import waterservice.mobile.clases.HojaDeRuta;
import waterservice.mobile.clases.Usuario;
import waterservice.mobile.services.Service_ControlDePlayaOnLine;
import waterservice.mobile.utiles.AsyncWork;
import waterservice.mobile.utiles.CurrentApp;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivityControlDePlaya extends Activity
        implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private RadioButton controlDePlaya_rdb_entrada;
    private RadioButton controlDePlaya_rdb_salida;
    private ListView controlDePlaya_lst_articulos;
    private EditText controlDePlaya_txt_kilometros;
    private LinearLayout pnl_cargador;
    private LinearLayout pnl_aprobador;
    private AppSession appSession;

    List<ArticuloControlDePlaya> articulos;

    Service_ControlDePlayaOnLine _srvControlDePlaya;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_control_playa);

        init();
    }

    private void init() {

        if(articulos==null)
            articulos=new ArrayList<>();

        appSession = new AppSession(this);

        controlDePlaya_rdb_entrada = (RadioButton) this.findViewById(R.id.controlDePlaya_rdb_entrada);
        controlDePlaya_rdb_salida = (RadioButton) this.findViewById(R.id.controlDePlaya_rdb_salida);
        controlDePlaya_txt_kilometros = (EditText) this.findViewById(R.id.controlDePlaya_txt_kilometros);
        controlDePlaya_lst_articulos = (ListView) this.findViewById(R.id.controlDePlaya_lst_articulos);
        controlDePlaya_lst_articulos.setOnItemClickListener(this);
        controlDePlaya_lst_articulos.setOnItemLongClickListener(this);

        pnl_cargador = (LinearLayout) this.findViewById(R.id.pnl_cargador);
        pnl_aprobador = (LinearLayout) this.findViewById(R.id.pnl_aprobador);

        _srvControlDePlaya=new Service_ControlDePlayaOnLine(this);

        articulos=new ArrayList<>();

        pnl_aprobador.setVisibility(View.GONE);
    }

    private void cargarArticulos(){

        ArrayAdapterArticulosControlPlaya ad=new ArrayAdapterArticulosControlPlaya(this,articulos);
        controlDePlaya_lst_articulos.setAdapter(ad);

    }

    public void controlDePlaya_btn_agregarOnClick(View view){

        try {

            SessionVars.Add_Session_Var(SessionKeys.ARTICULOS_CONTROL_PLAYA,articulos);
            SessionVars.Add_Session_Var(SessionKeys.TIPO_ARTICULO_AGREGAR, 1);
            Intent myIntent=new Intent(this,ActivityAgregarArticuloControlRuta.class);
            startActivityForResult(myIntent, Constantes.ACTIVITY_AGREGAR_ARTICULO_CONTROLDEPLAYA);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void controlDePlaya_btn_agregarRepuestoOnClick(View view){

        try {

            SessionVars.Add_Session_Var(SessionKeys.TIPO_ARTICULO_AGREGAR, 3);
            Intent myIntent=new Intent(this,ActivityAgregarArticuloControlRuta.class);
            startActivityForResult(myIntent, Constantes.ACTIVITY_AGREGAR_ARTICULO_CONTROLDEPLAYA);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void controlDePlaya_btn_agregarDispenserOnClick(View view){

        try {

            SessionVars.Add_Session_Var(SessionKeys.DISPENSER_SELECCIONADO, null);
            Intent myIntent=new Intent(this,ActivityAgregarDispenserControlDePlaya.class);
            startActivityForResult(myIntent, Constantes.ACTIVITY_AGREGAR_DISPENSER);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void controlDePlaya_btn_confirmarOnClick(View view){
        confirmarCarga();
    }

    public void controlDePlaya_btn_aprobarOnClick(View view){

        Intent myIntent=new Intent(this,ActivityLoginControlador.class);
        startActivityForResult(myIntent, Constantes.ACTIVITY_LOGIN);

    }

    public void controlDePlaya_btn_rechazarOnClick(View view){
        pnl_aprobador.setVisibility(View.GONE);
        pnl_cargador.setVisibility(View.VISIBLE);
    }

    private void onLoginSuccess( Intent data) {

        Usuario usuario = (Usuario) SessionVars.Get_Session_Var(SessionKeys.USUARIO_LOGUEADO);
        SessionVars.Add_Session_Var(SessionKeys.USUARIO_CONTROLADOR_ACTUAL, usuario);

        AsyncWork.run("aprobarCarga",this,"Registrando control de playa...");
    }

    public void confirmarCarga(){
        try{

            if(!controlDePlaya_rdb_entrada.isChecked() && !controlDePlaya_rdb_salida.isChecked() )
                throw new Exception("Debe seleccionar si es salida o entrada");

            pnl_cargador.setVisibility(View.GONE);
            pnl_aprobador.setVisibility(View.VISIBLE);


        }catch (Exception ex){
            ex.printStackTrace();
            Utiles.Mensaje_En_Pantalla("Error: " + ex.getMessage(), true, true, this);
        }
    }

    public void aprobarCarga() throws  Exception{

        Usuario usuarioAprueba = (Usuario) SessionVars.Get_Session_Var(SessionKeys.USUARIO_CONTROLADOR_ACTUAL);
        HojaDeRuta hojaDeRuta = new AppSession(this).getHojaDeRuta();

        ControlDePlaya controlDePlaya = new ControlDePlaya();
        controlDePlaya.usuarioControlaId= appSession.getUsuarioAcual().usuario_id;
        controlDePlaya.usuarioApruebaId = usuarioAprueba.usuario_id;
        controlDePlaya.hojaDeRutaId=hojaDeRuta.id;
        controlDePlaya.tipoEventoId = controlDePlaya_rdb_entrada.isChecked()?2:1;
        controlDePlaya.kilometros = Integer.valueOf(controlDePlaya_txt_kilometros.getText().toString());
        controlDePlaya.articulos = articulos;

        _srvControlDePlaya.guardarControlDePlaya(controlDePlaya,usuarioAprueba.token);

        Utiles.Mensaje_En_Pantalla("El control de playa se ha guardado.", true, true, this);

        this.finish();
    }

    private void onDispenserAgregado(){

        Dispenser d = (Dispenser) SessionVars.Get_Session_Var(SessionKeys.DISPENSER_SELECCIONADO);

        if(dispenserYaAgregado(d.id)){
            Utiles.Mensaje_En_Pantalla("El dispenser ya había sido agregado", true, false, this);
            return;
        }

        ArticuloControlDePlaya dispenserAgregado = new ArticuloControlDePlaya();
        dispenserAgregado.dispenser_id = d.id;
        dispenserAgregado.dispenser = d;

        articulos.add(dispenserAgregado);

        cargarArticulos();
    }

    private void onArticuloAgregado(){

        ArticuloControlDePlaya art = (ArticuloControlDePlaya) SessionVars.Get_Session_Var(SessionKeys.ARTICULO_SELECCIONADO);

        if(articuloYaAgregado(art.articulo_id)){
            Utiles.Mensaje_En_Pantalla("El artículo ya había sido agregado", true, false, this);
            return;
        }

        articulos.add(art);

        cargarArticulos();
    }

    private boolean articuloYaAgregado(int articulo_id){
        for(ArticuloControlDePlaya artC : articulos){
            if(artC.articulo.id==articulo_id)return true;
        }
        return false;
    }

    private boolean dispenserYaAgregado(int dispenserId){
        for (ArticuloControlDePlaya item:articulos) {
            if(item.dispenser_id == dispenserId) return true;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

        articulos.remove(position);
        cargarArticulos();

        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

            Utiles.MostrarConfirmacion("Salir", "Confirma salir de esta pantalla?",
                    "confirmarSalir",this,this);
        }

        return super.onKeyDown(keyCode, event);
    }

    public void confirmarSalir(){
        this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case Constantes.ACTIVITY_LOGIN:
                if(resultCode == RESULT_OK)
                    onLoginSuccess(data);
                break;
            case Constantes.ACTIVITY_AGREGAR_DISPENSER:
                if(resultCode == RESULT_OK)
                    onDispenserAgregado();
                break;
            case Constantes.ACTIVITY_AGREGAR_ARTICULO_CONTROLDEPLAYA:
                if(resultCode == RESULT_OK)
                    onArticuloAgregado();
                break;
        }

    }

}
