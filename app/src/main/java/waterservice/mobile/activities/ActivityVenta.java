package waterservice.mobile.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.activities.formasDePago.ActivityFormaDePagoEfectivo;
import waterservice.mobile.adapters.ArticuloParaSeleccionar;
import waterservice.mobile.adapters.ArticuloVendidoAdapter;
import waterservice.mobile.clases.ArticuloDeAbono;
import waterservice.mobile.clases.ArticuloDeLista;
import waterservice.mobile.clases.ArticuloEntregado;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.DescuentoPorCantidad;
import waterservice.mobile.clases.ListaDePrecios;
import waterservice.mobile.clases.PrecioEspecial;
import waterservice.mobile.clases.StockDeCliente;
import waterservice.mobile.clases.ValorSatelite;
import waterservice.mobile.clases.VentaActual;
import waterservice.mobile.services.Service_Articulos;
import waterservice.mobile.services.Service_General;
import waterservice.mobile.services.Service_Ventas;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivityVenta extends AppCompatActivity {

    private TextView lbl_saldoFinal;
    private TextView lbl_ventaTotalVentaActual;
    private TextView lbl_requiereRemito;
    private CardView cv_mensajeSinArticulos;
    private CheckBox ckb_pagoDeContado;
    private List<ValorSatelite> motivosDeRelevamiento;
    private boolean isNewActivity = true;
    private RecyclerView lts_articulos;
    private Button btn_efectivo;

    Service_Ventas srvVentas;

    private Cliente clienteActual;
    private double totalVenta=0;
    private double importeEfectivo = 0;
    private AppSession appSession;

    private boolean articulosDelClienteObtenidos;

    private List<ArticuloEntregado> articulosEntregados;

    private ArticulosDelCliente articulosDelCliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venta);
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {

        appSession = new AppSession(this);

        srvVentas = new Service_Ventas(this);
        clienteActual = appSession.getClienteActual();

        if(articulosEntregados==null)
        {
            articulosEntregados = new ArrayList<ArticuloEntregado>();
        }

        lbl_saldoFinal = (TextView) this.findViewById(R.id.lbl_saldoFinal);
        lbl_ventaTotalVentaActual = (TextView) this.findViewById(R.id.lbl_ventaTotalVentaActual);
        lbl_requiereRemito = (TextView) this.findViewById(R.id.lbl_requiereRemito);
        cv_mensajeSinArticulos = (CardView) this.findViewById(R.id.cv_mensajeSinArticulos);
        lts_articulos = (RecyclerView) this.findViewById(R.id.lts_articulos);
        ckb_pagoDeContado = (CheckBox) this.findViewById(R.id.ckb_pagoDeContado);
        btn_efectivo = (Button) this.findViewById(R.id.btn_efectivo);

        Utiles.prepareRecyclerView(lts_articulos,this);

        obtenerArticulosDelCliente();

        lbl_requiereRemito.setVisibility(clienteActual.requiereRemito? View.VISIBLE: View.GONE);

        if(articulosDelCliente==null)
            actualizarListaDeArticulosEntregados();

        onCalcularTotal();

    }

    public void obtenerArticulosDelCliente() throws Exception {

        Service_Articulos _srvArticulos = new Service_Articulos(this);
        articulosDelCliente = new ArticulosDelCliente();

        articulosDelCliente.articulosDeLista = _srvArticulos.obtenerArticulosDeLista();
        articulosDelCliente.preciosEspeciales  = _srvArticulos.obtenerPreciosEspecialesDeCliente(clienteActual.clienteFactura_id);
        articulosDelCliente.listasDePrecios  = _srvArticulos.obtenerArticulosDeListaDePrecio(clienteActual.listaDePrecioId);
        articulosDelCliente.descuentosDeListaDePrecios = _srvArticulos.obtenerDescuentosPorCantidad(clienteActual.listaDePrecioId);
        articulosDelCliente.articulosAbonos = _srvArticulos.obtenerArticulosDeAbonos(clienteActual.cliente_id);
        articulosDelCliente.stocks = _srvArticulos.obtenerStocksDeCliente(clienteActual.cliente_id);

        SessionVars.Add_Session_Var(SessionKeys.ARTICULOS_DEL_CLIENTE, articulosDelCliente);

        seleccionarArticulosPredeterminados();

        actualizarListaDeArticulosEntregados();
    }

    private void seleccionarArticulosPredeterminados(){

        List<ArticuloParaSeleccionar> articulosParaSeleccionar = ArticuloParaSeleccionar.obtenerArticulos(
                articulosDelCliente.articulosDeLista, articulosDelCliente.listasDePrecios, articulosDelCliente.preciosEspeciales,
                articulosDelCliente.articulosAbonos, articulosDelCliente.stocks);

        for (ArticuloDeAbono artAbono: articulosDelCliente.articulosAbonos) {

            ArticuloParaSeleccionar artParaSeleccionar = obtenerArticuloParaSeleccionarPorId(artAbono.articulo_id, articulosParaSeleccionar);

            if(artParaSeleccionar == null) continue;

            if(!verificarArticuloYaAgregado(artAbono.articulo_id)){
                StockDeCliente stock= null;
                for (StockDeCliente s: articulosDelCliente.stocks) {
                    if(s.articuloId == artAbono.articulo_id) {
                        stock=s;
                        break;
                    }
                }
                this.articulosEntregados.add(new ArticuloEntregado(artParaSeleccionar, stock));
            }
        }

        for (StockDeCliente artStock: articulosDelCliente.stocks) {

            ArticuloParaSeleccionar artParaSeleccionar = obtenerArticuloParaSeleccionarPorId(artStock.articuloId, articulosParaSeleccionar);

            if(artParaSeleccionar == null) continue;

            if(!verificarArticuloYaAgregado(artStock.articuloId)){
                this.articulosEntregados.add(new ArticuloEntregado(artParaSeleccionar, artStock));
            }
        }
    }

    private ArticuloParaSeleccionar obtenerArticuloParaSeleccionarPorId(int articuloId, List<ArticuloParaSeleccionar> articulosParaSeleccionar){

        ArticuloParaSeleccionar a = null;

        for (ArticuloParaSeleccionar art : articulosParaSeleccionar) {
            if(art.articuloId == articuloId){
               a = art;
                break;
            }
        }

        return a;
    }

    private boolean verificarArticuloYaAgregado(int articuloId){

        boolean r = false;

        if(this.articulosEntregados!=null && this.articulosEntregados.size() > 0) {
            for (ArticuloEntregado art : articulosEntregados) {
                if(art.articulo_id == articuloId){
                    r=true;
                    break;
                }
            }
        }

        return r;
    }

    public void btn_agregarArticuloOnClick(View view){

        SessionVars.Add_Session_Var(SessionKeys.ARTICULO_SELECCIONADO, null);
        Intent intent = new Intent(this, ActivitySeleccionarArticulo.class);
        startActivityForResult(intent, Constantes.ACTIVITY_SELECCIONAR_ARTICULO);
    }

    public void btn_confirmarOnClick(View view){

        VentaActual venta = new VentaActual();
        venta.clienteId = clienteActual.cliente_id;
        venta.nombreCliente = clienteActual.nombreCliente;
        venta.articulosEntregados = articulosEntregados;
        venta.esContadoEfectivo = ckb_pagoDeContado.isChecked();
        venta.importeEfectivo = importeEfectivo;

        venta.quitarArticulosSinGesitones();

        try {
            srvVentas.validarVenta(venta, clienteActual, articulosDelCliente.articulosDeLista);
        } catch (Exception e) {
            Dialog.error("Validación", e.getMessage(), this);
            return;
        }

        SessionVars.Add_Session_Var(SessionKeys.VENTA_ACTUAL, venta);
        Intent intent = new Intent(this, ActivityConfirmacionVenta.class);
        startActivityForResult(intent, Constantes.ACTIVITY_CONFIRMAR_VENTA_ACTUAL);
    }

    public void btn_efectivoOnClick(View v){
        Intent intent = new Intent(this, ActivityFormaDePagoEfectivo.class);
        intent.putExtra("enPantallaVentas",true);
        startActivityForResult(intent, Constantes.ACTIVITY_AGREGAR_ITEM_RECIBO);
    }

    private void onSeleccionarArticuloResult(int resultCode){

        if(resultCode != RESULT_OK) return;

        ArticuloParaSeleccionar art = (ArticuloParaSeleccionar) SessionVars.Get_Session_Var(SessionKeys.ARTICULO_SELECCIONADO);

        if(verificarArticuloYaAgregado(art.articuloId)){
            Dialog.error("Error", "El artículo seleccionado ya pertenece a la lista", this);
            return;
        }

        StockDeCliente stock= null;
        for (StockDeCliente s: articulosDelCliente.stocks) {
            if(s.articuloId == art.articuloId) {
                stock=s;
                break;
            }
        }

        this.articulosEntregados.add(new ArticuloEntregado(art, stock));
        actualizarListaDeArticulosEntregados();

    }

    private  void actualizarListaDeArticulosEntregados(){

        if(motivosDeRelevamiento==null){
            try {
                motivosDeRelevamiento = new Service_General(this).obtenerValoresSatelites(1000);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(articulosEntregados==null || articulosEntregados.size() == 0)
            cv_mensajeSinArticulos.setVisibility(View.VISIBLE);
        else
            cv_mensajeSinArticulos.setVisibility(View.GONE);

        if(articulosDelCliente==null) return;

        ArticuloVendidoAdapter.ArticuloVendidoViewHolder.Acciones acciones =
        new ArticuloVendidoAdapter.ArticuloVendidoViewHolder.Acciones() {
            @Override
            public void quitarArticuloVendido(int articuloId) {
                onQuitarArticuloEntregado(articuloId);
            }

            @Override
            public void onSubtotalChange() {
                onCalcularTotal();
            }
        };

        ArticuloVendidoAdapter ad = new ArticuloVendidoAdapter(articulosEntregados,
                this, articulosDelCliente.descuentosDeListaDePrecios,
                acciones, motivosDeRelevamiento);

        lts_articulos.setAdapter(ad);

        //Posiciona en el último item agregado
        if(articulosEntregados!= null && articulosEntregados.size()>0)
            lts_articulos.getLayoutManager().scrollToPosition(articulosEntregados.size()-1);
    }

    private void onCalcularTotal(){

        totalVenta = 0;

        for(int i=0; i<articulosEntregados.size(); i++){

            totalVenta += articulosEntregados.get(i).subtotal;
        }

        lbl_ventaTotalVentaActual.setText("$"+String.valueOf(totalVenta));

        double saldoTotal = totalVenta + clienteActual.obtenerSaldoAlDia();
        lbl_saldoFinal.setText("$"+String.valueOf(saldoTotal));
    };

    private void onQuitarArticuloEntregado(int articuloId){

        for(int i=0; i<articulosEntregados.size(); i++){

            if(articuloId == articulosEntregados.get(i).articulo_id){
                articulosEntregados.remove(articulosEntregados.get(i));
                break;
            }
        }

        actualizarListaDeArticulosEntregados();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch(requestCode) {
            case Constantes.ACTIVITY_SELECCIONAR_ARTICULO:
                onSeleccionarArticuloResult(resultCode);
                break;
            case Constantes.ACTIVITY_CONFIRMAR_VENTA_ACTUAL:
                onConfirmarVentaResult(resultCode);
                break;
            case Constantes.ACTIVITY_AGREGAR_ITEM_RECIBO:
                onAgregarEfectivoResult(resultCode, data);
                break;
        }

    }

    private void onAgregarEfectivoResult(int resultCode, Intent data){

        if(resultCode!=RESULT_OK) return;

        importeEfectivo = data.getDoubleExtra("importe", 0);

        if(importeEfectivo>0)
            btn_efectivo.setText("$"+ Utiles.Round(importeEfectivo));
        else
            btn_efectivo.setText("Efectivo");
    }

    private void onConfirmarVentaResult(int resultCode) {

        if(resultCode!=RESULT_OK) return;

        this.setResult(RESULT_OK);
        this.finish();
    }

    public static class ArticulosDelCliente{

        public List<ArticuloDeLista>  articulosDeLista ;
        public List<PrecioEspecial> preciosEspeciales;
        public List<ListaDePrecios> listasDePrecios;
        public List<ArticuloDeAbono> articulosAbonos;
        public List<StockDeCliente> stocks;
        public List<DescuentoPorCantidad> descuentosDeListaDePrecios;
    }
}
