package waterservice.mobile.activities;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.DispenserAsociado;

public class ArrayAdapterDispensersSt extends ArrayAdapter<DispenserAsociado> {

    List<DispenserAsociado> _values;
    Activity _context;

    public ArrayAdapterDispensersSt(Activity context, List<DispenserAsociado> values) {

        super(context, R.layout.row_dispensers_st ,values);
        this._context = context;
        this._values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) _context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.row_dispensers_st, parent, false);
        DispenserAsociado item=_values.get(position);
        RowDispenserAsociado row = new RowDispenserAsociado(rowView, item);

        setListeners(row);
        setValues(row);

        return rowView;
    }

    private void setListeners(RowDispenserAsociado row){

        final RowDispenserAsociado r = row;
    }

    private void setValues(RowDispenserAsociado row){

        row.lbl_nroDispenser.setText(row.item.nroDispenser);
        row.img_estado.setImageResource(row.item.mantemientoCargado?
                R.drawable.checked : R.drawable.unchecked);

        if(row.item.deCliente) row.img_deFabrica.setVisibility(View.GONE);
    }

}

class RowDispenserAsociado{

    public DispenserAsociado item;
    public TextView lbl_nroDispenser;
    public ImageView img_estado;
    public ImageView img_deFabrica;

    public RowDispenserAsociado(View row, DispenserAsociado _item){

        item = _item;
        lbl_nroDispenser = (TextView) row.findViewById(R.id.lbl_nroDispenser);
        img_estado = (ImageView) row.findViewById(R.id.img_estado);
        img_deFabrica = (ImageView) row.findViewById(R.id.img_deFabrica);
    }


}