package waterservice.mobile.activities;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.ContextMenu;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;

import java.util.concurrent.Callable;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.utiles.AsyncWork;

public abstract class TabViewStub implements ITabViewStub {

    protected Activity activity;
    public ViewStub tab;
    public Button btn;
    public TabContainer tabContainer;
    public boolean alreadySelected;


    public TabViewStub(Activity _activity, ViewStub _tab, Button _btn, TabContainer _tabContainer){
        activity = _activity;
        tab =_tab;
        btn = _btn;
        tabContainer = _tabContainer;

        tabContainer.addTab(this);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    tabContainer.onButtonClick(TabViewStub.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setVisible(boolean visible){
        this.tab.setVisibility(visible? View.VISIBLE : View.GONE);
    }

    public boolean getVisible(){
        return this.tab.getVisibility() == View.VISIBLE;
    }

    public void revertVisible(){
        setVisible(!getVisible());
    }

    @Override
    public void setSelected(Boolean selected) {

        Drawable d = selected?  ContextCompat.getDrawable(this.activity, R.drawable.dotted) :
                                ContextCompat.getDrawable(this.activity, R.drawable.btn_tab);
        setBackground(this.btn, d);
        this.setVisible(selected);

        if(selected)
        {
            alreadySelected = true;
            onTabSelected();
        }
        else
            onTabLeave();
    }

    private void setBackground(Button btn, Drawable d){
        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            btn.setBackgroundDrawable(d);
        } else {
            btn.setBackground(d);
        }
    }

    @Override
    public void onTabSelected() {

    }

    @Override
    public void onTabLeave() {

    }
}

