package waterservice.mobile.activities;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.HojaDeRuta;
import waterservice.mobile.clases.apiContracts.IncidentesTotalesResponse;
import waterservice.mobile.procesos.GPSTrackerTask;
import waterservice.mobile.procesos.ServiceSync;
import waterservice.mobile.server.ApiCallback;
import waterservice.mobile.server.SyncMobile;
import waterservice.mobile.services.Service_HojasDeRutas;
import waterservice.mobile.services.Service_NuevosPedidos;
import waterservice.mobile.services.Service_ServicioTecnico;
import waterservice.mobile.services.Service_Ventas;
import waterservice.mobile.utiles.AsyncWork;
import waterservice.mobile.utiles.BaseDeDatos;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSION_REQUEST_READ_FINE_LOCATION = 101;
    HojaDeRuta hojaDeRuta;
    Service_HojasDeRutas _srvHojasDeRutas;
    private TextView main_lbl_ruta;
    private TextView lbl_usuario;
    private AppSession appSession;
    private Button main_btnIncidentes;

    GPSTrackerTask t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       try{
           super.onCreate(savedInstanceState);
           setContentView(R.layout.activity_main);

           init();
       }catch (Exception ex){
           String m = ex.getMessage();
       }

    }

    private void init(){

        appSession = new AppSession(this);

        appSession.setValueInt(0, SessionKeys.CLIENTE_INDICE_ACTUAL);

        main_lbl_ruta = (TextView) this.findViewById(R.id.main_lbl_ruta);
        lbl_usuario  = (TextView) this.findViewById(R.id.lbl_usuario);
        main_btnIncidentes  = (Button) this.findViewById(R.id.main_btnIncidentes);

        _srvHojasDeRutas=new Service_HojasDeRutas(this);

        if(t==null){
            t = new GPSTrackerTask();
            t.execute(this);
        }

        try{
            if(!isMyServiceRunning(ServiceSync.class)){
                startService(new Intent(this, ServiceSync.class));
            }
            Utiles.Set_Config_Value("last_sync", Utiles.getDateNumber(), this);
        }catch (Exception ex){
            ex.printStackTrace();
            Dialog.error("Error al intentar iniciar el Sync", ex.getMessage(), this);
        }

        try {

            hojaDeRuta= appSession.getHojaDeRuta();
            main_lbl_ruta.setText(
                    "("+String.valueOf(hojaDeRuta.id) + ") " +
                    hojaDeRuta.nombreReparto + " > " +
                    Utiles.fecha(hojaDeRuta.fechaReparto, true));

            lbl_usuario.setText(appSession.getUsuarioAcual().nombreApellido);

        } catch (Exception e) {
            main_lbl_ruta.setText("Error: sin hoja de ruta");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.mainMenu_descargarBase:
                btn_DescargarBaseClick();
                break;

            case R.id.mainMenu_configuracion:
                btn_ConfiguracionBaseClick();
                break;
            case R.id.mainMenu_controlPlaya:
                btn_ControlDePlaya();
                break;
            case R.id.mainMenu_planificacionDeCarga:
                btn_PlanificacionDeCarga();
                break;
            case R.id.mainMenu_sync:
                btn_Sync();
                break;
            case R.id.mainMenu_subirBase:
                btn_SubirBaseClick();
                break;
            case R.id.mainMenu_stopSync:
                stopSync(true);
                break;
            case R.id.mainMenu_startSync:
                startSync(true);
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    private void btn_Sync() {

        AsyncWork.run("syncronizarBaseDeDatosConServer",this, "Trayendo datos del server...");
    }

    public void syncronizarBaseDeDatosConServer() throws Exception {

        String mensaje = "";
        try{
            new Service_NuevosPedidos(getApplicationContext()).SyncNuevosPedidos();
        }catch (Exception ex){
            mensaje += "Pedidos: "+ ex.getMessage() + ". ";
        }

        try{
            new Service_Ventas(getApplicationContext()).syncVisitasCerradas();
        }catch (Exception ex){
            mensaje += "Ventas: "+ex.getMessage() + ". ";
        }

        try{
            new Service_ServicioTecnico(getApplicationContext()).enviarServiciosTecnicosCerrados();
        }catch (Exception ex){
            mensaje += "STs: "+ex.getMessage() + ". ";
        }

        if(!mensaje.equals("")){
            throw new Exception(mensaje);
        }else{
            Dialog.success("Sync", "Los datos se han sincronizado",this);
        }
    }

    private void btn_PlanificacionDeCarga() {
        try {

            Intent myIntent=new Intent(this,ActivityPlanificacionDeCarga.class);
            startActivity(myIntent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void btn_ControlDePlaya(){
        try {

            if(hojaDeRuta.cerrada)
                throw new Exception("Esta hoja de ruta ya esta cerrada, descargue una nueva");

            SessionVars.Add_Session_Var(SessionKeys.ARTICULOS_CONTROL_PLAYA, null);

            Intent myIntent=new Intent(this, ActivityControlDePlaya.class);
            startActivity(myIntent);

        } catch (Exception e) {
            Utiles.Mensaje_En_Pantalla(e.getMessage(), true, true, this);
        }
    }

    private void btn_DescargarBaseClick(){

        try {

            Intent myIntent=new Intent(MainActivity.this,DescargarBase_Activity.class);
            startActivity(myIntent);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void btn_SubirBaseClick(){

        new AlertDialog.Builder(this)
                .setIcon(R.drawable.question)
                .setTitle("Enviando Base al servidor")
                .setMessage("Ultima vez enviada: "+BaseDeDatos.UltimaSubida(this)+"\n seguro que desea enviar la base de datos?")
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {

                    //@Override
                    public void onClick(DialogInterface dialog, int which) {

                        Utiles.Start_Working_Message(MainActivity.this,"Enviando base...");

                        Thread timer = new Thread(){

                            public void run(){

                                try {

                                    sleep(200);

                                    MainActivity.this.Proceso_Enviar();

                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        };

                        timer.start();

                    }

                })
                .setNegativeButton("NO", null)
                .show();
    }

    private void btn_ConfiguracionBaseClick(){
        try {

            Intent myIntent=new Intent(MainActivity.this,Configuracion_Activity.class);
            startActivity(myIntent);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void btn_hojaDeRutaClick(View view){

        try {

            Intent myIntent=new Intent(MainActivity.this,ActivityHojaDeRuta.class);
            startActivity(myIntent);

        } catch (Exception e) {
            Utiles.Mensaje_En_Pantalla(e.getMessage(), true, true, this);
        }
    }

    public void btnMainCierreRuta(View view){
        try {

            if(hojaDeRuta.cerrada)
                throw new Exception("Esta hoja de ruta ya ha sido cerrada");

            int cantidadSinSync =  _srvHojasDeRutas.obtenerCantidadVisitasSinSincronizar();

            if(cantidadSinSync>0)
                throw new Exception("Hay visitas sin sincronizar");

            Intent myIntent=new Intent(this,ActivityDeclaracionEfectivo.class);
            startActivity(myIntent);

        } catch (Exception e) {

            Dialog.error("Error", e.getMessage(), this);
        }

    }

    public void main_transaccionesActualesOnClick(View view){

        try {

            appSession.setClienteActual(null);

            Intent myIntent=new Intent(MainActivity.this,ActivityTransacciones.class);
            startActivity(myIntent);

        } catch (Exception e) {
            Utiles.Mensaje_En_Pantalla(e.getMessage(), true, true, this);
        }
    }

    public void main_VerMapaOnClick(View view){

        SessionVars.Remove_Session_Var(SessionKeys.CLIENTE_ACTUAL);

        Intent myIntent=new Intent(MainActivity.this,ActivityMapaDeClientes.class);
        startActivity(myIntent);
    }

    public void main_GastoOnClick(View view){

        Intent myIntent=new Intent(MainActivity.this,ActivityGasto.class);
        startActivity(myIntent);
    }

    void Proceso_Enviar(){

        try {
            String fechaReparto=hojaDeRuta.fechaReparto.split(" ")[0]+"_"+hojaDeRuta.codigoReparto;

            //BaseDeDatos.EnviarBase(fechaReparto); //Esta va al FTP
            BaseDeDatos.EnviarBase(hojaDeRuta.id, this); //Esta va al servidor web

            Utiles.Mensaje_En_Pantalla("Base Enviada!", true, true, MainActivity.this);

        } catch (Exception e) {

            Utiles.Stop_Working_Message();
            Utiles.Mensaje_En_Pantalla(e.getMessage(), true, true, MainActivity.this);
        }
    }

    void stopSync(boolean validate){
        try{

            if(validate && !isMyServiceRunning(ServiceSync.class)){
                throw new Exception("Sync no está activo");
            }

            stopService(new Intent(this, ServiceSync.class));
        }catch (Exception ex){
            ex.printStackTrace();
            Dialog.error("Error al intentar detener el Sync", ex.getMessage(), this);
        }

    }

    void startSync(boolean validate){
        try{

            if(validate && isMyServiceRunning(ServiceSync.class)){
                throw new Exception("Sync ya está activo");
            }

            startService(new Intent(this, ServiceSync.class));

        }catch (Exception ex){
            ex.printStackTrace();
            Dialog.error("Error al intentar iniciar el Sync", ex.getMessage(), this);
        }

    }

    void consultarIncidentes() throws Exception {

        SyncMobile srvSyncMobile = new SyncMobile(this);

        srvSyncMobile.obtenerIncidentesDeUsuario(appSession.getUsuarioAcual().usuario_id,
                new ApiCallback<IncidentesTotalesResponse>() {
                    @Override
                    public void onSuccess(IncidentesTotalesResponse response) {

                        if(response.cantidadIncidenes > 0 || response.cantidadIncidenesGrupales > 0
                        || response.cantidadIncidentesSeguidos > 0){

                            String textoIncidentes = "Incidentes: ";

                            if(response.cantidadIncidenes>0){
                                textoIncidentes = textoIncidentes + String.valueOf(response.cantidadIncidenes) +" prop / ";
                            }

                            if(response.cantidadIncidenesGrupales>0){
                                textoIncidentes = textoIncidentes + String.valueOf(response.cantidadIncidenesGrupales) +" grup / ";
                            }

                            if(response.cantidadIncidentesSeguidos>0){
                                textoIncidentes = textoIncidentes + String.valueOf(response.cantidadIncidentesSeguidos) +" seg";
                            }

                            main_btnIncidentes.setText(textoIncidentes);
                        }else{
                            main_btnIncidentes.setText("No hay Incidentes pendientes");
                        }
                    }

                    @Override
                    public void onError(String message, int errorCode) {
                        main_btnIncidentes.setText("Incidentes: error al actualizar ");
                    }
                });
    }

    public void main_IncidentesOnClick(View view){

        String urlSistema = getUrlSistema()+"/Incidentes";
        Utiles.openUrl(urlSistema, this);
    }

    public void main_altaDeClientesOnClick(View view){

        String urlSistema = getUrlSistema()+"/React/#/components/altaCliente";
        Utiles.openUrl(urlSistema, this);
    }

    String getUrlSistema(){
        final String serverRemoto= appSession.getValueString("servidor_web");
        return serverRemoto;
    }

    @Override
    protected void onPostResume() {

        /*
        String lastSync = Utiles.Get_Config_Value("last_sync", this);
        if(lastSync != null && !lastSync.equals("") && Utiles.getDateNumber() - Long.valueOf(lastSync) > 75000){
            this.btn_Sync();
            Utiles.Set_Config_Value("last_sync", Utiles.getDateNumber(), this);
        }
        */

        try {
            consultarIncidentes();
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onPostResume();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_READ_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    Utiles.Mensaje_En_Pantalla("Debe dar permiso de Location a esta app.",true, false,this);
                    this.finish();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

            Utiles.MostrarConfirmacion("Salir", "Confirma salir de esta pantalla, no podra volver a ingresar.",
                    "confirmarSalir",this,this);

            return true;
        }

        //return super.onKeyDown(keyCode, event);
        return false;
    }

    public void confirmarSalir(){
        this.finish();
    }

}
