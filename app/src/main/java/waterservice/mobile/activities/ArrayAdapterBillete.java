package waterservice.mobile.activities;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.DeclaracionEfectivo;
import waterservice.mobile.clases.Factura;
import waterservice.mobile.clases.OperacionConEnvase;
import waterservice.mobile.utiles.Utiles;

public class ArrayAdapterBillete extends ArrayAdapter<DeclaracionEfectivo> {

    List<DeclaracionEfectivo> _values;
    List<RowBillete> _rows;
    Context _context;

    private TextView row_billete_subtotal;
    private TextView row_billete_valorDisplay;
    private EditText row_billete_cantidad;

    public ArrayAdapterBillete(Context context, List<DeclaracionEfectivo> values) {

        super(context, R.layout.row_billete ,values);
        this._context = context;
        this._values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) _context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row = inflater.inflate(R.layout.row_billete, parent, false);
        DeclaracionEfectivo _item = _values.get(position);
        RowBillete r = new RowBillete(row, _item, position);
        return row;
    }

}


class RowBillete{

    DeclaracionEfectivo item;

    private TextView row_billete_subtotal;
    private TextView row_billete_valorDisplay;
    private EditText row_billete_cantidad;

    public RowBillete(View row, DeclaracionEfectivo _item, int position){

        item = _item;
        row_billete_subtotal = (TextView) row.findViewById(R.id.row_billete_subtotal);
        row_billete_valorDisplay = (TextView) row.findViewById(R.id.row_billete_valorDisplay);
        row_billete_cantidad = (EditText) row.findViewById(R.id.row_billete_cantidad);

        row_billete_cantidad.setTag(position);

        row_billete_valorDisplay.setText(item.valorDisplay);

        if(item.cantidad!=null)
        {
            row_billete_cantidad.setText(String.valueOf(item.cantidad ));
            row_billete_subtotal.setText( Utiles.Round((double) item.cantidad * item.valorNominal));
        }
        else{
            row_billete_subtotal.setText("0");
        }



        row_billete_cantidad.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                try{

                    item.cantidad= Integer.valueOf(row_billete_cantidad.getText().toString());
                    row_billete_subtotal.setText( Utiles.Round((double) item.cantidad * item.valorNominal));
                } catch (Exception ex){
                    item.cantidad = 0;
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,int after) {}

            public void onTextChanged(CharSequence s, int start, int before,int count) {}
        });

    }

}
