package waterservice.mobile.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import ar.com.waterservice.newapp.R
import kotlinx.android.synthetic.main.activity_entrega_envases.*
import waterservice.mobile.Session.AppSession
import waterservice.mobile.adapters.EnvaseDisponibleAdapter
import waterservice.mobile.clases.Constantes
import waterservice.mobile.clases.EnvaseDisponible
import waterservice.mobile.services.Service_EnvasesDisponibles
import waterservice.mobile.utiles.SessionKeys
import waterservice.mobile.utiles.SessionVars
import waterservice.mobile.utiles.Utiles


class ActivityEntregaEnvases : AppCompatActivity() {

    private var srvEnvasesDisponible: Service_EnvasesDisponibles? = null
    private var envasesEntregados: List<EnvaseDisponible>? = null
    private var appSession: AppSession? = null
    private var esDevolucion: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entrega_envases)
        init()
    }

    private fun init(){

        if(srvEnvasesDisponible==null) srvEnvasesDisponible = Service_EnvasesDisponibles(this)
        if(envasesEntregados==null) envasesEntregados = ArrayList<EnvaseDisponible>()
        appSession = AppSession(this)

        esDevolucion = intent.extras.getBoolean("esDevolucion")

        Utiles.prepareRecyclerView(lstEnvases, this)

        lblCliente.setText(appSession!!.clienteActual.nombreCliente)

        if(esDevolucion)
        {
            lbl_tituloPantalla.setText("Devolución de barriles")
        }
        else
        {
            lbl_tituloPantalla.setText("Entrega de barriles")
        }
    }

    private fun actualizarEntregados(){

        var adapter = EnvaseDisponibleAdapter(envasesEntregados, this,
                true, false,
                object: EnvaseDisponibleAdapter.Events{
                    override fun onItemSeleccionado(envase: EnvaseDisponible, position: Int, esSeleccionado: Boolean ){
                        onItemSeleccionado(envase, position, esSeleccionado)
                    }
                    override fun onItemEliminado(envase: EnvaseDisponible, position: Int){
                        onEnvaseEliminado(envase, position)
                    }
                })

        lstEnvases.adapter = adapter
    }

    fun onEnvaseEliminado(envase: EnvaseDisponible, position: Int){

        envasesEntregados = envasesEntregados!!.filter{x -> x.id != envase.id}
        actualizarEntregados()
    }

    fun onAgregarEnvase(view : View){

        SessionVars.Add_Session_Var(SessionKeys.ENVASES_REGISTRABLES_SELECCIONADOS, envasesEntregados);
        val intent = Intent(this, ActivityBuscarEnvaseRegistrable::class.java)
        intent.putExtra("clienteId", if (esDevolucion) appSession!!.clienteActual.cliente_id else 0)
        startActivityForResult(intent, Constantes.ACTIVITY_BUSCAR_ENVASE_REGISTRABLE)
    }

    fun onContinuar(view : View){

        if(esDevolucion){
            finalizarProcesoDeEntrega()
        }else{
            SessionVars.Add_Session_Var(SessionKeys.ENVASES_REGISTRABLES_ENTREGADOS, envasesEntregados);
            val intent = Intent(this, ActivityResumenEntregaEnvases::class.java)
            startActivityForResult(intent, Constantes.ACTIVITY_RESUMEN_ENTREGA_ENVASES)
        }
    }

    fun finalizarProcesoDeEntrega(){

        this.setResult(Activity.RESULT_OK, Intent())
        this.finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        run { onActivityResult(requestCode, resultCode, data) }

        when (requestCode) {
            Constantes.ACTIVITY_RESUMEN_ENTREGA_ENVASES -> onResumenEntregaEnvasesResult(resultCode)
            Constantes.ACTIVITY_BUSCAR_ENVASE_REGISTRABLE -> onBuscarEnvaseRegistrableResult(resultCode)
        }
    }

    private fun onBuscarEnvaseRegistrableResult(resultCode: Int) {
        if (resultCode != RESULT_OK) return

        envasesEntregados = SessionVars.Get_Session_Var(SessionKeys.ENVASES_REGISTRABLES_SELECCIONADOS) as List<EnvaseDisponible>

        actualizarEntregados()
    }

    private fun onResumenEntregaEnvasesResult(resultCode: Int) {

        if (resultCode != RESULT_OK) return

        this.setResult(Activity.RESULT_OK, Intent())
        this.finish()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED, Intent())
        super.onBackPressed()
    }
}
