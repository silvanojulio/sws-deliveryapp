package waterservice.mobile.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.CompoundButton
import ar.com.waterservice.newapp.R
import kotlinx.android.synthetic.main.activity_buscar_envase_registrable.*
import waterservice.mobile.Session.AppSession
import waterservice.mobile.adapters.EnvaseDisponibleAdapter
import waterservice.mobile.clases.EnvaseDisponible
import waterservice.mobile.clases.apiContracts.Sync.ObtenerBarrilesResponse
import waterservice.mobile.server.ApiCallback
import waterservice.mobile.server.SyncMobile
import waterservice.mobile.services.Service_EnvasesDisponibles
import waterservice.mobile.utiles.Dialog
import waterservice.mobile.utiles.SessionKeys
import waterservice.mobile.utiles.SessionVars
import waterservice.mobile.utiles.Utiles

class ActivityBuscarEnvaseRegistrable : AppCompatActivity() {

    private var srvEnvasesDisponible: Service_EnvasesDisponibles? = null
    private var envasesDisponibles: List<EnvaseDisponible>? = null
    private var envasesSeleccionados: ArrayList<EnvaseDisponible>? = null
    private var clienteId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buscar_envase_registrable)

        init()
    }

    private fun init() {

        clienteId = intent.extras.getInt("clienteId")
        envasesSeleccionados = SessionVars.Get_Session_Var(SessionKeys.ENVASES_REGISTRABLES_SELECCIONADOS) as ArrayList<EnvaseDisponible>

        if(envasesSeleccionados==null){
            envasesSeleccionados = ArrayList<EnvaseDisponible>()
        }

        if(srvEnvasesDisponible==null) srvEnvasesDisponible = Service_EnvasesDisponibles(this)
        Utiles.prepareRecyclerView(lst_envases, this)
        txt_busqueda.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                onBusquedaChanged()
            }
        })

        if (clienteId>0)
            envasesDisponibles =  srvEnvasesDisponible!!.obtenerEnvasesParaRecuperar(clienteId)
        else
            envasesDisponibles =  srvEnvasesDisponible!!.obtenerEnvasesDisponibles();

        envasesSeleccionados!!.forEach{x->
            var envase = envasesDisponibles!!.find { it.id==x.id }
            envase!!.seleccionado = x.seleccionado
        }

        this.poblarLista(envasesDisponibles!!)

        swt_seleccionarTodos.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener
        { compoundButton, b -> onSeleccionarTodosChanged() })
    }

    private fun onSeleccionarTodosChanged(){
        this.envasesDisponibles!!.forEach { x-> x.seleccionado = this.swt_seleccionarTodos.isChecked }
        this.txt_busqueda.setText("")

        envasesSeleccionados = this.envasesDisponibles!!.filter { it.seleccionado } as ArrayList<EnvaseDisponible>
        lbl_cantSeleccionados.text = envasesSeleccionados!!.count().toString() + " seleccionados"
    }

    private fun poblarLista(envases: List<EnvaseDisponible>) {

        lst_envases.adapter = EnvaseDisponibleAdapter(envases,
                this, false, true,
                object: EnvaseDisponibleAdapter.Events{
                    override fun onItemSeleccionado(envase: EnvaseDisponible, position: Int, esSeleccionado: Boolean ){
                        onEnvaseSeleccionado(envase, position, esSeleccionado)
                    }
                    override fun onItemEliminado(envase: EnvaseDisponible, position: Int){

                    }
                })
    }

    private fun onBusquedaChanged(){

        var envasesFiltrados = envasesDisponibles!!.filter { x-> x.codigo.contains(txt_busqueda.text, true)}
        poblarLista(envasesFiltrados)
    }

    private fun onEnvaseSeleccionado(envase: EnvaseDisponible, position: Int, esSeleccionado: Boolean){

        if(esSeleccionado){
            envasesSeleccionados!!.add(envase)
        }else{
            envasesSeleccionados = envasesSeleccionados!!.filter { x-> x.id != envase.id } as ArrayList<EnvaseDisponible>
        }

        lbl_cantSeleccionados.text = envasesSeleccionados!!.count().toString() + " seleccionados"
    }

    public fun onConfirmarSeleccionados(view : View){

        SessionVars.Add_Session_Var(SessionKeys.ENVASES_REGISTRABLES_SELECCIONADOS, envasesSeleccionados)
        this.setResult(Activity.RESULT_OK, Intent())
        this.finish()
    }

    override fun onBackPressed() {

        setResult(Activity.RESULT_CANCELED, Intent())
        super.onBackPressed()
    }

    fun onActualizarDisponibles(view : View){

        val srvSyncMobile = SyncMobile(this)
        val appSession = AppSession(this)
        val identificadorMovil = Utiles.Get_Config_Value("codigo_identificacion_movil", this)
        val context = this;

        var syncMobile = SyncMobile(this);

        syncMobile.obtenerBarriles(appSession.getHojaDeRuta().id, identificadorMovil,
                object : ApiCallback<ObtenerBarrilesResponse>() {
                    override fun onSuccess(response: ObtenerBarrilesResponse) {

                        try {

                            srvEnvasesDisponible!!.guardarEnvasesDisponibles(response.barriles)

                            if (clienteId>0)
                                envasesDisponibles =  srvEnvasesDisponible!!.obtenerEnvasesParaRecuperar(clienteId)
                            else
                                envasesDisponibles =  srvEnvasesDisponible!!.obtenerEnvasesDisponibles();

                            txt_busqueda.setText("")
                            Dialog.success("Exitoso", "Se han actualizado los barriles", context)

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(message: String, errorCode: Int) {
                        Dialog.error("Error", message, context)
                    }
                })
    }

}
