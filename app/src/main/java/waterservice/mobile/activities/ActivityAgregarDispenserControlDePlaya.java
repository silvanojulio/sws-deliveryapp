package waterservice.mobile.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.Dispenser;
import waterservice.mobile.services.Service_Dispensers;
import waterservice.mobile.utiles.AsyncWork;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;

public class ActivityAgregarDispenserControlDePlaya extends AppCompatActivity  {

    private EditText txt_nroDispenser;
    private Service_Dispensers _srvDispensers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_dispenser_control_de_playa);

        init();
    }

    private void init() {

        txt_nroDispenser = (EditText) this.findViewById(R.id.txt_nroDispenser);
        _srvDispensers = new Service_Dispensers(this);
    }

    public void btn_agregarOnClick(View view){
        AsyncWork.run("onConfirmarAgregarDispenser", this, "Agregando dispenser...");
    }

    public void onConfirmarAgregarDispenser() throws Exception {

        String nroDispenser = txt_nroDispenser.getText().toString();

        Dispenser d = _srvDispensers.obtenerDispenserParaControlDePlaya(nroDispenser);

        if(d==null) throw new Exception("El nro de dispenser no existe");

        SessionVars.Add_Session_Var(SessionKeys.DISPENSER_SELECCIONADO, d);

        setResult(RESULT_OK);
        this.finish();
    }
}
