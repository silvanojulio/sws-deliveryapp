package waterservice.mobile.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.Transaccion;
import waterservice.mobile.services.Service_Ventas;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;

public class ActivityTransacciones extends Activity {

    private ListView trasacciones_lst;
    private AppSession appSession;
    Cliente cliente;
    Service_Ventas _srvVentas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transacciones);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {

        appSession = new AppSession(this);

        trasacciones_lst = (ListView) this.findViewById(R.id.trasacciones_lst);

        _srvVentas=new Service_Ventas(this);
        cliente = appSession.getClienteActual();

        List<Transaccion> transacciones=null;

        if(cliente!=null)
            transacciones=_srvVentas.ObtenerTransacciones(cliente.cliente_id);
        else
            transacciones = _srvVentas.ObtenerTransacciones();

        ArrayAdapterTransacciones ad=new ArrayAdapterTransacciones(this,transacciones);

        trasacciones_lst.setAdapter(ad);

    }
}
