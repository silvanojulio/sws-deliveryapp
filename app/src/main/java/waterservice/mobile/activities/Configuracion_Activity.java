package waterservice.mobile.activities;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

import ar.com.waterservice.newapp.R;

public class Configuracion_Activity  extends  PreferenceActivity{
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        addPreferencesFromResource(  R.xml.movil_configuration_values_ftp);

    }

}
