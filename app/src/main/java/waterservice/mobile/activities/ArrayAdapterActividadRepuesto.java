package waterservice.mobile.activities;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.DispenserAsociado;
import waterservice.mobile.clases.MantenimientoDeDispenser;
import waterservice.mobile.clases.RepuestoActividad;

public class ArrayAdapterActividadRepuesto extends ArrayAdapter<RepuestoActividad> {

    List<RepuestoActividad> _values;
    Activity _context;

    public ArrayAdapterActividadRepuesto(Activity context, List<RepuestoActividad> values) {

        super(context, R.layout.row_actividad_repuesto ,values);
        this._context = context;
        this._values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) _context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.row_actividad_repuesto, parent, false);
        RowRepuestoActividad row = new RowRepuestoActividad(rowView, _values.get(position));

        return rowView;
    }
}

class RowRepuestoActividad{

    public RepuestoActividad item;
    private TextView lbl_cantidad;
    private TextView lbl_nombreItem;
    private TextView lbl_dispenser;
    private ImageView img_icono;

    public RowRepuestoActividad(View row, RepuestoActividad _item){

        item = _item;
        lbl_cantidad = (TextView) row.findViewById(R.id.lbl_cantidad);
        lbl_nombreItem = (TextView) row.findViewById(R.id.lbl_nombreItem);
        img_icono = (ImageView) row.findViewById(R.id.img_icono);
        lbl_dispenser = (TextView) row.findViewById(R.id.lbl_dispenser);

        setValues();
    }

    private void setValues() {

        lbl_cantidad.setText( " x " + String.valueOf(item.cantidad) );
        lbl_nombreItem.setText(String.valueOf(item.nombreDelItem) );

        img_icono.setImageResource(item.tipo_id==3?
                R.drawable.spare : R.drawable.activity);

        lbl_dispenser.setText(item.dispenser!=null? "Disp: "+ item.dispenser: "" );
    }
}