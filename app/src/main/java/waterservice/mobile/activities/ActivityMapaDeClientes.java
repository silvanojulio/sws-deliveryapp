package waterservice.mobile.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.services.Service_Clientes;
import waterservice.mobile.utiles.CurrentApp;

public class ActivityMapaDeClientes extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Service_Clientes srvCliente;
    private SupportMapFragment frg_mapa;
    private List<Cliente> clientes;

    private LatLng primeraUbicacion;

    Cliente clienteActual;
    private AppSession appSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa_de_clientes);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {

        appSession = new AppSession(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        srvCliente = new Service_Clientes(this);
        clienteActual = appSession.getClienteActual();

        if(clienteActual!=null){
            clientes = new ArrayList<>();
            clientes.add(clienteActual);
        }else{
            clientes = srvCliente.obtenerClientes(0, "", appSession.getHojaDeRuta(), 1);
        }

        if(mMap != null)
            poblarMapa();
    }

    private void poblarMapa() {

        primeraUbicacion = null;
        int orden = 1;

        for (Cliente cliente: clientes) {

            double _lat = 0, _long = 0;

            if(cliente.relevamientoCoordenadas_latitud != null && !cliente.relevamientoCoordenadas_latitud.equals(""))
            {
                _lat = Double.valueOf(cliente.relevamientoCoordenadas_latitud);
                _long = Double.valueOf(cliente.relevamientoCoordenadas_longitud);
            }else if(cliente.altitud != null && !cliente.altitud.equals("") ){
                _lat = Double.valueOf(cliente.altitud);
                _long = Double.valueOf(cliente.longitud);
            }else{

                orden++;
                continue;
            }

            // Add a marker in Sydney and move the camera
            LatLng ubicacionCliente = new LatLng(_lat, _long);
            Marker mCliente = mMap.addMarker(new MarkerOptions().position(ubicacionCliente)
                    .title(String.valueOf(orden) +" - "+ cliente.nombreCliente + " ("+String.valueOf(cliente.cliente_id)+")"));

            mCliente.setSnippet(cliente.domicilioCompleto);

            if(cliente.visitado)
                mCliente.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            else if(cliente.pendiente)
                mCliente.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
            else
                mCliente.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

            if(primeraUbicacion == null)
                primeraUbicacion = ubicacionCliente;

            orden++;
        }

        if(CurrentApp.currentLatutid!=null && !CurrentApp.currentLatutid.equals("")){

            LatLng ubicacionReparto = new LatLng(Double.valueOf(CurrentApp.currentLatutid), Double.valueOf(CurrentApp.currentLongitud));
            Marker mReparto = mMap.addMarker(new MarkerOptions().position(ubicacionReparto).title("Reparto"));

            mReparto.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.truck_small));

            float zoomLevel = (float) 18.0;
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(clienteActual!=null? primeraUbicacion : ubicacionReparto  , zoomLevel));

        }else if(primeraUbicacion != null){

            float zoomLevel = (float) 18.0;
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(primeraUbicacion,zoomLevel));
        }


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if(clientes != null)
            poblarMapa();

    }
}
