package waterservice.mobile.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.ComprobanteFisicoUtilizado;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.Recibo;
import waterservice.mobile.fragments.FragmentImputacionesFacturas;
import waterservice.mobile.fragments.FragmentRecibo;
import waterservice.mobile.services.Service_Clientes;
import waterservice.mobile.services.Service_Recibos;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivityRecibo extends AppCompatActivity {

    private FragmentRecibo frm_recibo;
    private FragmentImputacionesFacturas frm_imputacion;

    private TextView lbl_aCuenta;
    private TextView lbl_totalAdeudado;
    private TextView lbl_totalImputado;
    private TextView lbl_totalRecibo;
    private TextView lbl_mensajeVisita;

    boolean firstLoad = true;

    double totalRecibo = 0;
    Recibo reciboActual = null;
    ComprobanteFisicoUtilizado comprobanteFisico;
    private Cliente cliente;
    private Service_Recibos srvRecibos;
    private AppSession appSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo);

        if(firstLoad)
        {
            try {
                init();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            firstLoad = false;
        }
    }

    private void init() throws Exception {

        appSession = new AppSession(this);
        cliente = appSession.getClienteActual();
        srvRecibos = new Service_Recibos(this);
        reciboActual = srvRecibos.obtenerReciboDeCliente(cliente.cliente_id, false,false, false);

        cliente = new Service_Clientes(this).obtenerCliente(cliente.cliente_id);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        android.support.v4.app.FragmentManager fManager = getSupportFragmentManager();
        frm_recibo = (FragmentRecibo) fManager.findFragmentById(R.id.frm_recibo);
        frm_imputacion = (FragmentImputacionesFacturas) fManager.findFragmentById(R.id.frm_imputacion);
        frm_imputacion.getView().setVisibility(View.GONE);
        frm_recibo.getView().setVisibility(View.VISIBLE);

        frm_recibo.acciones = new FragmentRecibo.Acciones() {

            @Override
            public void onItemAgregado() {

            }

            @Override
            public void onActualizarTotal() {
                actualizarTotales();
            }
        };

        lbl_aCuenta = (TextView) this.findViewById(R.id.lbl_aCuenta);
        lbl_totalAdeudado = (TextView) this.findViewById(R.id.lbl_totalAdeudado);
        lbl_totalImputado = (TextView) this.findViewById(R.id.lbl_totalImputado);
        lbl_totalRecibo = (TextView) this.findViewById(R.id.lbl_totalRecibo);
        lbl_mensajeVisita = (TextView) this.findViewById(R.id.lbl_mensajeVisita);

        frm_imputacion.acciones = new FragmentImputacionesFacturas.Acciones() {
            @Override
            public void onConfirmarImputaciones() {
                try {
                    confirmarImputaciones();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onActualizarTotales() {
                actualizarTotales();
            }
        };

        lbl_mensajeVisita.setVisibility(cliente.fechaHoraTransferido != null? View.VISIBLE: View.GONE);

        actualizarTotales();
    }

    private void confirmarImputaciones() throws Exception {

        if(reciboActual != null && reciboActual.cerrado){

            Dialog.error("Error", "El recibo ya está cerrado", ActivityRecibo.this);
            return;
        }

        cliente = new Service_Clientes(this).obtenerCliente(cliente.cliente_id);
        boolean haSincronizado = cliente.fechaHoraTransferido!=null;

        String mensaje = comprobanteFisico == null ?
                "Está seguro que desea confirmar sin comprobante físico? "+(haSincronizado?" - VISITA YA SINCRONIZADA - ":""):
                "Ha registrado el comprobante físico "+
                        String.valueOf(comprobanteFisico.nroPrefijo) +"-"+
                        String.valueOf(comprobanteFisico.nroComprobante) +
                "."+(haSincronizado?" - VISITA YA SINCRONIZADA - ":"")+" Confirma los datos?";

        Dialog.confirm("Confirmación", mensaje, new Dialog.OnConfirm() {
            @Override
            public void confirm() {
                try{

                    srvRecibos.confirmarImputacionesDeRecibo(cliente, frm_imputacion.facturas, comprobanteFisico);
                    finish();

                }catch (Exception ex){
                    Dialog.error("Error", ex.getMessage(), ActivityRecibo.this);
                }
            }
        }, this);

        if(haSincronizado){
            Dialog.info("Advertencia","Mientras cargaba el recibo, la visita se ha sincronizado. Este recibo deberá ser cargado manualmente por sistema web", this);
        }
    }

    private void actualizarTotales(){

        if(frm_recibo == null) return;

        double totalRecibido = frm_recibo == null? 0: frm_recibo.obtenerTotal();
        double totalAdeudado = cliente.obtenerSaldoAlDia();
        double totalImputado = frm_imputacion==null? 0 : frm_imputacion.obtenerTotalImputado();
        double aCuenta = totalRecibido - totalImputado;

        lbl_totalRecibo.setText("$"+Utiles.Round(totalRecibido));
        lbl_aCuenta.setText("$"+Utiles.Round(aCuenta));
        lbl_totalAdeudado.setText("$"+Utiles.Round(totalAdeudado));
        lbl_totalImputado.setText("$"+Utiles.Round(totalImputado));
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_recibo:
                    frm_imputacion.getView().setVisibility(View.GONE);
                    frm_recibo.getView().setVisibility(View.VISIBLE);
                    onReciboTabClick();
                    return true;
                case R.id.navigation_imputaciones:
                    frm_imputacion.getView().setVisibility(View.VISIBLE);
                    frm_recibo.getView().setVisibility(View.GONE);
                    onImputacionesTabClick();
                    return true;
            }
            return false;
        }
    };

    private void onReciboTabClick(){

    }

    private void onImputacionesTabClick(){

    }

    public void btn_imprimirOnClick(View view){

        try{

            String salidaRecibo = srvRecibos.imprimirReciboDeCliente(cliente, this);
            //Utiles.Mensaje_En_Pantalla("Imprimiendo recibo...", true, false,this);

            Toast.makeText(this, salidaRecibo, Toast.LENGTH_LONG).show();

        }catch (Exception ex){
            Dialog.error("Error", ex.getMessage(), this);
        }

    }

    public void btn_reciboFisicoOnClick(View view){

        SessionVars.Add_Session_Var(SessionKeys.COMPROBANTE_FISICO_CREADO, null);
        SessionVars.Add_Session_Var(SessionKeys.TIPO_COMPROBANTE_FISICO_ID, Constantes.TIPO_COMPROBANTE_RECIBO);
        Intent intent = new Intent(this, ActivityComprobanteFisico.class);
        startActivityForResult(intent, Constantes.ACTIVITY_COMPROBANTE_FISICO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case Constantes.ACTIVITY_COMPROBANTE_FISICO:
                onComprobanteFisicoResult(resultCode);
                break;
        }

    }

    private void onComprobanteFisicoResult(int resultCode) {

        if(resultCode != RESULT_OK) return;

        comprobanteFisico = (ComprobanteFisicoUtilizado) SessionVars.Get_Session_Var(SessionKeys.COMPROBANTE_FISICO_CREADO);

    }
}
