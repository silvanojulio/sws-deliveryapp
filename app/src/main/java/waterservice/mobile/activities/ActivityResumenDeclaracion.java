package waterservice.mobile.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.sql.SQLException;

import ar.com.waterservice.newapp.R;
import okhttp3.internal.Util;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.ResumenDeHojaDeRuta;
import waterservice.mobile.clases.UsuarioApp;
import waterservice.mobile.services.Service_DeclaracionesEfectivo;
import waterservice.mobile.utiles.AsyncWork;
import waterservice.mobile.utiles.CurrentApp;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivityResumenDeclaracion extends Activity {

    private TextView resumenDec_lbl_CantidadCheques;
    private TextView resumenDec_lbl_TotalCheques;
    private TextView resumenDec_lbl_TotalEfectivoDecl;
    private TextView resumenDec_lbl_TotalRetenciones;
    private TextView resumenDec_lbl_TotalGastos;
    private TextView lbl_cantidadRetenciones;
    private TextView lbl_cantidadTarjetaDeDebito;
    private TextView lbl_cantidadTarjetasDeCredito;
    private TextView lbl_tarjetaDeDebito;
    private TextView lbl_totalTarjetaDeCredito;

    Service_DeclaracionesEfectivo _srvDeclaraciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_resumen_declaracion);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {

        lbl_cantidadRetenciones = (TextView) this.findViewById(R.id.lbl_cantidadRetenciones);
        lbl_cantidadTarjetaDeDebito = (TextView) this.findViewById(R.id.lbl_cantidadTarjetaDeDebito);
        lbl_cantidadTarjetasDeCredito = (TextView) this.findViewById(R.id.lbl_cantidadTarjetasDeCredito);
        lbl_tarjetaDeDebito = (TextView) this.findViewById(R.id.lbl_tarjetaDeDebito);
        lbl_totalTarjetaDeCredito = (TextView) this.findViewById(R.id.lbl_totalTarjetaDeCredito);
        resumenDec_lbl_CantidadCheques = (TextView) this.findViewById(R.id.resumenDec_lbl_CantidadCheques);
        resumenDec_lbl_TotalCheques = (TextView) this.findViewById(R.id.resumenDec_lbl_TotalCheques);
        resumenDec_lbl_TotalEfectivoDecl = (TextView) this.findViewById(R.id.resumenDec_lbl_TotalEfectivoDecl);
        resumenDec_lbl_TotalGastos = (TextView) this.findViewById(R.id.resumenDec_lbl_TotalGastos);
        resumenDec_lbl_TotalRetenciones = (TextView) this.findViewById(R.id.resumenDec_lbl_TotalRetenciones);

        _srvDeclaraciones = new Service_DeclaracionesEfectivo(this);

        ResumenDeHojaDeRuta resumen = _srvDeclaraciones.obtenerResumeDeHojaDeRuta();

        resumenDec_lbl_CantidadCheques.setText(String.valueOf(resumen.cheques.cantidadItems));
        resumenDec_lbl_TotalCheques.setText("$"+Utiles.Round(resumen.cheques.totalRegistrado));

        lbl_cantidadRetenciones.setText(String.valueOf(resumen.retenciones.cantidadItems));
        resumenDec_lbl_TotalRetenciones.setText("$"+Utiles.Round(resumen.retenciones.totalRegistrado));

        lbl_cantidadTarjetasDeCredito.setText(String.valueOf(resumen.tarjetasDeCredito.cantidadItems));
        lbl_totalTarjetaDeCredito.setText("$"+Utiles.Round(resumen.tarjetasDeCredito.totalRegistrado));

        lbl_cantidadTarjetaDeDebito.setText(String.valueOf(resumen.tarjetasDeDebito.cantidadItems));
        lbl_tarjetaDeDebito.setText("$"+Utiles.Round(resumen.tarjetasDeDebito.totalRegistrado));

        resumenDec_lbl_TotalEfectivoDecl.setText("$"+Utiles.Round(resumen.efectivo.totalDeclarado));

        resumenDec_lbl_TotalGastos.setText("$"+Utiles.Round(resumen.totalGastos));

    }

    public void resumenDec_btn_ConfirmarOnClick(View view){

        AsyncWork.run("confirmarCierreDeRuta",this,"Cerrando ruta...");
    }

    public boolean confirmando=false;
    public void confirmarCierreDeRuta() throws Exception{

        if(confirmando) return;

        confirmando = true;

        UsuarioApp usuarioApp= new AppSession(this).getUsuarioAcual();

        _srvDeclaraciones.confirmarCierreDeRuta(usuarioApp, new Service_DeclaracionesEfectivo.OnServerSimpleResponse() {
            @Override
            public void onSuccess() {

                ActivityResumenDeclaracion.this.setResult(RESULT_OK);
                ActivityResumenDeclaracion.this.finish();
            }

            @Override
            public void onError(String message) {
                confirmando = false;
                Dialog.error("Error", message, ActivityResumenDeclaracion.this);
            }
        });
    }

}
