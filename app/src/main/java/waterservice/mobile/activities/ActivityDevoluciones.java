package waterservice.mobile.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.ArticuloDeLista;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.ValorSatelite;
import waterservice.mobile.services.Service_Articulos;
import waterservice.mobile.services.Service_Clientes;
import waterservice.mobile.services.Service_General;
import waterservice.mobile.services.Service_Ventas;
import waterservice.mobile.utiles.Dialog;

public class ActivityDevoluciones extends Activity{

    private EditText devoluciones_txt_cantidad;
    private Spinner devoluciones_spn_motivos;
    private Spinner devoluciones_spn_articulos;
    private CheckBox devoluciones_ckb_cambioDirecto;

    Service_Clientes _srvClientes;
    Service_Ventas _srvVentas;
    Service_Articulos _srvArticulos;
    Service_General _srvGeneral;
    Cliente cliente;
    ArticuloDeLista articuloSeleccionado;
    ValorSatelite motivoSeleccionado;
    List<ValorSatelite> motivos;
    private AppSession appSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devoluciones);

        try{
            init();
        }catch (Exception ex){

        }
    }

    private void init() throws Exception {
        appSession = new AppSession(this);
        _srvClientes=new Service_Clientes(this);
        _srvGeneral = new Service_General(this);
        _srvVentas=new Service_Ventas(this);
        _srvArticulos=new Service_Articulos(this);

        devoluciones_txt_cantidad = (EditText) this.findViewById(R.id.devoluciones_txt_cantidad);
        devoluciones_spn_motivos = (Spinner) this.findViewById(R.id.devoluciones_spn_motivos);
        devoluciones_spn_articulos = (Spinner) this.findViewById(R.id.devoluciones_spn_articulos);
        devoluciones_ckb_cambioDirecto = (CheckBox) this.findViewById(R.id.devoluciones_ckb_cambioDirecto);

        cliente = appSession.getClienteActual();
        cliente.articulosAbonos=_srvArticulos.obtenerArticulosDeAbonos(cliente.cliente_id);
        cliente.articulosComodatos=_srvArticulos.obtenerArticulosDeComodatos(cliente.cliente_id);
        cliente.articulosLista=_srvArticulos.obtenerArticulosDeLista();

        String[] items= Service_Articulos.ObtenerNombresDeArticulosTodos(cliente.articulosLista, cliente.articulosAbonos, cliente.articulosComodatos);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        devoluciones_spn_articulos.setAdapter(adapter);

        motivos=_srvGeneral.obtenerValoresSatelites(Constantes.T_MOTIVOSDEDEVOLUCION);
        String[] itemsMotivos= _srvGeneral.ObtenerTextosDeValoresSatelites(motivos);

        ArrayAdapter<String> adapterMotivos = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, itemsMotivos);
        adapterMotivos.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        devoluciones_spn_motivos.setAdapter(adapterMotivos);

    }

    public void devoluciones_btn_confirmarOnClick(View view){

        try{

            articuloSeleccionado=cliente.articulosLista.get(devoluciones_spn_articulos.getSelectedItemPosition());
            motivoSeleccionado = motivos.get(devoluciones_spn_motivos.getSelectedItemPosition());
            double cantidad= Double.valueOf(devoluciones_txt_cantidad.getText().toString()) ;

            _srvVentas.GuardarDevolucionArticulo(articuloSeleccionado,cliente, cantidad,
                    motivoSeleccionado.valor_id, devoluciones_ckb_cambioDirecto.isChecked(),
                    motivoSeleccionado.valor_id == Constantes.MOTIVO_DEVOLUCION_NO_CONSUME,
                    motivoSeleccionado.valor_texto, articuloSeleccionado.nombreArticulo);

            Dialog.success("Devolución confirmada", "Se ha registrado la devolución", this);

            this.finish();

        }catch (Exception ex){
            Toast.makeText(getApplicationContext(),
                    ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void btn_imprimirComprobanteOnClick(View view){

        try{

            _srvVentas.imprimirComprobanteDeDevolucion(cliente, this);

            this.finish();

        }catch (Exception ex){
            Toast.makeText(getApplicationContext(),
                    ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

}
