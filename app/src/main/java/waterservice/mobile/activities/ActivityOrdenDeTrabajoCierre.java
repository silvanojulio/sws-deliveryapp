package waterservice.mobile.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Spinner;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.OrdenDeTrabajo;
import waterservice.mobile.clases.ValorSatelite;
import waterservice.mobile.services.Service_ServicioTecnico;
import waterservice.mobile.utiles.AsyncWork;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivityOrdenDeTrabajoCierre extends AppCompatActivity {

    private Spinner spn_motivosDeCierre;
    private Service_ServicioTecnico _srvServicioTecnico;
    private List<ValorSatelite> motivos;
    private Cliente cliente;
    private AppSession appSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orden_de_trabajo_cierre);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception{

        appSession = new AppSession(this);
        cliente = appSession.getClienteActual();

        _srvServicioTecnico = new Service_ServicioTecnico(this);

        spn_motivosDeCierre = (Spinner) this.findViewById(R.id.spn_motivosDeCierre);

        motivos = _srvServicioTecnico.obtenerMotivosDeCierresDeOrdenes();

        Utiles.SetItemsToSpinner(spn_motivosDeCierre,motivos,"valor_texto",this);
    }

    public void bnt_cerrarOrdenOnClick(View view){

        AsyncWork.run("runCerrarOden",this, "Cerrando orden...");
    }

    public void runCerrarOden() throws Exception {

        _srvServicioTecnico.cerrarOrdenDeTrabajo(
                (OrdenDeTrabajo) SessionVars.Get_Session_Var(SessionKeys.ORDEN_DE_TRABAJO_ACTUAL),
                motivos.get(spn_motivosDeCierre.getSelectedItemPosition()).valor_id, cliente);

        setResult(RESULT_OK);

        finish();
    }
}
