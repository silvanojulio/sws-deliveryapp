package waterservice.mobile.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.services.Service_Clientes;
import waterservice.mobile.utiles.CurrentApp;
import waterservice.mobile.utiles.Dialog;

public class ActivitySeleccionarUbicacionCliente extends AppCompatActivity implements OnMapReadyCallback, CompoundButton.OnCheckedChangeListener {

    private TextView lbl_direccion;
    private TextView lbl_nombreCliente;
    private ImageView target;
    private GoogleMap mMap;
    private Cliente cliente;
    private Service_Clientes srvCliente;
    Marker marker;
    private AppSession appSession;
    private Switch ck_satelite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccionar_ubicacion_cliente);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {
        appSession = new AppSession(this);
        ck_satelite = (Switch) this.findViewById(R.id.ck_satelite);
        lbl_direccion = (TextView) this.findViewById(R.id.lbl_direccion);
        lbl_nombreCliente = (TextView) this.findViewById(R.id.lbl_nombreCliente);
        target = (ImageView) this.findViewById(R.id.target);
        target.setVisibility(View.GONE);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        cliente = appSession.getClienteActual();

        lbl_direccion.setText(cliente.domicilioCompleto);
        lbl_nombreCliente.setText(cliente.nombreCliente);

        srvCliente = new Service_Clientes(this);

        ck_satelite.setOnCheckedChangeListener(this);
    }

    public void btn_seleccionarUbicacionOnClick(View view){

        Dialog.confirm("Ubicación de cliente", "Confirma que la posición del pin es la ubicación real del cliente?",
                new Dialog.OnConfirm() {
                    @Override
                    public void confirm() {

                        confirmarUbicacion();
                    }
                }, this);

    }

    private void confirmarUbicacion(){

        try{

            srvCliente.registrarCoordenadas( String.valueOf(marker.getPosition().latitude) ,
                    String.valueOf(marker.getPosition().longitude), cliente);

            Toast.makeText(getApplicationContext(),
                    "Exitoso! Se han registrado las coordenadas del cliente",
                    Toast.LENGTH_LONG).show();

            this.finish();

        }catch (Exception ex){
            Dialog.error("Error", ex.getMessage(),this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                mMap.clear();
                LatLng midLatLng = mMap.getCameraPosition().target;

                marker = mMap.addMarker(new MarkerOptions().position(midLatLng).title("Ubicación de cliente"));
                //marker.setDraggable(true);
                target.setVisibility(View.GONE);
            }
        });

        mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {

                target.setVisibility(View.VISIBLE);
            }
        });


        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(Double.valueOf(CurrentApp.currentLatutid), Double.valueOf(CurrentApp.currentLongitud));
        marker = mMap.addMarker(new MarkerOptions().position(sydney).title("Ubicación de cliente"));
        //marker.setDraggable(true);

        float zoomLevel = (float) 18.0;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,zoomLevel));
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        mMap.setMapType(b? GoogleMap.MAP_TYPE_HYBRID:GoogleMap.MAP_TYPE_NORMAL);
    }
}
