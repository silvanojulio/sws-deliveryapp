package waterservice.mobile.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.DispenserAsociado;
import waterservice.mobile.clases.OrdenDeTrabajo;
import waterservice.mobile.services.Service_ServicioTecnico;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivityOrdenDeTrabajo extends AppCompatActivity {

    private ViewStub vst_tabServiciosTecnicos;
    private ViewStub vst_tabDispensersAsociados;
    private Button btn_tab_datos_st;
    private Button btn_tab_dispensers;

    TabOrdenDeTrabajoDisplay tabOrdenDeTrabajo;
    TabDispensers tabDispensers;
    TabContainer tConteiner;

    OrdenDeTrabajo ordenDeTrabajo;
    List<DispenserAsociado> dispenserAsociados;

    Service_ServicioTecnico _srvServicioTecnico;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orden_de_trabajo);

        init();
    }

    private void init() {

        _srvServicioTecnico=new Service_ServicioTecnico(this);

        vst_tabServiciosTecnicos = (ViewStub) this.findViewById(R.id.vst_tabServiciosTecnicos);
        vst_tabDispensersAsociados = (ViewStub) this.findViewById(R.id.vst_tabDispensersAsociados);

        btn_tab_datos_st = (Button) this.findViewById(R.id.btn_tab_datos_st);
        btn_tab_dispensers = (Button) this.findViewById(R.id.btn_tab_dispensers);

        tConteiner = new TabContainer();
        tabOrdenDeTrabajo = new TabOrdenDeTrabajoDisplay(this, vst_tabServiciosTecnicos, btn_tab_datos_st, tConteiner);
        tabDispensers = new TabDispensers(this, vst_tabDispensersAsociados, btn_tab_dispensers, tConteiner);
        tConteiner.onButtonClick(tabOrdenDeTrabajo);

        ordenDeTrabajo = (OrdenDeTrabajo) SessionVars.Get_Session_Var(SessionKeys.ORDEN_DE_TRABAJO_ACTUAL);

        try {

            dispenserAsociados = _srvServicioTecnico.obtenerDispensersAsociados(ordenDeTrabajo);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        tabOrdenDeTrabajo.poblarCampos(ordenDeTrabajo);

        tabDispensers.ordenDeTrabajo = ordenDeTrabajo;
        tabDispensers.dispenserAsociados = dispenserAsociados;
    }

    public void btn_AgregarDispenserOnClick(View view){

        SessionVars.Add_Session_Var(SessionKeys.DISPENSER_ASOCIADO, null);
        SessionVars.Add_Session_Var(SessionKeys.REPUESTOSACTIVIDADES, null);

        Intent intent=new Intent(this, ActivityMantenimiento.class);
        this.startActivityForResult(intent, Constantes.ACTIVITY_MANTENIMIENTO);
    }

    public void btn_cerrarOrdenOnClick(View view){

        boolean hayDispensersSinMantenimientos=false;
        boolean noTieneDispensersAsociados = dispenserAsociados.size() == 0;

        try {

            if(ordenDeTrabajo.motivoDeCierreId != null)
                throw new Exception("Esta orden de trabajo ya ha sido cerrada");

            for (DispenserAsociado d : dispenserAsociados) {
                if(!d.mantemientoCargado) {
                    hayDispensersSinMantenimientos=true;
                    break;
                }
            }

            if(noTieneDispensersAsociados || hayDispensersSinMantenimientos){

                String mensaje = noTieneDispensersAsociados ?
                        "No hay dispensers asociados, desea continuar con el cierre de la orden?":
                        "Hay dispensers sin mantenimientos registrados, desea cotinuar con el cierre de la orden?";

                Utiles.MostrarConfirmacion("Cierre de orden", mensaje, "abrirCierreDeOrden", this, this);

            }else{
                abrirCierreDeOrden();
            }

        }catch (Exception ex){
            Utiles.Mensaje_En_Pantalla(ex.getMessage(),true, false, this);
        }
    }

    public void btn_verRemitoOnClick(View view){

        Intent intent=new Intent(this, ActivityRemitoServicioTecnico.class);
        this.startActivityForResult(intent, Constantes.ACTIVITY_CIERRE_OT);
    }

    public void abrirCierreDeOrden(){
        Intent intent=new Intent(this, ActivityOrdenDeTrabajoCierre.class);
        this.startActivityForResult(intent, Constantes.ACTIVITY_CIERRE_OT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch(requestCode) {
            case Constantes.ACTIVITY_MANTENIMIENTO:
                if(resultCode == RESULT_OK){
                    try {
                        dispenserAsociados = _srvServicioTecnico.obtenerDispensersAsociados(ordenDeTrabajo);
                        tabDispensers.dispenserAsociados = dispenserAsociados;
                        tabDispensers.poblarCampos();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case Constantes.ACTIVITY_CIERRE_OT:
                if(resultCode == RESULT_OK){
                    tabOrdenDeTrabajo.btn_cerrarOrden.setText("Orden CERRADA");
                    tabOrdenDeTrabajo.btn_cerrarOrden.setEnabled(false);
                }
                break;
        }

    }
}

class TabOrdenDeTrabajoDisplay extends TabViewStub{

    public TabOrdenDeTrabajoDisplay(Activity _activity, ViewStub _tab, Button _btn, TabContainer tConteiner){
        super(_activity, _tab, _btn, tConteiner);

        lbl_cantDispensers = (TextView) _activity.findViewById(R.id.lbl_cantDispensers);
        lbl_comentarios = (TextView) _activity.findViewById(R.id.lbl_comentarios);
        lbl_horario = (TextView) _activity.findViewById(R.id.lbl_horario);
        lbl_prioridad = (TextView) _activity.findViewById(R.id.lbl_prioridad);
        lbl_responsable = (TextView) _activity.findViewById(R.id.lbl_responsable);
        lbl_sectorUbicacion = (TextView) _activity.findViewById(R.id.lbl_sectorUbicacion);
        lbl_sintoma = (TextView) _activity.findViewById(R.id.lbl_sintoma);
        btn_cerrarOrden = (Button) _activity.findViewById(R.id.btn_cerrarOrden);
        btn_verRemito = (Button) _activity.findViewById(R.id.btn_verRemito);
    }

    public TextView lbl_cantDispensers;
    public TextView lbl_comentarios;
    public TextView lbl_horario;
    public TextView lbl_prioridad;
    public TextView lbl_responsable;
    public TextView lbl_sectorUbicacion;
    public TextView lbl_sintoma;
    public Button btn_cerrarOrden;
    public Button btn_verRemito;

    public void poblarCampos(OrdenDeTrabajo ordenDeTrabajo) {

        lbl_cantDispensers.setText(ordenDeTrabajo.cantDispensers);
        lbl_comentarios.setText(ordenDeTrabajo.comentarios);
        lbl_horario.setText(ordenDeTrabajo.franjaHoraria);
        lbl_prioridad.setText(ordenDeTrabajo.prioridad);
        lbl_responsable.setText(ordenDeTrabajo.responsableEnCliente+ " ("+ordenDeTrabajo.telefonoResponsable+")" );
        lbl_sectorUbicacion.setText(ordenDeTrabajo.sectorUbicacion);
        lbl_sintoma.setText(ordenDeTrabajo.sintoma);

        if(ordenDeTrabajo.motivoDeCierreId != null){
            this.btn_cerrarOrden.setText("Orden CERRADA");
            this.btn_cerrarOrden.setEnabled(false);

        }

    }
}

class TabDispensers extends TabViewStub{

    public ListView lst_dispensersAsociados;
    public OrdenDeTrabajo ordenDeTrabajo;
    public List<DispenserAsociado> dispenserAsociados;

    public TabDispensers(Activity _activity, ViewStub _tab, Button _btn, TabContainer tConteiner){
        super(_activity, _tab, _btn, tConteiner);

    }

    public void poblarCampos(){
        lst_dispensersAsociados = (ListView) activity.findViewById(R.id.lst_dispensersAsociados);
        ArrayAdapterDispensersSt ad = new ArrayAdapterDispensersSt(activity, dispenserAsociados);
        lst_dispensersAsociados.setAdapter(ad);

        lst_dispensersAsociados.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TabDispensers.this.cargarMantenimientoADispenser(position);
            }
        });
    }

    private void cargarMantenimientoADispenser(int position){

        DispenserAsociado dispenser = dispenserAsociados.get(position);

        if(dispenser.mantemientoCargado)
        {
            Utiles.Mensaje_En_Pantalla("El dispenser seleccionado ya posee un mantenimiento cargado", true, false, this.activity);
            return;
        }

        SessionVars.Add_Session_Var(SessionKeys.DISPENSER_ASOCIADO, dispenser);
        SessionVars.Add_Session_Var(SessionKeys.REPUESTOSACTIVIDADES, null);

        Intent intent=new Intent(activity, ActivityMantenimiento.class);
        activity.startActivityForResult(intent, Constantes.ACTIVITY_MANTENIMIENTO);
    }

    @Override
    public void onTabSelected() {
        poblarCampos();
    }
}