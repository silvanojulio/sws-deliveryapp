package waterservice.mobile.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.utiles.Utiles;

public class ActivityFichaCliente extends Activity {

    private TextView fichaCliente_lbl_codigo;
    private TextView fichaCliente_lbl_nombreCliente;
    private TextView fichaCliente_lbl_direccion;
    private TextView fichaCliente_lbl_tipoCliente;
    private TextView fichaCliente_lbl_saldoConsumos;
    private TextView fichaCliente_lbl_credito;
    private TextView fichaCliente_lbl_ultimaVenta;
    private TextView fichaCliente_lbl_ultimoCobro;
    private TextView fichaCliente_lbl_ultimoPrestamoEnv;

    private AppSession appSession;
    Cliente cliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ficha_cliente);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {

        appSession = new AppSession(this);

        fichaCliente_lbl_codigo = (TextView) this.findViewById(R.id.fichaCliente_lbl_codigo);
        fichaCliente_lbl_nombreCliente = (TextView) this.findViewById(R.id.fichaCliente_lbl_nombreCliente);
        fichaCliente_lbl_direccion = (TextView) this.findViewById(R.id.fichaCliente_lbl_direccion);
        fichaCliente_lbl_tipoCliente = (TextView) this.findViewById(R.id.fichaCliente_lbl_tipoCliente);
        fichaCliente_lbl_saldoConsumos = (TextView) this.findViewById(R.id.fichaCliente_lbl_saldoConsumos);
        fichaCliente_lbl_credito = (TextView) this.findViewById(R.id.fichaCliente_lbl_credito);
        fichaCliente_lbl_ultimaVenta = (TextView) this.findViewById(R.id.fichaCliente_lbl_ultimaVenta);
        fichaCliente_lbl_ultimoCobro = (TextView) this.findViewById(R.id.fichaCliente_lbl_ultimoCobro);
        fichaCliente_lbl_ultimoPrestamoEnv = (TextView) this.findViewById(R.id.fichaCliente_lbl_ultimoPrestamoEnv);

        cliente= appSession.getClienteActual();

        fichaCliente_lbl_codigo.setText(String.valueOf(cliente.cliente_id));
        fichaCliente_lbl_nombreCliente.setText(cliente.nombreCliente);
        fichaCliente_lbl_direccion.setText(cliente.domicilioCompleto);
        fichaCliente_lbl_tipoCliente.setText(cliente.tipoCliente_ids==1? "Familia": "Empresa");
        fichaCliente_lbl_saldoConsumos.setText(Utiles.Round(cliente.obtenerSaldoAlDia()));
        fichaCliente_lbl_credito.setText(Utiles.Round(cliente.creditoDisponible));
        fichaCliente_lbl_ultimaVenta.setText(cliente.fechaUtlimaEntrega);
        fichaCliente_lbl_ultimoCobro.setText(cliente.fechaUltimoCobroFactura);
        fichaCliente_lbl_ultimoPrestamoEnv.setText(cliente.fechaUltimaEnvases);

    }

    public void onTelefonosClick(View v){
        String urlSistema = getUrlSistema()+"/Clientes/DatosContacto?cliente_id="+
                String.valueOf(cliente.cliente_id);
        Utiles.openUrl(urlSistema, this);
    }

    String getUrlSistema(){
        AppSession appSession = new AppSession(this);
        final String serverRemoto= appSession.getValueString("servidor_web");
        return serverRemoto;
    }
}
