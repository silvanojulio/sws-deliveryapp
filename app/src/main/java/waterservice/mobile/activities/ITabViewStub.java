package waterservice.mobile.activities;

public interface  ITabViewStub{
    void setVisible(boolean visible);
    boolean getVisible();
    void revertVisible();
    void setSelected(Boolean selected);
    void onTabSelected();
    void onTabLeave();
}
