package waterservice.mobile.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.adapters.ArticuloAdapter;
import waterservice.mobile.adapters.ArticuloParaSeleccionar;
import waterservice.mobile.clases.ArticuloDeAbono;
import waterservice.mobile.clases.ArticuloDeLista;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.ListaDePrecios;
import waterservice.mobile.clases.PrecioEspecial;
import waterservice.mobile.clases.StockDeCliente;
import waterservice.mobile.services.Service_Articulos;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivitySeleccionarArticulo extends AppCompatActivity {

    private RecyclerView lts_articulos;
    private EditText txt_busqueda;

    private Service_Articulos _srvArticulos;

    private List<ArticuloParaSeleccionar> articulosParaSeleccionar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccionar_articulo);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {

        _srvArticulos = new Service_Articulos(this);
        lts_articulos = (RecyclerView) this.findViewById(R.id.lts_articulos);
        txt_busqueda = (EditText) this.findViewById(R.id.txt_busqueda);
        Utiles.prepareRecyclerView(lts_articulos, this);

        ActivityVenta.ArticulosDelCliente articulos = (ActivityVenta.ArticulosDelCliente)
                SessionVars.Get_Session_Var(SessionKeys.ARTICULOS_DEL_CLIENTE);

        articulosParaSeleccionar = ArticuloParaSeleccionar.obtenerArticulos(
                articulos.articulosDeLista, articulos.listasDePrecios, articulos.preciosEspeciales,
                articulos.articulosAbonos, articulos.stocks);

        poblarLista(articulosParaSeleccionar);

        txt_busqueda.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                onTextoBusquedaChanged();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
    }

    private void onTextoBusquedaChanged(){

        List<ArticuloParaSeleccionar> arts = new ArrayList<>();

        String busqueda = txt_busqueda.getText().toString();

        for (ArticuloParaSeleccionar art:articulosParaSeleccionar) {

            if(art.nombreArticulo.toUpperCase().contains(busqueda.toUpperCase()) ||
                    String.valueOf(art.articuloId).equals(busqueda)){
                arts.add(art);
            }
        }

        poblarLista(arts);
    }

    private void poblarLista(List<ArticuloParaSeleccionar> arts){

        ArticuloAdapter ad = new ArticuloAdapter(arts, this, new ArticuloAdapter.ArticuloViewHolder.Acciones() {
            @Override
            public void onSeleccionar(ArticuloParaSeleccionar articulo) {
                onSeleccionarArticulo(articulo);
            }
        });

        lts_articulos.setAdapter(ad);
    }

    private void onSeleccionarArticulo(ArticuloParaSeleccionar articulo){

        SessionVars.Add_Session_Var(SessionKeys.ARTICULO_SELECCIONADO, articulo);
        this.setResult(RESULT_OK);
        this.finish();
    }
}
