package waterservice.mobile.activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Switch;

import java.util.List;

import ar.com.waterservice.newapp.R;
import okhttp3.internal.Util;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.adapters.ClientesHojaDeRutaAdapter;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.procesos.ServiceSync;
import waterservice.mobile.services.Service_Articulos;
import waterservice.mobile.services.Service_Clientes;
import waterservice.mobile.services.Service_HojasDeRutas;
import waterservice.mobile.utiles.AsyncWork;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.Utiles;

public class ActivityHojaDeRuta extends Activity {

    private SearchView hojaDeRuta_buscar;
    private RecyclerView hojaDeRuta_lst_clientes;
    private Switch SwitchPendientes;

    private Service_Clientes _srvClientes;
    private Service_HojasDeRutas _srvHojasDeRuta;
    private Service_Articulos _srvArticulos;
    private String _textoBuscado="";
    private List<Cliente> _clientes;

    private int indexSelected=0;
    private int estadoVisita = 0;
    private AppSession appSession;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_todos:
                    estadoVisita = 0;
                    break;
                case R.id.navigation_pedidos:
                    estadoVisita = 6;
                    break;
                case R.id.navigation_noSync:
                    estadoVisita = 4;
                    break;
                case R.id.navigation_repaso:
                    estadoVisita = 3;
                    break;
                case R.id.navigation_servicioTecnico:
                    estadoVisita = 5;
                    break;
            }

            buscar();

            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_hoja_de_ruta);
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void init() throws Exception {

        appSession = new AppSession(this);

        if(appSession.getHojaDeRuta().cerrada){
            Utiles.Mensaje_En_Pantalla("La hoja de ruta está cerrada", true, true, this);
            this.finish();
        }
        SwitchPendientes=(Switch) findViewById(R.id.switch1);
        SwitchPendientes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) appSession.setValueInt(0,SessionKeys.CLIENTE_INDICE_ACTUAL);
                buscar();
            }
        });

        hojaDeRuta_buscar=(SearchView) findViewById(R.id.hojaDeRuta_buscar);
        hojaDeRuta_buscar.setQueryHint("Buscar clientes");
        hojaDeRuta_buscar.setOnQueryTextListener(new OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // TODO Auto-generated method stub

                 _textoBuscado=query;
                 buscar();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                _textoBuscado=newText;
                buscar();
                return false;
            }
        });

        hojaDeRuta_lst_clientes=(RecyclerView) findViewById(R.id.hojaDeRuta_lst_clientes);

        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        hojaDeRuta_lst_clientes.setLayoutManager(mLayoutManager);
        hojaDeRuta_lst_clientes.setHasFixedSize(true);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        _srvClientes=new Service_Clientes(this);
        _srvHojasDeRuta=new Service_HojasDeRutas(this);
        _srvArticulos=new Service_Articulos(this);

    }

    boolean estaBuscando = false;

    void buscar(){

        if(estaBuscando) return;

        estaBuscando = true;

        AsyncWork.run(new AsyncWork.RunAction() {
            @Override
            public void onRunAction() throws Exception {

                //1- Ninguna, 2- Solo dentro del reparto, 3- Todos los clientes
                String tipoDeVentaExtra = _srvClientes.obtenerConfiguracion("VENTA_EXTRA_PERMITIDA", "1");
                int filtroEstadoVisita=estadoVisita==0 && SwitchPendientes.isChecked()?1:estadoVisita;

                _clientes=_srvClientes.obtenerClientes(filtroEstadoVisita,_textoBuscado,
                        appSession.getHojaDeRuta(), Integer.valueOf(tipoDeVentaExtra));
            }

            @Override
            public void onActionFinished()throws Exception{

                ClientesHojaDeRutaAdapter ad=new ClientesHojaDeRutaAdapter(_clientes, ActivityHojaDeRuta.this);
                hojaDeRuta_lst_clientes.setAdapter(ad);

                indexSelected = appSession.getValueInt(SessionKeys.CLIENTE_INDICE_ACTUAL);
                int clienteActual = appSession.getValueInt(SessionKeys.CLIENTE_ACTUAL);
                for(int i=0;i<_clientes.size();i++){
                    if(clienteActual==_clientes.get(i).cliente_id){
                        indexSelected=i+1;
                        break;
                    }
                }

                //if(SwitchPendientes.isChecked()) indexSelected=0;
                if(indexSelected>=0)
                    hojaDeRuta_lst_clientes.getLayoutManager().scrollToPosition(indexSelected);

                estaBuscando = false;
            }
        }, "Espere por favor...", this);

    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    void startSync(boolean validate){
        try{

            if(validate && isMyServiceRunning(ServiceSync.class)){
                throw new Exception("Sync ya está activo");
            }

            startService(new Intent(this, ServiceSync.class));

        }catch (Exception ex){
            ex.printStackTrace();
            Dialog.error("Error al intentar iniciar el Sync", ex.getMessage(), this);
        }

    }

    @Override
    protected void onPostResume() {

        buscar();
        if(!isMyServiceRunning(ServiceSync.class)){
            startSync(false);
        }

        super.onPostResume();
    }



}
