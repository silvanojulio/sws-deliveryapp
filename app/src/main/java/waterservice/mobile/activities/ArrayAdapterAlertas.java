package waterservice.mobile.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.AlertaDeCliente;

public class ArrayAdapterAlertas extends ArrayAdapter<AlertaDeCliente> {


    List<AlertaDeCliente> _values;
    Context _context;

    public ArrayAdapterAlertas(Context context, List<AlertaDeCliente> values) {

        super(context, R.layout.row_prestamo_pendiente ,values);
        this._context = context;
        this._values = values;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) _context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row = inflater.inflate(R.layout.row_alerta, parent, false);

        TextView rowAlerta_blb_comentario;
        TextView rowAlerta_lbl_tipoAlerta;

        rowAlerta_blb_comentario = (TextView) row.findViewById(R.id.rowAlerta_blb_comentario);
        rowAlerta_lbl_tipoAlerta = (TextView) row.findViewById(R.id.rowAlerta_lbl_tipoAlerta);

        AlertaDeCliente alerta=_values.get(position);

        rowAlerta_blb_comentario.setText(alerta.comentarios);
        rowAlerta_lbl_tipoAlerta.setText(alerta.tipoDeAlerta);

        return row;

    }
}
