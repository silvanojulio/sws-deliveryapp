package waterservice.mobile.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.ArticuloControlDePlaya;
import waterservice.mobile.clases.ArticuloDeLista;
import waterservice.mobile.services.Service_Articulos;
import waterservice.mobile.utiles.AsyncWork;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivityAgregarArticuloControlRuta extends Activity {

    private EditText agrArtControlPlaya_txt_cantFallados;
    private EditText agrArtControlPlaya_txt_cantLlenos;
    private EditText agrArtControlPlaya_txt_cantVacios;
    private EditText agrArtControlPlaya_txt_cantLlenosPack;
    private EditText agrArtControlPlaya_txt_cantVaciosPack;
    private EditText agrArtControlPlaya_txt_cantFalladosPack;
    private EditText agrArtControlPlaya_txt_cantRepuestos;

    private Button agrArtControlPlaya_btn_confirmar;

    private Spinner agrArtControlPlaya_spn_articulo;

    private RadioButton agrArtControlPlaya_rdb_entrada;
    private RadioButton agrArtControlPlaya_rdb_salida;

    private LinearLayout pnl_producto;
    private LinearLayout pnl_repuesto;

    int cantLlenos=0, cantVacios=0, cantFallados=0;

    Service_Articulos _srvArticulos;

    List<ArticuloDeLista> articulosControlables;

    int tipoArticuloAgregar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_articulo_control);

        init();
    }

    void init(){

        agrArtControlPlaya_txt_cantFallados = (EditText) this.findViewById(R.id.agrArtControlPlaya_txt_cantFallados);
        agrArtControlPlaya_txt_cantLlenos = (EditText) this.findViewById(R.id.agrArtControlPlaya_txt_cantLlenos);
        agrArtControlPlaya_txt_cantVacios = (EditText) this.findViewById(R.id.agrArtControlPlaya_txt_cantVacios);
        agrArtControlPlaya_txt_cantLlenosPack = (EditText) this.findViewById(R.id.agrArtControlPlaya_txt_cantLlenosPack);
        agrArtControlPlaya_txt_cantVaciosPack = (EditText) this.findViewById(R.id.agrArtControlPlaya_txt_cantVaciosPack);
        agrArtControlPlaya_txt_cantFalladosPack = (EditText) this.findViewById(R.id.agrArtControlPlaya_txt_cantFalladosPack);
        agrArtControlPlaya_txt_cantRepuestos = (EditText) this.findViewById(R.id.agrArtControlPlaya_txt_cantRepuestos);

        agrArtControlPlaya_btn_confirmar = (Button) this.findViewById(R.id.agrArtControlPlaya_btn_confirmar);

        agrArtControlPlaya_spn_articulo = (Spinner) this.findViewById(R.id.agrArtControlPlaya_spn_articulo);

        pnl_producto = (LinearLayout) this.findViewById(R.id.pnl_producto);
        pnl_repuesto = (LinearLayout) this.findViewById(R.id.pnl_repuesto);

        _srvArticulos = new Service_Articulos(this);

        try {

            tipoArticuloAgregar = (int) SessionVars.Get_Session_Var(SessionKeys.TIPO_ARTICULO_AGREGAR);

            if(tipoArticuloAgregar==1)
            {
                articulosControlables=_srvArticulos.obtenerArticulosControlablesDeplaya();
                pnl_repuesto.setVisibility(View.GONE);
            }
            else if(tipoArticuloAgregar==3)
            {
                articulosControlables=_srvArticulos.obtenerRepuestos();
                pnl_producto.setVisibility(View.GONE);
            }

            String[] items= Service_Articulos.ObtenerNombresDeArticulosTodos(articulosControlables);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, items);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            agrArtControlPlaya_spn_articulo.setAdapter(adapter);

        }catch (Exception ex){}
    }

    public void confirmarAgregarArticulo(View btn){

        AsyncWork.run("confirmar", this, "Agregando item...");
    }

    public void confirmar(){

        try {

            ArticuloControlDePlaya art=new ArticuloControlDePlaya();

            art.articulo = articulosControlables.get(agrArtControlPlaya_spn_articulo.getSelectedItemPosition());
            art.articulo_id = art.articulo.id;

            if(tipoArticuloAgregar==1){
                art.cantidadLlenosPack=obtenerCantidad(agrArtControlPlaya_txt_cantLlenosPack) ;
                art.cantidadLlenosUni=obtenerCantidad(agrArtControlPlaya_txt_cantLlenos) ;

                art.cantidadVaciosPack=obtenerCantidad(agrArtControlPlaya_txt_cantVaciosPack) ;
                art.cantidadVaciosUni=obtenerCantidad(agrArtControlPlaya_txt_cantVacios) ;

                art.cantidadFalladosPack=obtenerCantidad(agrArtControlPlaya_txt_cantFalladosPack) ;
                art.cantidadFalladosUni=obtenerCantidad(agrArtControlPlaya_txt_cantFallados) ;

            }else if(tipoArticuloAgregar == 3){
                art.cantidadLlenosUni=obtenerCantidad(agrArtControlPlaya_txt_cantRepuestos) ;
            }
            art.generarCantidadesFinales();
            SessionVars.Add_Session_Var(SessionKeys.ARTICULO_SELECCIONADO, art);
            setResult(RESULT_OK);
            this.finish();

        }catch (Exception ex){
            Utiles.Mensaje_En_Pantalla(ex.getMessage(), true, true, this);
        }
    }

    int obtenerCantidad(EditText txt){
        return txt.getText().toString().equals("")?0:Integer.valueOf(txt.getText().toString());
    }

}
