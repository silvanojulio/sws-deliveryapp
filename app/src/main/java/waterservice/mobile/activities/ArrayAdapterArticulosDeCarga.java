package waterservice.mobile.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.ArticuloDePlanificacionDeCarga;
import waterservice.mobile.clases.Deposito;
import waterservice.mobile.utiles.Utiles;

public class ArrayAdapterArticulosDeCarga extends ArrayAdapter<ArticuloDePlanificacionDeCarga> {

    List<ArticuloDePlanificacionDeCarga> _values;
    Context _context;

    public ArrayAdapterArticulosDeCarga(Context context, List<ArticuloDePlanificacionDeCarga> values) {

        super(context, R.layout.row_articulo_de_carga ,values);
        this._context = context;
        this._values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) _context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row = inflater.inflate(R.layout.row_articulo_de_carga, parent, false);
        TextView rowCar_lbl_articulo;
        TextView rowCar_lbl_cantidad;
        TextView rowCar_lbl_codigoInterno;

        ArticuloDePlanificacionDeCarga item = _values.get(position);

        rowCar_lbl_articulo = (TextView) row.findViewById(R.id.rowCar_lbl_articulo);
        rowCar_lbl_cantidad = (TextView) row.findViewById(R.id.rowCar_lbl_cantidad);
        rowCar_lbl_codigoInterno = (TextView) row.findViewById(R.id.rowCar_lbl_codigoInterno);

        rowCar_lbl_articulo.setText(item.nombreArticulo);
        rowCar_lbl_cantidad.setText(String.valueOf(item.cantidad));
        rowCar_lbl_codigoInterno.setText(item.codigoInterno);

        return row;

    }

}
