package waterservice.mobile.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.AlertaDeCliente;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.services.Service_Clientes;
import waterservice.mobile.services.Service_Ventas;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.FontManager;
import waterservice.mobile.utiles.Utiles;


public class ActivityAlertasDelCliente extends Activity {

    private ListView lst_alertas;
    private Button btn_imprimir;

    Service_Clientes _srvClientes;
    AppSession appSession;
    Cliente cliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alertas);

        init();
    }

    private void init() {

        appSession = new AppSession(this);
        lst_alertas = (ListView) this.findViewById(R.id.lst_alertas);
        btn_imprimir = (Button) this.findViewById(R.id.btn_imprimir);

        FontManager.maskForFaIcon(this, R.id.btn_confirmarAlertas);
        FontManager.maskForFaIcon(this, R.id.btn_imprimir);

        _srvClientes=new Service_Clientes(this);

        try {
            cliente= appSession.getClienteActual();

            if(!cliente.esCuentaBloqueada)
                btn_imprimir.setVisibility(View.INVISIBLE);


            List<AlertaDeCliente> alertas = _srvClientes.obtenerAlertasDeClientes(cliente.cliente_id);

            ArrayAdapterAlertas ad = new ArrayAdapterAlertas(this, alertas);
            lst_alertas.setAdapter(ad);

        } catch (Exception e) {
            String ms = e.getMessage();
        }

    }

    public void btn_confirmarAlertasOnClick(View view){

        try {

            _srvClientes.registrarAlertasVistas(cliente);
            this.finish();

        } catch (Exception e) {
            Utiles.Mensaje_En_Pantalla(e.getMessage(),true,false,this);
        }

    }

    public void btn_ImprimirOnClick(View view){

        try {
            new Service_Ventas(getApplicationContext()).imprimirCuentaBloqueada(cliente, ActivityAlertasDelCliente.this);
        } catch (Exception e) {
            Utiles.Mensaje_En_Pantalla(e.getMessage(), true,false,ActivityAlertasDelCliente.this);
        }
    }

    public void btn_desbloquearOnClick(View view){

        try {

            Dialog.promptText("Código de desbloqueo", "Ingrese el código de desbloqueo",
                    new Dialog.OnConfirmTextInput() {
                        @Override
                        public void confirm(String textInput) {

                            try{
                                _srvClientes.desbloquearCliente(cliente, textInput);
                                _srvClientes.registrarAlertasVistas(cliente);

                                Utiles.Mensaje_En_Pantalla("Cliente desbloqueado",true,false,ActivityAlertasDelCliente.this);
                                ActivityAlertasDelCliente.this.finish();

                            }catch (Exception ex){
                                Utiles.Mensaje_En_Pantalla(ex.getMessage(),true,false,ActivityAlertasDelCliente.this);
                            }
                        }
                    },this);

        } catch (Exception e) {
            Utiles.Mensaje_En_Pantalla(e.getMessage(),true,false,this);
        }

    }

}
