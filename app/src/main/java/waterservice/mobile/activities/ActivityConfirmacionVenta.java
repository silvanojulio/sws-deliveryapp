package waterservice.mobile.activities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import ar.com.waterservice.newapp.R;
import okhttp3.internal.Util;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.adapters.ItemRemitoAdapter;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.ComprobanteFisicoUtilizado;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.FirmaRemito;
import waterservice.mobile.clases.VentaActual;
import waterservice.mobile.services.Service_ControlDePlayaOnLine;
import waterservice.mobile.services.Service_Ventas;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivityConfirmacionVenta extends AppCompatActivity {

    private TextView lbl_contadoEfectivo;
    private TextView lbl_totalVenta;
    private RecyclerView lst_articulosVendidos;
    private VentaActual ventaActual;
    private Cliente cliente;

    private Button btn_confirmarConRemitoFisico;
    private Button btn_confirmarSinRemito;
    private Button btn_firmaDeCliente;
    private AppSession appSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmacion_venta);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {

        appSession = new AppSession(this);
        lbl_contadoEfectivo = (TextView) this.findViewById(R.id.lbl_contadoEfectivo);
        lbl_totalVenta = (TextView) this.findViewById(R.id.lbl_totalVenta);
        lst_articulosVendidos = (RecyclerView) this.findViewById(R.id.lst_articulosVendidos);
        Utiles.prepareRecyclerView(lst_articulosVendidos, this);

        ventaActual = (VentaActual) SessionVars.Get_Session_Var(SessionKeys.VENTA_ACTUAL);
        if(ventaActual==null) finish();

        cliente = appSession.getClienteActual();

        lbl_totalVenta.setText("$"+ Utiles.Round(ventaActual.obtenerTotal()));

        if(ventaActual.esContadoEfectivo)
            lbl_contadoEfectivo.setText("SI");
        else
            lbl_contadoEfectivo.setText(ventaActual.importeEfectivo > 0?
                    "$"+ Utiles.Round(ventaActual.importeEfectivo) :"NO");

        ItemRemitoAdapter ad = new ItemRemitoAdapter(ventaActual.articulosEntregados, this);
        lst_articulosVendidos.setAdapter(ad);

        btn_confirmarConRemitoFisico = (Button) this.findViewById(R.id.btn_confirmarConRemitoFisico);
        btn_confirmarSinRemito = (Button) this.findViewById(R.id.btn_confirmarSinRemito);
        btn_firmaDeCliente = (Button) this.findViewById(R.id.btn_firmaDeCliente);

        if(ventaActual.tieneErrores()){
            btn_confirmarConRemitoFisico.setVisibility(View.GONE);
            btn_confirmarSinRemito.setVisibility(View.GONE);
            btn_firmaDeCliente.setVisibility(View.GONE);
        }else{
            btn_confirmarConRemitoFisico.setVisibility(View.VISIBLE);
            btn_confirmarSinRemito.setVisibility(View.VISIBLE);
            btn_firmaDeCliente.setVisibility(View.VISIBLE);
        }
    }

    public void btn_confirmarConRemitoFisicoOnClick(View view){

        SessionVars.Add_Session_Var(SessionKeys.COMPROBANTE_FISICO_CREADO, null);
        SessionVars.Add_Session_Var(SessionKeys.TIPO_COMPROBANTE_FISICO_ID, Constantes.TIPO_COMPROBANTE_REMITO);
        Intent intent = new Intent(this, ActivityComprobanteFisico.class);
        startActivityForResult(intent, Constantes.ACTIVITY_COMPROBANTE_FISICO);

    }

    public void btn_confirmarSinRemitoOnClick(View view){

       confirmarVentas(null, null);
    }

    public void btn_firmaDeClienteOnClick(View view){

        Intent intent = new Intent(this, ActivityRemitoFirma.class);
        startActivityForResult(intent, Constantes.ACTIVITY_FIRMA_CLIENTE);

    }

    private void onComprobanteFisicoResult(int resultCode){

        if(resultCode != RESULT_OK) return;

        ComprobanteFisicoUtilizado comprobante = (ComprobanteFisicoUtilizado) SessionVars.Get_Session_Var(SessionKeys.COMPROBANTE_FISICO_CREADO);

        confirmarVentas(comprobante, null);

    }

    private void onFirmaClienteResult(int resultCode){

        if(resultCode != RESULT_OK) return;

        FirmaRemito firmaRemito = (FirmaRemito) SessionVars.Get_Session_Var(SessionKeys.FIRMA_CLIENTE);

        confirmarVentas(null, firmaRemito);
    }

    void confirmarVentas(ComprobanteFisicoUtilizado comprobanteFisico, FirmaRemito firmaRemito){

        try{

            new Service_Ventas(this).confirmarVenta(ventaActual, cliente,
                    comprobanteFisico, firmaRemito);

            this.setResult(RESULT_OK);
            this.finish();
        }catch (Exception ex){
            Dialog.error("Error", ex.getMessage(), this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch(requestCode) {
            case Constantes.ACTIVITY_COMPROBANTE_FISICO:
                onComprobanteFisicoResult(resultCode);
                break;
            case Constantes.ACTIVITY_FIRMA_CLIENTE:
                onFirmaClienteResult(resultCode);
                break;
        }

    }
}
