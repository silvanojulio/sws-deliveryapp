package waterservice.mobile.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.apiContracts.Sync.VerificarConfiguracionMobileResponse;
import waterservice.mobile.server.ApiCallback;
import waterservice.mobile.server.SyncMobile;
import waterservice.mobile.utiles.AsyncWork;
import waterservice.mobile.utiles.BaseDeDatos;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.Utiles;

public class DescargarBase_Activity  extends AppCompatActivity {

    TextView desRuta_lbl_reparto;
    DatePicker desRuta_dtp_fecha;
    String reparto="";
    String codigoIdentificacionMovil="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descargar_ruta);

        init();
    }

    private void init() {

        desRuta_lbl_reparto=(TextView) findViewById(R.id.desRuta_lbl_reparto);
        desRuta_dtp_fecha=(DatePicker) findViewById(R.id.desRuta_dtp_fecha);

        reparto=Utiles.Get_Config_Value("codigo_movil",this);
        desRuta_lbl_reparto.setText(reparto);

        codigoIdentificacionMovil=Utiles.Get_Config_Value("codigo_identificacion_movil",this);
    }

    public void ConfirmarDescargaDeRutaDesdeWebServer(){

        try {

            String fecha=String.valueOf(desRuta_dtp_fecha.getYear())+
                    Utiles.CompleteTwoDigitsNumber(desRuta_dtp_fecha.getMonth() + 1)+
                    Utiles.CompleteTwoDigitsNumber(desRuta_dtp_fecha.getDayOfMonth());

            BaseDeDatos.RecibirBase(codigoIdentificacionMovil, fecha, this);
            Utiles.Mensaje_En_Pantalla("Base Recibida!", true, true, DescargarBase_Activity.this);

        } catch (Exception e) {
            Utiles.Stop_Working_Message();
            Utiles.Mensaje_En_Pantalla(e.getMessage(), true, true, DescargarBase_Activity.this);
        }
    }

    public void btn_descargarBaseOnClick(View view){

        new AlertDialog.Builder(this)
                //.setIcon(android.R.drawable.ic_dialog_alert)
                .setIcon(R.drawable.question)
                .setTitle("Recibiendo Base del FTP")
                .setMessage("Ultima Vez Recibida: "+BaseDeDatos.UltimaBajada(this)+"\n seguro que desea Recibir la Base?")
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {

                    //@Override
                    public void onClick(DialogInterface dialog, int which) {

                        Utiles.Start_Working_Message(DescargarBase_Activity.this,"Recibiendo Base...");

                        Thread timer = new Thread(){

                            public void run(){

                                try {

                                    sleep(200);

                                    DescargarBase_Activity.this.ConfirmarDescargaDeRutaDesdeWebServer();

                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        };

                        timer.start();

                    }

                })
                .setNegativeButton("NO", null)
                .show();
    }

    public void btn_verificarConfiguracionOnClick(View view){

        AppSession appSession = new AppSession(DescargarBase_Activity.this);

        final String identificadorMovil= appSession.getValueString("codigo_identificacion_movil");
        final String serverRemoto= appSession.getValueString("servidor_web");
        final String serverLocal= appSession.getValueString("servidor_web_local");
        final boolean modoRedLocal= appSession.getValueBoolean("modo_local");

        final String config = "\n\nCod móvil: "+ identificadorMovil+"\n" +
                "Srv remoto: "+ serverRemoto+"\n" +
                "Srv local: "+ serverLocal+"\n" +
                "Modo local: "+ (modoRedLocal?"SI":"NO") ;

        if(modoRedLocal && (serverLocal == null || serverLocal.equals(""))) {
            String message = "No ha configurado el Server Local";
            Dialog.error("Error",message + ". " + config, DescargarBase_Activity.this);
            return;
        }else if(serverRemoto == null || serverRemoto.equals("")){
            String message = "No ha configurado el Server Remoto";
            Dialog.error("Error",message + ". " + config, DescargarBase_Activity.this);
            return;
        }

        AsyncWork.run(new AsyncWork.RunAction() {
            @Override
            public void onRunAction() throws Exception {

                SyncMobile srvSyncMobile = new SyncMobile(DescargarBase_Activity.this);
                srvSyncMobile.verificarConfiguracion(identificadorMovil,
                        new ApiCallback<VerificarConfiguracionMobileResponse>() {

                            @Override
                            public void onSuccess(VerificarConfiguracionMobileResponse response) {
                                Dialog.success("Configuración válida", response.message, DescargarBase_Activity.this);
                            }

                            @Override
                            public void onError(String message, int errorCode) {
                                Dialog.error("Error",message + ". " + config, DescargarBase_Activity.this);
                            }
                        });
            }

            @Override
            public void onActionFinished() throws Exception {

            }
        }, "Verificando...", this);
    }

}


