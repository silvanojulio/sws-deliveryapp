package waterservice.mobile.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.UsuarioApp;
import waterservice.mobile.services.Service_HojasDeRutas;
import waterservice.mobile.utiles.AsyncWork;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.Utiles;

public class ActivityLogin extends AppCompatActivity {

    private EditText txt_password;
    private EditText txt_usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
    }

    private void init() {

        txt_password = (EditText) this.findViewById(R.id.txt_password);
        txt_usuario = (EditText) this.findViewById(R.id.txt_usuario);

        new AppSession(this).desloguearUsuario();

        String lastUsername = Utiles.Get_Config_Value("LAST_USER", this);

        if(lastUsername!=null)
        {
            txt_usuario.setText(lastUsername);
            txt_password.requestFocus();
        }

        solicitarPermisos();
    }

    private void solicitarPermisos(){

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED ) {

            ActivityCompat.requestPermissions(this,
                    obtenerPermisos(),
                    99);
        }
    }

    private String[] obtenerPermisos(){
        return new String[]{
                Manifest.permission.BLUETOOTH,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.INTERNET,
                Manifest.permission.RECEIVE_BOOT_COMPLETED,
        };
    };

    public void email_sign_in_buttonOnClick(View view){

        AsyncWork.run("onLoguarUsuario",this,"Ingresando...");
    }

    public void onLoguarUsuario() throws Exception {

        Service_HojasDeRutas _srvHojaDeRuta = new Service_HojasDeRutas(this);
        UsuarioApp usuario = _srvHojaDeRuta.obtenerUsuario(
                txt_usuario.getText().toString(),
                txt_password.getText().toString());

        new AppSession(this).setUsuarioActual(usuario);

        Utiles.Set_Config_Value("LAST_USER", txt_usuario.getText().toString(), this);

        try {

            Intent myIntent=new Intent(this,MainActivity.class);
            startActivity(myIntent);

        } catch (Exception e) {
            e.printStackTrace();
        }

        this.finish();

    }

    public void btn_DescargarBaseClick(View v){

        try {

            Intent myIntent=new Intent(this,DescargarBase_Activity.class);
            startActivity(myIntent);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void btn_ConfiguracionBaseClick(View v){
        try {

            final String passAdminActual = Utiles.Get_Config_Value("PASS_ADMIN",this);

            if(passAdminActual == null || passAdminActual.equals("")){

                Dialog.promptText("Password admin", "Debe establecer un password admin para la configuración",
                        new Dialog.OnConfirmTextInput() {
                            @Override
                            public void confirm(String textInput) {

                                if(textInput==null || textInput.equals("") || textInput.length()<4)
                                {
                                    Dialog.error("Error","El password debe tener al menos 4 caracteres", ActivityLogin.this);
                                    return;
                                }

                                Utiles.Set_Config_Value("PASS_ADMIN", textInput, ActivityLogin.this);

                                Intent myIntent=new Intent(ActivityLogin.this, Configuracion_Activity.class);
                                startActivity(myIntent);
                            }
                        }, this);
            }else{

                Dialog.promptText("Password admin", "Ingrese el password admin",
                        new Dialog.OnConfirmTextInput() {
                            @Override
                            public void confirm(String textInput) {

                                if(textInput==null || !textInput.equals(passAdminActual))
                                {
                                    Dialog.error("Error","El password es incorrecto", ActivityLogin.this);
                                    return;
                                }

                                Intent myIntent=new Intent(ActivityLogin.this, Configuracion_Activity.class);
                                startActivity(myIntent);
                            }
                        }, this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
