package waterservice.mobile.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.ArticuloDePlanificacionDeCarga;
import waterservice.mobile.clases.HojaDeRuta;
import waterservice.mobile.clases.PlanificacionDeCarga;
import waterservice.mobile.services.Service_ControlDePlayaOnLine;
import waterservice.mobile.services.Service_HojasDeRutas;
import waterservice.mobile.utiles.AsyncWork;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivityPlanificacionDeCarga extends Activity {

    Service_ControlDePlayaOnLine srvControlDePlaya;
    PlanificacionDeCarga planificacionDeCarga;

    private TextView lbl_factor;
    private TextView lbl_fechaDeCarga;
    private TextView lbl_fechaDeGeneracion;
    private TextView lbl_reparto;
    private ListView lsv_articulosDeCarga;
    private TextView lbl_pesoReparto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_planificacion_de_carga);

        try {
            init();
        } catch (Exception e) {
            Utiles.Mensaje_En_Pantalla(e.getMessage(),true,false,this);
        }
    }

    private void init() throws Exception {

        lbl_factor = (TextView) this.findViewById(R.id.lbl_factor);
        lbl_fechaDeCarga = (TextView) this.findViewById(R.id.lbl_fechaDeCarga);
        lbl_fechaDeGeneracion = (TextView) this.findViewById(R.id.lbl_fechaDeGeneracion);
        lbl_reparto = (TextView) this.findViewById(R.id.lbl_reparto);
        lbl_pesoReparto = (TextView) this.findViewById(R.id.lbl_pesoReparto);
        lsv_articulosDeCarga = (ListView) this.findViewById(R.id.lsv_articulosDeCarga);

        srvControlDePlaya = new Service_ControlDePlayaOnLine(this);

        AsyncWork.run("obtenerDatos",this, "Obteniendo datos");
    }

    public void obtenerDatos(){

        try{
            final HojaDeRuta hojaDeRuta = new AppSession(this).getHojaDeRuta();

            planificacionDeCarga = srvControlDePlaya.obtenerPlanificacionDeCarga(hojaDeRuta.id);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    lbl_reparto.setText(hojaDeRuta.nombreReparto);

                    String textoPeso=String.valueOf(planificacionDeCarga.obtenerPesoTotal())+"kg";

                    if(planificacionDeCarga.vehiculo!=null){
                        textoPeso+=" / Tara: "+ Utiles.Round(planificacionDeCarga.vehiculo.kilosDeCarga) + "kg" ;
                    }

                    lbl_pesoReparto.setText(textoPeso);
                    //stuff that updates ui
                    lbl_factor.setText(String.valueOf(planificacionDeCarga.factor));
                    lbl_fechaDeCarga.setText(
                            Utiles.ConvertToString(planificacionDeCarga.fechaDeCarga,"dd/MM/yyyy"));
                    lbl_fechaDeGeneracion.setText(
                            Utiles.ConvertToString(planificacionDeCarga.fechaDeGeneracion,"dd/MM/yyyy") );

                    ArrayAdapterArticulosDeCarga adp = new ArrayAdapterArticulosDeCarga(
                            ActivityPlanificacionDeCarga.this,
                            planificacionDeCarga.articulos);
                    lsv_articulosDeCarga.setAdapter(adp);

                }
            });

        }catch (Exception ex){
            Utiles.Mensaje_En_Pantalla(ex.getMessage(),true,true,this);
        }


    }
}
