package waterservice.mobile.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.ArticuloDeLista;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.Dispenser;
import waterservice.mobile.clases.DispenserAsociado;
import waterservice.mobile.clases.MantenimientoDeDispenser;
import waterservice.mobile.clases.OrdenDeTrabajo;
import waterservice.mobile.clases.RepuestoActividad;
import waterservice.mobile.clases.ValorSatelite;
import waterservice.mobile.services.Service_Articulos;
import waterservice.mobile.services.Service_Dispensers;
import waterservice.mobile.services.Service_ServicioTecnico;
import waterservice.mobile.utiles.AsyncWork;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class ActivityMantenimiento extends AppCompatActivity {

    private EditText txt_nroDispenser;
    private Button btn_tab_detalles;
    private Button btn_tab_principal;
    DispenserAsociado dispenser;

    private ViewStub vst_tabDetalles;
    private ViewStub vst_tabPrincipal;

    private List<ValorSatelite> sintomasReales;
    private List<ValorSatelite> accionesPrincipales;
    private List<RepuestoActividad> repuestosActividades;

    private Cliente clienteActual;

    private Service_ServicioTecnico _srvServicioTecnico;
    private OrdenDeTrabajo ordenDeTrabajo;

    private List<ArticuloDeLista> actividadesDeMantenimiento;
    private Service_Articulos _srvArticulos;
    private Service_Dispensers _srvDispensers;
    private AppSession appSession;

    TabContainer tConteiner;
    TabMantenimientoPrincipal tPrincipal;
    TabMantenimientoDetalles tDetalles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mantenimiento);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {

        appSession = new AppSession(this);
        repuestosActividades = (List<RepuestoActividad>) SessionVars.Get_Session_Var(SessionKeys.REPUESTOSACTIVIDADES);
        ordenDeTrabajo = (OrdenDeTrabajo) SessionVars.Get_Session_Var(SessionKeys.ORDEN_DE_TRABAJO_ACTUAL);
        clienteActual = appSession.getClienteActual();
        dispenser = (DispenserAsociado) SessionVars.Get_Session_Var(SessionKeys.DISPENSER_ASOCIADO);

        _srvArticulos = new Service_Articulos(this);
        actividadesDeMantenimiento = _srvArticulos.obtenerArticulosActividadesDeMantenimiento();

        if(repuestosActividades==null)
            repuestosActividades=new ArrayList<>();

        _srvServicioTecnico = new Service_ServicioTecnico(this);
        _srvDispensers = new Service_Dispensers(this);

        this.sintomasReales = _srvServicioTecnico.obtenerSintomasReales();
        this.accionesPrincipales = _srvServicioTecnico.obtenerAccionesSobreDispensers();

        btn_tab_detalles = (Button) this.findViewById(R.id.btn_tab_detalles);
        btn_tab_principal = (Button) this.findViewById(R.id.btn_tab_principal);
        txt_nroDispenser = (EditText) this.findViewById(R.id.txt_nroDispenser);

        if(dispenser!=null)
        {
            txt_nroDispenser.setText("Dispenser Nº: " + dispenser.nroDispenser);
            txt_nroDispenser.setFocusable(false);
        }

        vst_tabDetalles = (ViewStub) this.findViewById(R.id.vst_tabDetalles);
        vst_tabPrincipal = (ViewStub) this.findViewById(R.id.vst_tabPrincipal);

        tConteiner = new TabContainer();

        tPrincipal = new TabMantenimientoPrincipal(this, vst_tabPrincipal, btn_tab_principal,
                tConteiner, sintomasReales, accionesPrincipales);
        tDetalles = new TabMantenimientoDetalles(this, vst_tabDetalles, btn_tab_detalles, tConteiner, repuestosActividades);

        tConteiner.onButtonClick(tPrincipal);
    }

    private void onAccionPrincipalChanged(ValorSatelite itemSeleccionado) {

        if(itemSeleccionado.valor_id == Constantes.ACCIONES_PRINC_DIS_INSTALACION){
            agregarActividadSiNoEstaAgregada(_srvServicioTecnico.ARTICULO_ID_INSTALACION);
        }else if(itemSeleccionado.valor_id == Constantes.ACCIONES_PRINC_DIS_DESINSTALACION) {
            agregarActividadSiNoEstaAgregada(_srvServicioTecnico.ARTICULO_ID_DESINSTALACION);
        }else if(itemSeleccionado.valor_id == Constantes.ACCIONES_PRINC_DIS_REUBICACION) {
            agregarActividadSiNoEstaAgregada(_srvServicioTecnico.ARTICULO_ID_REUBICACION);
        }else if(itemSeleccionado.valor_id == Constantes.ACCIONES_PRINC_DIS_SANITIZACION) {
            agregarActividadSiNoEstaAgregada(_srvServicioTecnico.ARTICULO_ID_SANITIZACION);
        }
    }

    private void agregarActividadSiNoEstaAgregada(int articulo_id){

        if(!itemYaAgregado(articulo_id)){

            ArticuloDeLista a = obtenerArticuloSeleccionado(articulo_id);
            if(a==null) return;

            RepuestoActividad item = new RepuestoActividad();
            item.cantidad = 1;
            item.articulo_id = articulo_id;
            item.tipo_id = a.tipoArticulo_ids;
            item.nombreDelItem = a.nombreArticulo;
            repuestosActividades.add(item);
        }
    }

    private ArticuloDeLista obtenerArticuloSeleccionado(int articulo_id){
        for (ArticuloDeLista a: actividadesDeMantenimiento) {
            if(a.id == articulo_id) return a;
        }
        return null;
    }

    private boolean itemYaAgregado(int articulo_id){

        for (RepuestoActividad ra: repuestosActividades) {
            if(ra.articulo_id == articulo_id) return true;
        }
        return false;
    }

    public void btn_agregarActividadOnClick(View view){
        abrirAgregarRepuestoActividad(false);
    }

    public void btn_agregarRepuestoOnClick(View view){
        abrirAgregarRepuestoActividad(true);
    }

    private void abrirAgregarRepuestoActividad(boolean esRepuesto){

        SessionVars.Add_Session_Var(SessionKeys.ES_REPUESTO, esRepuesto);
        SessionVars.Add_Session_Var(SessionKeys.REPUESTOSACTIVIDADES, repuestosActividades);

        Intent intent=new Intent(this, ActivityAgregarRepuestoActividad.class);
        this.startActivityForResult(intent, Constantes.ACTIVITY_AGREGAR_REPUESTOACTIVIDAD);
    }

    public void btn_confirmarMantenimientoOnClick(View view){

        try{

            validar();

            Utiles.MostrarConfirmacion("Guardar mantenimiento",
                    "Confirma guardar este mantenimiento?",
                    "confirmarGuardarMantenimiento",this,this);

        }catch (Exception ex){
            Utiles.Mensaje_En_Pantalla(ex.getMessage(), true, false, this);
        }
    }

    public void confirmarGuardarMantenimiento(){

        AsyncWork.run("runGuardarMantenimiento",this, "Guardando datos...");
    }

    public void runGuardarMantenimiento() throws Exception{

        onAccionPrincipalChanged(tPrincipal.obtenerAccionPrincipalSeleccionada());

        MantenimientoDeDispenser mantenimiento = new MantenimientoDeDispenser();
        mantenimiento.comentarios = tPrincipal.txt_comentarios.getText().toString();
        mantenimiento.nroDispenser = dispenser!=null ?
                dispenser.nroDispenser :
                txt_nroDispenser.getText().toString();

        mantenimiento.danosAtribuidosAlCliente = tPrincipal.ckb_dañosAtribuidosAlCliente.isChecked();

        mantenimiento.accionPrincipal_ids = tPrincipal.obtenerAccionPrincipalSeleccionada().valor_id;
        mantenimiento.sintomaReal = mantenimiento.accionPrincipal_ids == Constantes.ACCIONES_PRINC_DIS_REPARACION?
                tPrincipal.obtenerSintomaSeleccionado().valor_id:0;

        _srvServicioTecnico.guardarMantenimiento(mantenimiento, dispenser,
                ordenDeTrabajo, tDetalles.repuestosActividades);

        this.setResult(RESULT_OK);
        this.finish();
    }

    private void validar() throws Exception {

        if(dispenser==null)
        {
            if(txt_nroDispenser.getText().toString().equals(""))
                throw new Exception("Debe ingresar un número de dispenser");

            try{
                long nro = Long.valueOf(txt_nroDispenser.getText().toString());
            }catch (Exception ex){
                throw new Exception("El número de dispenser no es válido");
            }

            Dispenser d = _srvDispensers.obtenerDispenserPorNro(txt_nroDispenser.getText().toString(), null);

            if(d==null)throw new Exception("El nro de dispenser ingresado no corresponde con "+
                    "ningún dispenser cargado en playa o perteneciente al cliente");

            if(d!=null &&  d.cliente_id != 0 && d.cliente_id != clienteActual.cliente_id)
                throw new Exception("El dispenser ingresado pertenece a otro cliente");
        }

        if(tPrincipal.txt_comentarios.getText().toString().equals(""))
            throw new Exception("Debe ingresar comentarios");

        if(tPrincipal.obtenerAccionPrincipalSeleccionada().valor_id == Constantes.ACCIONES_PRINC_DIS_REPARACION
                && (tDetalles.repuestosActividades==null || tDetalles.repuestosActividades.size()==0) )
            throw new Exception("Debe registrar al menos una actividad o repuesto");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch(requestCode) {
            case Constantes.ACTIVITY_AGREGAR_REPUESTOACTIVIDAD:
                try {
                    tConteiner.onButtonClick(tDetalles);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }
}

class TabMantenimientoPrincipal extends TabViewStub{

    public Spinner spn_sintomaReal;
    public Spinner spn_accionPrincipal;
    public LinearLayout pnl_sintomaReal;
    public EditText txt_comentarios;
    public CheckBox ckb_dañosAtribuidosAlCliente;

    public List<ValorSatelite> sintomasReales;
    public List<ValorSatelite> accionesPrincipales;

    private boolean camposPoblados = false;

    public TabMantenimientoPrincipal(Activity _activity, ViewStub _tab, Button _btn, TabContainer _tabContainer,
                                     List<ValorSatelite> _sintomasReales,
                                     List<ValorSatelite> _accionesPrincipales) {
        super(_activity, _tab, _btn, _tabContainer);
        sintomasReales = _sintomasReales;
        accionesPrincipales = _accionesPrincipales;
    }

    public void onTabSelected() {
        poblarCampos();
    }

    public void poblarCampos(){

        if(camposPoblados)return;

        txt_comentarios = (EditText) activity.findViewById(R.id.txt_comentarios);
        spn_sintomaReal = (Spinner) activity.findViewById(R.id.spn_sintomaReal);
        spn_accionPrincipal = (Spinner) activity.findViewById(R.id.spn_accionPrincipal);
        pnl_sintomaReal = (LinearLayout) activity.findViewById(R.id.pnl_sintomaReal);
        ckb_dañosAtribuidosAlCliente = (CheckBox) activity.findViewById(R.id.ckb_dañosAtribuidosAlCliente);

        if(sintomasReales!=null)
        {
            Utiles.SetItemsToSpinner(this.spn_sintomaReal,sintomasReales,"valor_texto",activity);

            spn_accionPrincipal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    ValorSatelite itemSeleccionado = accionesPrincipales.get(position);

                    if(itemSeleccionado.valor_id == Constantes.ACCIONES_PRINC_DIS_REPARACION){
                        pnl_sintomaReal.setVisibility(View.VISIBLE);

                    }else{
                        pnl_sintomaReal.setVisibility(View.GONE);

                    }

                    onAccionPrincipalChanged(itemSeleccionado);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        if(accionesPrincipales!=null)
        {
            Utiles.SetItemsToSpinner(this.spn_accionPrincipal,accionesPrincipales,"valor_texto",activity);
        }

        camposPoblados = true;
    }

    public ValorSatelite obtenerAccionPrincipalSeleccionada(){
        return accionesPrincipales.get(spn_accionPrincipal.getSelectedItemPosition());
    }

    public ValorSatelite obtenerSintomaSeleccionado(){
        return sintomasReales.get(spn_sintomaReal.getSelectedItemPosition());
    }

    public void onAccionPrincipalChanged(ValorSatelite itemSeleccionado){

    }
}

class TabMantenimientoDetalles extends TabViewStub{

    public ListView lst_actividadesRepuestos;
    public List<RepuestoActividad> repuestosActividades;
    public ArrayAdapterActividadRepuesto adapter;

    public TabMantenimientoDetalles(Activity _activity, ViewStub _tab, Button _btn, TabContainer _tabContainer,
                                    List<RepuestoActividad> _repuestosActividades) {
        super(_activity, _tab, _btn, _tabContainer);
        repuestosActividades = _repuestosActividades;
    }

    public void onTabSelected() {
        poblarCampos();
    }

    public void poblarCampos(){

        lst_actividadesRepuestos = (ListView) activity.findViewById(R.id.lst_actividadesRepuestos);
        adapter = new ArrayAdapterActividadRepuesto(activity,repuestosActividades);
        lst_actividadesRepuestos.setAdapter(adapter);
        lst_actividadesRepuestos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                TabMantenimientoDetalles.this.repuestosActividades.remove(position);
                TabMantenimientoDetalles.this.adapter.notifyDataSetChanged();

                return false;
            }
        });
    }
}