package waterservice.mobile.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.DeclaracionEfectivo;
import waterservice.mobile.services.Service_DeclaracionesEfectivo;
import waterservice.mobile.utiles.AsyncWork;
import waterservice.mobile.utiles.Utiles;

public class ActivityDeclaracionEfectivo extends Activity {

    private TextView declaracion_lbl_total;
    private TextView declaracion_lbl_billetes;
    private TextView declaracion_lbl_monedas;
    private Button declaracion_btn_billetes;
    private Button declaracion_btn_confirmar;
    private Button declaracion_btn_monedas;
    private Button declaracion_btn_total;
    private View declaracion_pnl_billetes;
    private View declaracion_pnl_monedas;

    private Billete billetes;
    private Billete monedas;

    private List<DeclaracionEfectivo> declaracionBilletes;
    private List<DeclaracionEfectivo> declaracionMonedas;

    Service_DeclaracionesEfectivo _srvDeclaraciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_declaracion_efectivo);

        try {
            init();
        } catch (Exception e) {

        }
    }

    private void init() throws Exception {

        _srvDeclaraciones=new Service_DeclaracionesEfectivo(this);

        declaracion_btn_billetes = (Button) this.findViewById(R.id.declaracion_btn_billetes);
        declaracion_btn_confirmar = (Button) this.findViewById(R.id.declaracion_btn_confirmar);
        declaracion_btn_monedas = (Button) this.findViewById(R.id.declaracion_btn_monedas);
        declaracion_btn_total = (Button) this.findViewById(R.id.declaracion_btn_total);
        declaracion_lbl_total = (TextView) this.findViewById(R.id.declaracion_lbl_total);
        declaracion_lbl_billetes = (TextView) this.findViewById(R.id.declaracion_lbl_billetes);
        declaracion_lbl_monedas = (TextView) this.findViewById(R.id.declaracion_lbl_monedas);
        declaracion_pnl_billetes =  this.findViewById(R.id.declaracion_pnl_billetes);
        declaracion_pnl_monedas =  this.findViewById(R.id.declaracion_pnl_monedas);

        declaracionBilletes = _srvDeclaraciones.obtenerDeclaracionesEfectivo(false);
        declaracionMonedas = _srvDeclaraciones.obtenerDeclaracionesEfectivo(true);

        billetes= new Billete(declaracionBilletes,"Billetes",declaracion_pnl_billetes, this);
        monedas = new Billete(declaracionMonedas,"Monedas",declaracion_pnl_monedas,this);

        declaracion_btn_billetesOnClick(null);
    }

    public void declaracion_btn_billetesOnClick(View view){

        declaracion_pnl_billetes.setVisibility(View.VISIBLE);
        declaracion_pnl_monedas.setVisibility(View.GONE);
        declaracion_btn_billetes.setEnabled(false);
        declaracion_btn_monedas.setEnabled(true);
    }

    public void declaracion_btn_monedasOnClick(View view){

        declaracion_pnl_billetes.setVisibility(View.GONE);
        declaracion_pnl_monedas.setVisibility(View.VISIBLE);
        declaracion_btn_billetes.setEnabled(true);
        declaracion_btn_monedas.setEnabled(false);

    }

    public void declaracion_btn_confirmarOnClick(View view){

        declaracion_btn_totalOnClick(null);

        AsyncWork.run("confirmarDeclaracion", this, "Guardando declaracion...");
    }

    public void confirmarDeclaracion(){

        for(DeclaracionEfectivo declaracion : declaracionBilletes){

            int cantidad=declaracion.cantidad == null ? 0 : declaracion.cantidad;
            _srvDeclaraciones.guardarCantidades(declaracion.id,cantidad);
        }

        for(DeclaracionEfectivo declaracion : declaracionMonedas){

            int cantidad=declaracion.cantidad == null ? 0 : declaracion.cantidad;
            _srvDeclaraciones.guardarCantidades(declaracion.id,cantidad);
        }

        Intent intent = new Intent(this, ActivityResumenDeclaracion.class);
        startActivityForResult(intent, Constantes.ACTIVITY_CONFIRMAR_DECLARACION);
    }

    public void declaracion_btn_totalOnClick(View view){

        double totalMonedas=monedas.obtenerTotal();
        double totalBilletes = billetes.obtenerTotal();
        double total = totalBilletes + totalMonedas;

        declaracion_lbl_billetes.setText("$"+Utiles.Round(totalBilletes));
        declaracion_lbl_monedas.setText("$"+Utiles.Round(totalMonedas));
        declaracion_lbl_total.setText("$" + Utiles.Round(total));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch(requestCode) {
            case Constantes.ACTIVITY_CONFIRMAR_DECLARACION:

                if (resultCode == RESULT_OK) {

                    setResult(RESULT_OK);
                    finish();
                }

                break;
        }

    }

}

class Billete{

    private TextView billetes_lbl_titulo;
    private ListView billetes_lst_billetes;
    private View panel;
    public List<DeclaracionEfectivo> billetes;
    private Context context;

    public Billete(List<DeclaracionEfectivo> _billetes, String _titulo, View _panel, Context _context){

        context = _context;
        billetes=_billetes;
        panel=_panel;

        init();

        billetes_lbl_titulo.setText(_titulo);
    }

    void init(){

        billetes_lbl_titulo = (TextView) panel.findViewById(R.id.billetes_lbl_titulo);
        billetes_lst_billetes = (ListView) panel.findViewById(R.id.billetes_lst_billetes);

        ArrayAdapterBillete ad=new ArrayAdapterBillete(context, billetes);
        billetes_lst_billetes.setAdapter(ad);
    }

    public double obtenerTotal(){

        double total=0;

        for (int i=0; i<billetes.size(); i++){

            double subtotal= (billetes.get(i).cantidad == null? 0: billetes.get(i).cantidad)
                            * billetes.get(i).valorNominal;
            total= total+subtotal;
        }

        return total;
    }
}