package waterservice.mobile.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.ArticuloEntregado;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.ItemDeRecibo;

public class ItemReciboAdapter extends RecyclerView.Adapter<ItemReciboAdapter.ItemReciboViewHolder>{

    private List<ItemDeRecibo> items;
    private Context context;
    private Acciones acciones;
    private boolean editable;

    public ItemReciboAdapter(List<ItemDeRecibo> items, Context context,
                             ItemReciboAdapter.Acciones acciones, boolean editable){

        this.items = items;
        this.context = context;
        this.acciones = acciones;
        this.editable = editable;
    }

    @NonNull
    @Override
    public ItemReciboViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_recibo, parent, false);

        ItemReciboViewHolder vh = new ItemReciboViewHolder(v, context, acciones, editable);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemReciboViewHolder holder, int position) {
        ItemDeRecibo art = items.get(position);
        holder.poblarRow(art, position);
    }

    @Override
    public int getItemCount() {
        return items == null? 0 : items.size();
    }

    public static class ItemReciboViewHolder extends RecyclerView.ViewHolder {

        ItemReciboAdapter.Acciones acciones;

        private Button btn_editar;
        private ImageView btn_eliminar;
        private ImageView img_formaDePago;
        private TextView lbl_descripcion;
        private TextView lbl_formaDePago;
        private TextView lbl_importe;

        private Context context;
        private boolean editable;
        public ItemDeRecibo itemDeRecibo;

        public int position;

        public ItemReciboViewHolder(View view, Context context,
                                    ItemReciboAdapter.Acciones acciones, boolean editable) {

            super(view);

            this.acciones = acciones;
            this.context = context;
            this.editable = editable;

            btn_editar = (Button) view.findViewById(R.id.btn_editar);
            btn_eliminar = (ImageView) view.findViewById(R.id.btn_eliminar);
            img_formaDePago = (ImageView) view.findViewById(R.id.img_formaDePago);
            lbl_descripcion = (TextView) view.findViewById(R.id.lbl_descripcion);
            lbl_formaDePago = (TextView) view.findViewById(R.id.lbl_formaDePago);
            lbl_importe = (TextView) view.findViewById(R.id.lbl_importe);

            btn_editar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEditar();
                }
            });
            btn_editar.setVisibility(editable? View.VISIBLE : View.GONE);

            btn_eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEliminar();
                }
            });
            btn_eliminar.setVisibility(editable? View.VISIBLE : View.GONE);
        }

        private void onEditar(){

            if(acciones !=null) acciones.onEditar(itemDeRecibo);
        }

        private void onEliminar(){
            if(acciones !=null) acciones.onEliminar(itemDeRecibo);
        }

        public void poblarRow(ItemDeRecibo item, int position){

            this.itemDeRecibo = item;
            this.position = position;

            lbl_descripcion.setText(item.descripcion);
            lbl_formaDePago.setText("> "+item.getFormaDePago());
            lbl_importe.setText("$"+String.valueOf(item.importe));

            if(itemDeRecibo.formaDePagoId== Constantes.FORMADEPAGO_EFECTIVO)
                img_formaDePago.setImageResource(R.drawable.efectivo);
            else if(itemDeRecibo.formaDePagoId== Constantes.FORMADEPAGO_RETENCIONES)
                img_formaDePago.setImageResource(R.drawable.retencion);
            else if(itemDeRecibo.formaDePagoId== Constantes.FORMADEPAGO_TARJETACREDITO)
                img_formaDePago.setImageResource(R.drawable.tarjetas);
            else if(itemDeRecibo.formaDePagoId== Constantes.FORMADEPAGO_TARJETADEBITO)
                img_formaDePago.setImageResource(R.drawable.tarjetas);
            else if(itemDeRecibo.formaDePagoId== Constantes.FORMADEPAGO_CHEQUE)
                img_formaDePago.setImageResource(R.drawable.cheque);


        }

    }

    public interface Acciones{
        void onEliminar(ItemDeRecibo item);
        void onEditar(ItemDeRecibo item);
    }
}
