package waterservice.mobile.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.EnvaseDisponible;

public class EnvaseDisponibleAdapter extends RecyclerView.Adapter<EnvaseDisponibleAdapter.Item>{

    private List<EnvaseDisponible> envasesDisponibles;
    private Context context;
    private boolean permiteEliminar;
    private boolean permiteSeleccionar;
    private EnvaseDisponibleAdapter.Events eventos;

    public EnvaseDisponibleAdapter(List<EnvaseDisponible> envasesDisponibles, Context context,
                                   boolean permiteEliminar, boolean permiteSeleccionar,
                                   EnvaseDisponibleAdapter.Events eventos){

        this.envasesDisponibles = envasesDisponibles;
        this.context = context;
        this.permiteEliminar = permiteEliminar;
        this.permiteSeleccionar = permiteSeleccionar;
        this.eventos = eventos;
    }

    @NonNull
    @Override
    public EnvaseDisponibleAdapter.Item onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_entrega_envase, parent, false);

        EnvaseDisponibleAdapter.Item vh = new EnvaseDisponibleAdapter.Item(v, context,
                permiteEliminar, permiteSeleccionar, eventos);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull EnvaseDisponibleAdapter.Item holder, int position) {
        EnvaseDisponible env = envasesDisponibles.get(position);
        holder.poblarRow(env, position);
    }

    @Override
    public int getItemCount() {
        return envasesDisponibles == null? 0 : envasesDisponibles.size();
    }

    public static class Item extends RecyclerView.ViewHolder {

        private TextView lblCodigoBarril;
        private TextView lblDescripcionBarril;
        private Switch swt_seleccionado;
        private ImageView img_seleccionado;
        private ImageView btn_eliminar;
        private Context context;

        public EnvaseDisponible envase;
        private boolean permiteEliminar;
        private boolean permiteSeleccionar;
        private EnvaseDisponibleAdapter.Events eventos;

        public int position;

        public Item(View view, Context context, boolean permiteEliminar, boolean permiteSeleccionar,
                    EnvaseDisponibleAdapter.Events eventos) {

            super(view);

            this.context = context;
            this.permiteEliminar = permiteEliminar;
            this.permiteSeleccionar = permiteSeleccionar;
            this.eventos = eventos;

            lblCodigoBarril = (TextView) itemView.findViewById(R.id.lbl_contenido);
            lblDescripcionBarril = (TextView) itemView.findViewById(R.id.lblDescripcionBarril);
            swt_seleccionado = (Switch) itemView.findViewById(R.id.swt_seleccionado);
            img_seleccionado = (ImageView) itemView.findViewById(R.id.img_seleccionado);
            btn_eliminar = (ImageView) itemView.findViewById(R.id.btn_eliminar);

            btn_eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onEliminar();
                }
            });

            if(!permiteEliminar){
                btn_eliminar.setVisibility(View.INVISIBLE);
            }

            if(!permiteSeleccionar){
                swt_seleccionado.setVisibility(View.INVISIBLE);
            }
        }

        public void onSeleccionadoChanged(){

            this.envase.seleccionado = swt_seleccionado.isChecked();
            this.img_seleccionado.setVisibility(this.envase.seleccionado? View.VISIBLE : View.INVISIBLE);
            eventos.onItemSeleccionado(this.envase, this.position, this.envase.seleccionado);
        }

        public void onEliminar(){
            eventos.onItemEliminado(this.envase, this.position);
        }

        public void poblarRow(EnvaseDisponible envase, int position){

            this.envase = envase;
            this.position = position;

            lblCodigoBarril.setText(envase.codigo);

            if(envase.enCliente!=null && envase.enCliente==1)
                lblDescripcionBarril.setText("Vacío - Para devolver");
            else
                lblDescripcionBarril.setText(envase.articuloContenidoNombre + " X " + envase.cantidadDeCarga + " " + envase.unidadDeCarga);

            this.swt_seleccionado.setChecked(this.envase.seleccionado);

            this.img_seleccionado.setVisibility(this.envase.seleccionado && this.permiteSeleccionar?
                    View.VISIBLE : View.INVISIBLE);


            swt_seleccionado.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    onSeleccionadoChanged();
                }
            });
        }
    }

    public interface Events {
        public void onItemSeleccionado(EnvaseDisponible envase, int position, boolean esSeleccionado);
        public void onItemEliminado(EnvaseDisponible envase, int position);
    }
}
