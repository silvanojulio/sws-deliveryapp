package waterservice.mobile.adapters;

import java.util.ArrayList;
import java.util.List;

import waterservice.mobile.clases.ArticuloDeAbono;
import waterservice.mobile.clases.ArticuloDeLista;
import waterservice.mobile.clases.ListaDePrecios;
import waterservice.mobile.clases.PrecioEspecial;
import waterservice.mobile.clases.StockDeCliente;

public class ArticuloParaSeleccionar{

    public ArticuloParaSeleccionar(){

    }

    public ArticuloParaSeleccionar(int articuloId, String nombreArticulo, String codigoInterno, Double precio, int cantidadDisponiblePorAbono,
                                   boolean esDestacado){

        this.articuloId = articuloId;
        this.nombreArticulo = nombreArticulo;
        this.codigoInterno = codigoInterno;
        this.precio = precio;
        this.cantidadDisponiblePorAbono = cantidadDisponiblePorAbono;
        this.esDestacado = esDestacado;
    }

    public String nombreArticulo;
    public int articuloId;
    public String codigoInterno;
    public Double precio;
    public int cantidadDisponiblePorAbono;
    public boolean esDestacado;
    public boolean esRetornable;

    public static List<ArticuloParaSeleccionar> obtenerArticulos(List<ArticuloDeLista> articulosDeLista,
                                                                 List<ListaDePrecios> listasDePrecios, List<PrecioEspecial> preciosEspeciales,
                                                                 List<ArticuloDeAbono> abonos, List<StockDeCliente> stockDeCliente){

        List<ArticuloParaSeleccionar> articulos = new ArrayList<>();


        //Obtener los artículos de lista que estan en el abono
        for (ArticuloDeAbono abono: abonos) {
            ArticuloDeLista artAbono = obtenerArticuloDeLista(abono.articulo_id, articulosDeLista);
            if(artAbono!=null){
                ArticuloParaSeleccionar a = new ArticuloParaSeleccionar();
                a.esDestacado = true;
                a.articuloId = artAbono.id;
                a.nombreArticulo = artAbono.nombreArticulo;
                a.codigoInterno = artAbono.codigoInterno;
                a.cantidadDisponiblePorAbono = abono.cantidad;
                a.esRetornable = artAbono.esRetornable();
                articulos.add(a);
            }
        }

        //Obtener los artículos de lista que estan en el precios especiales
        for (PrecioEspecial precioEspecial: preciosEspeciales) {
            ArticuloDeLista artPrecioEspcial = obtenerArticuloDeLista(precioEspecial.articuloId, articulosDeLista);
            if(artPrecioEspcial!=null && !existeEnLista(precioEspecial.articuloId, articulos)){
                ArticuloParaSeleccionar a = new ArticuloParaSeleccionar();
                a.esDestacado = true;
                a.articuloId = artPrecioEspcial.id;
                a.nombreArticulo = artPrecioEspcial.nombreArticulo;
                a.codigoInterno = artPrecioEspcial.codigoInterno;
                a.esRetornable = artPrecioEspcial.esRetornable();
                a.cantidadDisponiblePorAbono = 0;
                articulos.add(a);
            }
        }

        //Obtener los artículos de lista que estan en stock del cliente
        for (StockDeCliente stock: stockDeCliente) {
            ArticuloDeLista artStock = obtenerArticuloDeLista(stock.articuloId, articulosDeLista);
            if(artStock!=null && !existeEnLista(artStock.id, articulos)){
                ArticuloParaSeleccionar a = new ArticuloParaSeleccionar();
                a.esDestacado = true;
                a.articuloId = artStock.id;
                a.nombreArticulo = artStock.nombreArticulo;
                a.codigoInterno = artStock.codigoInterno;
                a.cantidadDisponiblePorAbono = 0;
                a.esRetornable = artStock.esRetornable();
                articulos.add(a);
            }
        }

        //Obtener el resto de los artículos
        for (ArticuloDeLista art: articulosDeLista) {
            if(!existeEnLista(art.id, articulos)){
                ArticuloParaSeleccionar a = new ArticuloParaSeleccionar();
                a.esDestacado = false;
                a.articuloId = art.id;
                a.nombreArticulo = art.nombreArticulo;
                a.codigoInterno = art.codigoInterno;
                a.cantidadDisponiblePorAbono = 0;
                a.esRetornable = art.esRetornable();
                articulos.add(a);
            }
        }

        List<Integer> idsArticulos = obtenerIdsDeArticulos(articulos);

        List<PrecioDeArticulo> precios = obtenerPreciosDeArticulo(idsArticulos, articulosDeLista,
                listasDePrecios, preciosEspeciales);

        for (ArticuloParaSeleccionar articulo:articulos) {
            for (PrecioDeArticulo precio:precios) {
                if(precio.articuloId == articulo.articuloId)
                    articulo.precio = precio.precio;
            }
        }

        return articulos;
    }

    public static List<Integer> obtenerIdsDeArticulos(List<ArticuloParaSeleccionar> articulos){

        List<Integer> ids = new ArrayList<>();

        for (ArticuloParaSeleccionar articulo:articulos) {
            ids.add(articulo.articuloId);
        }
        return ids;
    }

    public static boolean existeEnLista(int articuloId, List<ArticuloParaSeleccionar> articulos){
        for (ArticuloParaSeleccionar articulo:articulos) {
            if(articulo.articuloId == articuloId) return true;
        }
        return false;
    }

    private static ArticuloDeLista obtenerArticuloDeLista(int articuloId, List<ArticuloDeLista> articulosDeLista){

        for (ArticuloDeLista art: articulosDeLista) {
            if(art.id == articuloId) return art;
        }

        return null;
    }

    private static PrecioEspecial obtenerPrecioEspecial(int articuloId, List<PrecioEspecial> preciosEspeciales){

        for (PrecioEspecial precioEspcial: preciosEspeciales) {
            if(precioEspcial.articuloId == articuloId) return precioEspcial;
        }

        return null;
    }

    private static ListaDePrecios obtenerArticuloDeListaDePrecio(int articuloId, List<ListaDePrecios> listasDePrecios){

        for (ListaDePrecios art: listasDePrecios) {
            if(art.articuloId == articuloId) return art;
        }

        return null;
    }

    public static List<PrecioDeArticulo> obtenerPreciosDeArticulo(List<Integer> idsArticulos, List<ArticuloDeLista> articuloDeListas,
                                          List<ListaDePrecios> listasDePrecios, List<PrecioEspecial> preciosEspeciales ){

        List<PrecioDeArticulo> precios = new ArrayList<>();

        for (Integer articuloId : idsArticulos) {

            PrecioEspecial precioEspecial = obtenerPrecioEspecial(articuloId, preciosEspeciales);
            if(precioEspecial != null){

                PrecioDeArticulo p = new PrecioDeArticulo();
                p.articuloId = articuloId;
                p.precio = precioEspecial.precio;
                precios.add(p);
                continue;
            }

            ListaDePrecios artListaDePrecio = obtenerArticuloDeListaDePrecio(articuloId, listasDePrecios);
            if(artListaDePrecio != null){

                PrecioDeArticulo p = new PrecioDeArticulo();
                p.articuloId = articuloId;
                p.precio = artListaDePrecio.precio;
                precios.add(p);
                continue;
            }

            ArticuloDeLista articuloDeLista = obtenerArticuloDeLista(articuloId, articuloDeListas);
            if(articuloDeLista != null){

                PrecioDeArticulo p = new PrecioDeArticulo();
                p.articuloId = articuloId;
                p.precio = articuloDeLista.precio;
                precios.add(p);
                continue;
            }

            PrecioDeArticulo p = new PrecioDeArticulo();
            p.articuloId = articuloId;
            p.precio = 0;
            precios.add(p);
        }

        return precios;
    }

    public static class PrecioDeArticulo{
        public int articuloId;
        public double precio;
    }

}