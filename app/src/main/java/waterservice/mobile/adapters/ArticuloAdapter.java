package waterservice.mobile.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.ArticuloDeAbono;
import waterservice.mobile.clases.ArticuloDeLista;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.ListaDePrecios;
import waterservice.mobile.clases.PrecioEspecial;
import waterservice.mobile.clases.StockDeCliente;

public class ArticuloAdapter extends RecyclerView.Adapter<ArticuloAdapter.ArticuloViewHolder>{

    private List<ArticuloParaSeleccionar> articulos;
    private Context context;
    private  ArticuloViewHolder.Acciones acciones;

    public ArticuloAdapter(List<ArticuloParaSeleccionar> articulos,
                           Context context, ArticuloViewHolder.Acciones acciones){

        this.articulos = articulos;
        this.context = context;
        this.acciones = acciones;
    }

    @NonNull
    @Override
    public ArticuloViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_articulo, parent, false);

        ArticuloViewHolder vh = new ArticuloViewHolder(v, context);

        vh.acciones = acciones;

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ArticuloViewHolder holder, int position) {
        ArticuloParaSeleccionar art = articulos.get(position);
        holder.poblarRow(art, position);
    }

    @Override
    public int getItemCount() {
        return articulos == null? 0 : articulos.size();
    }

    public static class ArticuloViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView lbl_disponibles;
        private TextView lbl_codigoArticulo;
        private TextView lbl_nombreArticulo;
        private TextView lbl_precioUnitario;
        private Button btn_seleccionar;
        private ImageView img_favorito;
        private ImageView img_retornable;

        Context context;
        public ArticuloViewHolder.Acciones acciones;

        public ArticuloParaSeleccionar articulo;

        public int position;

        public ArticuloViewHolder(View view, Context context) {

            super(view);

            this.context = context;

            lbl_codigoArticulo = (TextView) itemView.findViewById(R.id.lbl_codigoArticulo);
            lbl_nombreArticulo = (TextView) itemView.findViewById(R.id.lbl_nombreArticulo);
            lbl_precioUnitario = (TextView) itemView.findViewById(R.id.lbl_precioUnitario);
            lbl_disponibles = (TextView) itemView.findViewById(R.id.lbl_disponibles);
            img_favorito = (ImageView) itemView.findViewById(R.id.img_favorito);
            img_retornable = (ImageView) itemView.findViewById(R.id.img_retornable);

            btn_seleccionar = (Button) itemView.findViewById(R.id.btn_seleccionar);
            btn_seleccionar.setOnClickListener(this);
        }

        public void poblarRow(ArticuloParaSeleccionar articulo, int position){

            this.articulo = articulo;
            this.position = position;

            lbl_codigoArticulo.setText(articulo.codigoInterno);
            lbl_nombreArticulo.setText(articulo.nombreArticulo);
            lbl_precioUnitario.setText("$" + String.valueOf(articulo.precio));
            lbl_disponibles.setText(String.valueOf(articulo.cantidadDisponiblePorAbono));

            img_favorito.setVisibility(articulo.esDestacado?View.VISIBLE : View.GONE);
            img_retornable.setVisibility(articulo.esRetornable?View.VISIBLE : View.GONE);
        }

        @Override
        public void onClick(View v) {

            if(v.getId()==R.id.btn_seleccionar)
                btn_seleccionarOnClick();
        }

        private void btn_seleccionarOnClick() {

            if(acciones!=null)
                acciones.onSeleccionar(articulo);
        }

        public interface Acciones{

            public void onSeleccionar(ArticuloParaSeleccionar articulo);
        }
    }

}


