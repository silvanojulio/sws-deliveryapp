package waterservice.mobile.adapters;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.ArticuloEntregado;
import waterservice.mobile.clases.DescuentoPorCantidad;
import waterservice.mobile.clases.ValorSatelite;
import waterservice.mobile.utiles.Utiles;
import waterservice.mobile.views.UICantidad;

public class ArticuloVendidoAdapter extends RecyclerView.Adapter<ArticuloVendidoAdapter.ArticuloVendidoViewHolder>{

    private List<ArticuloEntregado> articulosVendidos;
    private Context context;
    private  ArticuloVendidoViewHolder.Acciones acciones;
    private List<ValorSatelite> motivosPrestamosDevolucion;
    private List<DescuentoPorCantidad> descuentosPorCantidad;

    public ArticuloVendidoAdapter(List<ArticuloEntregado> articulosVendidos, Context context,
                                  List<DescuentoPorCantidad> descuentosPorCantidad,
                                  ArticuloVendidoViewHolder.Acciones acciones,
                                  List<ValorSatelite> motivosPrestamosDevolucion){

        this.descuentosPorCantidad = descuentosPorCantidad;
        this.articulosVendidos = articulosVendidos;
        this.context = context;
        this.acciones = acciones;
        this.motivosPrestamosDevolucion = motivosPrestamosDevolucion;
    }

    @NonNull
    @Override
    public ArticuloVendidoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

       try {

           View v = LayoutInflater.from(parent.getContext())
                   .inflate(R.layout.row_articulo_vendido, parent, false);

           ArticuloVendidoViewHolder vh = new ArticuloVendidoViewHolder(v, context, motivosPrestamosDevolucion);

           vh.acciones = acciones;

           return vh;
       }catch (Exception e){
           e.printStackTrace();
           throw e;
       }
    }

    @Override
    public void onBindViewHolder(@NonNull ArticuloVendidoViewHolder holder, int position) {

        ArticuloEntregado articuloVendido = articulosVendidos.get(position);

        List<DescuentoPorCantidad> descuentosArticulo = new ArrayList<>();

        if(descuentosPorCantidad!=null)
            for (DescuentoPorCantidad d: descuentosPorCantidad){
                if(d.articuloId == articuloVendido.articulo_id)descuentosArticulo.add(d);
            }

        holder.poblarRow(articuloVendido, position, descuentosArticulo);
    }

    @Override
    public int getItemCount() {
        return articulosVendidos == null? 0 : articulosVendidos.size();
    }

    public static class ArticuloVendidoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;
        List<ValorSatelite> motivosPrestamosDevolucion;
        List<DescuentoPorCantidad> descuentosPorCantidad;
        public ArticuloVendidoViewHolder.Acciones acciones;

        private UICantidad ctr_cantidadDevuelta;
        private UICantidad ctr_cantidadEntregada;
        private UICantidad ctr_cantidadPrestada;
        private UICantidad ctr_cantidadRelevada;

        private TextView lbl_articulo;
        private TextView lbl_cantidadARecuperar;
        private TextView lbl_envasesPermitidos;
        private TextView lbl_envasesStock;
        private TextView lbl_precioUnitario;
        private TextView lbl_subtotal;
        private TextView lbl_descuentoPorCantidad;
        private TextView lbl_labelMotivoRelevamiento;
        private ImageView img_retornable;
        private EditText txt_descuento;
        private Button btn_verMasMenos;
        private View pnl_mas;

        private ImageView btn_quitarArticulo;

        private Spinner spn_motivo;

        public ArticuloEntregado articuloVendido;

        public int position;

        public ArticuloVendidoViewHolder(View view, Context context,
                                         List<ValorSatelite> _motivosPrestamosDevolucion) {

            super(view);

            this.context = context;
            this.motivosPrestamosDevolucion = _motivosPrestamosDevolucion;

            lbl_labelMotivoRelevamiento = (TextView) view.findViewById(R.id.lbl_labelMotivoRelevamiento);
            lbl_articulo = (TextView) view.findViewById(R.id.lbl_articulo);
            lbl_cantidadARecuperar = (TextView) view.findViewById(R.id.lbl_cantidadARecuperar);
            lbl_envasesPermitidos = (TextView) view.findViewById(R.id.lbl_envasesPermitidos);
            lbl_envasesStock = (TextView) view.findViewById(R.id.lbl_envasesStock);
            lbl_precioUnitario = (TextView) view.findViewById(R.id.lbl_precioUnitario);
            lbl_subtotal = (TextView) view.findViewById(R.id.lbl_subtotal);
            lbl_descuentoPorCantidad = (TextView) view.findViewById(R.id.lbl_descuentoPorCantidad);
            lbl_descuentoPorCantidad.setText("");
            img_retornable = (ImageView) itemView.findViewById(R.id.img_retornable);

            btn_verMasMenos = (Button) itemView.findViewById(R.id.btn_verMasMenos);
            btn_verMasMenos.setOnClickListener(this);
            pnl_mas = itemView.findViewById(R.id.pnl_mas);

            txt_descuento = (EditText) view.findViewById(R.id.txt_descuento);
            txt_descuento.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {}

                @Override
                public void beforeTextChanged(CharSequence s, int start,int count, int after) {}

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    try{
                        articuloVendido.descuentoManual = Double.valueOf(txt_descuento.getText().toString());
                    }catch (Exception ex){
                        articuloVendido.descuentoManual = 0.0;
                    }
                    calcularSubtotal();
                }
            });

            btn_quitarArticulo = (ImageView) view.findViewById(R.id.btn_quitarArticulo);
            btn_quitarArticulo.setOnClickListener(this);

            spn_motivo = (Spinner) view.findViewById(R.id.spn_motivo);

            if(motivosPrestamosDevolucion!=null){

                Utiles.SetItemsToSpinner(spn_motivo, motivosPrestamosDevolucion,
                        "valor_texto", context );
            }

            spn_motivo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    articuloVendido.motivoPrestamoDevolucionId = motivosPrestamosDevolucion.get(position).valor_id;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            FragmentManager fManager = ((Activity)context).getFragmentManager();

            ctr_cantidadDevuelta = (UICantidad) view.findViewById(R.id.ctr_cantidadDevuelta);
            ctr_cantidadDevuelta.onCantidadChange = new UICantidad.OnCantidadChange() {
                @Override
                public void onChange(double nuevoValor) {
                    articuloVendido.cantidadEnvasesDevueltos = (int) nuevoValor;
                }
            };

            ctr_cantidadPrestada = (UICantidad) view.findViewById(R.id.ctr_cantidadPrestada);
            ctr_cantidadPrestada.onCantidadChange = new UICantidad.OnCantidadChange() {
                @Override
                public void onChange(double nuevoValor) {
                    articuloVendido.cantidadEnvasesPrestados = (int) nuevoValor;
                }
            };

            ctr_cantidadEntregada = (UICantidad) view.findViewById(R.id.ctr_cantidadEntregada);
            ctr_cantidadEntregada.onCantidadChange = new UICantidad.OnCantidadChange() {
                @Override
                public void onChange(double nuevoValor) {
                    articuloVendido.cantidadEntregada = nuevoValor;
                    calcularDescuentoPorCantidad();
                    calcularSubtotal();
                }
            };

            ctr_cantidadRelevada = (UICantidad) view.findViewById(R.id.ctr_cantidadRelevada);
            ctr_cantidadRelevada.onCantidadChange = new UICantidad.OnCantidadChange() {
                @Override
                public void onChange(double nuevoValor) {

                    articuloVendido.cantidadEnvasesRelevados = (int) nuevoValor;

                    spn_motivo.setVisibility(nuevoValor>0? View.VISIBLE : View.INVISIBLE);
                    lbl_labelMotivoRelevamiento.setVisibility(nuevoValor>0? View.VISIBLE : View.INVISIBLE);
                }
            };
        }

        public void calcularDescuentoPorCantidad(){

            DescuentoPorCantidad descuento = null;

            for (DescuentoPorCantidad d: descuentosPorCantidad) {
                if(articuloVendido.obtenerCantidadACobrar()  >= d.cantidad &&
                        (descuento==null || d.descuento > descuento.descuento))
                    descuento = d;
            }

            articuloVendido.descuentoPorCantidad = descuento!=null? descuento.descuento : 0;

            lbl_descuentoPorCantidad.setText(articuloVendido.descuentoPorCantidad>0 ?
                    "-"+String.valueOf(articuloVendido.descuentoPorCantidad)+"%" :"");
        }

        public void calcularSubtotal(){

            try{
                articuloVendido.subtotal = articuloVendido.precioUnitario * articuloVendido.obtenerCantidadACobrar();

            }catch (Exception ex){
                articuloVendido.subtotal = 0.0;
            }

            articuloVendido.subtotal = Utiles.aplicarDescuentos(articuloVendido.subtotal,
                    articuloVendido.descuentoManual, articuloVendido.descuentoPorCantidad, null);

            if(lbl_subtotal!=null)
                lbl_subtotal.setText("$"+String.valueOf(articuloVendido.subtotal));

            if(acciones!=null)
                acciones.onSubtotalChange();
        }

        public void poblarRow(ArticuloEntregado articuloVendido, int position,
                              List<DescuentoPorCantidad> descuentosPorCantidad){

            this.articuloVendido = articuloVendido;
            this.position = position;
            this.descuentosPorCantidad = descuentosPorCantidad;

            lbl_articulo.setText(articuloVendido.codigoArticulo + " - " +articuloVendido.nombreDelArticulo);
            lbl_cantidadARecuperar.setText(String.valueOf(articuloVendido.envasesARecuperar));
            lbl_envasesStock.setText(String.valueOf(articuloVendido.envasesEnCliente));
            lbl_envasesPermitidos.setText(String.valueOf(articuloVendido.envasesPermitidos));

            lbl_precioUnitario.setText("$"+ String.valueOf(articuloVendido.precioUnitario));

            ctr_cantidadDevuelta.asignarCantidad(articuloVendido.cantidadEnvasesDevueltos);
            ctr_cantidadEntregada.asignarCantidad(articuloVendido.cantidadEntregada);
            ctr_cantidadPrestada.asignarCantidad(articuloVendido.cantidadEnvasesPrestados);
            ctr_cantidadRelevada.asignarCantidad(articuloVendido.cantidadEnvasesRelevados);

            spn_motivo.setVisibility(articuloVendido.cantidadEnvasesRelevados>0? View.VISIBLE : View.INVISIBLE);
            lbl_labelMotivoRelevamiento.setVisibility(articuloVendido.cantidadEnvasesRelevados>0? View.VISIBLE : View.INVISIBLE);

            if(this.motivosPrestamosDevolucion!=null)
            {
                for (int i = 0; i<this.motivosPrestamosDevolucion.size(); i++){
                    if(articuloVendido.motivoPrestamoDevolucionId == motivosPrestamosDevolucion.get(i).valor_id){
                        spn_motivo.setSelection(i);
                        break;
                    }
                }
            }

            img_retornable.setVisibility(articuloVendido.esRetornable?View.VISIBLE : View.GONE);

            articuloVendido.orden = position + 1;

            pnl_mas.setVisibility(articuloVendido.verMas? View.VISIBLE : View.GONE);
            btn_verMasMenos.setText(articuloVendido.verMas?"Ver menos":"Ver más");
        }

        @Override
        public void onClick(View v) {

            if(v.getId()==R.id.btn_quitarArticulo)
                btn_quitarArticuloOnClick();
            else if(v.getId()==R.id.btn_verMasMenos)
                btn_verMasMenosOnClick();
        }

        private void btn_quitarArticuloOnClick() {

            if(acciones!=null)
                acciones.quitarArticuloVendido(articuloVendido.articulo_id);
        }

        private void btn_verMasMenosOnClick(){

            articuloVendido.verMas = !articuloVendido.verMas;

            pnl_mas.setVisibility(articuloVendido.verMas? View.VISIBLE : View.GONE);
            btn_verMasMenos.setText(articuloVendido.verMas?"Ver menos":"Ver más");
        }

        public interface Acciones{

            public void quitarArticuloVendido(int articuloId);
            public void onSubtotalChange();
        }
    }

}
