package waterservice.mobile.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.ArticuloDeLista;
import waterservice.mobile.clases.ArticuloEntregado;

public class ItemRemitoAdapter extends RecyclerView.Adapter<ItemRemitoAdapter.ItemRemitoViewHolder>{

    private List<ArticuloEntregado> articulosVendidos;
    private Context context;

    public ItemRemitoAdapter(List<ArticuloEntregado> articulosVendidos, Context context){

        this.articulosVendidos = articulosVendidos;
        this.context = context;
    }

    @NonNull
    @Override
    public ItemRemitoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_articulo_remito, parent, false);

        ItemRemitoViewHolder vh = new ItemRemitoViewHolder(v, context);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemRemitoViewHolder holder, int position) {
        ArticuloEntregado art = articulosVendidos.get(position);
        holder.poblarRow(art, position);
    }

    @Override
    public int getItemCount() {
        return articulosVendidos == null? 0 : articulosVendidos.size();
    }

    public static class ItemRemitoViewHolder extends RecyclerView.ViewHolder {

        private TextView lbl_cantidadDevuelta;
        private TextView lbl_cantidadEntregada;
        private TextView lbl_cantidadPrestada;
        private TextView lbl_cantidadRelevada;
        private TextView lbl_nombreArticulo;
        private TextView lbl_precioUnitario;
        private TextView lbl_subtotal;
        private TextView lbl_descuentos;
        private TextView lbl_error;

        private Context context;

        public ArticuloEntregado articuloVendido;

        public int position;

        public ItemRemitoViewHolder(View view, Context context) {

            super(view);

            this.context = context;

            lbl_cantidadDevuelta = (TextView) itemView.findViewById(R.id.lbl_cantidadDevuelta);
            lbl_cantidadEntregada = (TextView) itemView.findViewById(R.id.lbl_cantidadEntregada);
            lbl_cantidadPrestada = (TextView) itemView.findViewById(R.id.lbl_cantidadPrestada);
            lbl_cantidadRelevada = (TextView) itemView.findViewById(R.id.lbl_cantidadRelevada);
            lbl_nombreArticulo = (TextView) itemView.findViewById(R.id.lbl_nombreArticulo);
            lbl_precioUnitario = (TextView) itemView.findViewById(R.id.lbl_precioUnitario);
            lbl_subtotal = (TextView) itemView.findViewById(R.id.lbl_subtotal);
            lbl_descuentos = (TextView) itemView.findViewById(R.id.lbl_descuentos);
            lbl_error = (TextView) itemView.findViewById(R.id.lbl_error);

        }

        public void poblarRow(ArticuloEntregado articuloVendido, int position){

            this.articuloVendido = articuloVendido;
            this.position = position;

            this.articuloVendido.validar();
            this.lbl_error.setText(this.articuloVendido.error);

            lbl_cantidadDevuelta.setText(articuloVendido.cantidadEnvasesDevueltos==0?"-": String.valueOf(articuloVendido.cantidadEnvasesDevueltos));
            lbl_cantidadEntregada.setText(articuloVendido.cantidadEntregada==0?"-":String.valueOf(articuloVendido.cantidadEntregada));
            lbl_cantidadPrestada.setText(articuloVendido.cantidadEnvasesPrestados==0?"-":String.valueOf(articuloVendido.cantidadEnvasesPrestados));
            lbl_cantidadRelevada.setText(articuloVendido.cantidadEnvasesRelevados==0?"-":String.valueOf(articuloVendido.cantidadEnvasesRelevados));
            lbl_nombreArticulo.setText(articuloVendido.nombreDelArticulo + " ("+articuloVendido.codigoArticulo+")");
            lbl_precioUnitario.setText("$" + String.valueOf(articuloVendido.precioUnitario));
            lbl_subtotal.setText("$" +String.valueOf(String.valueOf(articuloVendido.subtotal)));

            String descuentos = "";

            if(articuloVendido.descuentoPorCantidad > 0.0)
                descuentos +=String.valueOf(articuloVendido.descuentoPorCantidad)+"%  ";

            if(articuloVendido.descuentoManual > 0.0)
                descuentos +=String.valueOf(articuloVendido.descuentoManual)+"%";

            if(descuentos.equals(""))
                descuentos="-";

            lbl_descuentos.setText(descuentos);
        }

    }

}
