package waterservice.mobile.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.EnvaseDisponible;

public class ResumenEnvaseEntregadoAdapter extends RecyclerView.Adapter<ResumenEnvaseEntregadoAdapter.Item>{

    private List<EnvaseDisponible.ResumenEnvaseEntregado> resumentEnvases;
    private Context context;

    public ResumenEnvaseEntregadoAdapter(List<EnvaseDisponible.ResumenEnvaseEntregado> resumentEnvases, Context context){

        this.resumentEnvases = resumentEnvases;
        this.context = context;
    }

    @NonNull
    @Override
    public ResumenEnvaseEntregadoAdapter.Item onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_entrega_envase_resumen, parent, false);

        ResumenEnvaseEntregadoAdapter.Item vh = new ResumenEnvaseEntregadoAdapter.Item(v, context);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ResumenEnvaseEntregadoAdapter.Item holder, int position) {
        EnvaseDisponible.ResumenEnvaseEntregado env = resumentEnvases.get(position);
        holder.poblarRow(env, position);
    }

    @Override
    public int getItemCount() {
        return resumentEnvases == null? 0 : resumentEnvases.size();
    }

    public static class Item extends RecyclerView.ViewHolder {

        private TextView lbl_contenido;
        private TextView lbl_precioUnitario;
        private TextView lbl_cantidad;
        private TextView lbl_subtotal;

        private Context context;

        public EnvaseDisponible.ResumenEnvaseEntregado item;

        public int position;

        public Item(View view, Context context) {

            super(view);

            this.context = context;

            lbl_contenido = (TextView) itemView.findViewById(R.id.lbl_contenido);
            lbl_precioUnitario = (TextView) itemView.findViewById(R.id.lbl_precioUnitario);
            lbl_cantidad = (TextView) itemView.findViewById(R.id.lbl_cantidad);
            lbl_subtotal = (TextView) itemView.findViewById(R.id.lbl_subtotal);
        }

        public void poblarRow(EnvaseDisponible.ResumenEnvaseEntregado item, int position){

            this.item = item;
            this.position = position;

            lbl_contenido.setText(item.articuloContenidoNombre);
            lbl_precioUnitario.setText("$"+String.valueOf(item.precio)+" c/u");
            lbl_cantidad.setText(String.valueOf(item.cantidad)+ " " + item.unidadCantidad);
            lbl_subtotal.setText("$"+String.valueOf(item.subtotal));
        }

    }

}
