package waterservice.mobile.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.activities.ActivityMapaDeClientes;
import waterservice.mobile.activities.ActivityMenuCliente;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.apiContracts.ResponseBase;
import waterservice.mobile.server.ApiCallback;
import waterservice.mobile.server.SyncMobile;
import waterservice.mobile.services.Service_Clientes;
import waterservice.mobile.services.Service_NuevosPedidos;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.Utiles;

public class ClientesHojaDeRutaAdapter extends RecyclerView.Adapter<ClientesHojaDeRutaAdapter.ClienteViewHolder>{

    private List<Cliente> clientes;
    private Context context;
    private AppSession appSession;

    public ClientesHojaDeRutaAdapter(List<Cliente> clientes, Context context){

        this.clientes = clientes;
        this.context = context;
        this.appSession = new AppSession(context);
    }

    @Override
    public int getItemViewType (int position){
        return clientes.get(position).esResumenDeCliente?
                R.layout.row_cliente_resumen:
                R.layout.row_cliente;
    }

    @NonNull
    @Override
    public ClienteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View viewCliente = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);

        ClienteViewHolder vh = new ClienteViewHolder(viewCliente, context, appSession);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ClienteViewHolder holder, int position) {
        Cliente cliente = clientes.get(position);
        holder.poblarRow(cliente, position);
    }

    @Override
    public int getItemCount() {
        return clientes == null? 0 : clientes.size();
    }

    public static class ClienteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context context;

        public Cliente cliente;
        public int position;
        private AppSession appSession;

        private TextView hojaDeRuta_lbl_cliente;
        private TextView hojaDeRuta_lbl_clienteId;
        private TextView hojaDeRuta_lbl_direccion;
        private TextView lbl_bloqueado;
        private TextView lbl_fechaUltimaCompra;
        private TextView lbl_fechaUltimoPago;
        private TextView lbl_proventivo;
        private TextView lbl_saldoConsumos;
        private TextView lbl_saldoFacturas;
        private TextView lbl_saldoFinal;
        private TextView lbl_orden;
        private TextView hojaDeRuta_lbl_mensajeCliente;
        private TextView lbl_tipoCliente;
        private TextView lbl_reparto;
        private TextView lbl_resultadoBusqueda;

        private ImageView hojaDeRuta_img_alerta;
        private ImageView hojaDeRuta_img_esPedido;
        private ImageView hojaDeRuta_img_esRepaso;
        private ImageView hojaDeRuta_img_estadoVisita;
        private ImageView hojaDeRuta_img_tipoCliente;
        private ImageView img_servicioTecnico;
        private ImageView img_noCompra;
        private ImageView img_ausente;

        private Button btn_ir;
        private Button btn_repaso;
        private Button btn_visita;
        private Button btn_agregarHojaDeRuta;

        public ClienteViewHolder(View itemView, Context context, AppSession appSession) {

            super(itemView);

            this.appSession = appSession;
            this.context = context;

            hojaDeRuta_img_alerta = (ImageView) itemView.findViewById(R.id.hojaDeRuta_img_alerta);
            hojaDeRuta_img_esPedido = (ImageView) itemView.findViewById(R.id.hojaDeRuta_img_esPedido);
            hojaDeRuta_img_esRepaso = (ImageView) itemView.findViewById(R.id.hojaDeRuta_img_esRepaso);
            hojaDeRuta_img_estadoVisita = (ImageView) itemView.findViewById(R.id.hojaDeRuta_img_estadoVisita);
            hojaDeRuta_img_tipoCliente = (ImageView) itemView.findViewById(R.id.hojaDeRuta_img_tipoCliente);
            img_servicioTecnico = (ImageView) itemView.findViewById(R.id.img_servicioTecnico);
            img_noCompra = (ImageView) itemView.findViewById(R.id.img_noCompra);
            img_ausente = (ImageView) itemView.findViewById(R.id.img_ausente);

            hojaDeRuta_lbl_cliente = (TextView) itemView.findViewById(R.id.hojaDeRuta_lbl_cliente);
            hojaDeRuta_lbl_clienteId = (TextView) itemView.findViewById(R.id.hojaDeRuta_lbl_clienteId);
            hojaDeRuta_lbl_direccion = (TextView) itemView.findViewById(R.id.hojaDeRuta_lbl_direccion);
            lbl_bloqueado = (TextView) itemView.findViewById(R.id.lbl_bloqueado);
            lbl_fechaUltimaCompra = (TextView) itemView.findViewById(R.id.lbl_fechaUltimaCompra);
            lbl_fechaUltimoPago = (TextView) itemView.findViewById(R.id.lbl_fechaUltimoPago);
            lbl_proventivo = (TextView) itemView.findViewById(R.id.lbl_proventivo);
            lbl_saldoConsumos = (TextView) itemView.findViewById(R.id.lbl_saldoConsumos);
            lbl_saldoFacturas = (TextView) itemView.findViewById(R.id.lbl_saldoFacturas);
            lbl_saldoFinal = (TextView) itemView.findViewById(R.id.lbl_saldoFinal);
            lbl_orden = (TextView) itemView.findViewById(R.id.lbl_orden);
            hojaDeRuta_lbl_mensajeCliente = (TextView) itemView.findViewById(R.id.hojaDeRuta_lbl_mensajeCliente);
            lbl_tipoCliente = (TextView) itemView.findViewById(R.id.lbl_tipoCliente);
            lbl_reparto = (TextView) itemView.findViewById(R.id.lbl_reparto);
            lbl_resultadoBusqueda = (TextView) itemView.findViewById(R.id.lbl_resultadoBusqueda);

            btn_ir = (Button) itemView.findViewById(R.id.btn_ir);
            btn_repaso = (Button) itemView.findViewById(R.id.btn_repaso);
            btn_visita = (Button) itemView.findViewById(R.id.btn_visita);
            btn_agregarHojaDeRuta = (Button) itemView.findViewById(R.id.btn_agregarHojaDeRuta);
        }

        public void poblarRow(Cliente cliente, int position){

            this.position = position;

            if(cliente.esResumenDeCliente){
                poblarRowClienteResumen(cliente);
            }else{
                poblarRowClienteDeRuta(cliente);
            }
        }

        public void poblarRowClienteResumen(Cliente cliente){

            this.cliente = cliente;

            hojaDeRuta_lbl_clienteId.setText(String.valueOf(cliente.cliente_id));
            hojaDeRuta_lbl_cliente.setText(cliente.nombreCliente);
            hojaDeRuta_lbl_direccion.setText(cliente.domicilioCompleto);
            lbl_reparto.setText(cliente.reparto);
            lbl_tipoCliente.setText(cliente.tipoCliente);
            lbl_resultadoBusqueda.setText(cliente.resumenDeClienteResultadoBusquedaTipoId==1?
                    "- NOMBRE -":"- DOMICILIO -");

            btn_agregarHojaDeRuta.setOnClickListener(this);
        }

        public void poblarRowClienteDeRuta(Cliente cliente){

            this.cliente = cliente;

            //cliente.orden = position+1;

            lbl_orden.setText(String.valueOf(cliente.orden));
            hojaDeRuta_lbl_cliente.setText(cliente.nombreCliente);
            hojaDeRuta_lbl_clienteId.setText(String.valueOf(cliente.cliente_id));
            hojaDeRuta_lbl_direccion.setText(cliente.domicilioCompleto);

            //Fechas
            lbl_fechaUltimaCompra.setText(Utiles.fecha(cliente.fechaUtlimaEntrega, true));
            lbl_fechaUltimoPago.setText(Utiles.fecha(cliente.fechaUltimoCobroFactura, true));

            if(cliente.visitado)
                hojaDeRuta_img_estadoVisita.setImageResource(R.drawable.ic_visitado);
            else if(cliente.pendiente)
                hojaDeRuta_img_estadoVisita.setImageResource(R.drawable.ic_repaso);
            else
                hojaDeRuta_img_estadoVisita.setImageResource(R.drawable.ic_no_visitado);

            if(cliente.visitado){

                img_ausente.setVisibility(cliente.ausente? View.VISIBLE : View.GONE);
                img_noCompra.setVisibility(!cliente.ausente && !cliente.ventaEntrega && !cliente.cobroFactura ? View.VISIBLE : View.GONE);
            }else{
                img_ausente.setVisibility(View.GONE);
                img_noCompra.setVisibility(View.GONE);
            }

            hojaDeRuta_img_tipoCliente.setImageResource(cliente.tipoCliente_ids==1? R.drawable.ic_familia : R.drawable.ic_empresa);

            if(!cliente.tieneAlertas)
                hojaDeRuta_img_alerta.setVisibility(View.GONE);

            if(!cliente.esSync)
                hojaDeRuta_img_esPedido.setVisibility(View.GONE);

            if(!cliente.esRepaso)
                hojaDeRuta_img_esRepaso.setVisibility(View.GONE);
            else
                hojaDeRuta_img_esRepaso.setVisibility(View.VISIBLE);

            lbl_bloqueado.setVisibility(cliente.esCuentaBloqueada?View.VISIBLE : View.GONE);
            lbl_proventivo.setVisibility(cliente.esCuentaPreventiva? View.VISIBLE : View.GONE);

            lbl_saldoConsumos.setText("$"+String.valueOf(cliente.saldoConsumos));
            lbl_saldoFacturas.setText("$"+String.valueOf(cliente.saldoFacturacion));
            lbl_saldoFinal.setText("$"+String.valueOf(Utiles.Round(cliente.saldoFacturacion + cliente.saldoConsumos)));

            btn_visita.setOnClickListener(this);
            btn_ir.setOnClickListener(this);
            btn_repaso.setOnClickListener(this);

            if(cliente.tipoDeVisitaId == Constantes.TIPO_DE_VISITA_SERVICIO_TECNICO)
                img_servicioTecnico.setVisibility(View.VISIBLE);
            else
                img_servicioTecnico.setVisibility(View.GONE);

            hojaDeRuta_lbl_mensajeCliente.setText(cliente.comunicacion3!= null ? cliente.comunicacion3 : "");
        }

        @Override
        public void onClick(View v) {

            if(v.getId()==R.id.btn_visita)
                seleccionarCliente();
            else if(v.getId()==R.id.btn_repaso)
                repaso();
            else if(v.getId()==R.id.btn_ir) {
                try {
                    irACliente();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else if(v.getId()==R.id.btn_agregarHojaDeRuta) {
                try {
                    agregarClienteAHojaDeRuta();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        private void seleccionarCliente() {

            try {
                if(cliente.ausente){
                    Dialog.confirm("Cliente ausente",
                            "El cliente había sido marcado como ausente. Desea ir al sistema web?",
                            new Dialog.OnConfirm() {
                                @Override
                                public void confirm() {
                                    try {
                                        String url = appSession.getValueString("servidor_web");
                                        String urlSistema = url + "/TransaccionesTemporales/Create?clienteId="+
                                                String.valueOf(cliente.cliente_id)+"&hojaDeRutaId="+String.valueOf(appSession.getHojaDeRuta().id);
                                        Utiles.openUrl(urlSistema, context);
                                    } catch (Exception e) {
                                        Utiles.Mensaje_En_Pantalla(e.getMessage(), true,false,context);
                                    }
                                }
                            }, context);
                    return;
                }

                appSession.setClienteActual(cliente);
                appSession.setValueInt(position, SessionKeys.CLIENTE_INDICE_ACTUAL);
                Intent intent=new Intent(context , ActivityMenuCliente.class);
                context.startActivity(intent);

            } catch (Exception ex) {
                Toast.makeText(context,
                        ex.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        private void irACliente() throws Exception {

            String destinationLatitude ="";
            String destinationLongitude ="";

            if(cliente.relevamientoCoordenadas_latitud != null){
                destinationLatitude = cliente.relevamientoCoordenadas_latitud;
                destinationLongitude = cliente.relevamientoCoordenadas_longitud;
            }else{
                destinationLatitude = cliente.altitud;
                destinationLongitude = cliente.longitud;
            }

            if(destinationLatitude==null || destinationLatitude.equals("")){
                Toast.makeText(context,
                        "No tenemos la ubicación del cliente por coordenadas",
                        Toast.LENGTH_LONG).show();
                return;
            }

            appSession.setClienteActual(cliente);
            Intent myIntent=new Intent(context, ActivityMapaDeClientes.class);
            context.startActivity(myIntent);

            //Abre google maps directamente
            //String uri = String.format(Locale.ENGLISH,"http://maps.google.com/maps?daddr=%s,%s (%s)",destinationLatitude,destinationLongitude,"Cliente");
            //Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            //intent.setPackage("com.google.android.apps.maps");
            //context.startActivity(intent);
        }

        private void repaso(){

            if(cliente.visitado) return;

            final boolean quitarRepaso = cliente.esRepaso;
            String mensaje = quitarRepaso ?
                    "Confirma quitarle al cliente el estado de REPASO?":
                    "Confirmar poner al cliente en estado REPASO";

            Dialog.confirm("Repaso", mensaje,
                    new Dialog.OnConfirm() {
                        @Override
                        public void confirm() {

                            try{
                                new Service_Clientes(context).ponerEnRepaso(cliente, quitarRepaso);
                                poblarRow(cliente, position);
                            }catch (Exception ex){}

                        }
                    }, this.context);
        }

        private void agregarClienteAHojaDeRuta(){

            Dialog.confirm("Confirmación", "Desea agregar al cliente " + this.cliente.nombreCliente.toUpperCase() +", a la hoja de ruta actual?",
                    new Dialog.OnConfirm() {
                        @Override
                        public void confirm() {
                            try {
                                agregarClienteAHojaDeRutaConfirmado();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, context);
        }

        private void agregarClienteAHojaDeRutaConfirmado() throws Exception {

            SyncMobile srvSyncMobile = new SyncMobile(context);
            AppSession appSession = new AppSession(context);
            String identificadorMovil= Utiles.Get_Config_Value("codigo_identificacion_movil", context);

            srvSyncMobile.agregarClienteAHojaDeRuta(appSession.getHojaDeRuta().id, identificadorMovil, cliente.cliente_id,
                    new ApiCallback<ResponseBase>() {
                        @Override
                        public void onSuccess(ResponseBase response) {

                            Dialog.success("Exitoso: "+cliente.nombreCliente,"Se ha agregado a la hoja de ruta.", context);

                            try {
                                new Service_NuevosPedidos(context).SyncNuevosPedidos();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String message, int errorCode) {
                            Dialog.error("Error: "+ cliente.nombreCliente, message, context);
                        }
                    });

            Dialog.toast("Espere un instante por favor...", context);

        }
    }

}
