package waterservice.mobile.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.Factura;
import waterservice.mobile.clases.ItemDeRecibo;
import waterservice.mobile.services.Service_Facturas;
import waterservice.mobile.services.Service_Recibos;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.Utiles;

public class ImputacionFacturaAdapter extends RecyclerView.Adapter<ImputacionFacturaAdapter.ImputacionFacturaAdapterViewHolder>{

    private List<Factura> items;
    private Context context;
    Acciones acciones;
    private boolean editar;

    public ImputacionFacturaAdapter(List<Factura> items, Context context, Acciones acciones, boolean editar){

        this.acciones = acciones;
        this.items = items;
        this.context = context;
        this.editar = editar;
    }

    @NonNull
    @Override
    public ImputacionFacturaAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_imputacion_factura, parent, false);

        ImputacionFacturaAdapterViewHolder vh = new ImputacionFacturaAdapterViewHolder(v, context, acciones, editar);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ImputacionFacturaAdapterViewHolder holder, int position) {
        Factura item = items.get(position);
        holder.poblarRow(item, position);
    }

    @Override
    public int getItemCount() {
        return items == null? 0 : items.size();
    }

    public static class ImputacionFacturaAdapterViewHolder extends RecyclerView.ViewHolder {

        private TextView lbl_fechaDeFactura;
        private TextView lbl_importeFactura;
        private TextView lbl_nroFactura;
        private TextView lbl_saldoFactura;
        private Button btn_imputarSaldo, btn_entregarFactura;
        private EditText txt_importeImputado;

        Acciones acciones;
        private Context context;
        private boolean editar;
        public Factura item;

        public int position;

        public ImputacionFacturaAdapterViewHolder(View view, Context context, Acciones acciones,
                                                  boolean editar) {

            super(view);

            this.acciones = acciones;
            this.context = context;
            this.editar = editar;

            lbl_fechaDeFactura = (TextView) view.findViewById(R.id.lbl_fechaDeFactura);
            lbl_importeFactura = (TextView) view.findViewById(R.id.lbl_importeFactura);
            lbl_nroFactura = (TextView) view.findViewById(R.id.lbl_nroFactura);
            lbl_saldoFactura = (TextView) view.findViewById(R.id.lbl_saldoFactura);
            btn_imputarSaldo = (Button) view.findViewById(R.id.btn_imputarSaldo);
            btn_imputarSaldo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onImputarSaldo();
                }
            });
            txt_importeImputado = (EditText) view.findViewById(R.id.txt_importeImputado);

            btn_entregarFactura = (Button) view.findViewById(R.id.btn_entregarFactura);
            btn_entregarFactura.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onMarcarComoRecibida();
                }
            });

            if(editar){
                txt_importeImputado.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void afterTextChanged(Editable s) {}

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,int count, int after) {}

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        try{
                            item.imputado = Double.valueOf(txt_importeImputado.getText().toString());
                        }catch (Exception ex){
                            item.imputado = 0.0;
                        }

                        if(ImputacionFacturaAdapterViewHolder.this.acciones!=null)
                            ImputacionFacturaAdapterViewHolder.this.acciones.onActualizarTotal();
                    }
                });
            }else{
                txt_importeImputado.setEnabled(false);
            }
        }

        private void onMarcarComoRecibida(){

            Dialog.confirm("Factura entregada", "Confirma que se ha entregado la factura?",
                    new Dialog.OnConfirm() {
                        @Override
                        public void confirm() {

                            new Service_Facturas(context).confirmarEntregaDeComprobante(1,
                                    item.id, item.cliente_id);

                        }
                    }, context);
        }

        private void onImputarSaldo(){

            txt_importeImputado.setText(String.valueOf(item.saldoActual));

            if(ImputacionFacturaAdapterViewHolder.this.acciones!=null)
                ImputacionFacturaAdapterViewHolder.this.acciones.onActualizarTotal();
        }

        public void poblarRow(Factura item, int position){

            this.item = item;
            this.position = position;

            lbl_fechaDeFactura.setText(Utiles.fecha(item.fechaFactura, true));
            lbl_importeFactura.setText("$"+Utiles.Round(item.montoFacturaTotal));
            lbl_nroFactura.setText(item.nroFactura);
            lbl_saldoFactura.setText("$"+Utiles.Round(item.saldoActual));
            txt_importeImputado.setText(Utiles.Round(item.imputado));

        }

    }

    public interface Acciones{
        void onActualizarTotal();
    }

}
