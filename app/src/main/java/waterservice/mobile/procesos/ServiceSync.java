package waterservice.mobile.procesos;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.apiContracts.Sync.VerificarConfiguracionMobileResponse;
import waterservice.mobile.server.ApiCallback;
import waterservice.mobile.server.SyncMobile;
import waterservice.mobile.services.Service_ComandosMobile;
import waterservice.mobile.services.Service_NuevosPedidos;
import waterservice.mobile.services.Service_ServicioTecnico;
import waterservice.mobile.services.Service_Ventas;
import waterservice.mobile.utiles.CurrentApp;
import waterservice.mobile.utiles.HttpJsonClient;
import waterservice.mobile.utiles.Utiles;

public class ServiceSync extends Service {

    private Timer mTimer;

    private static boolean isRunning  = false;
    private int waiting = 60000;
    private AppSession appSession;

    HttpJsonClient _apiClient;
    String urlRoot;

    Service_ServicioTecnico _srvServicioTecnico;

    TimerTask timerTask = new TimerTask() {

        @Override
        public void run() {
            try{
                ServiceSync.this.onProcess();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        _srvServicioTecnico = new Service_ServicioTecnico(this.getApplicationContext());
        appSession = new AppSession(this.getApplicationContext());

        _apiClient=new HttpJsonClient();
        urlRoot= Utiles.Get_Config_Value("servidor_web", getBaseContext());

        try {
            waiting = Integer.valueOf(_srvServicioTecnico.obtenerConfiguracion("CANT_SEG_SYNC").valor) * 1000;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("SERVICE >> onCreate",e.getMessage());
        }
    }

    private void onProcess() {

        try {
            enviarUbicacion();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(">>enviarUbicacion",e.getMessage());
            Toast.makeText(this, "Water Service Sync Ubicación: "+e.getMessage(), Toast.LENGTH_LONG).show();
        }

        try {
            enviarServiciosTecnicosRealizados();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(">>enviarUbicacion",e.getMessage());
            Toast.makeText(this, "Water Service Sync STs: "+e.getMessage(), Toast.LENGTH_LONG).show();
        }

        try {
            solicitarNuevosPedidos();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(">>solicitarNuevos",e.getMessage());
            Toast.makeText(this, "Water Service Sync Pedidos: "+e.getMessage(), Toast.LENGTH_LONG).show();
        }

        try {
            obtenerComandosParaEjecutar();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(">>obtenerComand",e.getMessage());
            Toast.makeText(this, "Water Service Sync Comandos: "+e.getMessage(), Toast.LENGTH_LONG).show();
        }

        try {
            enviarVentas();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(">>enviarVentas",e.getMessage());
            Toast.makeText(this, "Water Service Sync Ventas: "+e.getMessage(), Toast.LENGTH_LONG).show();
        }

        Utiles.Set_Config_Value("last_sync", Utiles.getDateNumber(), this);
    }

    private void enviarVentas() throws Exception{
        new Service_Ventas(getApplicationContext()).syncVisitasCerradas();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {

            mTimer = new Timer();
            mTimer.schedule(timerTask, 10000, waiting);

            Toast.makeText(this, "Water Service Sync se ha iniciado", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(this, "Water Service Sync ha dado un error: "+e.getMessage(), Toast.LENGTH_LONG).show();
        }
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "Water Service Sync se ha detenido", Toast.LENGTH_LONG).show();
        try {
            mTimer.cancel();
            timerTask.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void enviarServiciosTecnicosRealizados() throws Exception {
        _srvServicioTecnico.enviarServiciosTecnicosCerrados();
    }

    public void enviarUbicacion() throws Exception {

        String path=urlRoot+"/Moviles/InformarUbicacion";

        long hojaDeRutaId = appSession.getHojaDeRuta().id;
        String identificadorMovil= Utiles.Get_Config_Value("codigo_identificacion_movil", getBaseContext());
        String longitud=CurrentApp.currentLongitud;
        String latitud=CurrentApp.currentLatutid;

        if(longitud == null || latitud ==null) return;

        SyncMobile syncMobile = new SyncMobile( getBaseContext());
        Log.i("SYNC_LOCATION", String.valueOf(latitud) + "-" + String.valueOf(longitud));
        syncMobile.enviarUbicacion(identificadorMovil, hojaDeRutaId, latitud, longitud, new ApiCallback<VerificarConfiguracionMobileResponse>() {
            @Override
            public void onSuccess(VerificarConfiguracionMobileResponse response) {

            }

            @Override
            public void onError(String message, int errorCode) {
                Log.e(">>enviarUbicacion",message);
            }
        });
    }

    public void solicitarNuevosPedidos() throws Exception {

        new Service_NuevosPedidos(getApplicationContext()).SyncNuevosPedidos();
    }

    private void obtenerComandosParaEjecutar() throws Exception{
        new Service_ComandosMobile(getApplicationContext()).obtenerYProcesarNuevosComandos();
    }
}
