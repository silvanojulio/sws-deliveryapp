package waterservice.mobile.procesos;

import android.app.Activity;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import waterservice.mobile.utiles.CurrentApp;

public class GPSTrackerTask extends AsyncTask<Activity, Void, Void> {

    public boolean active = true;
    public boolean isRunning = false;
    public int waiting=15000;
    public Activity activity;

    @Override
    protected Void doInBackground(Activity ..._activity) {
        activity = _activity[0];
        try {
            while(active){

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        doAction();
                    }
                });

                try {
                    Thread.sleep(waiting);
                } catch (Exception e) { }

            }
        }catch (Exception ex){
            Log.e("ERROR GPS TRACKING APP",ex.getMessage());
        }

        return null;
    }

    static boolean doingAction = false;
    public void doAction(){

        if(doingAction) return;

        doingAction = true;

        try {

            // create class object
            GPSTracker gps = new GPSTracker(activity);

            // check if GPS enabled
            if(gps.canGetLocation()){
                normalSpeed();
                Location location = gps.getLocation();
                if(location != null){
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();

                    CurrentApp.currentLatutid = String.valueOf(latitude);
                    CurrentApp.currentLongitud = String.valueOf(longitude);
                }
            }else{
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
                slowSpeed();
            }
        }catch (Exception ex){

        }finally {
            doingAction = false;
        }
    }

    public void slowSpeed(){
        waiting = 45000;
    }

    public void normalSpeed(){
        waiting = 15000;
    }

    public void fastSpeed(){
        waiting = 5000;
    }

}
