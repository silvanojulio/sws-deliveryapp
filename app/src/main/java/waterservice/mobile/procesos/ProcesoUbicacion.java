package waterservice.mobile.procesos;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONObject;

import java.util.Locale;

import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.Usuario;
import waterservice.mobile.utiles.AsyncWork;
import waterservice.mobile.utiles.CurrentApp;
import waterservice.mobile.utiles.HttpJsonClient;
import waterservice.mobile.utiles.Utiles;

public class ProcesoUbicacion extends AsyncTask<Void, Void, Void> implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener
{
    HttpJsonClient _apiClient;
    String urlRoot;

    private Activity currentContext;
    private boolean procesoActivo;
    private FusedLocationProviderApi locApi = LocationServices.FusedLocationApi;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private AppSession appSession;

    public ProcesoUbicacion(Activity _contexto){
        currentContext=_contexto;
        appSession = new AppSession(_contexto);
        _apiClient=new HttpJsonClient();
        urlRoot= Utiles.Get_Config_Value("servidor_web", currentContext);
    }
    private int espera = 2000;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        googleApiClient = new GoogleApiClient.Builder(currentContext)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();

        locationRequest = new LocationRequest();
        locationRequest.setInterval(30000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        googleApiClient.connect();

        procesoActivo = true;

        if (ActivityCompat.checkSelfPermission(currentContext, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(currentContext, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
    }
    @Override
    protected Void doInBackground(Void... params) {

       while (procesoActivo){
            try {
                informarUbicacion();
                Thread.sleep(espera);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        //this method will be running on UI thread
        procesoActivo=false;

    }

    public void informarUbicacion() throws Exception {

        try{

            if(!googleApiClient.isConnected())
                return ;

            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);

            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final com.google.android.gms.common.api.Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:

                            CurrentApp.gpsActivado = true;

                            try {
                                //enviarUbicacion();
                                AsyncWork.run("enviarUbicacion",ProcesoUbicacion.this,null);
                                espera = 30000;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;

                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            Utiles.Mensaje_En_Pantalla("Debe activar el GPS y permitir la ubicación de Google en ANDROID", true, true, currentContext);
                            procesoActivo = false;

                            //CurrentApp.cerrarTodasLasActividades();

                            break;

                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            CurrentApp.gpsActivado = false;
                            break;
                    }
                }
            });

        }catch (Exception ex){
            CurrentApp.gpsActivado = false;
            espera = 3000;
            Utiles.Mensaje_En_Pantalla("El GPS y la ubicación de Android deben estar activados.", true, true, currentContext);
        }
    }

    public void enviarUbicacion() throws Exception {

        String path=urlRoot+"/Moviles/InformarUbicacion";

        String identificadorMovil="", hojaDeRuta_id="", longitud="", latitud="", estadoMovil_ids="1";

        identificadorMovil=Utiles.Get_Config_Value("codigo_identificacion_movil", currentContext);

        hojaDeRuta_id= String.valueOf(appSession.getHojaDeRuta().id);

        longitud=CurrentApp.currentLongitud;
        latitud=CurrentApp.currentLatutid;

        if(longitud == null || latitud ==null)
        {
            return;
        }

        JSONObject ubicacion = new JSONObject();
        ubicacion.put("hojaDeRuta_id", hojaDeRuta_id);
        ubicacion.put("longitud", longitud);
        ubicacion.put("latitud", latitud);
        ubicacion.put("estadoMovil_ids", estadoMovil_ids);

        JSONObject request = new JSONObject();
        request.put("ubicacion", ubicacion);
        request.put("codigoIdentificacionMovil", identificadorMovil);

        JSONObject response = _apiClient.postJson(path, request, null);

        int error=response.getInt("error");

        if(error!=0 ){
            String mensajeError = response.getString("message");
            //Utiles.Mensaje_En_Pantalla("Error al enviar la ubicacion: "+ mensajeError,false,false,currentContext);
        }else{
            //Utiles.Mensaje_En_Pantalla("Ubicacion enviada: LAT "+ longitud+". LNG "+longitud,false,false,currentContext);
        }

    }

    public void finalizar(){
        procesoActivo=false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(currentContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(currentContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                googleApiClient);
        if (mLastLocation != null) {
            CurrentApp.currentLatutid= String.valueOf(mLastLocation.getLatitude());
            CurrentApp.currentLongitud= String.valueOf(mLastLocation.getLongitude());
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        int j=0;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Utiles.Mensaje_En_Pantalla("Debe activar el GPS y permitir la ubicación de Google en ANDROID. Error: "+connectionResult.getErrorMessage(),
                true, true, currentContext);
        //procesoActivo = false;
        //CurrentApp.cerrarTodasLasActividades();
    }

    @Override
    public void onLocationChanged(Location location) {
        int i=0;
    }
}

/*---------- Listener class to get coordinates ------------- */
 class MyLocationListener implements LocationListener {

    @Override
    public void onLocationChanged(Location loc) {

        String longitude = String.valueOf(loc.getLongitude());
        String latitude = String.valueOf(loc.getLatitude());

        CurrentApp.currentLatutid=latitude;
        CurrentApp.currentLongitud=longitude;
    }

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}
}
