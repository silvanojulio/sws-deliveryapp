package waterservice.mobile.utiles;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

import waterservice.mobile.clases.HojaDeRuta;
import waterservice.mobile.clases.Usuario;
import waterservice.mobile.clases.UsuarioApp;
import waterservice.mobile.procesos.ProcesoUbicacion;
import waterservice.mobile.services.Service_HojasDeRutas;

public class CurrentApp {

    public static String currentLatutid;
    public static String currentLongitud;
    public static boolean gpsActivado;
}
