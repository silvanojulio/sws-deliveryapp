package waterservice.mobile.utiles;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ar.com.waterservice.newapp.R;

public class Utiles {

    private static  ProgressDialog dialog;

    public static String Get_Config_Value(String key_config, Context context){

        SharedPreferences pref=PreferenceManager.getDefaultSharedPreferences(context);

        String value=pref.getString(key_config, "");

        return value;
    }

    public static boolean Set_Config_Value(String key_config, Object new_value, Context context){

        try {

            SharedPreferences pref=PreferenceManager.getDefaultSharedPreferences(context);

            SharedPreferences.Editor editor=pref.edit();
            editor.putString(key_config, String.valueOf(new_value));
            editor.commit();

            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public static String Get_Fecha(){

        return  Get_Fecha(new Date());
    }

    public static long getDateNumber(){
        return new Date().getTime();
    }

    public static String Get_Fecha(Date date){

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        return  sdf.format(date);
    }

    public static Date ConvertDateFromString(String date){

        String dateOnly = date.split(" ")[0];
        String dia, mes, anio, hora, minuto;
        dia=dateOnly.split("/")[0];
        mes=dateOnly.split("/")[1];
        anio=dateOnly.split("/")[2];

        Calendar c =  Calendar.getInstance();
        c.set(Integer.valueOf(anio), Integer.valueOf(mes),Integer.valueOf(dia));

        return c.getTime();
    }

    public static String CompleteTwoDigitsNumber(int number){

        String value=String.valueOf(number);

        return value.length()>1?value:"0"+value;
    }

    public static String Get_FechaHoraMark(String fecha){

        return fecha.replace(' ', '_').replace('/', '-').replace(':', '.');
    }

    public static String getStampMark(){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HHmmss");

        String d = sdf.format(new Date());

        return d;
    }
    public static void Start_Working_Message(Context context,String mensaje){

        dialog= ProgressDialog.show(context, "", mensaje, true);
    }

    public static void Stop_Working_Message(){

        if(dialog!=null)
        dialog.cancel();
    }

    public static String Round(Double number ){

        DecimalFormat df = new DecimalFormat("#.##");

        return  df.format(number);
    }

    public static double roundTwoDecimals(double d)
    {
        try{
            return Math.round(d*100d)/100d;
        }catch (Exception ex){
            throw ex;
        }
    }

    public static void  Mensaje_En_Pantalla( String Mensaje_,Boolean Tiempo_Largo, Boolean Stop_Progress, Context actividad){

        final String mensaje_final=Mensaje_;
        final Context contexto_final=actividad.getApplicationContext();
        final Boolean largo_final=Tiempo_Largo;
        final Context actividad_final=actividad;

        Stop_Working_Message();

        ((Activity) actividad).runOnUiThread(new Runnable() {
            public void run() {

                if(largo_final)
                    Toast.makeText(contexto_final, mensaje_final, Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(contexto_final,mensaje_final, Toast.LENGTH_SHORT).show();

            }
        });
    }

    public static  String fecha(String date, boolean onlyDate){

      try{

          if(date == null || date.equalsIgnoreCase("")) return "-";

          String dia, mes, anio, hora, minuto;

          String fechaOnly=date.split(" ")[0];
          String horaOnly = date.split(" ")[1];

          dia=fechaOnly.split("-")[2];
          mes=fechaOnly.split("-")[1];
          anio=fechaOnly.split("-")[0];

          hora=horaOnly.split(":")[0];
          minuto=horaOnly.split(":")[1];

          return (dia+"/"+mes+"/"+anio)+( onlyDate? "": (" "+ hora+":"+minuto));
      }catch (Exception ex){
          return "-";
      }

    }

    public static void MostrarConfirmacion(String titulo, String mensaje, String metodo, Object currentClass, Context context){

        final Object _activity=currentClass;
        final String _metodo=metodo;

        new AlertDialog.Builder(context)
                .setIcon(R.drawable.question)
                .setTitle(titulo)
                .setMessage(mensaje )
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {

                    //@Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {

                           Method m= _activity.getClass().getMethod(_metodo);
                           m.invoke(_activity,null);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                })
                .setNegativeButton("NO", null)
                .show();
    }

    public static List<String> getValuesFromFields(Object list, String fieldName) throws Exception {

        List<String> values = new ArrayList<String>();
        List<Object> objectList= (List<Object>) list;

        for(Object x : objectList) {
            Class<?> clazz = x.getClass();
            Field field = clazz.getField(fieldName); //Note, this can throw an exception if the field doesn't exist.
            String value = String.valueOf(field.get(x)) ;
            values.add(value);
        }

        return values;
    }

    public static void SetItemsToSpinner(Spinner spinner, Object list, String fieldName, Context context){

        List<String> items= null;
        try {
            items = getValuesFromFields(list,fieldName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public static void SetItemsToSpinner(Spinner spinner, List<String> items, Context context){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public static Date GetDateFromJson(JSONObject jsonObject, String field) throws Exception {
        String dateStr = jsonObject.getString(field);

        dateStr = dateStr.split("\\(")[1].split("\\)")[0];

        Date createdOn = new Date(Long.parseLong(dateStr));

        return createdOn;
    }

    public static String ConvertToString(Date date, String format){
        if(date==null) return null;
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    public static Date getDate(CustomDatePicker datePicker){

        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();

        return new Date(year,month,day);
    }

    public static double getDouble(EditText editText, double valorPredefinido){
        try{
            return Double.valueOf(editText.getText().toString());
        }catch (Exception ex){
            return valorPredefinido;
        }
    }

    public static int getInt(EditText editText, int valorPredefinido){
        try{
            return Integer.valueOf(editText.getText().toString());
        }catch (Exception ex){
            return valorPredefinido;
        }
    }

    public static View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition ) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    public static byte[] getBytesFromFile(File file) throws IOException {
        // Get the size of the file
        long length = file.length();

        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            // File is too large
            throw new IOException("File is too large!");
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[(int)length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;

        InputStream is = new FileInputStream(file);
        try {
            while (offset < bytes.length
                    && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
                offset += numRead;
            }
        } finally {
            is.close();
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }
        return bytes;
    }

    public static String getDateTimeForApi(Date date){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String d = df.format(date);
        return d;
    }

    public static String encodeImage(String photoPath) {

        File imagefile = new File(photoPath);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(imagefile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 60, baos);
        byte[] b = baos.toByteArray();
        return encodeImage(b);
    }

    public static String encodeImage(byte[] b) {

        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    public static void copyFile(String sourceFilePath, String destFilePath) throws IOException {

        File sourceFile = new File(sourceFilePath);
        File destFile = new File(destFilePath);

        if(!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        }
        finally {
            if(source != null) {
                source.close();
            }
            if(destination != null) {
                destination.close();
            }
        }
    }

    public static void deleteOldFiles(String folderPath, int daysBeforeToKeep){
        File[] files = new File(folderPath).listFiles();

        Calendar dateToRemove = Calendar.getInstance();
        dateToRemove.add(Calendar.DATE, daysBeforeToKeep * -1);

        for (File file: files) {
            Date created = new Date(file.lastModified());

            if(created.before(dateToRemove.getTime()))
                file.delete();
        }
    }

    /*
    *  Convenience method to add a specified number of minutes to a Date object
    *  From: http://stackoverflow.com/questions/9043981/how-to-add-minutes-to-my-date
    *  @param  minutes  The number of minutes to add
    *  @param  beforeTime  The time that will have minutes added to it
    *  @return  A date object with the specified number of minutes added to it
    */
    public static Date addMinutesToDate(int minutes, Date beforeTime){
        final long ONE_MINUTE_IN_MILLIS = 60000;//millisecs

        long curTimeInMs = beforeTime.getTime();
        Date afterAddingMins = new Date(curTimeInMs + (minutes * ONE_MINUTE_IN_MILLIS));
        return afterAddingMins;
    }

    public static void prepareRecyclerView(RecyclerView lts_articulos, Context context) {

        lts_articulos.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        lts_articulos.setLayoutManager(mLayoutManager);

    }

    public static double aplicarDescuentos(double importe, List<Double> porcentajes)
    {
        if (porcentajes == null || porcentajes.size()==0) return importe;

        double imp = importe;

        for (Double porcenaje :  porcentajes) {
            imp = imp - (imp * porcenaje / 100);
        }

        return roundTwoDecimals(imp);
    }

    public static double aplicarDescuentos(double importe, Double porcentaje1, Double porcentaje2, Double porcentaje3)
    {
        List<Double> porcentajes = new ArrayList<>();

        if(porcentaje1!=null) porcentajes.add(porcentaje1);
        if(porcentaje2!=null) porcentajes.add(porcentaje2);
        if(porcentaje3!=null) porcentajes.add(porcentaje3);

        return aplicarDescuentos(importe, porcentajes);
    }

    public static void openUrl(String url, Context context){
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }
}
