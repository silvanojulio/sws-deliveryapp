package waterservice.mobile.utiles;

public class Objeto_Var {

	private String nombre;
	private Object objeto;
	
	public Object Get_Objeto(){
		
		return objeto;		
	}
	
	public String Get_Nombre(){
		
		return nombre;		
	}
	
	public Objeto_Var(String nombre_var, Object objeto_var){
		nombre=nombre_var;
		objeto=objeto_var;
		
	}
	
	public void Set_Objeto_Var(String nombre_var, Object objeto_var){
		nombre=nombre_var;
		objeto=objeto_var;
		
	}
	
	public void Set_Objeto_Var(String nombre_var){
		nombre=nombre_var;
		
	}
	
	public void Set_Objeto_Var( Object objeto_var){
		
		objeto=objeto_var;
		
	}
	
}
