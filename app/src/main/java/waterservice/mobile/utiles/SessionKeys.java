package waterservice.mobile.utiles;

public class SessionKeys {

    public static final String CLIENTE_ACTUAL="CLIENTE_ACTUAL";
    public static final String CLIENTE_INDICE_ACTUAL="CLIENTE_INDICE_ACTUAL";

    public static final String USUARIO_LOGUEADO="USUARIO_LOGUEADO";
    public static final String USUARIO_CONTROLADOR_ACTUAL="USUARIO_CONTROLADOR_ACTUAL";
    public static final String ARTICULOS_CONTROL_PLAYA ="ARTICULOS_CONTROL_PLAYA";

    public static final String TIPO_ARTICULO_AGREGAR ="TIPO_ARTICULO_AGREGAR";

    public static final String ORDEN_DE_TRABAJO_ACTUAL = "ORDEN_DE_TRABAJO_ACTUAL";
    public static final String DISPENSER_ASOCIADO = "DISPENSER_ASOCIADO";
    public static final String ES_REPUESTO = "ES_REPUESTO";
    public static final String REPUESTOSACTIVIDADES = "REPUESTOSACTIVIDADES";
    public static final String DISPENSER_SELECCIONADO = "DISPENSER_SELECCIONADO";
    public static final String ARTICULO_SELECCIONADO = "ARTICULO_SELECCIONADO";

    public static final String VENTA_ACTUAL = "VENTA_ACTUAL";
    public static final String COMPROBANTE_FISICO_CREADO = "COMPROBANTE_FISICO_CREADO";
    public static final String TIPO_COMPROBANTE_FISICO_ID = "TIPO_COMPROBANTE_FISICO_ID";

    public static final String ARTICULOS_DEL_CLIENTE = "ARTICULOS_DEL_CLIENTE";
    public static final String FORMA_DE_PAGO_SELECTED = "FORMA_DE_PAGO_SELECTED";
    public static final String FIRMA_CLIENTE = "FIRMA_CLIENTE";
    public static final String ENVASES_REGISTRABLES_ENTREGADOS = "ENVASES_REGISTRABLES_ENTREGADOS";
    public static final String ENVASES_REGISTRABLES_SELECCIONADOS = "ENVASES_REGISTRABLES_SELECCIONADOS";
    public static final String ENVASES_REGISTRABLES_DEVUELTOS = "ENVASES_REGISTRABLES_DEVUELTOS";
}
