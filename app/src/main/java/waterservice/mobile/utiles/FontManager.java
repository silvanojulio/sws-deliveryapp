package waterservice.mobile.utiles;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by admin on 16/7/16.
 */
public class FontManager {

    public static final String ROOT = "",
            FONTAWESOME = ROOT + "fontawesome_webfont.ttf";

    public static Typeface getTypeface(Context context, String font) {
        return Typeface.createFromAsset(context.getAssets(), font);
    }

    public static void markAsIconContainer(View v, Typeface typeface) {
        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0; i < vg.getChildCount(); i++) {
                View child = vg.getChildAt(i);
                markAsIconContainer(child, typeface);
            }
        } else if (v instanceof TextView) {
            ((TextView) v).setTypeface(typeface);
        }
    }

    public static void maskForFaIcon(Activity activity, int viewId){
        try{
            Typeface iconFont = FontManager.getTypeface(activity.getApplicationContext(), FontManager.FONTAWESOME);
            FontManager.markAsIconContainer(activity.findViewById(viewId), iconFont);
        }catch (Exception ex)
        {
            String ms = ex.getMessage();
        }
    }

    public static void maskForFaIcon(Context context, View v){
        try{
            Typeface iconFont = FontManager.getTypeface(context, FontManager.FONTAWESOME);
            FontManager.markAsIconContainer(v, iconFont);
        }catch (Exception ex)
        {
            String ms = ex.getMessage();
        }
    }
}
