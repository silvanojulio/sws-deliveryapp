package waterservice.mobile.utiles;

import android.content.Context;
import android.os.Environment;
import android.support.v4.content.ContextCompat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ConfiguracionInicial {

	private Context context;
	private String version="1.0.0.1"; //<<<-------------
	private static String KEY_VERSION="CURRENT_VERSION";
	private boolean CopiarBaseDeAssets=false;
	
	//<---- CONFIGURAR ----->
	public static String CARPETA_TRABAJO="WaterServiceApp";
	public static String db_file="db_waterservice.sqlite";

	//<------------------------>

	public String PathBase;
	public String RutaLocalBaseDeDatos;
	public String RutaLocalCarpeta;
	public String RutaLocalCarpetaBackups;

	private String movil_ID;
	private String cliente_ID;
	
	public ConfiguracionInicial(Context _context){
		context=_context;
		PathBase = ContextCompat.getExternalFilesDirs(context, null)[0].getAbsolutePath();
		RutaLocalBaseDeDatos= String.format( PathBase + "/%s/%s", ConfiguracionInicial.CARPETA_TRABAJO, ConfiguracionInicial.db_file);
		RutaLocalCarpeta= String.format( PathBase +"/%s/", ConfiguracionInicial.CARPETA_TRABAJO);
		RutaLocalCarpetaBackups=String.format( PathBase+"/%s/backups/", ConfiguracionInicial.CARPETA_TRABAJO);;
	} 	
	
	public void IniciarAplicacion(String _cliente_ID, String _movil_ID){
		
		movil_ID=_movil_ID;
		cliente_ID=_cliente_ID;				
		
		File folder = new File(Environment.getExternalStorageDirectory() + "/"+CARPETA_TRABAJO);
		boolean success = true;
		if (!folder.exists()) {
			 success = folder.mkdir();
	        if (success) {
				   //Utiles.MostrarMensajeToast(context,"El directorio fue creado");				   
				   try {
						
						copyDataBase() ;
						//Utiles.MostrarMensajeToast(context,"Se ha copiado la DB");
						
					} catch (Exception e) {
						//Utiles.MostrarMensajeToast(context,"No es posible copiar la DB. "+e.getMessage());
					}
				   
				} else {
					//Utiles.MostrarMensajeToast(context,"El directorio no pudo ser creado"); 
				}
		}
		else
			{
			//Utiles.MostrarMensajeToast(context,"El directorio ya existe");		
			  try {					
				  
				  folder.delete();		
				  folder.mkdir();
				  copyDataBase() ;
				  //Utiles.MostrarMensajeToast(context,"Se ha copiado la DB");
					
				} catch (Exception e) {
					//Utiles.MostrarMensajeToast(context,"No es posible copiar la DB. "+e.getMessage());
				}
			}

		Utiles.Set_Config_Value(KEY_VERSION,version, context);
	}
		
    private void copyDataBase() throws IOException{
     	
    	if(!CopiarBaseDeAssets)
    		return;
    	
    	if (db_file.equalsIgnoreCase("")) {
			return;
		}
    	
    	//Open your local db as the input stream
    	InputStream myInput = context.getApplicationContext().getAssets().open(db_file);
 
    	// Path to the just created empty db
    	String outFileName = Environment.getExternalStorageDirectory() + "/"+CARPETA_TRABAJO+"/"+db_file;
 
    	//Open the empty db as the output stream
    	OutputStream myOutput = new FileOutputStream(outFileName);
 
    	//transfer bytes from the inputfile to the outputfile
    	byte[] buffer = new byte[1024];
    	int length;
    	while ((length = myInput.read(buffer))>0){
    		myOutput.write(buffer, 0, length);
    	}
 
    	//Close the streams
    	myOutput.flush();
    	myOutput.close();
    	myInput.close();
 
    }

}
