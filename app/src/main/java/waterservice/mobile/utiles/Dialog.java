package waterservice.mobile.utiles;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.Toast;

import ar.com.waterservice.newapp.R;

public class Dialog {

    public static void confirm(String title, String mensaje, final Dialog.OnConfirm onConfirm, Context context){

        new AlertDialog.Builder(context)
                .setIcon(R.drawable.question)
                .setTitle(title)
                .setMessage(mensaje )
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {

                    //@Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {

                            onConfirm.confirm();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    //@Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {

                            onConfirm.onCancel();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .show();
    }

    public static void promptText(String title, String mensaje, final Dialog.OnConfirmTextInput onConfirm, Context context){

        final EditText txt_text = new EditText(context);

        new AlertDialog.Builder(context)
                .setIcon(R.drawable.question)
                .setTitle(title)
                .setView(txt_text)
                .setMessage(mensaje )
                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {

                    //@Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {

                            String text = txt_text.getText().toString();
                            onConfirm.confirm(text);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

                    //@Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {

                            onConfirm.onCancel();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .show();
    }

    public static void info(String title, String mensaje, Context context){
        showAlert(title, mensaje, context, R.drawable.info);
    }

    public static void error(String title, String mensaje, Context context){
        showAlert(title, mensaje, context, R.drawable.warning_orange_flat);
    }

    public static void toast(String mensaje, Context context){
        Toast.makeText(context, mensaje, Toast.LENGTH_LONG).show();
    }

    public static void success(String title, String mensaje, Context context){
        showAlert(title, mensaje, context, R.drawable.check_icon);
    }

    private static void showAlert(String title, String mensaje, Context context, int iconId){

        new AlertDialog.Builder(context)
                .setIcon(iconId)
                .setTitle(title)
                .setMessage(mensaje )
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    //@Override
                    public void onClick(DialogInterface dialog, int which) {}
                }).show();
    }

    public abstract static class OnConfirm{

        public abstract void confirm();

        public  void onCancel(){

        }
    }

    public abstract static class OnConfirmTextInput{

        public abstract void confirm(String textInput);

        public  void onCancel(){

        }
    }


}