package waterservice.mobile.utiles;


import android.app.Activity;
import android.app.ExpandableListActivity;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.util.concurrent.Callable;

public class AsyncWork {

    public static void run(String method, Object activity, String messageWorking) {

       AsyncTask t = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {

                try {

                    Method m = params[1].getClass().getMethod(String.valueOf(params[0]));
                    m.invoke(params[1],null);

                    Utiles.Stop_Working_Message();
                } catch (Exception e) {
                    Utiles.Stop_Working_Message();

                    String message = e.getMessage()==null && e.getCause() !=null?
                            e.getCause().getMessage():e.getMessage();

                    try{Utiles.Mensaje_En_Pantalla(message, true, true, (Activity) params[1]);}catch (Exception ex){}
                }

                return null;
            }
        };

        if(messageWorking!=null)
            Utiles.Start_Working_Message((Activity) activity,messageWorking);

        t.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, method, activity);

    }

    public static void run(final RunAction runAction, String messageWorking, final Context context) {

        AsyncTask t = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {

                try {

                    runAction.onRunAction();

                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {

                            try{
                                runAction.onActionFinished();
                            }catch (Exception ex){
                                try{
                                    Utiles.Mensaje_En_Pantalla(ex.getMessage(), true, true, context);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        }
                    });

                    Utiles.Stop_Working_Message();

                } catch (Exception e) {
                    Utiles.Stop_Working_Message();
                    String message = e.getMessage()==null && e.getCause() !=null?
                            e.getCause().getMessage():e.getMessage();

                    try{Utiles.Mensaje_En_Pantalla(message, true, true, context);}catch (Exception ex){}
                }

                return null;
            }
        };

        if(messageWorking!=null)
            Utiles.Start_Working_Message(context, messageWorking);

        t.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public interface RunAction{
        void onRunAction() throws Exception;
        void onActionFinished() throws Exception;
    }

}
