package waterservice.mobile.utiles;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.BitSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class Printer {

    private Activity activity;
    private String nombreImpresora;
    private StringBuilder salida;

    public Printer(Activity activity, String nombreImpresora){
        this.activity = activity;
        this.nombreImpresora = nombreImpresora == null ?
                Utiles.Get_Config_Value("impresora_bluetooth", activity):
                nombreImpresora;
    }

    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;

    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;

    private byte[] readBuffer;
    private int readBufferPosition;
    private volatile boolean stopWorker;

    public void findBT() throws Exception {

        if(nombreImpresora==null || nombreImpresora.equals(""))
            throw new Exception("No existe ninguna impresora configurada");

        if(nombreImpresora.equals("test"))
            return;

        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if(mBluetoothAdapter == null) {
               // lbl_totalVenta.setText("No bluetooth adapter available");
            }

            if(!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                activity.startActivityForResult(enableBluetooth, 0);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            if(pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {

                    // RPP300 is the name of the bluetooth printer device
                    // we got this name from the list of paired devices
                    if (device.getName().equals(nombreImpresora)) { //"MTP-II"
                        mmDevice = device;
                        break;
                    }
                }
            }

            //lbl_totalVenta.setText("Bluetooth device found.");

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void openBT() throws IOException {
        try {
            if(nombreImpresora.equals("test"))
                return;

            // Standard SerialPortService ID
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();

            beginListenForData();

            //lbl_totalVenta.setText("Bluetooth Opened");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void beginListenForData() {
        try {
            final Handler handler = new Handler();

            // this is the ASCII code for a newline character
            final byte delimiter = 10;

            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];

            workerThread = new Thread(new Runnable() {
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {

                        try {

                            int bytesAvailable = mmInputStream.available();

                            if (bytesAvailable > 0) {

                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);

                                for (int i = 0; i < bytesAvailable; i++) {

                                    byte b = packetBytes[i];
                                    if (b == delimiter) {

                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );

                                        // specify US-ASCII encoding
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;

                                        // tell the user data were sent to bluetooth printer device
                                        handler.post(new Runnable() {
                                            public void run() {
                                                //lbl_totalVenta.setText(data);
                                            }
                                        });

                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }

                        } catch (IOException ex) {
                            stopWorker = true;
                        }

                    }
                }
            });

            workerThread.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendData() throws IOException {
        try {

            // the text typed by the user
            String msg = salida.toString()
                    .replace('á','a')
                    .replace('é','e')
                    .replace('í','i')
                    .replace('ó','o')
                    .replace('ú','u')
                    .replace('Á','A')
                    .replace('É','E')
                    .replace('Í','I')
                    .replace('Ó','O')
                    .replace('Ú','U')
                    ;
            msg += "\n";

            mmOutputStream.write(msg.getBytes());

            // tell the user data were sent
            //lbl_totalVenta.setText("Data sent.");

        } catch (Exception e) {
            throw e;
        }
    }

    public void printBytes(byte[] bytes) throws IOException {
        try {

            mmOutputStream.write(bytes);

            // tell the user data were sent
            //lbl_totalVenta.setText("Data sent.");

        } catch (Exception e) {
            throw e;
        }
    }

    public void closeBT() throws IOException {
        try {
            if(nombreImpresora.equals("test"))
                return;

            stopWorker = true;
            mmOutputStream.flush();
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
            //lbl_totalVenta.setText("Bluetooth Closed");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void imprimirSalida() throws Exception{
        try {
            if(nombreImpresora.equals("test"))
                return;

            sendData();
        } catch (IOException ex) {
            throw ex;
        }
    }

    private void agregarBytesAArray(List<Byte> bts, byte[] bytes){

        for(int i=0; i<bytes.length; i++){

            bts.add(bytes[i]);
        }
    }

    private  byte[] toByteArray(List<Byte> bts){

        byte[] bytes = new byte[bts.size()];

        for(int i=0; i<bts.size(); i++){

            bytes[i] = bts.get(i);
        }

        return bytes;
    }


    public void imprimirImagen(byte[] bytes) throws Exception{
        try {

            byte[] printformat = new byte[]{0x1B,0x21,0x03};
            mmOutputStream.write(printformat);

            printCustom("Fair Group BD",2,1);

            mmOutputStream.write(PrinterCommands.SELECT_BIT_IMAGE_MODE);
            printText(bytes);

            mmOutputStream.write(PrinterCommands.ESC_ENTER);
            mmOutputStream.write(PrinterCommands.RESET);

            //printText("     >>>>   Thank you  <<<<     "); // total 32 char in a single line

            mmOutputStream.flush();
        } catch (IOException ex) {
            throw ex;
        }
    }


    //print text
    private void printText(String msg) {
        try {
            // Print normal text
            mmOutputStream.write(msg.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    //print custom
    private void printCustom(String msg, int size, int align) {
        //Print config "mode"
        byte[] cc = new byte[]{0x1B,0x21,0x03};  // 0- normal size text
        //byte[] cc1 = new byte[]{0x1B,0x21,0x00};  // 0- normal size text
        byte[] bb = new byte[]{0x1B,0x21,0x08};  // 1- only bold text
        byte[] bb2 = new byte[]{0x1B,0x21,0x20}; // 2- bold with medium text
        byte[] bb3 = new byte[]{0x1B,0x21,0x10}; // 3- bold with large text
        try {
            switch (size){
                case 0:
                    mmOutputStream.write(cc);
                    break;
                case 1:
                    mmOutputStream.write(bb);
                    break;
                case 2:
                    mmOutputStream.write(bb2);
                    break;
                case 3:
                    mmOutputStream.write(bb3);
                    break;
            }

            switch (align){
                case 0:
                    //left align
                    mmOutputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
                    break;
                case 1:
                    //center align
                    mmOutputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                    break;
                case 2:
                    //right align
                    mmOutputStream.write(PrinterCommands.ESC_ALIGN_RIGHT);
                    break;
            }
            mmOutputStream.write(msg.getBytes());
            mmOutputStream.write(PrinterCommands.LF);
            //outputStream.write(cc);
            //printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //print unicode
    public void printUnicode(){
        try {
            mmOutputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
            printText(Utils.UNICODE_TEXT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //print byte[]
    private void printText(byte[] msg) {
        try {
            // Print normal text
            mmOutputStream.write(msg);
            printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //print new line
    private void printNewLine() {
        try {
            mmOutputStream.write(PrinterCommands.FEED_LINE);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void iniciarSalida(){
        salida = new StringBuilder();
    }

    public void agregarLinea(String linea){

        salida.append(linea + "\n");

    }

    public void agregarSeparador(){
        salida.append("===============================\n");
    }

    public void agregarSeparadorLinea(){
        salida.append("_______________________________\n");
    }

    public void agregarEspacio(){
        salida.append(" \n");
    }

    public static class PrinterCommands {
        public static final byte HT = 0x9;
        public static final byte LF = 0x0A;
        public static final byte CR = 0x0D;
        public static final byte ESC = 0x1B;
        public static final byte DLE = 0x10;
        public static final byte GS = 0x1D;
        public static final byte FS = 0x1C;
        public static final byte STX = 0x02;
        public static final byte US = 0x1F;
        public static final byte CAN = 0x18;
        public static final byte CLR = 0x0C;
        public static final byte EOT = 0x04;

        public static final byte[] INIT = {27, 64};
        public static byte[] FEED_LINE = {10};

        public static byte[] SELECT_FONT_A = {20, 33, 0};

        public static byte[] SET_BAR_CODE_HEIGHT = {29, 104, 100};
        public static byte[] PRINT_BAR_CODE_1 = {29, 107, 2};
        public static byte[] SEND_NULL_BYTE = {0x00};

        public static byte[] SELECT_PRINT_SHEET = {0x1B, 0x63, 0x30, 0x02};
        public static byte[] FEED_PAPER_AND_CUT = {0x1D, 0x56, 66, 0x00};

        public static byte[] SELECT_CYRILLIC_CHARACTER_CODE_TABLE = {0x1B, 0x74, 0x11};

        public static byte[] SELECT_BIT_IMAGE_MODE = {0x1B, 0x2A, 33, 124, 1};
        public static byte[] SET_LINE_SPACING_24 = {0x1B, 0x33, 24};
        public static byte[] SET_LINE_SPACING_30 = {0x1B, 0x33, 30};

        public static byte[] TRANSMIT_DLE_PRINTER_STATUS = {0x10, 0x04, 0x01};
        public static byte[] TRANSMIT_DLE_OFFLINE_PRINTER_STATUS = {0x10, 0x04, 0x02};
        public static byte[] TRANSMIT_DLE_ERROR_STATUS = {0x10, 0x04, 0x03};
        public static byte[] TRANSMIT_DLE_ROLL_PAPER_SENSOR_STATUS = {0x10, 0x04, 0x04};

        public static final byte[] ESC_FONT_COLOR_DEFAULT = new byte[] { 0x1B, 'r',0x00 };
        public static final byte[] FS_FONT_ALIGN = new byte[] { 0x1C, 0x21, 1, 0x1B,
                0x21, 1 };
        public static final byte[] ESC_ALIGN_LEFT = new byte[] { 0x1b, 'a', 0x00 };
        public static final byte[] ESC_ALIGN_RIGHT = new byte[] { 0x1b, 'a', 0x02 };
        public static final byte[] ESC_ALIGN_CENTER = new byte[] { 0x1b, 'a', 0x01 };
        public static final byte[] ESC_CANCEL_BOLD = new byte[] { 0x1B, 0x45, 0 };


        /*********************************************/
        public static final byte[] ESC_HORIZONTAL_CENTERS = new byte[] { 0x1B, 0x44, 20, 28, 00};
        public static final byte[] ESC_CANCLE_HORIZONTAL_CENTERS = new byte[] { 0x1B, 0x44, 00 };
        /*********************************************/

        public static final byte[] ESC_ENTER = new byte[] { 0x1B, 0x4A, 0x40 };
        public static final byte[] PRINTE_TEST = new byte[] { 0x1D, 0x28, 0x41 };
        public static final byte[] RESET = new byte[]{0x1b, 0x40};

    }

    public void print_image(Bitmap bmp) throws IOException {

        convertBitmap(bmp);
        mmOutputStream.write(PrinterCommands.SET_LINE_SPACING_24);

        int offset = 0;
        while (offset < bmp.getHeight()) {
            mmOutputStream.write(PrinterCommands.SELECT_BIT_IMAGE_MODE);
            for (int x = 0; x < bmp.getWidth(); ++x) {

                for (int k = 0; k < 3; ++k) {

                    byte slice = 0;
                    for (int b = 0; b < 8; ++b) {
                        int y = (((offset / 8) + k) * 8) + b;
                        int i = (y * bmp.getWidth()) + x;
                        boolean v = false;
                        if (i < dots.length()) {
                            v = dots.get(i);
                        }
                        slice |= (byte) ((v ? 1 : 0) << (7 - b));
                    }
                    mmOutputStream.write(slice);
                }
            }
            offset += 24;
            mmOutputStream.write(PrinterCommands.FEED_LINE);

        }
        mmOutputStream.write(PrinterCommands.SET_LINE_SPACING_30);
    }

    BitSet dots;
    private int mWidth=380,mHeight=380;
    String mStatus;

    public String convertBitmap(Bitmap inputBitmap) {

        mWidth = inputBitmap.getWidth();
        mHeight = inputBitmap.getHeight();

        convertArgbToGrayscale(inputBitmap, mWidth, mHeight);
        mStatus = "ok";
        return mStatus;

    }

    public String obtenerContenido(){
        return this.salida.toString();
    }

    private void convertArgbToGrayscale(Bitmap bmpOriginal, int width,
                                        int height) {
        int pixel;
        int k = 0;
        int B = 0, G = 0, R = 0;
        dots = new BitSet();
        try {

            for (int x = 0; x < height; x++) {
                for (int y = 0; y < width; y++) {
                    // get one pixel color
                    pixel = bmpOriginal.getPixel(y, x);

                    // retrieve color of all channels
                    R = Color.red(pixel);
                    G = Color.green(pixel);
                    B = Color.blue(pixel);
                    // take conversion up to one single value by calculating
                    // pixel intensity.
                    R = G = B = (int) (0.299 * R + 0.587 * G + 0.114 * B);
                    // set bit into bitset, by calculating the pixel's luma
                    if (R < 55) {
                        dots.set(k);//this is the bitset that i'm printing
                    }
                    k++;

                }


            }


        } catch (Exception e) {

        }
    }



}

