package waterservice.mobile.utiles;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class HttpJsonClient {

    public static JSONObject postJson(String urlString, JSONObject obj, List<HttpHeaderValue> headers) throws Exception{

        String requestBody = obj.toString();
        URL url = new URL(urlString);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setDoOutput(true);
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/json");

        if(headers!=null)
            for (HttpHeaderValue header:headers) {
                urlConnection.setRequestProperty(header.key, header.value);
            }

        OutputStream outputStream = new BufferedOutputStream(urlConnection.getOutputStream());
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "utf-8"));
        writer.write(requestBody);
        writer.flush();
        writer.close();
        outputStream.close();

        InputStream inputStream;
        // get stream
        if (urlConnection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
            inputStream = urlConnection.getInputStream();
        } else {
            inputStream = urlConnection.getErrorStream();
        }
        // parse stream
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String temp, response = "";
        while ((temp = bufferedReader.readLine()) != null) {
            response += temp;
        }

        JSONObject jsonResponse = new JSONObject(response);

        return jsonResponse;
    }

    public static JSONObject getJson(String urlString, List<HttpHeaderValue> headers) throws Exception{

        URL url = new URL(urlString);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.setRequestProperty("Content-Type", "application/json");

        if(headers!=null)
            for (HttpHeaderValue header:headers) {
                urlConnection.setRequestProperty(header.key, header.value);
            }

        InputStream inputStream;
        // get stream
        if (urlConnection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
            inputStream = urlConnection.getInputStream();
        } else {
            inputStream = urlConnection.getErrorStream();
        }
        // parse stream
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String temp, response = "";
        while ((temp = bufferedReader.readLine()) != null) {
            response += temp;
        }

        JSONObject jsonResponse = new JSONObject(response);

        return jsonResponse;
    }

    static String crlf = "\r\n";
    static String twoHyphens = "--";
    static String boundary =  "*****";

    public static JSONObject postJonAndFile(String urlString, File file, JSONObject obj, List<HttpHeaderValue> headers) throws Exception {

        HttpURLConnection httpUrlConnection = null;
        URL url = new URL(urlString);
        httpUrlConnection = (HttpURLConnection) url.openConnection();
        httpUrlConnection.setUseCaches(false);
        httpUrlConnection.setDoOutput(true);

        httpUrlConnection.setRequestMethod("POST");
        httpUrlConnection.setRequestProperty("Connection", "Keep-Alive");
        httpUrlConnection.setRequestProperty("Cache-Control", "no-cache");
        httpUrlConnection.setRequestProperty(
                "Content-Type", "multipart/form-data;boundary=" + boundary);

        DataOutputStream request = new DataOutputStream(
                httpUrlConnection.getOutputStream());

        byte[] datos= Utiles.getBytesFromFile(file);
        request.write(datos);

        request.flush();
        request.close();

        InputStream responseStream = new
                BufferedInputStream(httpUrlConnection.getInputStream());

        BufferedReader responseStreamReader =
                new BufferedReader(new InputStreamReader(responseStream));

        String line = "";
        StringBuilder stringBuilder = new StringBuilder();

        while ((line = responseStreamReader.readLine()) != null) {
            stringBuilder.append(line).append("\n");
        }
        responseStreamReader.close();

        String response = stringBuilder.toString();
        responseStream.close();
        httpUrlConnection.disconnect();

        JSONObject jsonResponse = new JSONObject(response);

        return jsonResponse;
    }

}