package waterservice.mobile.utiles;

import android.content.Context;
import java.util.ArrayList;

public class SessionVars {

    private static ArrayList<Objeto_Var> objetos;

    public static void Add_Session_Var(String key_var, Object objeto_var) {

        if(objetos==null){
            objetos=new ArrayList<Objeto_Var>();
        }

        for(int i = 0; i<objetos.size(); i++){
            if(objetos.get(i).Get_Nombre().contentEquals(key_var)){
                objetos.get(i).Set_Objeto_Var(key_var,  objeto_var);
                return;
            }
        }

        objetos.add(new Objeto_Var(key_var,objeto_var));

    }

    public static Object Get_Session_Var(String key_var) {

        if(objetos==null){
            objetos=new ArrayList<Objeto_Var>();
        }

        for(int i = 0; i<objetos.size(); i++){
            if(objetos.get(i).Get_Nombre().contentEquals(key_var)){

                return objetos.get(i).Get_Objeto();
            }
        }

        return null;
    }

    public static int GetSessionVarInt(String key, int defaultValue){
        try{
            return Integer.valueOf(String.valueOf(Get_Session_Var(key)));
        }catch (Exception ex){
            return defaultValue;
        }
    }

    public static void Remove_Session_Var(String key_var){

        if(objetos==null){
            objetos=new ArrayList<Objeto_Var>();
        }

        for(int i = 0; i<objetos.size(); i++){

            if(objetos.get(i).Get_Nombre().contentEquals(key_var)){

                objetos.remove(i);
                return;
            }
        }

    }

}
