package waterservice.mobile.utiles;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import okhttp3.ResponseBody;
import waterservice.mobile.Session.AppSession;

import static com.google.android.gms.internal.zzs.TAG;

public class BaseDeDatos {

	private static String basePath="";//"/sdcard/APPL_INSA/db_insa_tec.sqlite";
	private static String basePathFolder="";//"/sdcard/APPL_INSA";
	private static String basePathFolderBackups="";//"/sdcard/APPL_INSA";
	private static boolean procesando=false;
	private static String urlRoot;

	public static String KEY_URL_SERVER="servidor_web";
	public static String KEY_URL_SERVER_LOCAL="servidor_web_local";
	public static String KEY_SERVER_LOCAL_MODE="modo_local";
	
	static void ObtenerDatosConexion(Context context) throws Exception{

		ConfiguracionInicial config = new ConfiguracionInicial(context);

		basePath=config.RutaLocalBaseDeDatos;
		basePathFolder=config.RutaLocalCarpeta;
		basePathFolderBackups=config.RutaLocalCarpetaBackups;
		urlRoot = new AppSession(context).getValueString(KEY_URL_SERVER);
	}

    public static void EnviarBase(long hojaDeRutaId, Context context) throws Exception{
        try {

            if(!procesando)
            {
                procesando=true;
                ObtenerDatosConexion(context);

                String fecha=Utiles.Get_Fecha();

                String url = new AppSession(context).getCurrentUrlForLocal()+"/HojasDeRuta/SubirBaseDeDatos?hojaDeRutaId="+String.valueOf(hojaDeRutaId);
                File file = new File( basePath);

                JSONObject resp = HttpJsonClient.postJonAndFile(url, file, null, null);

                procesando=false;

                Utiles.Set_Config_Value("ultima_subida",fecha, context);
            }

        } catch (Exception e) {
            procesando=false;
            throw e;
        }
    }

	public static void EnviarBaseBackup(long hojaDeRutaId, String codigoDeMovil, Context context) throws Exception{
		try {

			ObtenerDatosConexion(context);

			String url =  new AppSession(context).getCurrentUrlForLocal()+"/Sync/SubirBaseDeDatos?" +
					"hojaDeRutaId="+String.valueOf(hojaDeRutaId)+
					"&codigoMovil="+codigoDeMovil;

			File file = new File(basePath);

			JSONObject resp = HttpJsonClient.postJonAndFile(url, file, null, null);

			if(resp.getInt("error")!=0)
				throw new Exception(resp.getString("message"));

		} catch (Exception e) {
			procesando=false;
			throw e;
		}
	}

	public static void RecibirBase(String codigoIdentificacionMovil, String fecha, Context context)
			throws Exception{

		try {

			if(!procesando)
			{
				procesando=true;
				ObtenerDatosConexion(context);
				final Context _context= context;

				File folder = new File(basePathFolder);
				if (!folder.exists() && !folder.mkdirs()) {
					throw new Exception("No es posible crear la carpeta en el móvil");
				}

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
					URL url = new URL( new AppSession(context).getCurrentUrlForLocal()+"/HojasDeRuta/DescargarBaseDeDatos?fecha="+fecha+
							"&codigoIdentificacionMovil="+codigoIdentificacionMovil+"&version="+ String.valueOf(43));
					InputStream inputStream = url.openStream();

					createBackupBeforeDownload(basePath, basePathFolderBackups); //Si existe un archivo anterior le hace backup

					Files.copy(inputStream, Paths.get(basePath),
							StandardCopyOption.REPLACE_EXISTING);
				}else{
					RecibirBaseOldVersion(codigoIdentificacionMovil, fecha, context);
				}

			}

		}
		catch (Exception e) {
			throw e;
		}
		finally{
			procesando=false;
		}
	}

	static boolean writeResponseBodyToDisk(ResponseBody body) {
		try {
			// todo change the file location/name according to your needs
			File databaseFile = new File(basePath);

			InputStream inputStream = null;
			OutputStream outputStream = null;

			try {
				byte[] fileReader = new byte[4096];

				long fileSize = body.contentLength();
				long fileSizeDownloaded = 0;

				inputStream = body.byteStream();
				outputStream = new FileOutputStream(databaseFile);

				while (true) {
					int read = inputStream.read(fileReader);

					if (read == -1) {
						break;
					}

					outputStream.write(fileReader, 0, read);

					fileSizeDownloaded += read;

					Log.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
				}

				outputStream.flush();

				return true;
			} catch (IOException e) {
				return false;
			} finally {
				if (inputStream != null) {
					inputStream.close();
				}

				if (outputStream != null) {
					outputStream.close();
				}
			}
		} catch (IOException e) {
			return false;
		}
	}

	public static void RecibirBaseOldVersion(String codigoIdentificacionMovil, String fecha, Context context) throws Exception{

		File folder = new File(basePathFolder);
		if (!folder.exists()) {
			boolean success = folder.mkdirs();
			int i = 0;
			i++;
		}

		InputStream input = null;
		OutputStream output = null;
		HttpURLConnection connection = null;

		try{

			URL url = new URL( new AppSession(context).getCurrentUrlForLocal()+"/HojasDeRuta/DescargarBaseDeDatos?fecha="+fecha+
					"&codigoIdentificacionMovil="+codigoIdentificacionMovil+"&version="+ String.valueOf(42));

			connection = (HttpURLConnection) url.openConnection();
			connection.connect();

			if (connection.getResponseCode() == HttpURLConnection.HTTP_NOT_ACCEPTABLE) {
				throw new Exception("Debe descargar la nueva versión de la app");
			}

			// expect HTTP 200 OK, so we don't mistakenly save error report
			// instead of the file
			if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new Exception("Error al conectarse al servidor");
			}

			// this will be useful to display download percentage
			// might be -1: server did not report the length
			int fileLength = connection.getContentLength();

			createBackupBeforeDownload(basePath, basePathFolderBackups); //Si existe un archivo anterior le hace backup

			// download the file
			input = connection.getInputStream();
			output = new FileOutputStream(basePath);

			byte data[] = new byte[4096];
			long total = 0;
			int count;
			while ((count = input.read(data)) != -1) {

				total += count;
				output.write(data, 0, count);
			}

			Utiles.Set_Config_Value("ultima_bajada", Utiles.Get_Fecha(), context);

		}catch (Exception ex){
			throw ex;
		}finally {
			procesando=false;

			try {
				if (output != null)
					output.close();
				if (input != null)
					input.close();
			} catch (IOException ignored) {}

			if (connection != null)
				connection.disconnect();
		}
	}

	private static void createBackupBeforeDownload(String basePath, String basePathFolderBackups) {

		try {

			File folderBackup = new File(basePathFolderBackups);
			if (!folderBackup.exists()) {
				boolean success = folderBackup.mkdir();
			}

			File currentDatabase = new File(basePath);

			if (!currentDatabase.exists()) return;

			Utiles.copyFile(basePath, basePathFolderBackups + Utiles.getStampMark()+".sqlite");
			Utiles.deleteOldFiles(basePathFolderBackups, 8);

			if (currentDatabase.exists()){
				boolean wasDeleted = currentDatabase.delete();
				if(wasDeleted){
					int a = 0;
					a++;
				}
			}

		}catch (Exception ex){
			ex.printStackTrace();
		}
	}


	static public String UltimaSubida(Context context){

		try {
			return new AppSession( context).getValueString( "ultima_subida");
		} catch (Exception e) {
			return "--/--/--";
		}
	}
	
	static public String UltimaBajada(Context context){
		
		try {			
			return  new AppSession( context).getValueString("ultima_bajada");
		} catch (Exception e) {
			return "--/--/--";
		}
	}

	static public String obtenerPathArchivoBaseDeDatos(Context context) throws Exception {
		ObtenerDatosConexion(context);
		return basePath;
	}

	static public long obtenerTamañoDeBaseDeDatos(Context cxt) throws Exception {

		File baseDeDatos = new File(obtenerPathArchivoBaseDeDatos(cxt));
		return baseDeDatos.length();
	}
}
