package waterservice.mobile.clases;

import org.json.JSONException;
import org.json.JSONObject;

public class ArticuloDePlanificacionDeCarga {

    public ArticuloDePlanificacionDeCarga(){

    }

    public ArticuloDePlanificacionDeCarga(JSONObject object) throws JSONException {

        this.articuloId = object.getInt("articulo_id");
        this.cantidad = object.getInt("cantidadFinal");
        this.codigoInterno = object.getString("codigoInterno");
        this.nombreArticulo = object.getString("nombreArticulo");
        this.peso = object.getDouble("pesoKilos");
    }

    public int articuloId;
    public String codigoInterno;
    public String nombreArticulo;
    public int cantidad;
    public Double peso;
}
