package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ListasDePrecios")
public class ListaDePrecios {

    @DatabaseField( generatedId = true)
    public int id;
    @DatabaseField public int listaId;
    @DatabaseField public int articuloId;
    @DatabaseField public double precio;

}
