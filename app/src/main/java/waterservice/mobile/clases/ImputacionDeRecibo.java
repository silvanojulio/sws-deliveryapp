package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ImputacionesDeRecibos")
public class ImputacionDeRecibo{

    @DatabaseField( generatedId = true)
    public int id;
    @DatabaseField
    public int reciboId;
    @DatabaseField
    public int facturaId;
    @DatabaseField
    public double monto;

}
