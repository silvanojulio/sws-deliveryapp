package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.List;

@DatabaseTable(tableName = "AlertasDeCliente")
public class AlertaDeCliente {


    @DatabaseField( generatedId = true)
    public int id;

    @DatabaseField
    public int cliente_id;

    @DatabaseField
    public int tipoDeAlerta_ids;

    @DatabaseField
    public String tipoDeAlerta;

    @DatabaseField
    public String comentarios;

    @DatabaseField
    public Boolean esBloqueante;

    public static boolean tieneBloqueantes(List<AlertaDeCliente> alertas){

        boolean tiene=false;
        for (AlertaDeCliente al: alertas) {
            if(al.esBloqueante) {
                tiene=true;
                return tiene;
            }
        }
        return tiene;
    }

    public static boolean tienePreventivo(List<AlertaDeCliente> alertas){

        boolean tiene=false;
        for (AlertaDeCliente al: alertas) {
            if(al.tipoDeAlerta_ids == 3 || al.tipoDeAlerta_ids == 4) {
                tiene=true;
                return tiene;
            }
        }
        return tiene;
    }

    public static List<AlertaDeCliente> filtrarPorCliente(List<AlertaDeCliente> alertasTodas, int clienteId){

        List<AlertaDeCliente> alertas=new ArrayList<>();

        for (AlertaDeCliente al: alertasTodas) {
            if(al.cliente_id == clienteId) {
                alertas.add(al);
            }
        }

        return alertas;
    }
}
