package waterservice.mobile.clases;


import java.util.List;

public class ControlDePlaya {

    public long hojaDeRutaId;
    public int usuarioControlaId;
    public int usuarioApruebaId;
    public int tipoEventoId;
    public int kilometros;
    public List<ArticuloControlDePlaya> articulos;
    public List<Integer> dispensersIds;

}
