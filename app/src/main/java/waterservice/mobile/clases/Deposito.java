package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "Depositos")
public class Deposito {

    @DatabaseField( generatedId = true)
    public int deposito_id;
    @DatabaseField
    public String nroDeComprobante;
    @DatabaseField
    public int banco_id;
    @DatabaseField
    public double importe;
    @DatabaseField
    public Date fechaDeDeposito;
    @DatabaseField
    public int cliente_id;

}
