package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Retenciones")
public class Retencion {

    @DatabaseField( generatedId = true)
    public int id;
    @DatabaseField
    public int reciboId;
    @DatabaseField
    public int tipoRetencionId;
    @DatabaseField
    public String descripcion;
    @DatabaseField
    public double importe;
    @DatabaseField
    public String fecha;

    public String descripcionResumen;

}
