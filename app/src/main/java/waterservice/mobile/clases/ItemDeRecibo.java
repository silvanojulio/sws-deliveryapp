package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ItemsDeRecibos")
public class ItemDeRecibo {

    @DatabaseField( generatedId = true)
    public int id;
    @DatabaseField
    public int reciboId;
    @DatabaseField
    public Double importe;
    @DatabaseField
    public int formaDePagoId;
    @DatabaseField
    public String descripcion;

    @DatabaseField
    public Integer chequeId;
    @DatabaseField
    public Integer tarjetaDeCreditoId;
    @DatabaseField
    public Integer tarjetaDeDebitoId;
    @DatabaseField
    public Integer retencionId;

    public String getFormaDePago(){

        if(formaDePagoId == Constantes.FORMADEPAGO_RETENCIONES)
            return "Retención";
        else if(formaDePagoId == Constantes.FORMADEPAGO_TARJETACREDITO)
            return "Tarjeta de crédito";
        else if(formaDePagoId == Constantes.FORMADEPAGO_TARJETADEBITO)
            return "Tarjeta de débito";
        else if(formaDePagoId == Constantes.FORMADEPAGO_EFECTIVO)
            return "Efectivo";
        else if(formaDePagoId == Constantes.FORMADEPAGO_CHEQUE)
            return "Cheque";
        else
            return "";
    }

}


