package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ImputacionesFacturas")
public class ImputacionFactura {

    @DatabaseField( generatedId = true)
    public int id;

    @DatabaseField
    public int reciboId;

    @DatabaseField
    public Double importe;

    @DatabaseField
    public int facturaId;
}
