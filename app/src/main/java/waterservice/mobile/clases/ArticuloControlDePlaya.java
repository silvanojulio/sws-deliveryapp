package waterservice.mobile.clases;

public class ArticuloControlDePlaya {

    public int articulo_id;
    public int dispenser_id;

    public int cantidadVaciosPack;
    public int cantidadLlenosPack;
    public int cantidadFalladosPack;

    public int cantidadVaciosUni;
    public int cantidadLlenosUni;
    public int cantidadFalladosUni;

    public int cantidadVacios;
    public int cantidadLlenos;
    public int cantidadFallados;

    public ArticuloDeLista articulo;
    public Dispenser dispenser;

    public void generarCantidadesFinales(){

        if(articulo.cantidadPorPack==0)
            articulo.cantidadPorPack =1;

        cantidadVacios = cantidadVaciosPack * articulo.cantidadPorPack + cantidadVaciosUni;
        cantidadFallados = cantidadFalladosPack * articulo.cantidadPorPack + cantidadFalladosUni;
        cantidadLlenos = cantidadLlenosPack * articulo.cantidadPorPack + cantidadLlenosUni;
    }
}
