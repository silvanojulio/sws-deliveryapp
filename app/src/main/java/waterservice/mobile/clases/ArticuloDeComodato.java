package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ArticulosDeComodatos")
public class ArticuloDeComodato {

    @DatabaseField(generatedId=true)
    public int id;
    @DatabaseField public int articulo_id;
    @DatabaseField public int cliente_id;
    @DatabaseField public int cantidad;
    @DatabaseField public double precio;
}
