package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.types.StringBytesType;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "OrdenesDeTrabajo")
public class OrdenDeTrabajo {
    @DatabaseField(generatedId = true)
    public long id;

    @DatabaseField public Long ordenDeTrabajoServer_id;
    @DatabaseField public int cliente_id;
    @DatabaseField public int sintoma_id;
    @DatabaseField public String sintoma;
    @DatabaseField public String prioridad;
    @DatabaseField public String comentarios;
    @DatabaseField public String cantDispensers;
    @DatabaseField public String sectorUbicacion;
    @DatabaseField public String responsableEnCliente;
    @DatabaseField public String telefonoResponsable;
    @DatabaseField public Integer motivoDeCierreId;
    @DatabaseField public String franjaHoraria;
    @DatabaseField public Date fechaHoraTransferido;
    @DatabaseField public Integer usuarioCierra_id;
    @DatabaseField public Long nroRemitoFisico;
    @DatabaseField public Long nroRemitoFisicoPrefijo;

}
