package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "TarjetasDeCredito")
public class TarjetaDeCredito {

    @DatabaseField( generatedId = true)
    public int id;
    @DatabaseField
    public int reciboId;
    @DatabaseField
    public String lote;
    @DatabaseField
    public String cupon;
    @DatabaseField
    public String codigoAutorizacion;
    @DatabaseField
    public int marcaTarjeta_id;
    @DatabaseField
    public int banco_id;
    @DatabaseField
    public double importe;
    @DatabaseField
    public int cuotas;

    public String descripcionResumen;

}
