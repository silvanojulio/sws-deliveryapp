package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;
import java.util.List;

@DatabaseTable(tableName = "Clientes")
public class Cliente {

    public Cliente(){

    }

    @DatabaseField( id = true)
    public int cliente_id;

    @DatabaseField public int clienteFactura_id;
    @DatabaseField public String nombreCliente;
    @DatabaseField public int tipoCliente_ids;
    @DatabaseField public int estadoCliente_ids;
    @DatabaseField public String nombreProvincia;
    @DatabaseField public String nombreCiudad;
    @DatabaseField public String nombreBarrio;
    @DatabaseField public String domicilioCompleto;
    @DatabaseField public String torre;
    @DatabaseField public String piso;
    @DatabaseField public String depto;
    @DatabaseField public String manzana;
    @DatabaseField public String lote;
    @DatabaseField public String numeroPuerta;
    @DatabaseField public String nombreCalle;
    @DatabaseField public String actividadCliente;
    @DatabaseField public String fechaIngreso;
    @DatabaseField public String altitud;
    @DatabaseField public String longitud;
    @DatabaseField public String fechaUtlimaEntrega;
    @DatabaseField public String fechaUltimoCobroFactura;
    @DatabaseField public String fechaUltimaEnvases;
    @DatabaseField public String fechaUltimaDevoluciones;
    @DatabaseField public int orden;
    @DatabaseField public Boolean esVentaExtra;
    @DatabaseField public double saldoFacturacion;
    @DatabaseField public double saldoConsumos;
    @DatabaseField public Boolean permiteVender;
    @DatabaseField public Boolean permiteCobrar;
    @DatabaseField public Boolean permitePrestarEnvases;
    @DatabaseField public Boolean enviarSMS;
    @DatabaseField public Boolean esPedido;
    @DatabaseField public String comunicacion1;
    @DatabaseField public String comunicacion2;
    @DatabaseField public String comunicacion3;
    @DatabaseField public String comunicacion4;
    @DatabaseField public double creditoDisponible;
    @DatabaseField public Boolean visitado;
    @DatabaseField public Boolean ausente;
    @DatabaseField public Boolean ventaEntrega;
    @DatabaseField public Boolean cobroConsumo;
    @DatabaseField public Boolean cobroFactura;
    @DatabaseField public Boolean devolucionEnvases;
    @DatabaseField public Boolean prestamoEnvases;
    @DatabaseField public Boolean devolucionArticulo;
    @DatabaseField public String visita_altitud;
    @DatabaseField public String visita_longitud;
    @DatabaseField public String visita_fechaHora;
    @DatabaseField public Boolean pendiente= false;
    @DatabaseField public Boolean descargado= false;
    @DatabaseField public double saldoNotasDeCredito;
    @DatabaseField public int tipoDeVisitaId;
    @DatabaseField public Boolean relevarCoordenadas;
    @DatabaseField public String relevamientoCoordenadas_latitud;
    @DatabaseField public String relevamientoCoordenadas_longitud;
    @DatabaseField public Boolean tieneAlertas= false;
    @DatabaseField public Boolean haVistoAlertas= false;
    @DatabaseField public Boolean requiereRemito= false;
    @DatabaseField public Integer listaDePrecioId;
    @DatabaseField public Boolean esSync;
    @DatabaseField public Boolean esRepaso;
    @DatabaseField public boolean cerrado;
    @DatabaseField public Date fechaHoraTransferido;
    @DatabaseField public Boolean desbloqueado;
    @DatabaseField public Double totalEntregadoActual;
    @DatabaseField(readOnly = true) public String formaPagoHabitual;

    public boolean esCuentaBloqueada;
    public boolean esCuentaPreventiva;

    public List<ArticuloDeComodato> articulosComodatos;
    public List<ArticuloDeAbono> articulosAbonos;
    public List<ArticuloDeLista> articulosLista;

    public Double obtenerSaldoAlDia() {

        return this.saldoConsumos + this.saldoFacturacion;
    }

    public Double obtenerTotalVentaActual() {

        return this.totalEntregadoActual != null? this.totalEntregadoActual : 0;
    }

    public boolean esResumenDeCliente;
    public int resumenDeClienteResultadoBusquedaTipoId; //1- Por nombre. 2- Por domicilio
    public String reparto;
    public String tipoCliente;
}