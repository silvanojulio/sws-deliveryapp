package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ArticulosDeAbonos")
public class ArticuloDeAbono {

    @DatabaseField(generatedId=true)
    public int id;

    @DatabaseField public int articulo_id;
    @DatabaseField public int cliente_id;
    @DatabaseField public String vigenciaDesde;
    @DatabaseField public String vigenciaHasta;
    @DatabaseField public int cantidad;
    @DatabaseField public double precioExcendente;

}


