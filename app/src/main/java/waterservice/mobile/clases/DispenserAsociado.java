package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.types.StringBytesType;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "DispensersAsociados")
public class DispenserAsociado {

    @DatabaseField(generatedId = true)
    public long id;

    @DatabaseField public Long OrdenDeTrabajoServer_id;
    @DatabaseField public String nroDispenser;
    @DatabaseField public Integer dispenser_id;
    @DatabaseField public boolean deCliente;
    @DatabaseField public Long ordenDeTrabajoId;
    @DatabaseField public Boolean mantemientoCargado;
}

