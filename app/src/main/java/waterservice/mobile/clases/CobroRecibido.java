package waterservice.mobile.clases;


import java.util.ArrayList;
import java.util.List;

public class CobroRecibido {
    public Double monto=0.0;
    public int formaDePagoId;
    public int cheque_id;
    public int deposito_id;

    public Deposito deposito;
    public Cheque cheque;

    public int posicionSeleccionadaFormaDePago;

    public static List<CobroRecibido> Clonar(List<CobroRecibido> cobrosRecibidos){

        if(cobrosRecibidos==null)
            cobrosRecibidos=new ArrayList<>();

        List<CobroRecibido> lista = new ArrayList<>();

        for (CobroRecibido cobro:cobrosRecibidos) {
            lista.add(Clonar(cobro));
        }

        return lista;
    }

    public static CobroRecibido Clonar(CobroRecibido cobroRecibido){

        if(cobroRecibido==null)
            return null;

        CobroRecibido item = new CobroRecibido();
        item.cheque = cobroRecibido.cheque;
        item.deposito = cobroRecibido.deposito;
        item.monto = cobroRecibido.monto;
        item.formaDePagoId = cobroRecibido.formaDePagoId;
        item.posicionSeleccionadaFormaDePago = cobroRecibido.posicionSeleccionadaFormaDePago;
        item.cheque_id = cobroRecibido.cheque_id;
        item.deposito_id = cobroRecibido.deposito_id;

        return item;
    }
}
