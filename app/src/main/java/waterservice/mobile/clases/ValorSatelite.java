package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ValoresSatelites")
public class ValorSatelite {

    @DatabaseField(generatedId = true)
    public long id;

    @DatabaseField public int tabla_id;
    @DatabaseField public int valor_id;
    @DatabaseField public String valor_texto;
}
