package waterservice.mobile.clases;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import waterservice.mobile.adapters.ArticuloParaSeleccionar;

@DatabaseTable(tableName = "ArticulosEntregados")
public class ArticuloEntregado {

    @DatabaseField( generatedId = true)
    public int id;

    @DatabaseField public String nombreDelArticulo;
    @DatabaseField public String codigoArticulo;
    @DatabaseField public int articulo_id=0;
    @DatabaseField public double cantidadEntregada=0;
    @DatabaseField public int cantidadEnvasesRelevados=0;
    @DatabaseField public int cantidadEnvasesPrestados=0;
    @DatabaseField public int cantidadEnvasesDevueltos=0;
    @DatabaseField public int motivoPrestamoDevolucionId=0;
    @DatabaseField public int envasesEnCliente=0;
    @DatabaseField public int envasesPermitidos=0;
    @DatabaseField public int envasesARecuperar=0;
    @DatabaseField public Double precioUnitario=0.0;
    @DatabaseField public boolean esRetornable;
    @DatabaseField public double cantidadDisponibles= 0.0;
    @DatabaseField public double descuentoPorCantidad=0.0;
    @DatabaseField public double descuentoManual=0.0;
    @DatabaseField public double subtotal=0.0;
    @DatabaseField public boolean esPredeterminado=false;
    @DatabaseField public int orden=0;
    @DatabaseField public int clienteId=0;

    public boolean verMas=false;

    public String error = "";

    public ArticuloEntregado(){}

    public ArticuloEntregado(ArticuloParaSeleccionar articulo, StockDeCliente stock){

        this.articulo_id = articulo.articuloId;
        this.nombreDelArticulo = articulo.nombreArticulo;
        this.codigoArticulo = articulo.codigoInterno;
        this.precioUnitario = articulo.precio;
        this.cantidadDisponibles =  articulo.cantidadDisponiblePorAbono;
        this.esRetornable = articulo.esRetornable;

        if(stock!=null){
            envasesARecuperar = stock.cantidadARecuperar;
            envasesEnCliente = stock.stockActual;
            envasesPermitidos = stock.cantidadPermitida;
        }
    }

    public double obtenerCantidadACobrar(){
        if(cantidadDisponibles >= cantidadEntregada)
            return 0;
        else
            return cantidadEntregada - cantidadDisponibles;
    }

    public void validar(){

        error = "";

        if(cantidadEnvasesDevueltos == 0 && cantidadEnvasesPrestados == 0 &&
                cantidadEnvasesPrestados == 0 && cantidadEntregada ==0)
        {
            error = "No posee ninguna gestión con este artículo";
            return;
        }

        if(esRetornable){

            if(cantidadEnvasesDevueltos >0 && cantidadEnvasesPrestados > 0)
            {
                error = "No es posible registrar devoluciones de envases y préstamos a la vez";
                return;
            }

            //if(cantidadEnvasesPrestados > cantidadEntregada)
            //{
            //    error = "No es posible prestar más envases de los que se entregan.";
            //    return;
            //}

            double envasesFinalesEnCliente = (cantidadEnvasesRelevados >0?
                    cantidadEnvasesRelevados + cantidadEnvasesPrestados:
                    envasesEnCliente + cantidadEnvasesPrestados) - cantidadEnvasesDevueltos;

            if(cantidadEntregada > envasesFinalesEnCliente)
            {
                error = "La cantidad entregada supera a la cantidad de envases que tiene el cliente";
                return;
            }

        }else{
            if(cantidadEnvasesRelevados>0 || cantidadEnvasesDevueltos > 0 || cantidadEnvasesPrestados > 0)
            {
                error = "Los envases no retornables no pueden ser prestados, devueltos o relevados";
                return;
            }
        }

        if(subtotal == 0 && descuentoManual > 0)
        {
            error = "No puede cargar descuentos si el subtotal es $0";
            return;
        }
    }

    public boolean tieneError(){
        this.validar();
        return error != null && !error.equals("");
    }

    public boolean tieneGestiones(){
        return cantidadEntregada > 0 || cantidadEnvasesPrestados > 0 || cantidadEnvasesRelevados > 0 || cantidadEnvasesDevueltos > 0;
    }

}


