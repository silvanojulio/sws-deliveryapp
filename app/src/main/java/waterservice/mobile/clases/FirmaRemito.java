package waterservice.mobile.clases;


import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "FirmasRemitos")
public class FirmaRemito {

    @DatabaseField( generatedId = true) public int id;
    @DatabaseField public int cliente_id;
    @DatabaseField(dataType = DataType.BYTE_ARRAY) public byte[] firmaRemito;
    @DatabaseField public String firmante;
    @DatabaseField public String fechaHora;
    @DatabaseField public int esServicioTecnico;

}
