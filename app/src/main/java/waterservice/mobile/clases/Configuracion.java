package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Configuraciones")
public class Configuracion {

    @DatabaseField( generatedId = true)
    public int id;

    @DatabaseField
    public String clave;

    @DatabaseField
    public String valor;
}
