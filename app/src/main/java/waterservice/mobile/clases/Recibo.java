package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@DatabaseTable(tableName = "Recibos")
public class Recibo {

    @DatabaseField( generatedId = true)
    public int id;

    @DatabaseField
    public String nombreCliente;

    @DatabaseField
    public int clienteId;

    @DatabaseField
    public Date fechaHora;

    @DatabaseField
    public boolean cerrado;

    @DatabaseField
    public Long comprobanteFisicoPrefijo;

    @DatabaseField
    public Long comprobanteFisicoNro;

    @DatabaseField
    public int clienteFacturaId;

    public List<Cheque> cheques;
    public List<TarjetaDeCredito> tarjetasDeCredito;
    public List<TarjetaDeDebito> tarjetasDeDebito;
    public List<Retencion> retenciones;

    public List<ItemDeRecibo> items;
    public List<Factura> imputaciones;

    public Double obtenerTotal(){

        Double total = 0.0;

        if(items!=null){
            for (ItemDeRecibo item:  items) {
                total += item.importe;
            }
        }

        return total;
    }

    public Double obtenerTotalImptuado(){

        Double total = 0.0;

        if(imputaciones!=null){
            for (Factura item:  imputaciones) {
                total += item.imputado;
            }
        }

        return total;
    }

    public Double obtenerTotalImptuado(List<Factura> facturas){

        Double total = 0.0;

        if(facturas!=null){
            for (Factura item:  facturas) {
                total += item.imputado;
            }
        }

        return total;
    }
}

