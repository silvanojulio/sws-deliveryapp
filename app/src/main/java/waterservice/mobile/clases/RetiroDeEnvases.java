package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "RetirosDeEnvases")
public class RetiroDeEnvases {

    @DatabaseField( generatedId = true)
    public int id;

    @DatabaseField
    public String codigo;

    @DatabaseField
    public int envaseId;

    @DatabaseField
    public Long clienteEntregadoId;
}

