package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Silvano on 08/03/2017.
 */
@DatabaseTable(tableName = "Dispensers")
public class Dispenser {

    @DatabaseField(id=true)
    public int id;

    @DatabaseField
    public String nroDispenser;
    @DatabaseField
    public String marca;
    @DatabaseField
    public String tipo;
    @DatabaseField
    public String color;
    @DatabaseField
    public int cliente_id;

}
