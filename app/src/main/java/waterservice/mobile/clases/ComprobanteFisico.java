package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ComprobantesFisicos")
public class ComprobanteFisico {

    @DatabaseField( id = true) public int idRango;
    @DatabaseField public String tipoDeComprobante;
    @DatabaseField public int idTipoComprobante;
    @DatabaseField public long prefijo;

}
