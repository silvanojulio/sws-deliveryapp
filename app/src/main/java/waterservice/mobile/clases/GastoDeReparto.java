package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "GastosDeReparto")
public class GastoDeReparto {

    @DatabaseField( generatedId = true) public int id;
    @DatabaseField public double montoGasto;
    @DatabaseField public String descripcion;
    @DatabaseField public int tipoGasto_id;

}
