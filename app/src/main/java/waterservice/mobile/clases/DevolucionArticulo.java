package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "DevolucionesArticulos")
public class DevolucionArticulo {

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField
    public int articulo_id;

    @DatabaseField
    public double cantidad;

    @DatabaseField
    public String fecha;

    @DatabaseField
    public int cliente_id;

    @DatabaseField
    public Boolean esCambioDirecto=false;

    @DatabaseField
    public int motivoDevolucion_ids;

    @DatabaseField
    public Boolean esReutilizable=false;

    @DatabaseField
    public String nombreArticulo;

    @DatabaseField
    public String motivoDevolucion;

}
