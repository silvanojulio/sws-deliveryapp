package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "VIEW_Facturas")
public class ViewFactura {

    @DatabaseField(id=true)
    public long id;

    @DatabaseField public int cliente_id;
    @DatabaseField public String nroFactura;
    @DatabaseField public String fechaFactura;
    @DatabaseField public String tipoFactura;
    @DatabaseField public double montoFacturaTotal;
    @DatabaseField public String fechaVencimiento1;
    @DatabaseField public String fechaVencimiento2;
    @DatabaseField public String fechaVencimiento3;
    @DatabaseField public double cobradoActual;
    @DatabaseField public double saldoActual;
    @DatabaseField public Boolean entregada;
    @DatabaseField public double imputado;

    @DatabaseField public double totalCobrado;
    @DatabaseField public double saldoFinal;
}
