package waterservice.mobile.clases.apiContracts.Sync;

import java.util.List;

import waterservice.mobile.clases.apiContracts.ResponseBase;

public class ObtenerComandosParaEjecutarResponse extends ResponseBase {
    public List<ComandoMobile> comandos;
}

