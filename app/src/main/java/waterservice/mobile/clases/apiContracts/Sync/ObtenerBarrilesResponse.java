package waterservice.mobile.clases.apiContracts.Sync;

import java.util.List;

import waterservice.mobile.clases.EnvaseDisponible;
import waterservice.mobile.clases.apiContracts.ResponseBase;

public class ObtenerBarrilesResponse extends ResponseBase {
    public List<EnvaseDisponible> barriles;
}

