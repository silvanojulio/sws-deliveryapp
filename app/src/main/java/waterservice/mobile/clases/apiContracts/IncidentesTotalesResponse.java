package waterservice.mobile.clases.apiContracts;

import java.util.List;

public class IncidentesTotalesResponse extends ResponseBase{
    public int totalIncidentes;
    public int cantidadIncidenes;
    public int cantidadIncidenesGrupales;
    public int cantidadIncidentesSeguidos;
    public List<IncidentesDeGruposResponse> IncidentesGrupales;
}

