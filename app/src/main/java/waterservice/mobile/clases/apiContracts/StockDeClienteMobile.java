package waterservice.mobile.clases.apiContracts;

public class StockDeClienteMobile
{
    public int clienteId ;
    public int articulo_id ;
    public String nombreArticulo ;
    public int stockActual ;
    public double consumoPromedio ;
    public int cantidadPermitida ;
    public int cantidadARecuperar ;
}
