package waterservice.mobile.clases.apiContracts;

import java.util.List;

public class OrdenDeTrabajoMobile
{
    public long ordenDeTrabajoServer_id ;
    public int cliente_id ;
    public int sintoma_id ;
    public String sintoma ;
    public String prioridad ;
    public String comentarios ;
    public String cantDispensers ;
    public String sectorUbicacion ;
    public String responsableEnCliente ;
    public String telefonoResponsable ;
    public String franjaHoraria ;

    public List<DispenserAsociadoAOrdenDeTrabajoMobile> dispensers ;
}
