package waterservice.mobile.clases.apiContracts;

import java.util.List;

import waterservice.mobile.clases.Cheque;
import waterservice.mobile.clases.Retencion;
import waterservice.mobile.clases.TarjetaDeCredito;
import waterservice.mobile.clases.TarjetaDeDebito;

public class VisitaClienteMobile{

    public ClienteMobile ClienteVisitado;

    public List<ImputacionDeReciboMobile> Imputaciones ;
    public List<ArticuloEntregadoMobile> ArticulosEntregados ;
    public List<DevolucionArticuloMobile> DevolucionesArticulos ;
    public List<FirmaRemitoMobile> FirmasRemito ;
    public List<ComprobanteEntregadoMobile> ComprobantesEntregados ;
    public List<ComprobanteFisicoUtilizadoMobile> ComprobantesFisicosUtilizados ;

    public List<ReciboMobile> Recibos ;
    public List<ItemDeReciboMobile> ItemsDeRecibos ;
    public List<Cheque> Cheques ;
    public List<TarjetaDeCredito> TarjetasDeCredito ;
    public List<waterservice.mobile.clases.TarjetaDeDebito> TarjetaDeDebito ;
    public List<Retencion> Retenciones ;

}
