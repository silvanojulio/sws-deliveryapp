package waterservice.mobile.clases.apiContracts;

import java.util.Date;

public class FacturaMobile {

    public long factura_id;
    public int cliente_id ;
    public String nroFactura ;
    public Date fechaFactura ;
    public String tipoFactura ;
    public double montoFacturaTotal ;
    public Date fechaVencimiento1 ;
    public Date fechaVencimiento2 ;
    public Date fechaVencimiento3 ;
    public double cobradoActual ;
    public double saldoActual ;
    public boolean entregada ;
}
