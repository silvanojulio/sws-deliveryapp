package waterservice.mobile.clases.apiContracts;

public class ArticuloEntregadoMobile
{
    public int id ;
    public int clienteId ;
    public int articulo_id ;
    public int cantidadEnvasesRelevados ;
    public int cantidadEnvasesPrestados ;
    public int cantidadEnvasesDevueltos ;
    public Integer motivoPrestamoDevolucionId;
    public double cantidadEntregada ;
    public double descuentoPorCantidad ;
    public double descuentoManual ;
    public double precioUnitario ;
}
