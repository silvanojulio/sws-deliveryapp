package waterservice.mobile.clases.apiContracts;

import java.util.List;

public class ClientePedidoMobile {

    public VIEW_ClientesXHojaDeRuta_DTO cliente;
    public List<AlertaDeClienteMobile> alertasDeCliente;
    public List<ArticuloDeAbonoMobile> articulosDeAbonos;
    public List<ArticuloDeComodatoMobile> comodatos;
    public List<DispenserDeClienteMobile> dispensers;
    public List<FacturaMobile> facturas;
    public List<OrdenDeTrabajoMobile> ordenesDeTrabajo;
    public List<StockDeClienteMobile> stock;
    public List<PrecioEspecialMobile> preciosEspeciales;
}
