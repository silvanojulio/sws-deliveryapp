package waterservice.mobile.clases.apiContracts.Sync;

import waterservice.mobile.clases.apiContracts.NuevosPedidos;
import waterservice.mobile.clases.apiContracts.ResponseBase;

public class ObtenerNuevosPedidosResponse extends ResponseBase {
    public NuevosPedidos nuevosPedidos;
}
