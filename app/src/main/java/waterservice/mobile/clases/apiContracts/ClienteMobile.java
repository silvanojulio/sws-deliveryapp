package waterservice.mobile.clases.apiContracts;

public class ClienteMobile{
    public int cliente_id ;
    public boolean visitado ;
    public boolean ausente ;
    public boolean ventaEntrega ;
    public boolean cobroConsumo;
    public boolean cobroFactura ;
    public boolean devolucionEnvases ;
    public boolean prestamoEnvases ;
    public boolean devolucionArticulo ;
    public String visita_altitud ;
    public String visita_longitud ;
    public String visita_fechaHora ;
    public String latitud ;
    public String longitud ;
    public boolean descargado ;
    public String relevamientoCoordenadas_latitud ;
    public String relevamientoCoordenadas_longitud ;
}
