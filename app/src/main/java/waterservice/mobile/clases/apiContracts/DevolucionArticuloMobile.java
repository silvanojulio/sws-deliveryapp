package waterservice.mobile.clases.apiContracts;

public class DevolucionArticuloMobile
{
    public int id ;
    public int articulo_id ;
    public double cantidad ;
    public String fecha ;
    public int cliente_id ;
    public boolean esCambioDirecto ;
    public int motivoDolucion_ids ;
    public boolean esReutilizable ;

}
