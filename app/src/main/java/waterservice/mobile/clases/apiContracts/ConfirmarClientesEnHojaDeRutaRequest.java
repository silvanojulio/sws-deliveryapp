package waterservice.mobile.clases.apiContracts;

import java.util.List;

public class ConfirmarClientesEnHojaDeRutaRequest {
    public long hojaDeRutaId;
    public List<Integer> clientesIds;
    public String codigoDeMovil;
}
