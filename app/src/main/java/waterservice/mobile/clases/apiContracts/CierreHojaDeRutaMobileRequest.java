package waterservice.mobile.clases.apiContracts;

import java.util.List;

import waterservice.mobile.clases.DeclaracionEfectivo;
import waterservice.mobile.clases.GastoDeReparto;

public class CierreHojaDeRutaMobileRequest{

    public List<DeclaracionEfectivo> declaracionesEfectivo;
    public List<GastoDeReparto> gastos;
    public int usuarioId;
    public String codigoMovil;
    public long hojaDeRutaId;
}
