package waterservice.mobile.clases;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "VIEW_PrestamosEnvases")
public class ViewPrestamoEnvase {

    @DatabaseField(id = true) public int id;
    @DatabaseField public int articulo_id;
    @DatabaseField public int cantidad;
    @DatabaseField public String fecha;
    @DatabaseField public int cliente_id;

    @DatabaseField public String nombreCliente;
    @DatabaseField public String domicilioCompleto;

    @DatabaseField public String codigoInterno;
    @DatabaseField public String nombreArticulo;
    @DatabaseField public int tipoDeEnvase_ids;
    @DatabaseField public Boolean controlableEnPlaya;

}
