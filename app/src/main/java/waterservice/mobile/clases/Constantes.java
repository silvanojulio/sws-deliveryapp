package waterservice.mobile.clases;

public class Constantes {
    public static final int FORMADEPAGO_EFECTIVO= 1;
    public static final int FORMADEPAGO_CHEQUE= 2;
    public static final int FORMADEPAGO_DEPOSITO= 3;
    public static final int FORMADEPAGO_RETENCIONES = 9;
    public static final int FORMADEPAGO_TARJETADEBITO = 10;
    public static final int FORMADEPAGO_TARJETACREDITO = 8;

    public static final int ACTIVITY_CHEQUE = 101;
    public static final int ACTIVITY_DEPOSITO = 102;
    public static final int ACTIVITY_SELECCIONAR_CHEQUE = 103;
    public static final int ACTIVITY_SELECCIONAR_DEPOSITO = 104;
    public static final int ACTIVITY_DECLARACION_EFECTIVO = 105;
    public static final int ACTIVITY_CONFIRMAR_DECLARACION = 106;
    public static final int ACTIVITY_COBROS_MULTIPLES = 107;
    public static final int ACTIVITY_LOGIN = 108;
    public static final int ACTIVITY_REMITO = 109;
    public static final int ACTIVITY_MANTENIMIENTO = 110;
    public static final int ACTIVITY_AGREGAR_REPUESTOACTIVIDAD = 111;
    public static final int ACTIVITY_CIERRE_OT = 112;

    public static final int TIPO_CLIENTE_EMPRESA = 2;
    public static final int TIPO_CLIENTE_FAMILIA = 1;

    public static final int TIPO_ENVASE_RETORNABLE = 1;
    public static final int TIPO_ENVASE_DESCARTABLE = 2;

    public static final int VISITA_NORMAL = 1;
    public static final int VISITA_VENTA_EXTRA = 2;
    public static final int VISITA_PEDIDO = 3;
    public static final int VISITA_SERVICIO_TECNICO = 4;

    public static final int MOTIVO_CIERRE_SERVICIO_TECNICO_EXITOSO = 1;

    public static final int ACCIONES_PRINC_DIS_INSTALACION = 1;
    public static final int ACCIONES_PRINC_DIS_DESINSTALACION = 2;
    public static final int ACCIONES_PRINC_DIS_REPARACION = 3;
    public static final int ACCIONES_PRINC_DIS_SANITIZACION = 4;
    public static final int ACCIONES_PRINC_DIS_REUBICACION = 5;
    public static final int ACTIVITY_AGREGAR_DISPENSER = 6 ;
    public static final int ACTIVITY_AGREGAR_ARTICULO_CONTROLDEPLAYA = 7 ;
    public static final int ACTIVITY_FIRMA_REMITO = 8;
    public static final int ACTIVITY_SELECCIONAR_ARTICULO = 9;
    public static final int ACTIVITY_CONFIRMAR_VENTA_ACTUAL = 10;
    public static final int ACTIVITY_RECIBO = 11;
    public static final int ACTIVITY_CONFIRMAR_VENTA= 12;
    public static final int ACTIVITY_SELECCIONAR_FORMA_DE_PAGO= 13;
    public static final int ACTIVITY_COMPROBANTE_FISICO= 14;
    public static final int ACTIVITY_FIRMA_CLIENTE = 15;
    public static final int ACTIVITY_RESUMEN_ENTREGA_ENVASES = 16;
    public static final int ACTIVITY_BUSCAR_ENVASE_REGISTRABLE = 17;
    public static final int ACTIVITY_ENTREGA_ENVASES = 18;

    public static final int ACTIVITY_AGREGAR_ITEM_RECIBO= 14;

    public static final int T_MOTIVOSDEDEVOLUCION = 280;
    public static final int T_FORMASDEPAGO = 380;
    public static final int T_BANCOS = 370;
    public static final int T_TIPOSDEGASTOS = 390;
    public static final int T_MOTIVOSDESOLICITUDDESTOCK = 790;
    public static final int T_SINTOMASSERVICIOTECNICO = 550;
    public static final int T_MOTIVOS_DE_CIERRE_ST = 580;
    public static final int T_ACCIONES_DISPENSERS_ST = 860;
    public static final int T_MARCAS_TARJETAS = 990;
    public static final int T_TIPOS_RETENCIONES = 400;

    public static final int TIPO_COMPROBANTE_REMITO = 1;
    public static final int TIPO_COMPROBANTE_RECIBO = 2;
    public static final int TIPO_DE_VISITA_SERVICIO_TECNICO = 4;
    public static final int MOTIVO_DEVOLUCION_NO_CONSUME = 1;
}
