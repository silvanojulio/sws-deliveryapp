package waterservice.mobile.clases;

import org.json.JSONException;
import org.json.JSONObject;

public class Vehiculo {

    public Vehiculo(JSONObject object) throws JSONException {
        this.id = object.getInt("id");
        this.nombreVehiculo = object.getString("nombreVehiculo");
        this.kilosDeCarga = object.getDouble("kilosDeCarga");
    }

    public int id;
    public String nombreVehiculo;
    public Double kilosDeCarga;

}
