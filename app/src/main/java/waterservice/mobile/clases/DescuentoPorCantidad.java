package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "DescuentosPorCantidad")
public class DescuentoPorCantidad {

    @DatabaseField(id=true)
    public int id;

    @DatabaseField public int listaDePrecioId;
    @DatabaseField public int articuloId;
    @DatabaseField public double descuento;
    @DatabaseField public int cantidad;
}
