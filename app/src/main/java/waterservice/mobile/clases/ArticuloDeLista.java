package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

@DatabaseTable(tableName = "ArticulosDeLista")
public class ArticuloDeLista {

    @DatabaseField(id=true)
    public int id;

    @DatabaseField public String codigoInterno;
    @DatabaseField public String nombreArticulo;
    @DatabaseField public double precio;
    @DatabaseField public int tipoDeEnvase_ids;
    @DatabaseField public Boolean controlableEnPlaya;
    @DatabaseField public int cantidadPorPack;
    @DatabaseField public int tipoArticulo_ids;
    @DatabaseField public boolean permiteDecimales;

    public static ArticuloDeLista ObtenerArticulo(int articulo_id, List<ArticuloDeLista> articulos){

        ArticuloDeLista art=null;

        if(articulos!=null){

            for (ArticuloDeLista item : articulos) {
                if(item.id==articulo_id)
                {
                    art = item;
                    break;
                }
            }
        }

        return art;
    }

    public Boolean esRetornable(){

        return tipoDeEnvase_ids==Constantes.TIPO_ENVASE_RETORNABLE;
    }
}

