package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "DeclaracionesEfectivo")
public class DeclaracionEfectivo {

    @DatabaseField (id = true) public int id;
    @DatabaseField public double valorNominal;
    @DatabaseField public String esMoneda;
    @DatabaseField public String valorDisplay;
    @DatabaseField public Integer cantidad;

}
