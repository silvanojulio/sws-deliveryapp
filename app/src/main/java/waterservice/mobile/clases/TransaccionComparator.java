package waterservice.mobile.clases;

import java.util.Comparator;

public class TransaccionComparator implements Comparator<Transaccion> {

    public int compare(Transaccion left, Transaccion right) {
        return String.valueOf(left.cliente_id).compareToIgnoreCase(String.valueOf(right.cliente_id));
    }
}
