package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "VIEW_ArticulosEntregados")
public class ViewArticuloEntregado {

    @DatabaseField( generatedId = true)
    public int id;

    @DatabaseField public String nombreDelArticulo;
    @DatabaseField public String codigoArticulo;
    @DatabaseField public int articulo_id=0;
    @DatabaseField public int cantidadEntregada=0;
    @DatabaseField public int cantidadEnvasesRelevados=0;
    @DatabaseField public int cantidadEnvasesPrestados=0;
    @DatabaseField public int cantidadEnvasesDevueltos=0;
    @DatabaseField public int motivoPrestamoDevolucionId=0;
    @DatabaseField public int envasesEnCliente=0;
    @DatabaseField public int envasesPermitidos=0;
    @DatabaseField public int envasesARecuperar=0;
    @DatabaseField public Double precioUnitario=0.0;
    @DatabaseField public boolean esRetornable;
    @DatabaseField public int cantidadDisponibles= 0;
    @DatabaseField public double descuentoPorCantidad=0.0;
    @DatabaseField public double descuentoManual=0.0;
    @DatabaseField public double subtotal=0.0;
    @DatabaseField public boolean esPredeterminado=false;
    @DatabaseField public int orden=0;
    @DatabaseField public int clienteId=0;
    @DatabaseField public String nombreCliente;

    public int obtenerCantidadACobrar(){
        if(cantidadDisponibles >= cantidadEntregada)
            return 0;
        else
            return cantidadEntregada - cantidadDisponibles;
    }

}

