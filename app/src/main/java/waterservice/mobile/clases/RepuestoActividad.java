package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "RepuestosActividades")
public class RepuestoActividad{

    @DatabaseField(generatedId = true)
    public long id;

    @DatabaseField public int articulo_id;
    @DatabaseField public int cantidad;
    @DatabaseField public int tipo_id;
    @DatabaseField public int mantenimiento_id;
    @DatabaseField public String nombreDelItem;


    public String dispenser;
}
