package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "StocksDeClientes")
public class StockDeCliente{

    @DatabaseField(generatedId=true)
    public int id;

    @DatabaseField public int articuloId;
    @DatabaseField public String nombreArticulo;
    @DatabaseField public int stockActual;
    @DatabaseField public Double consumoPromedio;
    @DatabaseField public int cantidadPermitida;
    @DatabaseField public int cantidadARecuperar;
    @DatabaseField public int clienteId;


}
