package waterservice.mobile.clases;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "VIEW_DevolucionesArticulos")
public class ViewDevolucionArticulo {

    @DatabaseField(id = true) public int id;
    @DatabaseField public int articulo_id;
    @DatabaseField public int cantidad;
    @DatabaseField public String fecha;
    @DatabaseField public int cliente_id;
    @DatabaseField public Boolean esCambioDirecto=false;
    @DatabaseField public int motivoDevolucion_ids;
    @DatabaseField public Boolean esReutilizable=false;

    @DatabaseField public String nombreCliente;
    @DatabaseField public String domicilioCompleto;

    @DatabaseField public String codigoInterno;
    @DatabaseField public String nombreArticulo;
    @DatabaseField public int tipoDeEnvase_ids;
    @DatabaseField public Boolean controlableEnPlaya;

}
