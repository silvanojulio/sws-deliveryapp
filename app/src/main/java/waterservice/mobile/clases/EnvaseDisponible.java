package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "EnvasesDisponibles")
public class EnvaseDisponible {

    @DatabaseField(id=true)
    public int id;

    @DatabaseField
    public String codigo;

    @DatabaseField
    public Integer articuloContenidoId;

    @DatabaseField
    public String articuloContenidoNombre;

    @DatabaseField
    public Double cantidadDeCarga;

    @DatabaseField
    public String unidadDeCarga;

    @DatabaseField
    public Integer enCliente;

    @DatabaseField
    public Long clienteEntregadoId;

    public String obtenerDescripcion(){
        return this.articuloContenidoNombre + " " + this.codigo + " (x" + String.valueOf(cantidadDeCarga) + " " + unidadDeCarga+")";
    }

    public boolean seleccionado;

    public class ResumenEnvaseEntregado{

        public Integer articuloContenidoId;

        public String articuloContenidoNombre;

        public double cantidad;

        public double descuento;

        public double precio;

        public double subtotal;

        public String unidadCantidad;

    }
}

