package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Cheques")
public class Cheque {

    @DatabaseField( generatedId = true)
    public int id;
    @DatabaseField
    public String librador;
    @DatabaseField
    public String nroCheque;
    @DatabaseField
    public int banco_id;
    @DatabaseField
    public double importe;
    @DatabaseField
    public String fechaCobro;
    @DatabaseField
    public String nroSucursalBanco;

    public String descripcionResumen;

}

