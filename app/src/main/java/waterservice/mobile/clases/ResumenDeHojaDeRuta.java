package waterservice.mobile.clases;

import waterservice.mobile.utiles.Utiles;

public class ResumenDeHojaDeRuta {

    public double totalRecibido;
    public double totalGastos;

    public TotalFormaDePago efectivo;
    public TotalFormaDePago cheques;
    public TotalFormaDePago tarjetasDeCredito;
    public TotalFormaDePago tarjetasDeDebito;
    public TotalFormaDePago retenciones;

    public int cantidadClientes;
    public int cantidadClientesVisitados;

    public double obtenerPorcentajeDeVisitados(){
        return Utiles.roundTwoDecimals(Double.valueOf (cantidadClientesVisitados) / Double.valueOf(cantidadClientes));
    }

    public static class TotalFormaDePago{
        public double totalRegistrado;
        public double totalDeclarado;
        public double cantidadItems;

        public double diferencia(){
            return totalDeclarado - totalRegistrado;
        }

        public double diferencia(double consideracion){
            return (totalDeclarado + consideracion) - totalRegistrado;
        }
    }
}
