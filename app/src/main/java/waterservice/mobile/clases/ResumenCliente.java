package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ResumenClientes")
public class ResumenCliente {

    @DatabaseField(id = true) public int clienteId;
    @DatabaseField public String nombreCliente;
    @DatabaseField public String domicilioCompleto ;
    @DatabaseField public String tipoCliente;
    @DatabaseField public String reparto;
    @DatabaseField public int repartoId;
    @DatabaseField public String latitud;
    @DatabaseField public String longitud;
    @DatabaseField(readOnly = true) public String usuario;
    @DatabaseField(readOnly = true) public String clave;
}
