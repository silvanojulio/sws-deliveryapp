package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ComprobantesFisicosUtilizados")
public class ComprobanteFisicoUtilizado {

    @DatabaseField( generatedId = true)
    public int id;

    @DatabaseField public int cliente_id;
    @DatabaseField public int tipoDeComprobanteFisico_ids;
    @DatabaseField public long nroPrefijo;
    @DatabaseField public long nroComprobante;

}
