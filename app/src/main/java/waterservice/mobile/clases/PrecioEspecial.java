package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

@DatabaseTable(tableName = "PreciosEspeciales")
public class PrecioEspecial {

    @DatabaseField( generatedId = true)
    public int id;
    @DatabaseField public int clienteId;
    @DatabaseField public int articuloId;
    @DatabaseField public double precio;


}
