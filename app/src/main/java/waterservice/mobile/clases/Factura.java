package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "Facturas")
public class Factura {

    @DatabaseField(id=true)
    public long id;

    @DatabaseField public int cliente_id;
    @DatabaseField public String nroFactura;
    @DatabaseField public String fechaFactura;
    @DatabaseField public String tipoFactura;
    @DatabaseField public double montoFacturaTotal;
    @DatabaseField public String fechaVencimiento1;
    @DatabaseField public String fechaVencimiento2;
    @DatabaseField public String fechaVencimiento3;
    @DatabaseField public double cobradoActual;
    @DatabaseField public double saldoActual;
    @DatabaseField public Boolean entregada;
    @DatabaseField public double imputado;

}

