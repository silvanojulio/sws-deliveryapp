package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "VIEW_ItemsDeRecibos")
public class ViewItemDeRecibo {

    @DatabaseField( generatedId = true)
    public int id;

    @DatabaseField public int formaDePagoId;
    @DatabaseField public String descripcion;
    @DatabaseField public double importe;
    @DatabaseField public boolean cerrado;
    @DatabaseField public int clienteId;
    @DatabaseField public String nombreCliente;


    public String getFormaDePago(){

        if(formaDePagoId == Constantes.FORMADEPAGO_RETENCIONES)
            return "Retención";
        else if(formaDePagoId == Constantes.FORMADEPAGO_TARJETACREDITO)
            return "Tarjeta de crédito";
        else if(formaDePagoId == Constantes.FORMADEPAGO_TARJETADEBITO)
            return "Tarjeta de débito";
        else if(formaDePagoId == Constantes.FORMADEPAGO_EFECTIVO)
            return "Efectivo";
        else if(formaDePagoId == Constantes.FORMADEPAGO_CHEQUE)
            return "Cheque";
        else
            return "";
    }
}
