package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "UsuariosApp")
public class UsuarioApp {
    @DatabaseField(id=true)
    public int usuario_id;
    @DatabaseField public String nombreApellido;
    @DatabaseField public String username;
    @DatabaseField public String password;
    @DatabaseField public Boolean esSupervisor;
}
