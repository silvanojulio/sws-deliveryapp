package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "MantenimientosDeDispensers")
public class MantenimientoDeDispenser {

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField public String nroDispenser;
    @DatabaseField public int sintomaReal;
    @DatabaseField public String comentarios;
    @DatabaseField public Date fechaHora;
    @DatabaseField public Boolean danosAtribuidosAlCliente;
    @DatabaseField public String latitud;
    @DatabaseField public String longitud;
    @DatabaseField public int usuario_id;
    @DatabaseField public int accionPrincipal_ids;

}
