package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "NumerosDeComprobantesDisponibles")
public class NumeroDeComprobanteDisponible {

    @DatabaseField( id = true) public int id;
    @DatabaseField public int idRango;
    @DatabaseField public long nroComprobante;
    @DatabaseField public Boolean utilizado;

}
