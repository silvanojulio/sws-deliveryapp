package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "TarjetasDeDebito")
public class TarjetaDeDebito {

    @DatabaseField( generatedId = true)
    public int id;
    @DatabaseField
    public int reciboId;
    @DatabaseField
    public String lote;
    @DatabaseField
    public String cupon;
    @DatabaseField
    public String codigoAutorizacion;
    @DatabaseField
    public int banco_id;
    @DatabaseField
    public double importe;

    public String descripcionResumen;

}
