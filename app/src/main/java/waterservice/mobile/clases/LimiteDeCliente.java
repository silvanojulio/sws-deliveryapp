package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "LimitesDeClientes")
public class LimiteDeCliente{

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField
    public int clienteId;
    @DatabaseField
    public boolean validaLimiteFacturas;
    @DatabaseField
    public boolean validaLimiteDeSaldo;
    @DatabaseField
    public int limiteDeFacturas;
    @DatabaseField
    public double limiteDeSaldo;
}
