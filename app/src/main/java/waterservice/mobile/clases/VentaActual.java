package waterservice.mobile.clases;

import java.nio.DoubleBuffer;
import java.util.ArrayList;
import java.util.List;

public class VentaActual {

    public int clienteId;
    public String nombreCliente;
    public List<ArticuloEntregado> articulosEntregados;
    public boolean esContadoEfectivo;
    public double importeEfectivo;

    public Double obtenerTotal(){

        Double total = 0.0;

        for (ArticuloEntregado art: articulosEntregados) {
            total += art.subtotal;
        }

        return total;
    }

    public boolean tieneEntregas(){

        boolean tiene=false;

        if(articulosEntregados!=null && articulosEntregados.size()>0)
        {
            for (ArticuloEntregado art: articulosEntregados) {
                if(art.cantidadEntregada > 0 ){
                    tiene = true;
                    break;
                }
            }
        }

        return tiene;
    }

    public boolean tieneDevolucionesEnvases(){

        boolean tiene=false;

        if(articulosEntregados!=null && articulosEntregados.size()>0)
        {
            for (ArticuloEntregado art: articulosEntregados) {
                if(art.cantidadEnvasesDevueltos > 0 ){
                    tiene = true;
                    break;
                }
            }
        }

        return tiene;
    }

    public boolean tienePrestamosEnvases(){

        boolean tiene=false;

        if(articulosEntregados!=null && articulosEntregados.size()>0)
        {
            for (ArticuloEntregado art: articulosEntregados) {
                if(art.cantidadEnvasesPrestados > 0 ){
                    tiene = true;
                    break;
                }
            }
        }

        return tiene;
    }

    public boolean tieneRelevamientosEnvases(){

        boolean tiene=false;

        if(articulosEntregados!=null && articulosEntregados.size()>0)
        {
            for (ArticuloEntregado art: articulosEntregados) {
                if(art.cantidadEnvasesRelevados > 0 ){
                    tiene = true;
                    break;
                }
            }
        }

        return tiene;
    }

    public boolean tieneErrores(){

        boolean tError = articulosEntregados==null || articulosEntregados.size()==0;

        for (ArticuloEntregado art: articulosEntregados) {
            if(art.tieneError()){
                tError = true;
                break;
            }
        }
        return tError;
    }

    public void quitarArticulosSinGesitones(){

        if(articulosEntregados==null) return;

        List<ArticuloEntregado> arts = new ArrayList<>();

        for (ArticuloEntregado art: articulosEntregados) {
            if(art.tieneGestiones()) arts.add(art);
        }

        articulosEntregados = arts;
    }

}
