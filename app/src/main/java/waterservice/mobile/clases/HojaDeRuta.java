package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "HojasDeRuta")
public class HojaDeRuta {

    @DatabaseField(id=true)
    public long id;

    @DatabaseField public String usuario;
    @DatabaseField public String password;
    @DatabaseField public String nombreReparto;
    @DatabaseField public String fechaReparto;
    @DatabaseField public double montoDeclarado;
    @DatabaseField public Boolean cerrada=false;
    @DatabaseField public String codigoReparto;
    @DatabaseField public int usuarioRepartidor_id;
    @DatabaseField public String centroDeDistribucion;
    @DatabaseField public Integer centroDeDistribucionId;
    @DatabaseField public int reparto_id;
}
