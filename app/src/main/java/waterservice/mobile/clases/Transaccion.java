package waterservice.mobile.clases;


public class Transaccion {

    public String cliente;
    public int cliente_id;

    public int tipoTransaccion;
    public String articulo;
    public int cantidad;
    public double precio;

    public static class Tipos{
        public static final int VENTA=1;
        public static final int COBRO=2;
        public static final int ARTICULO_DEVOLUCION=3;
        public static final int ENVASES_PRESTAMO=4;
        public static final int ENVASES_DEVOLUCION=5;
        public static final int ENVASES_RELEVAMIENTO=6;

    }
}
