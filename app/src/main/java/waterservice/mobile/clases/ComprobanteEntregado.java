package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ComprobantesEntregados")
public class ComprobanteEntregado {

    @DatabaseField( generatedId = true)
    public int id;

    @DatabaseField
    public int tipoComprobanteInterno_id;

    @DatabaseField
    public long idEntidad;

    @DatabaseField
    public int cliente_id;

}
