package waterservice.mobile.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Calendar;
import java.util.Date;

@DatabaseTable(tableName = "Logs")
public class Log {

    @DatabaseField(generatedId = true)public int id;
    @DatabaseField public String mensaje;
    @DatabaseField public String funcionalidad;
    @DatabaseField public String entidad;
    @DatabaseField public Long idEntidad;
    @DatabaseField public Date fechaHora;
}
