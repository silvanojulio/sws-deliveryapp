package waterservice.mobile.clases;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import waterservice.mobile.utiles.Utiles;


public class PlanificacionDeCarga {

    public PlanificacionDeCarga(){


    }

    public PlanificacionDeCarga(JSONObject object) throws Exception {

        this.factor = object.getDouble("factor");
        this.fechaDeGeneracion = Utiles.GetDateFromJson(object,"fechaDeGeneracion");
        this.fechaDeCarga = Utiles.GetDateFromJson(object,"fechaDeCarga");
        JSONArray jArticulos = object.getJSONArray("Articulos");

        this.articulos = new ArrayList<>();

        for (int i = 0; i < jArticulos.length(); i++) {
            this.articulos.add(new ArticuloDePlanificacionDeCarga(jArticulos.optJSONObject(i)));
        }

        if(object.has("VehiculoDeHojaDeRuta"))
        {
            JSONObject jvehiculo = object.getJSONObject("VehiculoDeHojaDeRuta");
            if(jvehiculo!=null)
                this.vehiculo = new Vehiculo(jvehiculo);
        }
    }

    public Date fechaDeCarga;
    public Date fechaDeGeneracion;
    public Double factor;
    public Vehiculo vehiculo;

    public List<ArticuloDePlanificacionDeCarga> articulos;

    public Double obtenerPesoTotal(){

        if(articulos!=null){

            Double totalPeso = 0.0;

            for (ArticuloDePlanificacionDeCarga art: this.articulos ) {
                totalPeso +=art.peso;
            }

            return totalPeso;

        }else{
            return 0.0;
        }
    }
}
