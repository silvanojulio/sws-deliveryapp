package waterservice.mobile.Session;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.HojaDeRuta;
import waterservice.mobile.clases.UsuarioApp;
import waterservice.mobile.services.Service_Clientes;
import waterservice.mobile.services.Service_HojasDeRutas;
import waterservice.mobile.utiles.SessionKeys;

public class AppSession {

    Context context;

    public final static int INT_NOT_FOUND = -999999;
    public final static String STRING_NOT_FOUND = "";
    public final static long LONG_NOT_FOUND = -999999;
    public final static boolean BOOLEAN_NOT_FOUND = false;
    public final static float FLOAT_NOT_FOUND = -999999;
    public final static String KEY_CURRENT_USER_ID = "KEY_CURRENT_USER_ID";
    public static String KEY_URL_SERVER="servidor_web";
    public static String KEY_URL_SERVER_LOCAL="servidor_web_local";
    public static String KEY_SERVER_LOCAL_MODE="modo_local";

    private HojaDeRuta hojaDeRuta;
    private UsuarioApp usuario;
    private static Cliente clienteActual;
    private Boolean bloquearCuenta;

    public AppSession(Context ctx){

        context = ctx;

    }

    public HojaDeRuta getHojaDeRuta() throws Exception {

        if(hojaDeRuta == null){
            hojaDeRuta = new Service_HojasDeRutas(context).obtenerHojaDeRutaActual();
        }

        return hojaDeRuta;
    }

    public UsuarioApp getUsuarioAcual() throws Exception {

        int idUsuarioActual = getValueInt(KEY_CURRENT_USER_ID);

        if(usuario == null && idUsuarioActual != INT_NOT_FOUND){

            usuario =  new Service_HojasDeRutas(context).obtenerUsuarioPorId(idUsuarioActual);
        }

        return usuario;
    }

    public void setUsuarioActual(UsuarioApp usuarioApp) throws Exception {
        if(usuarioApp==null) throw new Exception("El usuario es nulo");
        usuario= usuarioApp;
        setValueInt(usuarioApp.usuario_id, KEY_CURRENT_USER_ID);
    }

    public String getCurrentUrlForLocal(){

        SharedPreferences pref= PreferenceManager.getDefaultSharedPreferences(context);
        Boolean iSLocal=pref.getBoolean(KEY_SERVER_LOCAL_MODE, false);

        String url = iSLocal? pref.getString(KEY_URL_SERVER_LOCAL, ""):pref.getString(KEY_URL_SERVER, "");

        return url;
    }

    public boolean getBloquearCuenta() throws Exception {

        if(bloquearCuenta == null){
            bloquearCuenta = new Service_Clientes(this.context)
                        .obtenerConfiguracion("BLOQUEO_DE_CUENTA", false);
        }

        return bloquearCuenta;
    }

    public void desloguearUsuario() {

        usuario = null;
        removeValue(KEY_CURRENT_USER_ID);
    }

    public void setClienteActual(Cliente cliente) {

        clienteActual= cliente;
        setValueInt(cliente==null? 0 : cliente.cliente_id, SessionKeys.CLIENTE_ACTUAL);
    }

    public Cliente getClienteActual() throws Exception {

        int idClienteActual = getValueInt(SessionKeys.CLIENTE_ACTUAL);

        if(clienteActual == null && idClienteActual != INT_NOT_FOUND && idClienteActual != 0){

            clienteActual =  new Service_Clientes(context).obtenerCliente(idClienteActual);
        }

        return clienteActual;
    }

    //===========================================

    public void setValueString(String value, String keyConfig){

        SharedPreferences pref= PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor=pref.edit();
        editor.putString(keyConfig, String.valueOf(value));
        editor.commit();
    }

    public void setValueInt(int value, String keyConfig){

        SharedPreferences pref= PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor=pref.edit();
        editor.putInt(keyConfig, value);
        editor.commit();
    }

    public void removeValue(String keyConfig){

        SharedPreferences pref= PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor=pref.edit();
        editor.remove(keyConfig);
        editor.commit();
    }

    public int getValueInt(String keyConfig){
        SharedPreferences pref=PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getInt(keyConfig, INT_NOT_FOUND);
    }

    public String getValueString(String keyConfig){
        SharedPreferences pref=PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getString(keyConfig, STRING_NOT_FOUND);
    }

    public long getValueLong(String keyConfig){
        SharedPreferences pref=PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getLong(keyConfig, LONG_NOT_FOUND);
    }

    public boolean getValueBoolean(String keyConfig){
        SharedPreferences pref=PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getBoolean(keyConfig, BOOLEAN_NOT_FOUND);
    }

    public float getValueFloat(String keyConfig){
        SharedPreferences pref=PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getFloat(keyConfig, FLOAT_NOT_FOUND);
    }

}
