package waterservice.mobile.orm;

import java.io.IOException;
import java.sql.SQLException;


import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import waterservice.mobile.clases.ArticuloDeAbono;
import waterservice.mobile.clases.ArticuloDeComodato;
import waterservice.mobile.clases.ArticuloDeLista;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.DeclaracionEfectivo;
import waterservice.mobile.clases.DevolucionArticulo;
import waterservice.mobile.clases.DevolucionEnvase;
import waterservice.mobile.clases.Factura;
import waterservice.mobile.clases.FirmaRemito;
import waterservice.mobile.clases.GastoDeReparto;
import waterservice.mobile.clases.HojaDeRuta;
import waterservice.mobile.clases.PrestamoEnvase;

public class DataBaseConfigUtil extends OrmLiteConfigUtil {

	private static final Class<?>[] clasesORM=new Class<?>[] {
		    Cliente.class,
            ArticuloDeLista.class,
            ArticuloDeComodato.class,
            ArticuloDeAbono.class,
            Factura.class,
            HojaDeRuta.class,
            DevolucionArticulo.class,
            DevolucionEnvase.class,
            PrestamoEnvase.class,
            FirmaRemito.class,
            DeclaracionEfectivo.class,
            GastoDeReparto.class,
	};


	public static void main(String[] args) throws SQLException, IOException {
	
		writeConfigFile("ormlite_config.txt",clasesORM);

	}
	
	
}
