package waterservice.mobile.orm;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.clases.AlertaDeCliente;
import waterservice.mobile.clases.ArticuloDeAbono;
import waterservice.mobile.clases.ArticuloDeComodato;
import waterservice.mobile.clases.ArticuloDeLista;
import waterservice.mobile.clases.ArticuloEntregado;
import waterservice.mobile.clases.Cheque;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.ComprobanteEntregado;
import waterservice.mobile.clases.ComprobanteFisico;
import waterservice.mobile.clases.ComprobanteFisicoUtilizado;
import waterservice.mobile.clases.Configuracion;
import waterservice.mobile.clases.DeclaracionEfectivo;
import waterservice.mobile.clases.Deposito;
import waterservice.mobile.clases.DescuentoPorCantidad;
import waterservice.mobile.clases.DevolucionArticulo;
import waterservice.mobile.clases.Dispenser;
import waterservice.mobile.clases.DispenserAsociado;
import waterservice.mobile.clases.EnvaseDisponible;
import waterservice.mobile.clases.Factura;
import waterservice.mobile.clases.FirmaRemito;
import waterservice.mobile.clases.GastoDeReparto;
import waterservice.mobile.clases.HojaDeRuta;
import waterservice.mobile.clases.ImputacionDeRecibo;
import waterservice.mobile.clases.ItemDeRecibo;
import waterservice.mobile.clases.LimiteDeCliente;
import waterservice.mobile.clases.ListaDePrecios;
import waterservice.mobile.clases.Log;
import waterservice.mobile.clases.MantenimientoDeDispenser;
import waterservice.mobile.clases.NumeroDeComprobanteDisponible;
import waterservice.mobile.clases.OrdenDeTrabajo;
import waterservice.mobile.clases.PrecioEspecial;
import waterservice.mobile.clases.Recibo;
import waterservice.mobile.clases.RepuestoActividad;
import waterservice.mobile.clases.ResumenCliente;
import waterservice.mobile.clases.Retencion;
import waterservice.mobile.clases.StockDeCliente;
import waterservice.mobile.clases.TarjetaDeCredito;
import waterservice.mobile.clases.TarjetaDeDebito;
import waterservice.mobile.clases.UsuarioApp;
import waterservice.mobile.clases.ValorSatelite;
import waterservice.mobile.clases.ViewArticuloEntregado;
import waterservice.mobile.clases.ViewDevolucionArticulo;
import waterservice.mobile.clases.ViewFactura;
import waterservice.mobile.clases.ViewItemDeRecibo;


public class ORMSQLiteHelper extends OrmLiteSqliteOpenHelper{

	private static final int DATABASE_VERSION=1;
	
	public ORMSQLiteHelper( Context context, String databaseName ){
		super(context, databaseName,null, DATABASE_VERSION,  R.raw.ormlite_config);
	}

	@Override
	public void onCreate(SQLiteDatabase arg0, ConnectionSource arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}


	//=============================================================
	
	private Dao<Cliente, Integer> Dao_Cliente=null;
	public Dao<Cliente, Integer> getDao_Cliente() throws SQLException {
	
		if (Dao_Cliente==null) {
            Dao_Cliente=getDao(Cliente.class);
		}
		
		return Dao_Cliente;
	}
	
	private RuntimeExceptionDao<Cliente, Integer> RTE_Dao_Cliente=null;
	public RuntimeExceptionDao<Cliente, Integer> getRTE_Dao_Cliente() {
		
		if (RTE_Dao_Cliente==null) {
            RTE_Dao_Cliente=getRuntimeExceptionDao(Cliente.class);
		}
		
		return RTE_Dao_Cliente;
	}
	
	//=============================================================

    private Dao<HojaDeRuta, Integer> Dao_HojaDeRuta=null;
    public Dao<HojaDeRuta, Integer> getDao_HojaDeRuta() throws SQLException {

        if (Dao_HojaDeRuta==null) {
            Dao_HojaDeRuta=getDao(HojaDeRuta.class);
        }

        return Dao_HojaDeRuta;
    }

    private RuntimeExceptionDao<HojaDeRuta, Integer> RTE_Dao_HojaDeRuta=null;
    public RuntimeExceptionDao<HojaDeRuta, Integer> getRTE_Dao_HojaDeRuta() {

        if (RTE_Dao_HojaDeRuta==null) {
            RTE_Dao_HojaDeRuta=getRuntimeExceptionDao(HojaDeRuta.class);
        }

        return RTE_Dao_HojaDeRuta;
    }

    //=============================================================

    private Dao<ArticuloDeLista, Integer> Dao_ArticuloDeLista=null;
    public Dao<ArticuloDeLista, Integer> getDao_ArticuloDeLista() throws SQLException {

        if (Dao_ArticuloDeLista==null) {
            Dao_ArticuloDeLista=getDao(ArticuloDeLista.class);
        }

        return Dao_ArticuloDeLista;
    }

    private RuntimeExceptionDao<ArticuloDeLista, Integer> RTE_Dao_ArticuloDeLista=null;
    public RuntimeExceptionDao<ArticuloDeLista, Integer> getRTE_Dao_ArticuloDeLista() {

        if (RTE_Dao_ArticuloDeLista==null) {
            RTE_Dao_ArticuloDeLista=getRuntimeExceptionDao(ArticuloDeLista.class);
        }

        return RTE_Dao_ArticuloDeLista;
    }

    //=============================================================

    private Dao<ArticuloDeComodato, Integer> Dao_ArticuloDeComodato=null;
    public Dao<ArticuloDeComodato, Integer> getDao_ArticuloDeComodato() throws SQLException {

        if (Dao_ArticuloDeComodato==null) {
            Dao_ArticuloDeComodato=getDao(ArticuloDeComodato.class);
        }

        return Dao_ArticuloDeComodato;
    }

    private RuntimeExceptionDao<ArticuloDeComodato, Integer> RTE_Dao_ArticuloDeComodato=null;
    public RuntimeExceptionDao<ArticuloDeComodato, Integer> getRTE_Dao_ArticuloDeComodato() {

        if (RTE_Dao_ArticuloDeComodato==null) {
            RTE_Dao_ArticuloDeComodato=getRuntimeExceptionDao(ArticuloDeComodato.class);
        }

        return RTE_Dao_ArticuloDeComodato;
    }

    //=============================================================

    private Dao<ArticuloDeAbono, Integer> Dao_ArticuloDeAbono=null;
    public Dao<ArticuloDeAbono, Integer> getDao_ArticuloDeAbono() throws SQLException {

        if (Dao_ArticuloDeAbono==null) {
            Dao_ArticuloDeAbono=getDao(ArticuloDeAbono.class);
        }

        return Dao_ArticuloDeAbono;
    }

    private RuntimeExceptionDao<ArticuloDeAbono, Integer> RTE_Dao_ArticuloDeAbono=null;
    public RuntimeExceptionDao<ArticuloDeAbono, Integer> getRTE_Dao_ArticuloDeAbono() {

        if (RTE_Dao_ArticuloDeAbono==null) {
            RTE_Dao_ArticuloDeAbono=getRuntimeExceptionDao(ArticuloDeAbono.class);
        }

        return RTE_Dao_ArticuloDeAbono;
    }

    //=============================================================


    private Dao<Factura, Integer> Dao_Factura=null;
    public Dao<Factura, Integer> getDao_Factura() throws SQLException {

        if (Dao_Factura==null) {
            Dao_Factura=getDao(Factura.class);
        }

        return Dao_Factura;
    }

    private RuntimeExceptionDao<Factura, Integer> RTE_Dao_Factura=null;
    public RuntimeExceptionDao<Factura, Integer> getRTE_Dao_Factura() {

        if (RTE_Dao_Factura==null) {
            RTE_Dao_Factura=getRuntimeExceptionDao(Factura.class);
        }

        return RTE_Dao_Factura;
    }

    //=============================================================


    private Dao<DevolucionArticulo, Integer> Dao_DevolucionArticulo=null;
    public Dao<DevolucionArticulo, Integer> getDao_DevolucionArticulo() throws SQLException {

        if (Dao_DevolucionArticulo==null) {
            Dao_DevolucionArticulo=getDao(DevolucionArticulo.class);
        }

        return Dao_DevolucionArticulo;
    }

    private RuntimeExceptionDao<DevolucionArticulo, Integer> RTE_Dao_DevolucionArticulo=null;
    public RuntimeExceptionDao<DevolucionArticulo, Integer> getRTE_Dao_DevolucionArticulo() {

        if (RTE_Dao_DevolucionArticulo==null) {
            RTE_Dao_DevolucionArticulo=getRuntimeExceptionDao(DevolucionArticulo.class);
        }

        return RTE_Dao_DevolucionArticulo;
    }

    //=============================================================

    private Dao<ViewDevolucionArticulo, Integer> Dao_ViewDevolucionArticulo=null;
    public Dao<ViewDevolucionArticulo, Integer> getDao_ViewDevolucionArticulo() throws SQLException {

        if (Dao_ViewDevolucionArticulo==null) {
            Dao_ViewDevolucionArticulo=getDao(ViewDevolucionArticulo.class);
        }

        return Dao_ViewDevolucionArticulo;
    }

    private RuntimeExceptionDao<ViewDevolucionArticulo, Integer> RTE_Dao_ViewDevolucionArticulo=null;
    public RuntimeExceptionDao<ViewDevolucionArticulo, Integer> getRTE_Dao_ViewDevolucionArticulo() {

        if (RTE_Dao_ViewDevolucionArticulo==null) {
            RTE_Dao_ViewDevolucionArticulo=getRuntimeExceptionDao(ViewDevolucionArticulo.class);
        }

        return RTE_Dao_ViewDevolucionArticulo;
    }

    //=============================================================

    private Dao<FirmaRemito, Integer> Dao_FirmaRemito=null;
    public Dao<FirmaRemito, Integer> getDao_FirmaRemito() throws SQLException {

        if (Dao_FirmaRemito==null) {
            Dao_FirmaRemito=getDao(FirmaRemito.class);
        }

        return Dao_FirmaRemito;
    }

    private RuntimeExceptionDao<FirmaRemito, Integer> RTE_Dao_FirmaRemito=null;
    public RuntimeExceptionDao<FirmaRemito, Integer> getRTE_Dao_FirmaRemito() {

        if (RTE_Dao_FirmaRemito==null) {
            RTE_Dao_FirmaRemito=getRuntimeExceptionDao(FirmaRemito.class);
        }

        return RTE_Dao_FirmaRemito;
    }

    //=============================================================

    private Dao<UsuarioApp, Integer> Dao_UsuarioApp=null;
    public Dao<UsuarioApp, Integer> getDao_UsuarioApp() throws SQLException {

        if (Dao_UsuarioApp==null) {
            Dao_UsuarioApp=getDao(UsuarioApp.class);
        }

        return Dao_UsuarioApp;
    }

    private RuntimeExceptionDao<UsuarioApp, Integer> RTE_Dao_UsuarioApp=null;
    public RuntimeExceptionDao<UsuarioApp, Integer> getRTE_Dao_UsuarioApp() {

        if (RTE_Dao_UsuarioApp==null) {
            RTE_Dao_UsuarioApp=getRuntimeExceptionDao(UsuarioApp.class);
        }

        return RTE_Dao_UsuarioApp;
    }

    //=============================================================

    private Dao<Deposito, Integer> Dao_Deposito=null;
    public Dao<Deposito, Integer> getDao_Deposito() throws SQLException {

        if (Dao_Deposito==null) {
            Dao_Deposito=getDao(Deposito.class);
        }

        return Dao_Deposito;
    }

    private RuntimeExceptionDao<Deposito, Integer> RTE_Dao_Deposito=null;
    public RuntimeExceptionDao<Deposito, Integer> getRTE_Dao_Deposito() {

        if (RTE_Dao_Deposito==null) {
            RTE_Dao_Deposito=getRuntimeExceptionDao(Deposito.class);
        }

        return RTE_Dao_Deposito;
    }

    //=============================================================


    private Dao<Cheque, Integer> Dao_Cheque=null;
    public Dao<Cheque, Integer> getDao_Cheque() throws SQLException {

        if (Dao_Cheque==null) {
            Dao_Cheque=getDao(Cheque.class);
        }

        return Dao_Cheque;
    }

    private RuntimeExceptionDao<Cheque, Integer> RTE_Dao_Cheque=null;
    public RuntimeExceptionDao<Cheque, Integer> getRTE_Dao_Cheque() {

        if (RTE_Dao_Cheque==null) {
            RTE_Dao_Cheque=getRuntimeExceptionDao(Cheque.class);
        }

        return RTE_Dao_Cheque;
    }

    //=============================================================



    private Dao<GastoDeReparto, Integer> Dao_GastoDeReparto=null;
    public Dao<GastoDeReparto, Integer> getDao_GastoDeReparto() throws SQLException {

        if (Dao_GastoDeReparto==null) {
            Dao_GastoDeReparto=getDao(GastoDeReparto.class);
        }

        return Dao_GastoDeReparto;
    }

    private RuntimeExceptionDao<GastoDeReparto, Integer> RTE_Dao_GastoDeReparto=null;
    public RuntimeExceptionDao<GastoDeReparto, Integer> getRTE_Dao_GastoDeReparto() {

        if (RTE_Dao_GastoDeReparto==null) {
            RTE_Dao_GastoDeReparto=getRuntimeExceptionDao(GastoDeReparto.class);
        }

        return RTE_Dao_GastoDeReparto;
    }

    //=============================================================


    private Dao<DeclaracionEfectivo, Integer> Dao_DeclaracionEfectivo=null;
    public Dao<DeclaracionEfectivo, Integer> getDao_DeclaracionEfectivo() throws SQLException {

        if (Dao_DeclaracionEfectivo==null) {
            Dao_DeclaracionEfectivo=getDao(DeclaracionEfectivo.class);
        }

        return Dao_DeclaracionEfectivo;
    }

    private RuntimeExceptionDao<DeclaracionEfectivo, Integer> RTE_Dao_DeclaracionEfectivo=null;
    public RuntimeExceptionDao<DeclaracionEfectivo, Integer> getRTE_Dao_DeclaracionEfectivo() {

        if (RTE_Dao_DeclaracionEfectivo==null) {
            RTE_Dao_DeclaracionEfectivo=getRuntimeExceptionDao(DeclaracionEfectivo.class);
        }

        return RTE_Dao_DeclaracionEfectivo;
    }

    //=============================================================

    private Dao<AlertaDeCliente, Integer> Dao_AlertaDeCliente=null;
    public Dao<AlertaDeCliente, Integer> getDao_AlertaDeCliente() throws SQLException {

        if (Dao_AlertaDeCliente==null) {
            Dao_AlertaDeCliente=getDao(AlertaDeCliente.class);
        }

        return Dao_AlertaDeCliente;
    }

    private RuntimeExceptionDao<AlertaDeCliente, Integer> RTE_Dao_AlertaDeCliente=null;
    public RuntimeExceptionDao<AlertaDeCliente, Integer> getRTE_Dao_AlertaDeCliente() {

        if (RTE_Dao_AlertaDeCliente==null) {
            RTE_Dao_AlertaDeCliente=getRuntimeExceptionDao(AlertaDeCliente.class);
        }

        return RTE_Dao_AlertaDeCliente;
    }

    //=============================================================

    private Dao<ComprobanteEntregado, Integer> Dao_ComprobanteEntregado=null;
    public Dao<ComprobanteEntregado, Integer> getDao_ComprobanteEntregado() throws SQLException {

        if (Dao_ComprobanteEntregado==null) {
            Dao_ComprobanteEntregado=getDao(ComprobanteEntregado.class);
        }

        return Dao_ComprobanteEntregado;
    }

    private RuntimeExceptionDao<ComprobanteEntregado, Integer> RTE_Dao_ComprobanteEntregado=null;
    public RuntimeExceptionDao<ComprobanteEntregado, Integer> getRTE_Dao_ComprobanteEntregado() {

        if (RTE_Dao_ComprobanteEntregado==null) {
            RTE_Dao_ComprobanteEntregado=getRuntimeExceptionDao(ComprobanteEntregado.class);
        }

        return RTE_Dao_ComprobanteEntregado;
    }

    //=============================================================


    private Dao<NumeroDeComprobanteDisponible, Integer> Dao_NumeroDeComprobanteDisponible=null;
    public Dao<NumeroDeComprobanteDisponible, Integer> getDao_NumeroDeComprobanteDisponible() throws SQLException {

        if (Dao_NumeroDeComprobanteDisponible==null) {
            Dao_NumeroDeComprobanteDisponible=getDao(NumeroDeComprobanteDisponible.class);
        }

        return Dao_NumeroDeComprobanteDisponible;
    }

    private RuntimeExceptionDao<NumeroDeComprobanteDisponible, Integer> RTE_Dao_NumeroDeComprobanteDisponible=null;
    public RuntimeExceptionDao<NumeroDeComprobanteDisponible, Integer> getRTE_Dao_NumeroDeComprobanteDisponible() {

        if (RTE_Dao_NumeroDeComprobanteDisponible==null) {
            RTE_Dao_NumeroDeComprobanteDisponible=getRuntimeExceptionDao(NumeroDeComprobanteDisponible.class);
        }

        return RTE_Dao_NumeroDeComprobanteDisponible;
    }

    //=============================================================


    private Dao<ComprobanteFisico, Integer> Dao_ComprobanteFisico=null;
    public Dao<ComprobanteFisico, Integer> getDao_ComprobanteFisico() throws SQLException {

        if (Dao_ComprobanteFisico==null) {
            Dao_ComprobanteFisico=getDao(ComprobanteFisico.class);
        }

        return Dao_ComprobanteFisico;
    }

    private RuntimeExceptionDao<ComprobanteFisico, Integer> RTE_Dao_ComprobanteFisico=null;
    public RuntimeExceptionDao<ComprobanteFisico, Integer> getRTE_Dao_ComprobanteFisico() {

        if (RTE_Dao_ComprobanteFisico==null) {
            RTE_Dao_ComprobanteFisico=getRuntimeExceptionDao(ComprobanteFisico.class);
        }

        return RTE_Dao_ComprobanteFisico;
    }

    //=============================================================


    private Dao<ComprobanteFisicoUtilizado, Integer> Dao_ComprobanteFisicoUtilizado=null;
    public Dao<ComprobanteFisicoUtilizado, Integer> getDao_ComprobanteFisicoUtilizado() throws SQLException {

        if (Dao_ComprobanteFisicoUtilizado==null) {
            Dao_ComprobanteFisicoUtilizado=getDao(ComprobanteFisicoUtilizado.class);
        }

        return Dao_ComprobanteFisicoUtilizado;
    }

    private RuntimeExceptionDao<ComprobanteFisicoUtilizado, Integer> RTE_Dao_ComprobanteFisicoUtilizado=null;
    public RuntimeExceptionDao<ComprobanteFisicoUtilizado, Integer> getRTE_Dao_ComprobanteFisicoUtilizado() {

        if (RTE_Dao_ComprobanteFisicoUtilizado==null) {
            RTE_Dao_ComprobanteFisicoUtilizado=getRuntimeExceptionDao(ComprobanteFisicoUtilizado.class);
        }

        return RTE_Dao_ComprobanteFisicoUtilizado;
    }

    //=============================================================


    private Dao<RepuestoActividad, Integer> Dao_RepuestoActividad=null;
    public Dao<RepuestoActividad, Integer> getDao_RepuestoActividad() throws SQLException {

        if (Dao_RepuestoActividad==null) {
            Dao_RepuestoActividad=getDao(RepuestoActividad.class);
        }

        return Dao_RepuestoActividad;
    }

    private RuntimeExceptionDao<RepuestoActividad, Integer> RTE_Dao_RepuestoActividad=null;
    public RuntimeExceptionDao<RepuestoActividad, Integer> getRTE_Dao_RepuestoActividad() {

        if (RTE_Dao_RepuestoActividad==null) {
            RTE_Dao_RepuestoActividad=getRuntimeExceptionDao(RepuestoActividad.class);
        }

        return RTE_Dao_RepuestoActividad;
    }

    //=============================================================

    private Dao<ValorSatelite, Integer> Dao_ValorSatelite=null;
    public Dao<ValorSatelite, Integer> getDao_ValorSatelite() throws SQLException {

        if (Dao_ValorSatelite==null) {
            Dao_ValorSatelite=getDao(ValorSatelite.class);
        }

        return Dao_ValorSatelite;
    }

    private RuntimeExceptionDao<ValorSatelite, Integer> RTE_Dao_ValorSatelite=null;
    public RuntimeExceptionDao<ValorSatelite, Integer> getRTE_Dao_ValorSatelite() {

        if (RTE_Dao_ValorSatelite==null) {
            RTE_Dao_ValorSatelite=getRuntimeExceptionDao(ValorSatelite.class);
        }

        return RTE_Dao_ValorSatelite;
    }

    //=============================================================


    private Dao<OrdenDeTrabajo, Integer> Dao_OrdenDeTrabajo=null;
    public Dao<OrdenDeTrabajo, Integer> getDao_OrdenDeTrabajo() throws SQLException {

        if (Dao_OrdenDeTrabajo==null) {
            Dao_OrdenDeTrabajo=getDao(OrdenDeTrabajo.class);
        }

        return Dao_OrdenDeTrabajo;
    }

    private RuntimeExceptionDao<OrdenDeTrabajo, Integer> RTE_Dao_OrdenDeTrabajo=null;
    public RuntimeExceptionDao<OrdenDeTrabajo, Integer> getRTE_Dao_OrdenDeTrabajo() {

        if (RTE_Dao_OrdenDeTrabajo==null) {
            RTE_Dao_OrdenDeTrabajo=getRuntimeExceptionDao(OrdenDeTrabajo.class);
        }

        return RTE_Dao_OrdenDeTrabajo;
    }

    //=============================================================
    private Dao<DispenserAsociado, Integer> Dao_DispenserAsociado=null;
    public Dao<DispenserAsociado, Integer> getDao_DispenserAsociado() throws SQLException {

        if (Dao_DispenserAsociado==null) {
            Dao_DispenserAsociado=getDao(DispenserAsociado.class);
        }

        return Dao_DispenserAsociado;
    }

    //=============================================================
    private Dao<MantenimientoDeDispenser, Integer> Dao_MantenimientoDeDispenser=null;
    public Dao<MantenimientoDeDispenser, Integer> getDao_MantenimientoDeDispenser() throws SQLException {

        if (Dao_MantenimientoDeDispenser==null) {
            Dao_MantenimientoDeDispenser=getDao(MantenimientoDeDispenser.class);
        }

        return Dao_MantenimientoDeDispenser;
    }

    //=============================================================
    private Dao<Configuracion, Integer> Dao_Configuracion=null;
    public Dao<Configuracion, Integer> getDao_Configuracion() throws SQLException {

        if (Dao_Configuracion==null) {
            Dao_Configuracion=getDao(Configuracion.class);
        }

        return Dao_Configuracion;
    }

    //=============================================================

    private Dao<Dispenser, Integer> Dao_Dispenser=null;
    public Dao<Dispenser, Integer> getDao_Dispenser() throws SQLException {

        if (Dao_Dispenser==null) {
            Dao_Dispenser=getDao(Dispenser.class);
        }

        return Dao_Dispenser;
    }

    //=============================================================

    private Dao<Log, Integer> Dao_Log=null;
    public Dao<Log, Integer> getDao_Log() throws SQLException {

        if (Dao_Log==null) {
            Dao_Log=getDao(Log.class);
        }

        return Dao_Log;
    }

    //=============================================================

    private RuntimeExceptionDao<PrecioEspecial, Integer> RTE_Dao_PrecioEspecial=null;
    public RuntimeExceptionDao<PrecioEspecial, Integer> getRTE_Dao_PrecioEspecial() {

        if (RTE_Dao_PrecioEspecial==null) {
            RTE_Dao_PrecioEspecial=getRuntimeExceptionDao(PrecioEspecial.class);
        }

        return RTE_Dao_PrecioEspecial;
    }

    //=============================================================

    private RuntimeExceptionDao<ListaDePrecios, Integer> RTE_Dao_ListaDePrecios=null;
    public RuntimeExceptionDao<ListaDePrecios, Integer> getRTE_Dao_ListaDePrecios() {

        if (RTE_Dao_ListaDePrecios==null) {
            RTE_Dao_ListaDePrecios=getRuntimeExceptionDao(ListaDePrecios.class);
        }

        return RTE_Dao_ListaDePrecios;
    }

    //=============================================================


    private RuntimeExceptionDao<StockDeCliente, Integer> RTE_Dao_StockDeCliente=null;
    public RuntimeExceptionDao<StockDeCliente, Integer> getRTE_Dao_StockDeCliente() {

        if (RTE_Dao_StockDeCliente==null) {
            RTE_Dao_StockDeCliente=getRuntimeExceptionDao(StockDeCliente.class);
        }

        return RTE_Dao_StockDeCliente;
    }

    //=============================================================

    private RuntimeExceptionDao<DescuentoPorCantidad, Integer> RTE_Dao_DescuentoPorCantidad=null;
    public RuntimeExceptionDao<DescuentoPorCantidad, Integer> getRTE_Dao_DescuentoPorCantidad() {

        if (RTE_Dao_DescuentoPorCantidad==null) {
            RTE_Dao_DescuentoPorCantidad=getRuntimeExceptionDao(DescuentoPorCantidad.class);
        }

        return RTE_Dao_DescuentoPorCantidad;
    }

    //=============================================================

    private RuntimeExceptionDao<Recibo, Integer> RTE_Dao_Recibo=null;
    public RuntimeExceptionDao<Recibo, Integer> getRTE_Dao_Recibo() {

        if (RTE_Dao_Recibo==null) {
            RTE_Dao_Recibo=getRuntimeExceptionDao(Recibo.class);
        }

        return RTE_Dao_Recibo;
    }

    //=============================================================

    private RuntimeExceptionDao<ItemDeRecibo, Integer> RTE_Dao_ItemDeRecibo=null;
    public RuntimeExceptionDao<ItemDeRecibo, Integer> getRTE_Dao_ItemDeRecibo() {

        if (RTE_Dao_ItemDeRecibo==null) {
            RTE_Dao_ItemDeRecibo=getRuntimeExceptionDao(ItemDeRecibo.class);
        }

        return RTE_Dao_ItemDeRecibo;
    }

    //=============================================================


    private RuntimeExceptionDao<ImputacionDeRecibo, Integer> RTE_Dao_ImputacionDeRecibo=null;
    public RuntimeExceptionDao<ImputacionDeRecibo, Integer> getRTE_Dao_ImputacionDeRecibo() {

        if (RTE_Dao_ImputacionDeRecibo==null) {
            RTE_Dao_ImputacionDeRecibo=getRuntimeExceptionDao(ImputacionDeRecibo.class);
        }

        return RTE_Dao_ImputacionDeRecibo;
    }

    //=============================================================


    private RuntimeExceptionDao<ViewFactura, Integer> RTE_Dao_ViewFactura=null;
    public RuntimeExceptionDao<ViewFactura, Integer> getRTE_Dao_ViewFactura() {

        if (RTE_Dao_ViewFactura==null) {
            RTE_Dao_ViewFactura=getRuntimeExceptionDao(ViewFactura.class);
        }

        return RTE_Dao_ViewFactura;
    }

    //=============================================================

    private RuntimeExceptionDao<LimiteDeCliente, Integer> RTE_Dao_LimiteDeCliente=null;
    public RuntimeExceptionDao<LimiteDeCliente, Integer> getRTE_Dao_LimiteDeCliente() {

        if (RTE_Dao_LimiteDeCliente==null) {
            RTE_Dao_LimiteDeCliente=getRuntimeExceptionDao(LimiteDeCliente.class);
        }

        return RTE_Dao_LimiteDeCliente;
    }

    //=============================================================


    private RuntimeExceptionDao<TarjetaDeDebito, Integer> RTE_Dao_TarjetaDeDebito=null;
    public RuntimeExceptionDao<TarjetaDeDebito, Integer> getRTE_Dao_TarjetaDeDebito() {

        if (RTE_Dao_TarjetaDeDebito==null) {
            RTE_Dao_TarjetaDeDebito=getRuntimeExceptionDao(TarjetaDeDebito.class);
        }

        return RTE_Dao_TarjetaDeDebito;
    }

    //=============================================================

    private RuntimeExceptionDao<TarjetaDeCredito, Integer> RTE_Dao_TarjetaDeCredito=null;
    public RuntimeExceptionDao<TarjetaDeCredito, Integer> getRTE_Dao_TarjetaDeCredito() {

        if (RTE_Dao_TarjetaDeCredito==null) {
            RTE_Dao_TarjetaDeCredito=getRuntimeExceptionDao(TarjetaDeCredito.class);
        }

        return RTE_Dao_TarjetaDeCredito;
    }

    //=============================================================

    private RuntimeExceptionDao<Retencion, Integer> RTE_Dao_Retencion=null;
    public RuntimeExceptionDao<Retencion, Integer> getRTE_Dao_Retencion() {

        if (RTE_Dao_Retencion==null) {
            RTE_Dao_Retencion=getRuntimeExceptionDao(Retencion.class);
        }

        return RTE_Dao_Retencion;
    }

    //=============================================================

    private RuntimeExceptionDao<ArticuloEntregado, Integer> RTE_Dao_ArticuloEntregado=null;
    public RuntimeExceptionDao<ArticuloEntregado, Integer> getRTE_Dao_ArticuloEntregado() {

        if (RTE_Dao_ArticuloEntregado==null) {
            RTE_Dao_ArticuloEntregado=getRuntimeExceptionDao(ArticuloEntregado.class);
        }

        return RTE_Dao_ArticuloEntregado;
    }

    //=============================================================


    private RuntimeExceptionDao<ViewArticuloEntregado, Integer> RTE_Dao_ViewArticuloEntregado=null;
    public RuntimeExceptionDao<ViewArticuloEntregado, Integer> getRTE_Dao_ViewArticuloEntregado() {

        if (RTE_Dao_ViewArticuloEntregado==null) {
            RTE_Dao_ViewArticuloEntregado=getRuntimeExceptionDao(ViewArticuloEntregado.class);
        }

        return RTE_Dao_ViewArticuloEntregado;
    }

    //=============================================================


    private RuntimeExceptionDao<ViewItemDeRecibo, Integer> RTE_Dao_ViewItemDeRecibo=null;
    public RuntimeExceptionDao<ViewItemDeRecibo, Integer> getRTE_Dao_ViewItemDeRecibo() {

        if (RTE_Dao_ViewItemDeRecibo==null) {
            RTE_Dao_ViewItemDeRecibo=getRuntimeExceptionDao(ViewItemDeRecibo.class);
        }

        return RTE_Dao_ViewItemDeRecibo;
    }

    //=============================================================

    private RuntimeExceptionDao<EnvaseDisponible, Integer> RTE_Dao_EnvaseDisponible=null;
    public RuntimeExceptionDao<EnvaseDisponible, Integer> getRTE_Dao_EnvaseDisponible() {

        if (RTE_Dao_EnvaseDisponible==null) {
            RTE_Dao_EnvaseDisponible=getRuntimeExceptionDao(EnvaseDisponible.class);
        }

        return RTE_Dao_EnvaseDisponible;
    }

    //=============================================================

    private RuntimeExceptionDao<ResumenCliente, Integer> RTE_Dao_ResumenCliente=null;
    public RuntimeExceptionDao<ResumenCliente, Integer> getRTE_Dao_ResumenCliente() {

        if (RTE_Dao_ResumenCliente==null) {
            RTE_Dao_ResumenCliente=getRuntimeExceptionDao(ResumenCliente.class);
        }

        return RTE_Dao_ResumenCliente;
    }

    //=============================================================
}
