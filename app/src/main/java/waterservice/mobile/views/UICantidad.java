package waterservice.mobile.views;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;

import ar.com.waterservice.newapp.R;

public class UICantidad extends FrameLayout {

    private EditText txt_cantidad;
    private ImageView btn_mas;
    private ImageView btn_menos;
    private int cantidadActual = 0;
    private double cantidadActualDecimal = 0;
    private boolean esDecimal = false;

    public UICantidad.OnCantidadChange onCantidadChange;

    public UICantidad(Context context) {
        super(context);
        init();
    }

    public UICantidad(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UICantidad(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.frm_cantidad, null);
        addView(view);

        txt_cantidad = (EditText) this.findViewById(R.id.txt_cantidad);
        btn_mas = (ImageView) this.findViewById(R.id.btn_mas);
        btn_menos = (ImageView) this.findViewById(R.id.btn_menos);

        btn_menos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(esDecimal){
                    if(cantidadActualDecimal==0) return;
                    cantidadActualDecimal --;
                    txt_cantidad.setText(String.valueOf( (int)cantidadActualDecimal));
                }else{
                    if(cantidadActual==0) return;
                    cantidadActual --;
                    txt_cantidad.setText(String.valueOf(cantidadActual));
                }
            }
        });

        btn_mas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(esDecimal){
                    cantidadActualDecimal ++;
                    txt_cantidad.setText(String.valueOf((int) cantidadActualDecimal));
                }else{
                    cantidadActual ++;
                    txt_cantidad.setText(String.valueOf(cantidadActual));
                }
            }
        });

        txt_cantidad.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                if(esDecimal){
                    try{
                        cantidadActualDecimal = Double.valueOf(txt_cantidad.getText().toString());
                    }catch (Exception ex){
                        cantidadActualDecimal = 0;
                    }

                    if(onCantidadChange!=null)
                        onCantidadChange.onChange(cantidadActualDecimal);
                }else{
                    try{
                        cantidadActual = Double.valueOf(txt_cantidad.getText().toString()).intValue();
                    }catch (Exception ex){
                        cantidadActual = 0;
                    }

                    if(onCantidadChange!=null)
                        onCantidadChange.onChange(cantidadActual);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
    }

    public interface OnCantidadChange{

        public void onChange(double nuevoValor);
    }


    public void asignarCantidad(int cantidad){
        esDecimal = false;
        this.cantidadActual = cantidad;
        txt_cantidad.setText(String.valueOf(cantidad));
    }

    public void asignarCantidad(double cantidad){

        this.cantidadActualDecimal = cantidad;
        txt_cantidad.setText(cantidad==0? "" : String.valueOf(cantidad));

        esDecimal = true;
    }

    public double obtenerCantidadActual(){

        if(esDecimal){
            try{
                cantidadActualDecimal = Double.valueOf(txt_cantidad.getText().toString());
            }catch (Exception ex){
                cantidadActualDecimal = 0;
            }

            return cantidadActualDecimal;
        }else{
            try{
                cantidadActual = Integer.valueOf(txt_cantidad.getText().toString());
            }catch (Exception ex){
                cantidadActual = 0;
            }

            return cantidadActual;
        }
    }

}
