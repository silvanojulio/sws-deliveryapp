package waterservice.mobile.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.utiles.Utiles;

public class UIFechaSimple extends FrameLayout {

    private Spinner spn_dia, spn_mes, spn_ano;
    private int diaActual = 1, mesActual = 1, anoActual= 0;
    private boolean iniciado = false;

    private List<DateEntity> dias, meses, anos;

    public UIFechaSimple(Context context) {
        super(context);
        init();
    }

    public UIFechaSimple(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UIFechaSimple(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }


    public void asignarFecha(Calendar fecha){

    }

    public void asignarFecha(Date fecha){

    }

    public void asignarFecha(int ano, int mes, int dia){

    }

    public FechaSeleccionada obtenerFechaSeleccionada() throws Exception{

        FechaSeleccionada f = null;

        try{

            anoActual = anos.get(spn_ano.getSelectedItemPosition()).valor;
            mesActual = meses.get(spn_mes.getSelectedItemPosition()).valor;
            diaActual = dias.get(spn_dia.getSelectedItemPosition()).valor;

            f = new FechaSeleccionada(diaActual, mesActual, anoActual);

            if(!f.esValida())
                throw new Exception("Fecha no válida");

        }catch (Exception ex){
            throw new Exception("Fecha no válida");
        }

        return f;
    }

    private void init() {
        View view = inflate(getContext(), R.layout.ui_fecha_simple, null);
        addView(view);

        spn_ano = (Spinner) findViewById(R.id.spn_ano);
        spn_mes = (Spinner) findViewById(R.id.spn_mes);
        spn_dia = (Spinner) findViewById(R.id.spn_dia);

        setEntidades();

        setDia();
        setMes();
        setAno();

        if(!iniciado){
            setDiaDeHoy();
            iniciado=true;
        }
    }

    private void setDiaDeHoy() {

        Calendar hoy = Calendar.getInstance();

        spn_ano.setSelection(hoy.get(Calendar.YEAR) - 2017);
        spn_mes.setSelection(hoy.get(Calendar.MONTH));
        spn_dia.setSelection(hoy.get(Calendar.DAY_OF_MONTH) - 1);
    }

    private void setEntidades() {

        if(anos!=null) return;

        anos = new ArrayList<>();
        anos.add(new DateEntity(2017,"2017"));
        anos.add(new DateEntity(2018,"2018"));
        anos.add(new DateEntity(2019,"2019"));
        anos.add(new DateEntity(2020,"2020"));
        anos.add(new DateEntity(2021,"2021"));
        anos.add(new DateEntity(2022,"2022"));
        anos.add(new DateEntity(2023,"2023"));
        anos.add(new DateEntity(2024,"2024"));


        meses = new ArrayList<>();
        meses.add(new DateEntity(1,"Enero"));
        meses.add(new DateEntity(2,"Febrero"));
        meses.add(new DateEntity(3,"Marzo"));
        meses.add(new DateEntity(4,"Abril"));
        meses.add(new DateEntity(5,"Mayo"));
        meses.add(new DateEntity(6,"Junio"));
        meses.add(new DateEntity(7,"Julio"));
        meses.add(new DateEntity(8,"Agosto"));
        meses.add(new DateEntity(9,"Septiembre"));
        meses.add(new DateEntity(10,"Octubre"));
        meses.add(new DateEntity(11,"Noviembre"));
        meses.add(new DateEntity(12,"Diciembre"));

        dias = new ArrayList<>();
        dias.add(new DateEntity(1,"1"));
        dias.add(new DateEntity(2,"2"));
        dias.add(new DateEntity(3,"3"));
        dias.add(new DateEntity(4,"4"));
        dias.add(new DateEntity(5,"5"));
        dias.add(new DateEntity(6,"6"));
        dias.add(new DateEntity(7,"7"));
        dias.add(new DateEntity(8,"8"));
        dias.add(new DateEntity(9,"9"));
        dias.add(new DateEntity(10,"10"));
        dias.add(new DateEntity(11,"11"));
        dias.add(new DateEntity(12,"12"));
        dias.add(new DateEntity(13,"13"));
        dias.add(new DateEntity(14,"14"));
        dias.add(new DateEntity(15,"15"));
        dias.add(new DateEntity(16,"16"));
        dias.add(new DateEntity(17,"17"));
        dias.add(new DateEntity(18,"18"));
        dias.add(new DateEntity(19,"19"));
        dias.add(new DateEntity(20,"20"));
        dias.add(new DateEntity(21,"21"));
        dias.add(new DateEntity(22,"22"));
        dias.add(new DateEntity(23,"23"));
        dias.add(new DateEntity(24,"24"));
        dias.add(new DateEntity(25,"25"));
        dias.add(new DateEntity(26,"26"));
        dias.add(new DateEntity(27,"27"));
        dias.add(new DateEntity(28,"28"));
        dias.add(new DateEntity(29,"29"));
        dias.add(new DateEntity(30,"30"));
        dias.add(new DateEntity(31,"31"));


    }

    private void setDia(){

        Utiles.SetItemsToSpinner(spn_dia, dias,"display", getContext());
    }

    private void setMes(){

        Utiles.SetItemsToSpinner(spn_mes, meses,"display", getContext());
    }

    private void setAno(){
        Utiles.SetItemsToSpinner(spn_ano, anos,"display", getContext());
    }

    private DateEntity obtenerSeleccionado(Spinner spn, List<DateEntity> lista){
        return lista.get(spn.getSelectedItemPosition());
    }

    public static class FechaSeleccionada{

        public int dia;
        public int mes;
        public int ano;

        public FechaSeleccionada(int _dia, int _mes, int _ano){
            this.dia = _dia;
            this.mes = _mes;
            this.ano = _ano;
        }

        public Calendar getFechaCalendar (){

            try{
                Calendar c = Calendar.getInstance();
                c.set(ano, mes -1, dia);
                return c;
            }catch (Exception e){
                return null;
            }
        }

        public Date getFechaDate(){
            try{
                Calendar c = getFechaCalendar();
                return c.getTime();
            }catch (Exception e){
                return null;
            }
        }

        public String getFechaString(){
            try{
                Calendar c = getFechaCalendar();
                return "";
            }catch (Exception e){
                return null;
            }
        }

        public boolean esValida(){
            return  !(getFechaDate()==null || getFechaCalendar() == null || dia==0 || mes==0 || ano==0);
        }
    }

    public static class DateEntity{

        public DateEntity(int valor, String display){
            this.valor = valor;
            this.display = display;
        }

        public int valor;
        public String display;
    }
}
