package waterservice.mobile.server;

import android.content.Context;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import waterservice.mobile.clases.DeclaracionEfectivo;
import waterservice.mobile.clases.GastoDeReparto;
import waterservice.mobile.clases.apiContracts.AgregarClienteAHojaDeRutaMobileRequest;
import waterservice.mobile.clases.apiContracts.CierreHojaDeRutaMobileRequest;
import waterservice.mobile.clases.apiContracts.ConfirmarClientesEnHojaDeRutaRequest;
import waterservice.mobile.clases.apiContracts.EnviarUbicacionRequest;
import waterservice.mobile.clases.apiContracts.IncidentesTotalesResponse;
import waterservice.mobile.clases.apiContracts.RegistrarVisitaClienteMobileRequest;
import waterservice.mobile.clases.apiContracts.ResponseBase;
import waterservice.mobile.clases.apiContracts.Sync.FileApiResponse;
import waterservice.mobile.clases.apiContracts.Sync.ObtenerBarrilesResponse;
import waterservice.mobile.clases.apiContracts.Sync.ObtenerComandosParaEjecutarResponse;
import waterservice.mobile.clases.apiContracts.Sync.ObtenerNuevosPedidosResponse;
import waterservice.mobile.clases.apiContracts.Sync.VerificarConfiguracionMobileResponse;
import waterservice.mobile.clases.apiContracts.UbicacionMovil;
import waterservice.mobile.clases.apiContracts.VisitaClienteMobile;

public class SyncMobile extends  BaseService{

    private ISyncMobile srv;
    public SyncMobile( Context ctx){
        srv = getService(ctx).create(ISyncMobile.class);
    }

    public void obtenerNuevosPedidos(long hojaDeRutaId, String codigoDeMovil, ApiCallback<ObtenerNuevosPedidosResponse> callback){

        Call<ObtenerNuevosPedidosResponse> call = srv.obtenerNuevosPedidos(
                hojaDeRutaId,
                codigoDeMovil);

        Callback<ObtenerNuevosPedidosResponse> cb = getCallback(callback);

        call.enqueue(cb);
    }

    public void obtenerNuevosComandos(String codigoDeMovil, ApiCallback<ObtenerComandosParaEjecutarResponse> callback){

        Call<ObtenerComandosParaEjecutarResponse> call = srv.obtenerComandosParaEjecutar(codigoDeMovil);

        Callback<ObtenerComandosParaEjecutarResponse> cb = getCallback(callback);

        call.enqueue(cb);
    }

    public <T>  void confirmarClientesEnRuta(long hojaDeRutaId, String codigoDeMovil,
                                               List<Integer> idsClientes, ApiCallback<ResponseBase> callback){

        ConfirmarClientesEnHojaDeRutaRequest request = new ConfirmarClientesEnHojaDeRutaRequest();
        request.codigoDeMovil = codigoDeMovil;
        request.hojaDeRutaId = hojaDeRutaId;
        request.clientesIds = idsClientes;

        Call<ResponseBase> call = srv.confirmarClientesEnHojaDeRuta(request);

        Callback<ResponseBase> cb = getCallback(callback);

        call.enqueue(cb);
    }

    public void registrarVisitaClienteMobile(long hojaDeRutaId, String codigoDeMovil,
                                                  String firmaBase64, VisitaClienteMobile visita,
                                                  ApiCallback<ResponseBase> callback){

        RegistrarVisitaClienteMobileRequest request = new RegistrarVisitaClienteMobileRequest();
        request.codigoMovil = codigoDeMovil;
        request.hojaDeRutaId = hojaDeRutaId;
        request.firmaBase64 = firmaBase64;

        request.visita = visita;

        Call<ResponseBase> call = srv.registrarVisitaClienteMobile(request);

        Callback<ResponseBase> cb = getCallback(callback);

        call.enqueue(cb);
    }

    public void cerrarHojaDeRutaMobile(long hojaDeRutaId, String codigoDeMovil, int usuarioId,
                                       List<GastoDeReparto> gastos, List<DeclaracionEfectivo> declaracionesEfectivo,
                                       ApiCallback<ResponseBase> callback){

        CierreHojaDeRutaMobileRequest request = new CierreHojaDeRutaMobileRequest();
        request.codigoMovil = codigoDeMovil;
        request.hojaDeRutaId = hojaDeRutaId;
        request.gastos = gastos;
        request.declaracionesEfectivo = declaracionesEfectivo;
        request.usuarioId = usuarioId;

        Call<ResponseBase> call = srv.cerrarHojaDeRutaMobile(request);

        Callback<ResponseBase> cb = getCallback(callback);

        call.enqueue(cb);
    }


    public void verificarConfiguracion(String codigoDeMovil, ApiCallback<VerificarConfiguracionMobileResponse> callback){

        Call<VerificarConfiguracionMobileResponse> call = srv.verificarConfiguracionMobile(codigoDeMovil);

        Callback<VerificarConfiguracionMobileResponse> cb = getCallback(callback);

        call.enqueue(cb);
    }

    public void enviarUbicacion(String identificadorMovil, long hojaDeRuta_id, String latitud, String longitud,
                                ApiCallback<VerificarConfiguracionMobileResponse> callback){

        EnviarUbicacionRequest request = new EnviarUbicacionRequest();
        request.codigoIdentificacionMovil = identificadorMovil;
        request.ubicacion = new UbicacionMovil();
        request.ubicacion.latitud = latitud;
        request.ubicacion.longitud = longitud;
        request.ubicacion.hojaDeRuta_id = hojaDeRuta_id;
        request.ubicacion.estadoMovil_ids = 1;

        Call<VerificarConfiguracionMobileResponse> call = srv.informarUbicacion(request);

        if(callback!=null){
            Callback<VerificarConfiguracionMobileResponse> cb = getCallback(callback);
            call.enqueue(cb);
        }
    }

    public void agregarClienteAHojaDeRuta(long hojaDeRutaId, String codigoDeMovil, int clienteId,
                                             ApiCallback<ResponseBase> callback){

        AgregarClienteAHojaDeRutaMobileRequest request = new AgregarClienteAHojaDeRutaMobileRequest();
        request.codigoDeMovil = codigoDeMovil;
        request.hojaDeRutaId = hojaDeRutaId;
        request.clienteId = clienteId;

        Call<ResponseBase> call = srv.agregarClienteAHojaDeRuta(request);

        Callback<ResponseBase> cb = getCallback(callback);

        call.enqueue(cb);
    }

    public void obtenerBarriles(long hojaDeRutaId, String codigoDeMovil,
                                ApiCallback<ObtenerBarrilesResponse> callback){

        Call<ObtenerBarrilesResponse> call = srv.obtenerBarriles(hojaDeRutaId, codigoDeMovil);

        Callback<ObtenerBarrilesResponse> cb = getCallback(callback);

        call.enqueue(cb);
    }

    public void descargarBaseDeDatos(String fecha, String codigoDeMovil, String version,
                                          ApiCallback<FileApiResponse> callback){


        Call<ResponseBody> call = srv.descargarBaseDeDatos(fecha, codigoDeMovil, version);

        Callback<ResponseBody> cb = getCallbackForFile(callback);

        call.enqueue(cb);
    }

    public void obtenerIncidentesDeUsuario(int usuarioId, ApiCallback<IncidentesTotalesResponse> callback){

        Call<IncidentesTotalesResponse> call = srv.obtenerIncidentesAbiertosPorUsuario(usuarioId);

        Callback<IncidentesTotalesResponse> cb = getCallback(callback);

        call.enqueue(cb);
    }
}
