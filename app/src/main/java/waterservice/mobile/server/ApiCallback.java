package waterservice.mobile.server;

public abstract class ApiCallback<T> {

    public abstract void onSuccess(T response);
    public abstract void onError(String message, int errorCode);

}
