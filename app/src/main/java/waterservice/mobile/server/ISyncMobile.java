package waterservice.mobile.server;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import waterservice.mobile.clases.apiContracts.AgregarClienteAHojaDeRutaMobileRequest;
import waterservice.mobile.clases.apiContracts.CierreHojaDeRutaMobileRequest;
import waterservice.mobile.clases.apiContracts.ConfirmarClientesEnHojaDeRutaRequest;
import waterservice.mobile.clases.apiContracts.EnviarUbicacionRequest;
import waterservice.mobile.clases.apiContracts.IncidentesTotalesResponse;
import waterservice.mobile.clases.apiContracts.RegistrarVisitaClienteMobileRequest;
import waterservice.mobile.clases.apiContracts.ResponseBase;
import waterservice.mobile.clases.apiContracts.Sync.ObtenerBarrilesResponse;
import waterservice.mobile.clases.apiContracts.Sync.ObtenerComandosParaEjecutarResponse;
import waterservice.mobile.clases.apiContracts.Sync.ObtenerNuevosPedidosResponse;
import waterservice.mobile.clases.apiContracts.Sync.VerificarConfiguracionMobileResponse;

public interface ISyncMobile {

    @GET("Sync/ObtenerNuevosPedidos")
    Call<ObtenerNuevosPedidosResponse> obtenerNuevosPedidos(@Query("hojaDeRutaId") long hojaDeRutaId,
                                                            @Query("codigoDeMovil") String codigoDeMovil);

    @POST("Sync/ConfirmarClienteAgregadoALaRuta")
    Call<ResponseBase> confirmarClientesEnHojaDeRuta(@Body() ConfirmarClientesEnHojaDeRutaRequest request);

    @GET("Sync/ObtenerComandosParaEjecutar")
    Call<ObtenerComandosParaEjecutarResponse> obtenerComandosParaEjecutar(@Query("codiIdentificacion") String codiIdentificacion);

    @POST("Sync/RegistrarVisitaClienteMobile")
    Call<ResponseBase> registrarVisitaClienteMobile(@Body() RegistrarVisitaClienteMobileRequest request);

    @POST("HojasDeRuta/CerrarHojaDeRutaMobile")
    Call<ResponseBase> cerrarHojaDeRutaMobile(@Body() CierreHojaDeRutaMobileRequest request);

    @GET("Sync/VerificarConfiguracion")
    Call<VerificarConfiguracionMobileResponse> verificarConfiguracionMobile(@Query("codigoIdentificacion") String codigoIdentificacion);

    @POST("Moviles/InformarUbicacion")
    Call<VerificarConfiguracionMobileResponse> informarUbicacion(@Body() EnviarUbicacionRequest request);

    @POST("Sync/AgregarClienteAHojaDeRuta")
    Call<ResponseBase> agregarClienteAHojaDeRuta(@Body() AgregarClienteAHojaDeRutaMobileRequest request);

    @GET("Sync/ObtenerBarriles")
    Call<ObtenerBarrilesResponse> obtenerBarriles(@Query("hojaDeRutaId") long hojaDeRutaId,
                                                  @Query("codigoDeMovil") String codigoDeMovil);

    @GET("HojasDeRuta/DescargarBaseDeDatos")
    Call<ResponseBody> descargarBaseDeDatos(@Query("fecha") String fecha,
                                            @Query("codigoIdentificacionMovil") String codigoDeMovil,
                                            @Query("version") String version);

    @GET("Incidentes/ObtenerIncidentesAbiertosPorUsuario")
    Call<IncidentesTotalesResponse> obtenerIncidentesAbiertosPorUsuario(@Query("usuarioId") int usuarioId);
}
