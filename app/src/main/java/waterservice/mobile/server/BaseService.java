package waterservice.mobile.server;

import android.content.Context;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.apiContracts.ErrorResponse;
import waterservice.mobile.clases.apiContracts.ResponseBase;
import waterservice.mobile.clases.apiContracts.Sync.FileApiResponse;

public class BaseService {

    static String urlApi;

    <T> Callback<T> getCallback(final ApiCallback<T> callback){

        Callback<T> cb = new Callback<T>() {

            @Override
            public void onResponse(Call<T> call, Response<T> response)  {

                if(response.isSuccessful())
                {
                    T r =  response.body();
                    ResponseBase rBase = (ResponseBase) r ;

                    if(rBase.error != 0)
                        callback.onError(rBase.message, rBase.error);
                    else
                        callback.onSuccess(r);

                }else{

                    try {

                        String bodyContent = response.errorBody().string();
                        ErrorResponse error = new Gson().fromJson(bodyContent, ErrorResponse.class);
                        callback.onError(error==null? "Error no determinado" : error.message, 1);

                    } catch (Exception e) {
                        callback.onError("Error: "+e.getMessage(), 1);
                    }
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                callback.onError("Error al conectar con el servidor. "+t.getMessage(), 2);
            }
        };
        return cb;
    }

    Callback<ResponseBody> getCallbackForFile(final ApiCallback<FileApiResponse> callback){

        Callback<ResponseBody> cb = new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)  {

                if(response.isSuccessful())
                {
                    FileApiResponse r =  new FileApiResponse();
                    r.bodyResponse = response.body();
                    callback.onSuccess(r);

                }else{

                    try {

                        String bodyContent = response.errorBody().string();
                        ErrorResponse error = new Gson().fromJson(bodyContent, ErrorResponse.class);
                        callback.onError(error==null? "Error no determinado" : error.message, 1);

                    } catch (Exception e) {
                        callback.onError("Error: "+e.getMessage(), 1);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onError("Error al conectar con el servidor. "+t.getMessage(), 2);
            }
        };
        return cb;
    }

    public static Retrofit getService(Context ctx) {

        AppSession appSession = new AppSession(ctx);
        final String serverRemoto= appSession.getValueString("servidor_web");
        final String serverLocal= appSession.getValueString("servidor_web_local");
        final boolean modoRedLocal= appSession.getValueBoolean("modo_local");

        urlApi= modoRedLocal? serverLocal : serverRemoto;

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        OkHttpClient client = httpClient
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(40, TimeUnit.SECONDS).build();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(urlApi)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}