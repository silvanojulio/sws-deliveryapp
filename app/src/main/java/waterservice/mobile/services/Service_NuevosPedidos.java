package waterservice.mobile.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.AlertaDeCliente;
import waterservice.mobile.clases.ArticuloDeAbono;
import waterservice.mobile.clases.ArticuloDeComodato;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.Dispenser;
import waterservice.mobile.clases.DispenserAsociado;
import waterservice.mobile.clases.Factura;
import waterservice.mobile.clases.OrdenDeTrabajo;
import waterservice.mobile.clases.PrecioEspecial;
import waterservice.mobile.clases.StockDeCliente;
import waterservice.mobile.clases.apiContracts.AlertaDeClienteMobile;
import waterservice.mobile.clases.apiContracts.ArticuloDeAbonoMobile;
import waterservice.mobile.clases.apiContracts.ArticuloDeComodatoMobile;
import waterservice.mobile.clases.apiContracts.ClientePedidoMobile;
import waterservice.mobile.clases.apiContracts.DispenserAsociadoAOrdenDeTrabajoMobile;
import waterservice.mobile.clases.apiContracts.DispenserDeClienteMobile;
import waterservice.mobile.clases.apiContracts.FacturaMobile;
import waterservice.mobile.clases.apiContracts.OrdenDeTrabajoMobile;
import waterservice.mobile.clases.apiContracts.PrecioEspecialMobile;
import waterservice.mobile.clases.apiContracts.ResponseBase;
import waterservice.mobile.clases.apiContracts.StockDeClienteMobile;
import waterservice.mobile.clases.apiContracts.Sync.ObtenerNuevosPedidosResponse;
import waterservice.mobile.server.ApiCallback;
import waterservice.mobile.server.SyncMobile;
import waterservice.mobile.utiles.Utiles;

import static android.content.Context.NOTIFICATION_SERVICE;

public class Service_NuevosPedidos extends  Service_Base{

    Service_Clientes srvClientes;
    SyncMobile srvSyncMobile;

    public Service_NuevosPedidos(Context context) {
        super(context);
        srvClientes = new Service_Clientes(context);
        srvSyncMobile = new SyncMobile(context);
    }

    private static boolean estaSyncronizando = false;

    public void SyncNuevosPedidos() throws Exception {

        if(estaSyncronizando) throw new Exception("En este momento está syncronizando");

        try{
            estaSyncronizando = true;
            String identificadorMovil= Utiles.Get_Config_Value("codigo_identificacion_movil", CurrentContext);

            srvSyncMobile.obtenerNuevosPedidos(appSession.getHojaDeRuta().id, identificadorMovil,
                    new ApiCallback<ObtenerNuevosPedidosResponse>() {
                        @Override
                        public void onSuccess(ObtenerNuevosPedidosResponse response) {
                            estaSyncronizando = false;
                            ProcesarNuevosPedidos(response.nuevosPedidos.clientes);
                        }

                        @Override
                        public void onError(String message, int errorCode) {
                            int i = 0;
                            estaSyncronizando = false;

                            boolean mostrarError =  new AppSession(CurrentContext).getValueBoolean("mostrar_errores");

                            if(mostrarError)
                                Toast.makeText(CurrentContext, "Sync: Error al conectar al servidor: "+message, Toast.LENGTH_SHORT).show();
                        }
                    });
        }catch (Exception ex){
            estaSyncronizando = false;
            throw ex;
        }
    }

    public void ProcesarNuevosPedidos(List<ClientePedidoMobile> pedidos){

        List<Integer> idsClientes = new ArrayList<>();
        int cantidadClientesNuevos = 0;

        for (ClientePedidoMobile pedido: pedidos) {
           try{
               boolean esAgregado = agregarPedidoEnHojaDeRuta(pedido);
               idsClientes.add(pedido.cliente.cliente_id);
               if(esAgregado)cantidadClientesNuevos++;
           }catch (Exception ex){
               ex.printStackTrace();
           }
        }

        try{
            if(cantidadClientesNuevos>0)
                generarNotificacionPorClientesNuevos(cantidadClientesNuevos);
        }catch (Exception ex){

        }

        try{
            if(idsClientes!=null && idsClientes.size()>0 )
                confirmarClientesEnHojaDeRuta(idsClientes);
        }catch (Exception ex){

        }
    }

    public boolean agregarPedidoEnHojaDeRuta(ClientePedidoMobile pedido) throws Exception{

        Cliente clt = obtenerClienteDePedido(pedido);

        if(existeClienteEnHojaDeRuta(pedido.cliente.cliente_id)) {

            boolean resultado = false;

            if(pedido.ordenesDeTrabajo!=null && pedido.ordenesDeTrabajo.size()>0 && !verificarTieneOrdenDeTrabajo(clt)){

                List<OrdenDeTrabajo> ordenesDeTrabajo = obtenerOrdenesDeTrabajoDePedido(pedido.ordenesDeTrabajo);
                List<DispenserAsociado> dispenserAsociadosAServicioTecnico = obtenerDispensersAsociadosDeOTDePedido(pedido.ordenesDeTrabajo);
                List<Dispenser> dispensers = obtenerDispensersDePedido(pedido.dispensers);

                srvClientes.GuardarDispensersDeCliente(dispensers);
                srvClientes.GuardarOrdenesDeTrabajoDeCliente(ordenesDeTrabajo);
                srvClientes.GuardarDispensersDeOT(dispenserAsociadosAServicioTecnico);

                Cliente cliente = srvClientes.obtenerCliente(clt.cliente_id);
                cliente.tipoDeVisitaId = clt.tipoDeVisitaId;
                cliente.comunicacion1 = clt.comunicacion1;
                cliente.comunicacion2 = clt.comunicacion2;
                cliente.comunicacion3 = clt.comunicacion3;
                cliente.comunicacion4 = clt.comunicacion4;
                cliente.esSync = true;
                _dbHelper.getRTE_Dao_Cliente().update(cliente);
                resultado = true;
            }

            if(pedido.cliente.esPedido){
                Cliente cliente = srvClientes.obtenerCliente(clt.cliente_id);
                cliente.esPedido = true;
                cliente.esSync = true;
                cliente.tipoDeVisitaId = clt.tipoDeVisitaId;
                cliente.comunicacion1 = clt.comunicacion1;
                cliente.comunicacion2 = clt.comunicacion2;
                cliente.comunicacion3 = clt.comunicacion3;
                cliente.comunicacion4 = clt.comunicacion4;
                _dbHelper.getRTE_Dao_Cliente().update(cliente);
                resultado = true;
            }

            return resultado;

        }else{

            List<ArticuloDeAbono> articulosDeAbono = obtenerArticulosDeAbonoDePedido(pedido.articulosDeAbonos);
            List<ArticuloDeComodato> comodatos = obtenerArticuloDeComodatoDePedido(pedido.comodatos);
            List<PrecioEspecial> preciosEspeciales = obtenerPreciosEspecialesDePedido(pedido.preciosEspeciales);
            List<Factura> facturas = obtenerFacturasDePedido(pedido.facturas);
            List<AlertaDeCliente> alertas = obtenerAlertasDeClienteDePedido(pedido.alertasDeCliente);
            List<Dispenser> dispensers = obtenerDispensersDePedido(pedido.dispensers);
            List<OrdenDeTrabajo> ordenesDeTrabajo = obtenerOrdenesDeTrabajoDePedido(pedido.ordenesDeTrabajo);
            List<DispenserAsociado> dispenserAsociadosAServicioTecnico = obtenerDispensersAsociadosDeOTDePedido(pedido.ordenesDeTrabajo);
            List<StockDeCliente> stocks = obtenerStocksDePedido(pedido.stock);

            srvClientes.agregarCliente(clt, articulosDeAbono, comodatos, preciosEspeciales,
                    facturas, alertas, dispensers, ordenesDeTrabajo,
                    dispenserAsociadosAServicioTecnico, stocks);

            return true;
        }
    }

    private boolean verificarTieneOrdenDeTrabajo(Cliente clt) {

        List<OrdenDeTrabajo> ordenesDeTrabajo = _dbHelper.getRTE_Dao_OrdenDeTrabajo().queryForEq("cliente_id",clt.cliente_id);
        return ordenesDeTrabajo!=null && ordenesDeTrabajo.size()>0;
    }

    private List<PrecioEspecial> obtenerPreciosEspecialesDePedido(List<PrecioEspecialMobile> preciosEspeciales) {
        List<PrecioEspecial> itemsDb = new ArrayList<>();
        if(preciosEspeciales==null) return itemsDb;
        for (PrecioEspecialMobile p: preciosEspeciales) {
            PrecioEspecial itemDb = new PrecioEspecial();
            itemDb.articuloId = p.articulo_id;
            itemDb.clienteId = p.cliente_id;
            itemDb.precio = p.precio;
            itemsDb.add(itemDb);
        }

        return itemsDb;
    }

    private List<StockDeCliente> obtenerStocksDePedido(List<StockDeClienteMobile> stocks) {
        List<StockDeCliente> itemsDb = new ArrayList<>();
        if(stocks==null) return itemsDb;
        for (StockDeClienteMobile stock: stocks) {
            StockDeCliente itemDb = new StockDeCliente();
            itemDb.articuloId = stock.articulo_id;
            itemDb.clienteId = stock.clienteId;
            itemDb.cantidadARecuperar = stock.cantidadARecuperar;
            itemDb.nombreArticulo = stock.nombreArticulo;
            itemDb.cantidadPermitida = stock.cantidadPermitida;
            itemDb.consumoPromedio = stock.consumoPromedio;
            itemDb.stockActual = stock.stockActual;
            itemsDb.add(itemDb);
        }

        return itemsDb;
    }

    private List<DispenserAsociado> obtenerDispensersAsociadosDeOTDePedido(List<OrdenDeTrabajoMobile> ordenesDeTrabajo) {
        List<DispenserAsociado> itemsDb = new ArrayList<>();
        if(ordenesDeTrabajo==null) return itemsDb;
        for (OrdenDeTrabajoMobile ot: ordenesDeTrabajo) {

            if(ot.dispensers==null) continue;

            for (DispenserAsociadoAOrdenDeTrabajoMobile dispenser: ot.dispensers) {
                DispenserAsociado itemDb = new DispenserAsociado();
                itemDb.deCliente = dispenser.deCliente;
                itemDb.dispenser_id = dispenser.dispenser_id;
                itemDb.mantemientoCargado = false;
                itemDb.nroDispenser = dispenser.nroDispenser;
                itemDb.ordenDeTrabajoId = dispenser.ordenDeTrabajoServer_id;
                itemDb.OrdenDeTrabajoServer_id = dispenser.ordenDeTrabajoServer_id;
                itemsDb.add(itemDb);
            }
        }

        return itemsDb;
    }

    private List<OrdenDeTrabajo> obtenerOrdenesDeTrabajoDePedido(List<OrdenDeTrabajoMobile> ordenesDeTrabajo) {
        List<OrdenDeTrabajo> itemsDb = new ArrayList<>();
        if(ordenesDeTrabajo==null) return itemsDb;
        for (OrdenDeTrabajoMobile p: ordenesDeTrabajo) {
            OrdenDeTrabajo itemDb = new OrdenDeTrabajo();
            itemDb.cantDispensers = p.cantDispensers;
            itemDb.cliente_id = p.cliente_id;
            itemDb.comentarios = p.comentarios;
            itemDb.franjaHoraria = p.franjaHoraria;
            itemDb.ordenDeTrabajoServer_id = p.ordenDeTrabajoServer_id;
            itemDb.responsableEnCliente = p.responsableEnCliente;
            itemDb.prioridad = p.prioridad;
            itemDb.sectorUbicacion = p.sectorUbicacion;
            itemDb.sintoma = p.sintoma;
            itemDb.sintoma_id = p.sintoma_id;
            itemDb.telefonoResponsable = p.telefonoResponsable;

            itemsDb.add(itemDb);
        }

        return itemsDb;
    }

    private List<Dispenser> obtenerDispensersDePedido(List<DispenserDeClienteMobile> dispensers) {

        List<Dispenser> itemsDb = new ArrayList<>();
        if(dispensers==null) return itemsDb;
        for (DispenserDeClienteMobile p: dispensers) {
            Dispenser itemDb = new Dispenser();
            itemDb.id = p.dispenserId;
            itemDb.cliente_id = p.cliente_id;
            itemDb.color = p.color;
            itemDb.marca = p.marca;
            itemDb.nroDispenser = p.nroDispenser;
            itemDb.tipo = p.tipo;
            itemsDb.add(itemDb);
        }

        return itemsDb;
    }

    private List<AlertaDeCliente> obtenerAlertasDeClienteDePedido(List<AlertaDeClienteMobile> alertasDeCliente) {
        List<AlertaDeCliente> itemsDb = new ArrayList<>();
        if(alertasDeCliente==null) return itemsDb;
        for (AlertaDeClienteMobile a: alertasDeCliente) {
            AlertaDeCliente itemDb = new AlertaDeCliente();
            itemDb.cliente_id = a.cliente_id;
            itemDb.comentarios = a.comentarios;
            itemDb.esBloqueante = a.esBloqueante;
            itemDb.tipoDeAlerta = a.tipoDeAlerta;
            itemDb.tipoDeAlerta_ids = a.tipoDeAlerta_ids;
            itemsDb.add(itemDb);
        }

        return itemsDb;
    }

    private List<Factura> obtenerFacturasDePedido(List<FacturaMobile> facturas) {
        List<Factura> itemsDb = new ArrayList<>();
        if(facturas==null) return itemsDb;
        for (FacturaMobile f: facturas) {
            Factura itemDb = new Factura();
            itemDb.id = f.factura_id;
            itemDb.cliente_id = f.cliente_id;
            itemDb.cobradoActual = f.cobradoActual;
            itemDb.entregada = f.entregada;
            itemDb.fechaFactura = Utiles.ConvertToString(f.fechaFactura, "yyyy-MM-dd HH:mm:ss");
            itemDb.cobradoActual = f.cobradoActual;
            itemDb.fechaVencimiento1 = Utiles.ConvertToString(f.fechaVencimiento1, "yyyy-MM-dd HH:mm:ss");
            itemDb.fechaVencimiento2 = Utiles.ConvertToString(f.fechaVencimiento2, "yyyy-MM-dd HH:mm:ss");
            itemDb.fechaVencimiento3 = Utiles.ConvertToString(f.fechaVencimiento3, "yyyy-MM-dd HH:mm:ss");
            itemDb.montoFacturaTotal = f.montoFacturaTotal;
            itemDb.nroFactura = f.nroFactura;
            itemDb.tipoFactura = f.tipoFactura;
            itemDb.saldoActual = f.saldoActual;

            itemsDb.add(itemDb);
        }

        return itemsDb;
    }

    private List<ArticuloDeComodato> obtenerArticuloDeComodatoDePedido(List<ArticuloDeComodatoMobile> comodatos) {
        List<ArticuloDeComodato> itemsDb = new ArrayList<>();
        if(comodatos==null) return itemsDb;
        for (ArticuloDeComodatoMobile c: comodatos) {
            ArticuloDeComodato itemDb = new ArticuloDeComodato();
            itemDb.articulo_id = c.articulo_id;
            itemDb.cantidad = c.cantidad;
            itemDb.cliente_id = c.cliente_id;
            itemDb.precio = c.precio;
            itemsDb.add(itemDb);
        }

        return itemsDb;
    }

    private List<ArticuloDeAbono> obtenerArticulosDeAbonoDePedido(List<ArticuloDeAbonoMobile> articulosDeAbonos) {
        List<ArticuloDeAbono> itemsDb = new ArrayList<>();
        if(articulosDeAbonos==null) return itemsDb;
        for (ArticuloDeAbonoMobile a: articulosDeAbonos) {
            ArticuloDeAbono itemDb = new ArticuloDeAbono();
            itemDb.articulo_id = a.articulo_id;
            itemDb.cantidad = a.cantidad;
            itemDb.cliente_id = a.cliente_id;
            itemDb.precioExcendente = a.precioExcendente;
            itemsDb.add(itemDb);
        }

        return itemsDb;
    }

    private Cliente obtenerClienteDePedido(ClientePedidoMobile pedido) {

        Cliente clt=new Cliente();
        clt.actividadCliente = pedido.cliente.actividadCliente;
        clt.cliente_id = pedido.cliente.cliente_id;
        clt.nombreCliente = pedido.cliente.nombreCliente;

        clt.tipoCliente_ids = pedido.cliente.tipoCliente_ids;
        clt.estadoCliente_ids = pedido.cliente.estadoCliente_ids;

        clt.nombreProvincia = pedido.cliente.nombreProvincia;
        clt.nombreCiudad = pedido.cliente.nombreCiudad;
        clt.nombreBarrio = pedido.cliente.nombreBarrio;
        clt.domicilioCompleto = pedido.cliente.domicilioCompleto;

        clt.torre = pedido.cliente.torre;
        clt.piso = pedido.cliente.piso;
        clt.depto = pedido.cliente.depto;
        clt.manzana = pedido.cliente.manzana;
        clt.lote = pedido.cliente.lote;
        clt.numeroPuerta = pedido.cliente.numeroPuerta;
        clt.nombreCalle = pedido.cliente.nombreCalle;
        clt.actividadCliente = pedido.cliente.actividadCliente;

        clt.clienteFactura_id = pedido.cliente.clientePadre==null?
                                pedido.cliente.cliente_id :
                                pedido.cliente.clientePadre;

        clt.altitud = pedido.cliente.altitud;
        clt.longitud = pedido.cliente.longitud;

        clt.fechaIngreso = Utiles.ConvertToString(pedido.cliente.fechaIngreso, "dd/MM/yyyy");
        clt.fechaUtlimaEntrega = Utiles.ConvertToString(pedido.cliente.fechaUtlimaEntrega, "dd/MM/yyyy");
        clt.fechaUltimoCobroFactura = Utiles.ConvertToString(pedido.cliente.fechaUltimoCobroFactura, "dd/MM/yyyy");
        clt.fechaUltimaEnvases = Utiles.ConvertToString(pedido.cliente.fechaUltimaEnvases, "dd/MM/yyyy");
        clt.fechaUltimaDevoluciones = Utiles.ConvertToString(pedido.cliente.fechaUltimaDevoluciones, "dd/MM/yyyy");

        clt.cliente_id = pedido.cliente.cliente_id;
        clt.orden = pedido.cliente.orden;
        clt.esVentaExtra = pedido.cliente.esVentaExtra;
        clt.saldoFacturacion = pedido.cliente.saldoFacturacion;
        clt.saldoConsumos = pedido.cliente.saldoConsumos;
        clt.permiteVender = pedido.cliente.permiteVender;
        clt.permiteCobrar = pedido.cliente.permiteCobrar;
        clt.permitePrestarEnvases = pedido.cliente.permitePrestarEnvases;
        clt.enviarSMS = pedido.cliente.enviarSMS;
        clt.esPedido = pedido.cliente.esPedido;

        clt.comunicacion1 = pedido.cliente.comunicacion1;
        clt.comunicacion2 = pedido.cliente.comunicacion2;
        clt.comunicacion3 = pedido.cliente.comunicacion3;
        clt.comunicacion4 = pedido.cliente.comunicacion4;

        clt.creditoDisponible = pedido.cliente.creditoDisponible;
        clt.visitado = pedido.cliente.visitado;
        clt.ausente = pedido.cliente.ausente;
        clt.ventaEntrega = pedido.cliente.ventaEntrega;
        clt.cobroConsumo = pedido.cliente.cobroConsumo;
        clt.cobroFactura = pedido.cliente.cobroFactura;
        clt.devolucionEnvases = pedido.cliente.devolucionEnvases;
        clt.prestamoEnvases = pedido.cliente.prestamoEnvases;
        clt.devolucionArticulo = pedido.cliente.devolucionArticulo;
        clt.visita_altitud = pedido.cliente.visita_altitud;
        clt.visita_longitud = pedido.cliente.visita_longitud;

        clt.tieneAlertas = pedido.cliente.tieneAlarmas;
        clt.saldoNotasDeCredito = pedido.cliente.saldoNotasDeCredito;
        clt.requiereRemito = pedido.cliente.conRemito==null? false:pedido.cliente.conRemito;

        clt.listaDePrecioId = pedido.cliente.listaDePrecios_id;

        clt.relevarCoordenadas = pedido.cliente.relevarCoordenadas;

        clt.tipoDeVisitaId = pedido.cliente.tipoDeVisitaId;

        clt.esSync = true;
        clt.esRepaso = false;

        return clt;
    }

    private boolean existeClienteEnHojaDeRuta(int clienteId)throws Exception{
        Cliente clt = srvClientes.obtenerCliente(clienteId);
        return clt != null;
    }

    public void confirmarClientesEnHojaDeRuta(List<Integer> idsClientesAConfirmar)throws Exception{

        String identificadorMovil= Utiles.Get_Config_Value("codigo_identificacion_movil", CurrentContext);

        srvSyncMobile.confirmarClientesEnRuta(
                appSession.getHojaDeRuta().id,
                identificadorMovil,
                idsClientesAConfirmar,
                new ApiCallback<ResponseBase>() {
                    @Override
                    public void onSuccess(ResponseBase response) {
                        int i = 0;
                    }
                    @Override
                    public void onError(String message, int errorCode) {
                        int i = 0;
                    }
                });
    }

    public void generarNotificacionPorClientesNuevos(int cantidadClientes){
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(CurrentContext)
                        .setSmallIcon(R.drawable.ic_stat_sync)
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setAutoCancel(true)
                        .setContentTitle("Hoja de ruta actualizada")
                        .setContentText("Se han agregado "+ String.valueOf(cantidadClientes) +
                                " clientes a la hoja de ruta");

        int mNotificationId = 001;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) CurrentContext.getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());

    }


}
