package waterservice.mobile.services;

import android.content.Context;

import com.j256.ormlite.dao.GenericRawResults;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import waterservice.mobile.clases.ArticuloDeAbono;
import waterservice.mobile.clases.ArticuloDeComodato;
import waterservice.mobile.clases.ArticuloDeLista;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.DescuentoPorCantidad;
import waterservice.mobile.clases.ListaDePrecios;
import waterservice.mobile.clases.PrecioEspecial;
import waterservice.mobile.clases.StockDeCliente;

public class Service_Articulos extends  Service_Base{

    public Service_Articulos(Context context){
        super(context);
    }

    public List<ArticuloDeLista> obtenerArticulosDeLista()throws Exception
    {
        String select="SELECT * FROM ArticulosDeLista WHERE tipoArticulo_ids=1";

        GenericRawResults<ArticuloDeLista> result=_dbHelper.getRTE_Dao_ArticuloDeLista()
                .queryRaw(select, _dbHelper.getRTE_Dao_ArticuloDeLista().getRawRowMapper());

        List<ArticuloDeLista> list=result.getResults();

        return list;
    }

    public List<ArticuloDeLista> obtenerRepuestos()throws Exception
    {
        String select="SELECT * FROM ArticulosDeLista WHERE tipoArticulo_ids=3";

        GenericRawResults<ArticuloDeLista> result=_dbHelper.getRTE_Dao_ArticuloDeLista()
                .queryRaw(select, _dbHelper.getRTE_Dao_ArticuloDeLista().getRawRowMapper());

        List<ArticuloDeLista> list=result.getResults();

        return list;
    }

    public List<ArticuloDeLista> obtenerArticulosControlablesDeplaya()throws Exception
    {
        String select="SELECT * FROM ArticulosDeLista WHERE controlableEnPlaya=1 AND tipoArticulo_ids=1";

        GenericRawResults<ArticuloDeLista> result=_dbHelper.getRTE_Dao_ArticuloDeLista()
                .queryRaw(select, _dbHelper.getRTE_Dao_ArticuloDeLista().getRawRowMapper());

        List<ArticuloDeLista> list=result.getResults();

        return list;
    }

    public static ArticuloDeLista obtenerArticulo(int articuloId, List<ArticuloDeLista> articulos){

        ArticuloDeLista r = null;

        for (ArticuloDeLista art: articulos) {
            if(art.id == articuloId)
                r=art;
        }

        return r;
    }

    public List<ArticuloDeComodato> obtenerArticulosDeComodatos(int cliente_id)throws Exception
    {
        String select="SELECT * FROM ArticulosDeComodatos  WHERE cliente_id="+String.valueOf(cliente_id);

        GenericRawResults<ArticuloDeComodato> result=_dbHelper.getRTE_Dao_ArticuloDeComodato()
                .queryRaw(select, _dbHelper.getRTE_Dao_ArticuloDeComodato().getRawRowMapper());

        List<ArticuloDeComodato> list=result.getResults();

        return list;
    }

    public List<ArticuloDeAbono> obtenerArticulosDeAbonos(int cliente_id)throws Exception
    {
        String select="SELECT * FROM ArticulosDeAbonos WHERE cliente_id="+String.valueOf(cliente_id);

        GenericRawResults<ArticuloDeAbono> result=_dbHelper.getRTE_Dao_ArticuloDeAbono()
                .queryRaw(select, _dbHelper.getRTE_Dao_ArticuloDeAbono().getRawRowMapper());

        List<ArticuloDeAbono> list=result.getResults();

        return list;
    }

    public static String[] ObtenerNombresDeArticulosTodos(List<ArticuloDeLista> articulos ){

        String[] result= new String[articulos.size()];

        for (int i=0; i<articulos.size(); i++){
            result[i]=articulos.get(i).nombreArticulo;
        }

        return result;
    }

    public static String[] ObtenerNombresDeArticulosTodos(List<ArticuloDeLista> articulos ,
                                                          List<ArticuloDeAbono> _artAbonos,
                                                          List<ArticuloDeComodato> _artComodato ){

        String[] result= new String[articulos.size()];

        for (int i=0; i<articulos.size(); i++){
            result[i]=articulos.get(i).nombreArticulo;

            Boolean encontrado=false;

            if(_artAbonos!=null && !encontrado){

                for (ArticuloDeAbono item : _artAbonos) {
                    if(item.articulo_id==  articulos.get(i).id)
                    {
                        result[i]+=" (ABO)";
                        encontrado=true;
                        break;
                    }
                }
            }

            if(_artComodato!=null && !encontrado){

                for (ArticuloDeComodato item : _artComodato) {
                    if(item.articulo_id==articulos.get(i).id)
                    {
                        result[i]+=" (COM)";
                        encontrado=true;
                        break;
                    }
                }
            }

        }

        return result;
    }

    public List<ArticuloDeLista> obtenerArticulosRepuestos()throws Exception
    {
        String select="SELECT * FROM ArticulosDeLista WHERE tipoArticulo_ids=3";

        GenericRawResults<ArticuloDeLista> result=_dbHelper.getRTE_Dao_ArticuloDeLista()
                .queryRaw(select, _dbHelper.getRTE_Dao_ArticuloDeLista().getRawRowMapper());

        List<ArticuloDeLista> list=result.getResults();

        return list;
    }

    public List<ArticuloDeLista> obtenerArticulosActividadesDeMantenimiento()throws Exception
    {
        String select="SELECT * FROM ArticulosDeLista WHERE tipoArticulo_ids=4";

        GenericRawResults<ArticuloDeLista> result=_dbHelper.getRTE_Dao_ArticuloDeLista()
                .queryRaw(select, _dbHelper.getRTE_Dao_ArticuloDeLista().getRawRowMapper());

        List<ArticuloDeLista> list=result.getResults();

        return list;
    }

    public Hashtable<Integer, Double> obtenerPreciosDeArticulo(List<Integer> idsArticulos, Cliente cliente ) throws Exception {

        Hashtable<Integer, Double> r = new Hashtable<Integer, Double>();

        if(idsArticulos == null) return r;

        List<PrecioEspecial> preciosEspeciales = obtenerPreciosEspecialesDeCliente(cliente.clienteFactura_id);
        List<ListaDePrecios> listasDePrecios = this.obtenerArticulosDeListaDePrecio(cliente.listaDePrecioId);

        for (Integer articuloId : idsArticulos) {

            boolean encontrado = false;

            for (PrecioEspecial art : preciosEspeciales) {
                if(art.articuloId == articuloId){

                    if(!r.containsKey(articuloId))
                        r.put(articuloId, art.precio);

                    encontrado = true;
                    continue;
                }
            }

            if(encontrado)continue;

            for (ListaDePrecios art : listasDePrecios) {
                if(art.articuloId == articuloId){

                    if(!r.containsKey(articuloId))
                        r.put(articuloId, art.precio);

                    encontrado = true;
                    continue;
                }
            }

            if(encontrado)continue;
        }

        return r;
    }

    public List<PrecioEspecial> obtenerPreciosEspecialesDeCliente(int clienteId)throws Exception{

        String select="SELECT * FROM PreciosEspeciales WHERE clienteId = "+String.valueOf(clienteId);

        GenericRawResults<PrecioEspecial> result=_dbHelper.getRTE_Dao_PrecioEspecial()
                .queryRaw(select, _dbHelper.getRTE_Dao_PrecioEspecial().getRawRowMapper());

        List<PrecioEspecial> list=result.getResults();

        return list;
    }

    public List<ListaDePrecios> obtenerArticulosDeListaDePrecio(Integer listaId) throws Exception{

        if(listaId==null) return new ArrayList<ListaDePrecios>();

        String select="SELECT * FROM ListasDePrecios WHERE listaId = "+String.valueOf(listaId);

        GenericRawResults<ListaDePrecios> result=_dbHelper.getRTE_Dao_ListaDePrecios()
                .queryRaw(select, _dbHelper.getRTE_Dao_ListaDePrecios().getRawRowMapper());

        List<ListaDePrecios> list=result.getResults();

        return list;
    }

    public List<DescuentoPorCantidad> obtenerDescuentosPorCantidad(Integer listaId) throws Exception{

        if(listaId==null) return new ArrayList<DescuentoPorCantidad>();

        String select="SELECT * FROM DescuentosPorCantidad WHERE listaDePrecioId = "+String.valueOf(listaId);

        GenericRawResults<DescuentoPorCantidad> result=_dbHelper.getRTE_Dao_DescuentoPorCantidad()
                .queryRaw(select, _dbHelper.getRTE_Dao_DescuentoPorCantidad().getRawRowMapper());

        List<DescuentoPorCantidad> list=result.getResults();

        return list;
    }

    public List<StockDeCliente> obtenerStocksDeCliente(int clienteId) throws Exception{

        String select="SELECT * FROM StocksDeClientes WHERE clienteId = "+String.valueOf(clienteId);

        GenericRawResults<StockDeCliente> result=_dbHelper.getRTE_Dao_StockDeCliente()
                .queryRaw(select, _dbHelper.getRTE_Dao_StockDeCliente().getRawRowMapper());

        List<StockDeCliente> list=result.getResults();

        return list;
    }

    public PrecioEspecial obtenerArticuloPrecioEspecial(int articuloId, List<PrecioEspecial> preciosEspeciales){

        for (PrecioEspecial item: preciosEspeciales) {
            if(item.articuloId == articuloId) return item;
        }

        return null;
    }

    public ListaDePrecios obtenerArticuloListaDePrecio(int articuloId, List<ListaDePrecios> listasDePrecios){

        for (ListaDePrecios item: listasDePrecios) {
            if(item.articuloId == articuloId) return item;
        }
        return null;
    }

    public void asignarPreciosSegunPrioridad(Cliente cliente) throws Exception{

        List<Integer> idsArticulos = new ArrayList<Integer>();

        for (ArticuloDeLista art: cliente.articulosLista) {
            if(!idsArticulos.contains(art.id)) idsArticulos.add(art.id);
        }

        for (ArticuloDeComodato art: cliente.articulosComodatos) {
            if(!idsArticulos.contains(art.articulo_id)) idsArticulos.add(art.articulo_id);
        }

        for (ArticuloDeAbono art: cliente.articulosAbonos) {
            if(!idsArticulos.contains(art.articulo_id)) idsArticulos.add(art.articulo_id);
        }

        Hashtable<Integer, Double> precios = obtenerPreciosDeArticulo(idsArticulos, cliente);

        for (ArticuloDeLista art: cliente.articulosLista) {
           if(precios.containsKey(art.id))
               art.precio = precios.get(art.id);
        }

        for (ArticuloDeComodato art: cliente.articulosComodatos) {
            if(precios.containsKey(art.articulo_id))
                art.precio = precios.get(art.articulo_id);
        }

        for (ArticuloDeAbono art: cliente.articulosAbonos) {
            if(precios.containsKey(art.articulo_id))
                art.precioExcendente = precios.get(art.articulo_id);
        }

    }
}
