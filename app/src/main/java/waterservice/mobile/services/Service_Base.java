package waterservice.mobile.services;

import android.content.Context;
import android.os.AsyncTask;

import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.Configuracion;
import waterservice.mobile.clases.Log;
import waterservice.mobile.clases.ValorSatelite;
import waterservice.mobile.orm.ORMSQLiteHelper;
import waterservice.mobile.utiles.ConfiguracionInicial;
import waterservice.mobile.utiles.HttpHeaderValue;
import waterservice.mobile.utiles.HttpJsonClient;
import waterservice.mobile.utiles.Utiles;


public class Service_Base {

	public Context CurrentContext;	
	ORMSQLiteHelper _dbHelper;

	HttpJsonClient _apiClient;
	String urlRoot;

	AppSession appSession;


	//{{ Tablas
	
	public final static String TABLA_CLIENTES="Clientes";

	//}}
	
	public Service_Base(Context _context){
		CurrentContext=_context;

		ConfiguracionInicial config = new ConfiguracionInicial(CurrentContext);
		appSession = new AppSession(CurrentContext);
		_dbHelper=new ORMSQLiteHelper(CurrentContext, config.RutaLocalBaseDeDatos);

		_apiClient=new HttpJsonClient();
		urlRoot= Utiles.Get_Config_Value("servidor_web", CurrentContext);
		
	}
	
	public Service_Base(){
		
	}

	public String SecureStringSQL(String value){
		
		value="'"+value.replace("'", "")+"'";
		
		return value;
	}

	public String obtenerListaDeIds(List<Integer> ids){

		if(ids==null) return "";

		String select = "";

		int i = 0;
		for (Integer id: ids ) {

			if(i==0){
				select = " (";
			}

			select += String.valueOf(id);

			if(i == ids.size()-1){
				select += ") ";
			}else{
				select += ", ";
			}

			i++;
		}

		return select;
	}

	public void CrearLog(String mensaje, String funcionalidad, String entidad, Long idEntidad){

		try{
			final Log l = new Log();
			l.mensaje = mensaje;
			l.funcionalidad = funcionalidad;
			l.entidad = entidad;
			l.idEntidad = idEntidad;
			l.fechaHora = Calendar.getInstance().getTime();

			AsyncTask t = new AsyncTask() {
				@Override
				protected Object doInBackground(Object[] params) {
					try{
						_dbHelper.getDao_Log().create(l);
					}catch (Exception ex){
						ex.printStackTrace();
					}
					return null;
				}
			};

			t.execute();
		}catch (Exception ex){

		}
	}

	public List<ValorSatelite> obtenerValoresSatelites(int table) throws SQLException {

		String select="SELECT * FROM ValoresSatelites WHERE tabla_id=" + String.valueOf(table);

		GenericRawResults<ValorSatelite> result=_dbHelper.getRTE_Dao_ValorSatelite()
				.queryRaw(select, _dbHelper.getRTE_Dao_ValorSatelite().getRawRowMapper());

		List<ValorSatelite> list=result.getResults();

		return list;
	}

	private List<Configuracion> configuraciones;
	private void cargarConfiguraciones() throws Exception {

		String select="SELECT * FROM Configuraciones";

		GenericRawResults<Configuracion> result=_dbHelper.getDao_Configuracion()
				.queryRaw(select, _dbHelper.getDao_Configuracion().getRawRowMapper());

		configuraciones=result.getResults();
	}

	public Configuracion obtenerConfiguracion(String clave) throws Exception {

		if(configuraciones == null) cargarConfiguraciones();

		for (Configuracion c:configuraciones) {
			if(c.clave.equals(clave))return c;
		}
		return null;
	}

	public String obtenerConfiguracion(String clave, String valorPorDefecto) throws Exception {

		Configuracion configuracion = obtenerConfiguracion(clave);

		return configuracion == null? valorPorDefecto : configuracion.valor;
	}

	public boolean obtenerConfiguracion(String clave, boolean valorPorDefecto) throws Exception {

		String config = obtenerConfiguracion(clave, "");

		return config.toUpperCase().equals("TRUE");
	}

	public int obtenerConfiguracion(String clave, int valorPorDefecto) throws Exception {

		String config = obtenerConfiguracion(clave, "");

		try{
			return Integer.valueOf(config);
		}catch (Exception e){
			return valorPorDefecto;
		}
	}

	public static List<HttpHeaderValue> getHeaderForToken(String token){

		List headers=new ArrayList<HttpHeaderValue>();

		HttpHeaderValue httpHeaderValue = new HttpHeaderValue();
		httpHeaderValue.key="CURRENTTOKENVALUE";
		httpHeaderValue.value=token;

		headers.add(httpHeaderValue);

		return headers;
	}
}
