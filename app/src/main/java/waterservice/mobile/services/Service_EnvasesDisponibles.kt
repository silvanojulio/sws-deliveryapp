package waterservice.mobile.services

import android.content.Context
import waterservice.mobile.clases.EnvaseDisponible

public class Service_EnvasesDisponibles(_context: Context?) : Service_Base(_context) {

    public fun obtenerEnvasesDisponibles():List<EnvaseDisponible>{

        val select = "SELECT * FROM EnvasesDisponibles WHERE clienteEntregadoId IS NULL"

        val result = _dbHelper.rtE_Dao_EnvaseDisponible.queryRaw(select,
                _dbHelper.rtE_Dao_EnvaseDisponible.getRawRowMapper())

        return result.getResults()
    }

    public fun obtenerEnvasesParaRecuperar(clienteId:Int):List<EnvaseDisponible>{

        val select = "SELECT * FROM EnvasesDisponibles WHERE clienteEntregadoId = " + clienteId.toString() +
                " AND enCliente = 1"

        val result = _dbHelper.rtE_Dao_EnvaseDisponible.queryRaw(select,
                _dbHelper.rtE_Dao_EnvaseDisponible.getRawRowMapper())

        return result.getResults()
    }

    public fun crearEnvaseDisponible(envase:EnvaseDisponible){

        _dbHelper.rtE_Dao_EnvaseDisponible.create(envase)
    }

    public fun guardarEnvasesDisponibles(envases:List<EnvaseDisponible>){

        var envasesActuales = obtenerEnvasesDisponibles()

        envases.forEach{ env ->

            if(!envasesActuales.any{envActual -> env.id==envActual.id}){
                crearEnvaseDisponible(env)
            }
        }

    }
}