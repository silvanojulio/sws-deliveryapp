package waterservice.mobile.services;

import android.content.Context;

import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.field.types.IntegerObjectType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.ComprobanteFisicoUtilizado;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.Dispenser;
import waterservice.mobile.clases.DispenserAsociado;
import waterservice.mobile.clases.FirmaRemito;
import waterservice.mobile.clases.MantenimientoDeDispenser;
import waterservice.mobile.clases.OrdenDeTrabajo;
import waterservice.mobile.clases.RepuestoActividad;
import waterservice.mobile.clases.ValorSatelite;
import waterservice.mobile.utiles.CurrentApp;
import waterservice.mobile.utiles.Utiles;

public class Service_ServicioTecnico extends  Service_Base {

    public int ARTICULO_ID_INSTALACION;
    public int ARTICULO_ID_DESINSTALACION;
    public int ARTICULO_ID_SANITIZACION;
    public int ARTICULO_ID_REUBICACION;

    public Service_ServicioTecnico(Context context) {
        super(context);

        try {
            ARTICULO_ID_INSTALACION = Integer.valueOf(obtenerConfiguracion("ART_ID_INSTALACION").valor);
            ARTICULO_ID_DESINSTALACION = Integer.valueOf(obtenerConfiguracion("ART_ID_DESINSTALACION").valor);
            ARTICULO_ID_SANITIZACION = Integer.valueOf(obtenerConfiguracion("ART_ID_SANITIZACION").valor);
            ARTICULO_ID_REUBICACION = Integer.valueOf(obtenerConfiguracion("ART_ID_REUBICACION").valor);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public OrdenDeTrabajo ObtenerOrdenDeTrabajoDeCliente(int clienteId) throws SQLException {

        String select="SELECT * FROM OrdenesDeTrabajo WHERE cliente_id= "+String.valueOf(clienteId);

        GenericRawResults<OrdenDeTrabajo> result=_dbHelper.getRTE_Dao_OrdenDeTrabajo()
                .queryRaw(select, _dbHelper.getRTE_Dao_OrdenDeTrabajo().getRawRowMapper());

        List<OrdenDeTrabajo> list=result.getResults();

        return list.size()>0?list.get(0):null;
    }

    public List<OrdenDeTrabajo> ObtenerOrdenDeTrabajoCerradasPendientesDeSubir() throws SQLException {

        String select="SELECT * FROM OrdenesDeTrabajo WHERE motivoDeCierreId IS NOT NULL AND fechaHoraTransferido IS NULL";

        GenericRawResults<OrdenDeTrabajo> result=_dbHelper.getRTE_Dao_OrdenDeTrabajo()
                .queryRaw(select, _dbHelper.getRTE_Dao_OrdenDeTrabajo().getRawRowMapper());

        List<OrdenDeTrabajo> list=result.getResults();

        return list;
    }

    public List<DispenserAsociado> obtenerDispensersAsociados(OrdenDeTrabajo ordenDeTrabajo) throws SQLException {

        String select="SELECT * FROM DispensersAsociados WHERE ";

        select += ordenDeTrabajo.ordenDeTrabajoServer_id != null ?
                "OrdenDeTrabajoServer_id= "+String.valueOf(ordenDeTrabajo.ordenDeTrabajoServer_id):
                "ordenDeTrabajoId= "+String.valueOf(ordenDeTrabajo.id);

        GenericRawResults<DispenserAsociado> result=_dbHelper.getDao_DispenserAsociado()
                .queryRaw(select, _dbHelper.getDao_DispenserAsociado().getRawRowMapper());

        List<DispenserAsociado> list=result.getResults();

        return list;
    }

    public List<ValorSatelite> obtenerSintomasReales() throws SQLException {

        return obtenerValoresSatelites(550);
    }

    public List<ValorSatelite> obtenerAccionesSobreDispensers() throws SQLException {

        return obtenerValoresSatelites(860);
    }

    public List<ValorSatelite> obtenerMotivosDeCierresDeOrdenes() throws Exception {

        return obtenerValoresSatelites(580);
    }

    public void guardarMantenimiento(MantenimientoDeDispenser mantenimiento,
                                     DispenserAsociado dispenser, OrdenDeTrabajo ordenDeTrabajo,
                                     List<RepuestoActividad> repuestosActividades) throws Exception {

        MantenimientoDeDispenser mantenimientoExistente = obtenerMantenimientoPorDispenser(mantenimiento.nroDispenser);
        if(mantenimientoExistente!=null) throw new Exception("Ya existe un mantenimimento de este dispenser");

        mantenimiento.fechaHora = Calendar.getInstance().getTime();
        mantenimiento.latitud = CurrentApp.currentLatutid;
        mantenimiento.longitud = CurrentApp.currentLongitud;
        mantenimiento.usuario_id = appSession.getUsuarioAcual().usuario_id;

        _dbHelper.getDao_MantenimientoDeDispenser().create(mantenimiento);

        if(dispenser != null){

            dispenser.mantemientoCargado = true;
            _dbHelper.getDao_DispenserAsociado().update(dispenser);
        }else{
            Service_Dispensers srvDispenser = new Service_Dispensers(this.CurrentContext);
            Dispenser d = srvDispenser.obtenerDispenserPorNro(mantenimiento.nroDispenser, null);

            dispenser = new DispenserAsociado();
            dispenser.deCliente = d.cliente_id != 0;
            dispenser.mantemientoCargado = true;
            dispenser.nroDispenser = mantenimiento.nroDispenser;
            dispenser.ordenDeTrabajoId = ordenDeTrabajo.id;
            dispenser.OrdenDeTrabajoServer_id = ordenDeTrabajo.ordenDeTrabajoServer_id;

            _dbHelper.getDao_DispenserAsociado().create(dispenser);
        }

        for (RepuestoActividad r: repuestosActividades) {
            r.mantenimiento_id = mantenimiento.id;
            _dbHelper.getDao_RepuestoActividad().create(r);
        }
    }

    public void cerrarOrdenDeTrabajo(OrdenDeTrabajo ordenDeTrabajo, int motivoId, Cliente cliente) throws Exception {

        validarMotivoDeCierre(ordenDeTrabajo, motivoId);

        ordenDeTrabajo.motivoDeCierreId = motivoId;
        ordenDeTrabajo.usuarioCierra_id = appSession.getUsuarioAcual().usuario_id;
        _dbHelper.getDao_OrdenDeTrabajo().update(ordenDeTrabajo);

        cliente.visitado = true;
        cliente.visita_fechaHora = Utiles.Get_Fecha();
        cliente.visita_longitud = CurrentApp.currentLongitud;
        cliente.visita_altitud = CurrentApp.currentLatutid;

        _dbHelper.getDao_Cliente().update(cliente);
    }

    public List<RepuestoActividad> obtenerRepuestosActividadesDeOrdenDeTrabajo(OrdenDeTrabajo ot) throws  Exception{

        List<RepuestoActividad> items = new ArrayList<>();

        List<DispenserAsociado> dispensers = obtenerDispensersAsociados(ot);

        for (DispenserAsociado d:dispensers) {

            List<RepuestoActividad> repuestosActividadesDeDispenser = obtenerRepuestosActividadesDeDispenser(d.nroDispenser);

            for ( RepuestoActividad item: repuestosActividadesDeDispenser ) {
                item.dispenser = d.nroDispenser;
                items.add(item);
            }
        }
        return items;
    }

    public List<RepuestoActividad> obtenerRepuestosActividadesDeDispenser(String nroDispenser) throws SQLException {

        String select="SELECT * FROM RepuestosActividades WHERE mantenimiento_id IN "+
                "(SELECT id as mantenimiento_id FROM MantenimientosDeDispensers WHERE nroDispenser = '"+nroDispenser+"')";

        GenericRawResults<RepuestoActividad> result=_dbHelper.getDao_RepuestoActividad()
                .queryRaw(select, _dbHelper.getDao_RepuestoActividad().getRawRowMapper());

        List<RepuestoActividad> list=result.getResults();

        return list;
    }

    public MantenimientoDeDispenser obtenerMantenimientoPorDispenser(String nroDispenser) throws Exception {

        String select="SELECT * FROM MantenimientosDeDispensers WHERE nroDispenser='" +nroDispenser + "' ";

        GenericRawResults<MantenimientoDeDispenser> result=_dbHelper.getDao_MantenimientoDeDispenser()
                .queryRaw(select, _dbHelper.getDao_MantenimientoDeDispenser().getRawRowMapper());

        List<MantenimientoDeDispenser> list=result.getResults();

        return list.size()>0? list.get(0):null;
    }

    private void validarMotivoDeCierre(OrdenDeTrabajo ordenDeTrabajo, int motivoId)throws Exception{

        if(motivoId != Constantes.MOTIVO_CIERRE_SERVICIO_TECNICO_EXITOSO) return;

        Service_Clientes _srvCliente = new Service_Clientes(this.CurrentContext);
        FirmaRemito firmaRemito = _srvCliente.obtenerFirmaRemito(ordenDeTrabajo.cliente_id, true);

        if(firmaRemito==null){

            ComprobanteFisicoUtilizado cUtilizado = _srvCliente.obtenerComprobanteFisicoUtilizado(ordenDeTrabajo.cliente_id, 1);

            if(cUtilizado == null){
                throw new Exception("Antes de cerrar como exitoso, debe hacer firmar el remito al cliente, o indicar qué remito físico ha utilizado");
            }
        }

        List<DispenserAsociado> dispensers = obtenerDispensersAsociados(ordenDeTrabajo);

        for (DispenserAsociado d: dispensers) {
            if(d.mantemientoCargado) return;
        }

        throw new Exception("No hay mantenimientos registrados en los dispensers asociados");
    }

    static boolean enviandoSTs = false;

    public void enviarServiciosTecnicosCerrados()throws  Exception {

        if(enviandoSTs==true) return;

        enviandoSTs = true;

        try {

            List<OrdenDeTrabajo> ordenesPendientes = ObtenerOrdenDeTrabajoCerradasPendientesDeSubir();

            for (OrdenDeTrabajo ot : ordenesPendientes) {

                List<DispenserAsociado> dispensersAsociados = obtenerDispensersAsociados(ot);

                JSONObject jsonOrdenPendiente = obtenerJsonServicioTecnico(ot);
                JSONArray jsonDispensersAsociados = new JSONArray();

                for (DispenserAsociado d : dispensersAsociados) {

                    MantenimientoDeDispenser ma = obtenerMantenimientoDeDispenserAsociado(d);

                    //Si no tiene cargado un mantenimiento no es correcto actualizar el dispenser en el server
                    if(ma==null) continue;

                    JSONObject jsonDispenserAsociado = obtenerJsonDispenserAsociado(d);
                    JSONObject jsonMantenimiento = obtenerJsonMatenimiento(ma);

                    List<RepuestoActividad> ra = obtenerRepuestosActividadesDeMantenimiento(ma);
                    JSONArray jsonRa = obtenerJsonRepuestosActividades(ra);

                    jsonMantenimiento.put("detalles", jsonRa);
                    jsonDispenserAsociado.put("mantenimiento",jsonMantenimiento);

                    jsonDispensersAsociados.put(jsonDispenserAsociado);
                }

                jsonOrdenPendiente.put("dispensers", jsonDispensersAsociados);

                FirmaRemito f = new Service_Clientes(this.CurrentContext).obtenerFirmaRemito(ot.cliente_id, true);

                if(f!=null)
                {
                    jsonOrdenPendiente.put("firma", Utiles.encodeImage(f.firmaRemito));
                    jsonOrdenPendiente.put("firmante", f.firmante);
                }

                jsonOrdenPendiente.put("nroRemitoFisico", ot.nroRemitoFisico);
                jsonOrdenPendiente.put("nroRemitoFisicoPrefijo", ot.nroRemitoFisicoPrefijo);

                syncOrdenDeTrabajo(jsonOrdenPendiente, ot);
            }

        }catch (Exception ex){}

        enviandoSTs = false;
    }

    private void syncOrdenDeTrabajo(JSONObject ordenDeTrabajo, OrdenDeTrabajo ot){

        try {

            String path=urlRoot+"/Sync/RegistrarServicioTecnico";

            String identificadorMovil="";

            identificadorMovil= Utiles.Get_Config_Value("codigo_identificacion_movil", CurrentContext);

            JSONObject request = new JSONObject();
            request.put("servicioTecnico", ordenDeTrabajo);
            request.put("hojaDeRutaId", appSession.getHojaDeRuta().id);
            request.put("identificadorMovil", identificadorMovil);
            request.put("latitud", CurrentApp.currentLatutid);
            request.put("longitud", CurrentApp.currentLongitud);
            request.put("usuario_id", appSession.getUsuarioAcual().usuario_id);

             JSONObject response = _apiClient.postJson(path, request, null);

            int error=response.getInt("error");

            if(error!=0 ){
                String mensajeError = response.getString("message");
                CrearLog(mensajeError, "ST Sync", "ST Sync", ot.id);
            }else{
                ot.fechaHoraTransferido = Calendar.getInstance().getTime();
                _dbHelper.getDao_OrdenDeTrabajo().update(ot);
            }

        }catch (Exception ex){

        }
    }

    private JSONArray obtenerJsonRepuestosActividades(List<RepuestoActividad> ras) throws JSONException{

        if(ras==null) return null;

        JSONArray jRas = new JSONArray();

        for (RepuestoActividad ra:ras) {
            JSONObject j = new JSONObject();
            j.put("articulo_id", ra.articulo_id);
            j.put("cantidad", ra.cantidad);
            j.put("tipo_id", ra.tipo_id);
            j.put("mantenimiento_id", ra.mantenimiento_id);
            j.put("nombreDelItem", ra.nombreDelItem);

            jRas.put(j);
        }
        return jRas;
    }

    private JSONObject obtenerJsonMatenimiento(MantenimientoDeDispenser ma) throws JSONException {

        if(ma==null) return null;

        JSONObject json = new JSONObject();
        json.put("nroDispenser", ma.nroDispenser);
        json.put("sintomaReal", ma.sintomaReal);
        json.put("accionPrincipal_id", ma.accionPrincipal_ids);
        json.put("comentarios", ma.comentarios);
        json.put("fechaHora", Utiles.getDateTimeForApi(ma.fechaHora));
        json.put("danosAtribuidosAlCliente", ma.danosAtribuidosAlCliente);
        json.put("latitud", ma.latitud);
        json.put("longitud", ma.longitud);
        json.put("usuario_id", ma.usuario_id);

        return json;
    }

    private JSONObject obtenerJsonDispenserAsociado(DispenserAsociado d) throws JSONException {

        if(d==null) return null;

        JSONObject json = new JSONObject();
        json.put("ordenDeTrabajoServer_id", d.OrdenDeTrabajoServer_id);
        json.put("nroDispenser", d.nroDispenser);
        json.put("dispenser_id", d.dispenser_id);
        json.put("deCliente", d.deCliente);
        json.put("ordenDeTrabajoId", d.ordenDeTrabajoId);

        return json;
    }

    private JSONObject obtenerJsonServicioTecnico(OrdenDeTrabajo ot) throws JSONException {

        if(ot==null) return null;

        JSONObject servicioTecnico = new JSONObject();
        servicioTecnico.put("ordenDeTrabajoServer_id", ot.ordenDeTrabajoServer_id);
        servicioTecnico.put("cliente_id", ot.cliente_id);
        servicioTecnico.put("sintoma_id", ot.sintoma_id);
        servicioTecnico.put("sintoma", ot.sintoma);
        servicioTecnico.put("prioridad", ot.prioridad);
        servicioTecnico.put("comentarios", ot.comentarios);
        servicioTecnico.put("cantDispensers", ot.cantDispensers);
        servicioTecnico.put("sectorUbicacion", ot.sectorUbicacion);
        servicioTecnico.put("responsableEnCliente", ot.responsableEnCliente);
        servicioTecnico.put("telefonoResponsable", ot.telefonoResponsable);
        servicioTecnico.put("motivoDeCierreId", ot.motivoDeCierreId);
        servicioTecnico.put("franjaHoraria", ot.franjaHoraria);
        servicioTecnico.put("usuario_id", ot.usuarioCierra_id);

        return servicioTecnico;
    }

    private List<RepuestoActividad> obtenerRepuestosActividadesDeMantenimiento(MantenimientoDeDispenser ma) throws Exception{

        String select="SELECT * FROM RepuestosActividades WHERE mantenimiento_id = "+ma.id;

        GenericRawResults<RepuestoActividad> result=_dbHelper.getDao_RepuestoActividad()
                .queryRaw(select, _dbHelper.getDao_RepuestoActividad().getRawRowMapper());

        List<RepuestoActividad> list=result.getResults();

        return list;
    }

    private MantenimientoDeDispenser obtenerMantenimientoDeDispenserAsociado(DispenserAsociado d) throws Exception {

        String select="SELECT * FROM MantenimientosDeDispensers WHERE nroDispenser = '"+ d.nroDispenser +"'";

        GenericRawResults<MantenimientoDeDispenser> result=_dbHelper.getDao_MantenimientoDeDispenser()
                .queryRaw(select, _dbHelper.getDao_MantenimientoDeDispenser().getRawRowMapper());

        List<MantenimientoDeDispenser> list=result.getResults();

        return list.size()>0? list.get(0): null;
    }

    public void asignarRemitoFisico(OrdenDeTrabajo ordenDeTrabajo, Long nroRemito, Long prefijo) throws Exception {

        Service_ComprobantesFisicos srvVentas = new Service_ComprobantesFisicos(this.CurrentContext);

        srvVentas.registrarComprobanteFisico(1, prefijo, nroRemito, ordenDeTrabajo.cliente_id);

        ordenDeTrabajo.nroRemitoFisicoPrefijo = prefijo;
        ordenDeTrabajo.nroRemitoFisico = nroRemito;

        _dbHelper.getDao_OrdenDeTrabajo().update(ordenDeTrabajo);

    }

}

