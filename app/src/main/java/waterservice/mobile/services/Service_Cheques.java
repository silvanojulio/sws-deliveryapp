package waterservice.mobile.services;

import android.content.Context;

import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.List;

import waterservice.mobile.clases.Cheque;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.ItemDeRecibo;

public class Service_Cheques extends Service_Base {
    public Service_Cheques(Context context){
        super(context);
    }


    public void guardarCheque(Cheque cheque, int reciboId) throws Exception {

        if(cheque.id == 0){

            _dbHelper.getRTE_Dao_Cheque().create(cheque);

            ItemDeRecibo item = new ItemDeRecibo();
            item.formaDePagoId = Constantes.FORMADEPAGO_CHEQUE;
            item.importe = cheque.importe;
            item.reciboId = reciboId;
            item.chequeId = cheque.id;
            item.descripcion = cheque.descripcionResumen;

            _dbHelper.getRTE_Dao_ItemDeRecibo().create(item);

        }else{

            ItemDeRecibo item = obtenerItemDeReciboPorChequeId(cheque.id, reciboId);
            if(item == null) throw new Exception("Debe crear nuevamente el cheque");

            item.importe = cheque.importe;
            item.descripcion = cheque.descripcionResumen;
            item.formaDePagoId = Constantes.FORMADEPAGO_CHEQUE;

            _dbHelper.getRTE_Dao_Cheque().update(cheque);
            _dbHelper.getRTE_Dao_ItemDeRecibo().update(item);

        }
    }

    public ItemDeRecibo obtenerItemDeReciboPorChequeId(int chequeId, int reciboId) throws SQLException {

        String select="SELECT * FROM ItemsDeRecibos WHERE reciboId= "+String.valueOf(reciboId) +
                " and chequeId = " + String.valueOf(chequeId);

        GenericRawResults<ItemDeRecibo> result=_dbHelper.getRTE_Dao_ItemDeRecibo()
                .queryRaw(select, _dbHelper.getRTE_Dao_ItemDeRecibo().getRawRowMapper());

        List<ItemDeRecibo> list=result.getResults();

        ItemDeRecibo item = list.size()>0? list.get(0) : null;

        return item;
    }
}

