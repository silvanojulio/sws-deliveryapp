package waterservice.mobile.services;

import android.content.Context;

import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.List;

import waterservice.mobile.clases.Cheque;
import waterservice.mobile.clases.ComprobanteFisicoUtilizado;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.ItemDeRecibo;
import waterservice.mobile.clases.NumeroDeComprobanteDisponible;

public class Service_ComprobantesFisicos extends Service_Base {
    public Service_ComprobantesFisicos(Context context){
        super(context);
    }


    public NumeroDeComprobanteDisponible validarComprobanteFisico(int tipoId, long prefijo, long nroComprobante) throws Exception{

        String select = "select NumerosDeComprobantesDisponibles.*  from NumerosDeComprobantesDisponibles " +
                " inner join ComprobantesFisicos on NumerosDeComprobantesDisponibles.idRango = ComprobantesFisicos.idRango " +
                " where ComprobantesFisicos.prefijo = " + String.valueOf(prefijo) +
                " and NumerosDeComprobantesDisponibles.nroComprobante = " + String.valueOf(nroComprobante) +
                " and NumerosDeComprobantesDisponibles.utilizado = 0";

        GenericRawResults<NumeroDeComprobanteDisponible> result = _dbHelper.getRTE_Dao_NumeroDeComprobanteDisponible ()
                .queryRaw(select, _dbHelper.getRTE_Dao_NumeroDeComprobanteDisponible ().getRawRowMapper());

        List<NumeroDeComprobanteDisponible> disponibles=result.getResults();

        if(disponibles.size() == 0) throw new Exception("Nro de remito no valido");

        return disponibles.get(0);
    }

    public void registrarComprobanteFisico(int tipoId, long prefijo, long nroComprobante, int cliente_id)
            throws Exception{

        NumeroDeComprobanteDisponible nroDisponible =  validarComprobanteFisico(tipoId, prefijo, nroComprobante);

        nroDisponible.utilizado =true;
        _dbHelper.getRTE_Dao_NumeroDeComprobanteDisponible().update(nroDisponible);

        ComprobanteFisicoUtilizado c = new ComprobanteFisicoUtilizado();
        c.cliente_id = cliente_id;
        c.tipoDeComprobanteFisico_ids = tipoId;
        c.nroPrefijo = prefijo;
        c.nroComprobante = nroComprobante;

        _dbHelper.getRTE_Dao_ComprobanteFisicoUtilizado().create(c);
    }
}
