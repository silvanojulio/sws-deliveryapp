package waterservice.mobile.services;

import android.content.Context;

import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.List;

import waterservice.mobile.clases.ComprobanteEntregado;
import waterservice.mobile.clases.Factura;
import waterservice.mobile.clases.ImputacionDeRecibo;
import waterservice.mobile.clases.ItemDeRecibo;
import waterservice.mobile.clases.Recibo;
import waterservice.mobile.clases.ViewFactura;

public class Service_Facturas extends  Service_Base{

    public Service_Facturas(Context context){
        super(context);
    }

    public List<ViewFactura> obtenerFacturasDeCliente(int clienteId) throws SQLException {

        String select="SELECT * FROM VIEW_Facturas WHERE cliente_id = "+String.valueOf(clienteId);

        GenericRawResults<ViewFactura> result=_dbHelper.getRTE_Dao_ViewFactura()
                .queryRaw(select, _dbHelper.getRTE_Dao_ViewFactura().getRawRowMapper());

        List<ViewFactura> list=result.getResults();

        return list;
    }


    public void confirmarEntregaDeComprobante(int tipoComprobante, long idComprobante, int cliente_id){

        ComprobanteEntregado comprobante = new ComprobanteEntregado();
        comprobante.cliente_id = cliente_id;
        comprobante.tipoComprobanteInterno_id = tipoComprobante;
        comprobante.idEntidad = idComprobante;

        _dbHelper.getRTE_Dao_ComprobanteEntregado().create(comprobante);

        if(tipoComprobante==1){
            Factura factura = _dbHelper.getRTE_Dao_Factura()
                    .queryForEq("id",String.valueOf(idComprobante)).get(0);

            factura.entregada = true;

            _dbHelper.getRTE_Dao_Factura().update(factura);
        }
    }

}
