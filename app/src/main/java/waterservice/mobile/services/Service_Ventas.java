package waterservice.mobile.services;


import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import waterservice.mobile.clases.AlertaDeCliente;
import waterservice.mobile.clases.ArticuloDeLista;
import waterservice.mobile.clases.ArticuloEntregado;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.ComprobanteEntregado;
import waterservice.mobile.clases.ComprobanteFisicoUtilizado;
import waterservice.mobile.clases.DevolucionArticulo;
import waterservice.mobile.clases.Factura;
import waterservice.mobile.clases.FirmaRemito;
import waterservice.mobile.clases.HojaDeRuta;
import waterservice.mobile.clases.ItemDeRecibo;
import waterservice.mobile.clases.LimiteDeCliente;
import waterservice.mobile.clases.Recibo;
import waterservice.mobile.clases.Transaccion;
import waterservice.mobile.clases.VentaActual;
import waterservice.mobile.clases.ViewArticuloEntregado;
import waterservice.mobile.clases.ViewDevolucionArticulo;
import waterservice.mobile.clases.ViewFactura;
import waterservice.mobile.clases.ViewItemDeRecibo;
import waterservice.mobile.clases.apiContracts.ArticuloEntregadoMobile;
import waterservice.mobile.clases.apiContracts.ClienteMobile;
import waterservice.mobile.clases.apiContracts.ComprobanteEntregadoMobile;
import waterservice.mobile.clases.apiContracts.ComprobanteFisicoUtilizadoMobile;
import waterservice.mobile.clases.apiContracts.DevolucionArticuloMobile;
import waterservice.mobile.clases.apiContracts.FirmaRemitoMobile;
import waterservice.mobile.clases.apiContracts.ImputacionDeReciboMobile;
import waterservice.mobile.clases.apiContracts.ItemDeReciboMobile;
import waterservice.mobile.clases.apiContracts.ReciboMobile;
import waterservice.mobile.clases.apiContracts.ResponseBase;
import waterservice.mobile.clases.apiContracts.VisitaClienteMobile;
import waterservice.mobile.server.ApiCallback;
import waterservice.mobile.server.SyncMobile;
import waterservice.mobile.utiles.CurrentApp;
import waterservice.mobile.utiles.Printer;
import waterservice.mobile.utiles.Utiles;

public class Service_Ventas extends  Service_Base{

    public Service_Ventas(Context context){
        super(context);
    }

    public void GuardarDevolucionArticulo(ArticuloDeLista articulo, Cliente cliente, double cantidad,
                                          int motivoDevolucionId, Boolean cambioDirecto, Boolean esReutilizable,
                                          String motivoDevolucion, String nombreArticulo){

        DevolucionArticulo dev = new DevolucionArticulo();
        dev.articulo_id=articulo.id;
        dev.cantidad=cantidad;
        dev.fecha= Utiles.Get_Fecha();
        dev.cliente_id=cliente.cliente_id;
        dev.motivoDevolucion_ids=motivoDevolucionId;
        dev.esCambioDirecto=cambioDirecto;
        dev.motivoDevolucion = motivoDevolucion;
        dev.nombreArticulo = nombreArticulo;

        _dbHelper.getRTE_Dao_DevolucionArticulo().create(dev);

        cliente.devolucionArticulo=true;

        GuardarVisita(cliente);

    }

    public List<DevolucionArticulo> obtenerArticulosDevueltos(Cliente cliente){

        List<DevolucionArticulo> arts = _dbHelper.getRTE_Dao_DevolucionArticulo()
                .queryForEq("cliente_id", cliente.cliente_id);

        return arts;
    }

    public void GuardarNoCompra(Cliente cliente){

        cliente.cerrado = true;
        GuardarVisita(cliente);

    }

    public void GuardarAusente(Cliente cliente){

        cliente.ausente=true;
        cliente.cerrado = true;
        GuardarVisita(cliente);

    }

    public void GuardarVisita(Cliente cliente){

        cliente.visitado=true;
        cliente.visita_fechaHora= Utiles.Get_Fecha();
        cliente.visita_longitud= CurrentApp.currentLongitud;
        cliente.visita_altitud= CurrentApp.currentLatutid;

        _dbHelper.getRTE_Dao_Cliente().update(cliente);
    }

    public void guardarArticulosEntregados(Cliente cliente,
                                     List<ArticuloEntregado> articulos) throws Exception {
        try{

            eliminarOperacionesAnteriores(cliente.cliente_id);

            for (ArticuloEntregado art: articulos) {
                art.clienteId = cliente.cliente_id;
                _dbHelper.getRTE_Dao_ArticuloEntregado().create(art);
            }

        }catch (Exception ex)
        {
            try{
                eliminarOperacionesAnteriores(cliente.cliente_id);
            }catch (Exception e){}

            throw ex;
        }

    }

    private void eliminarOperacionesAnteriores(int cliente_id){
        String delete = "DELETE FROM ArticulosEntregados WHERE clienteId = "+ String.valueOf(cliente_id);
        _dbHelper.getWritableDatabase().execSQL(delete);

    }

    List<Transaccion> ObtenerTrasacciones(List<ViewArticuloEntregado> articulos,
                                          List<ViewDevolucionArticulo> devolucionArticulos,
                                          List<ViewItemDeRecibo> itemsDeRecibos)
    {
        List<Transaccion> ts=new ArrayList<Transaccion>();

        for( ViewArticuloEntregado art: articulos ){

           if(art.cantidadEntregada > 0){

               Transaccion t=new Transaccion();

               t.tipoTransaccion = Transaccion.Tipos.VENTA;
               t.cantidad = art.cantidadEntregada;
               t.articulo = art.nombreDelArticulo;
               t.cliente_id= art.clienteId;
               t.precio = art.subtotal;
               t.cliente = art.nombreCliente;

               ts.add( t);
           }

            if(art.cantidadEnvasesPrestados > 0){
                Transaccion t=new Transaccion();

                t.tipoTransaccion = Transaccion.Tipos.ENVASES_PRESTAMO;
                t.cantidad = art.cantidadEnvasesPrestados;
                t.articulo = art.nombreDelArticulo;
                t.cliente_id= art.clienteId;
                t.cliente = art.nombreCliente;

                ts.add( t);
            }

            if(art.cantidadEnvasesDevueltos > 0){
                Transaccion t=new Transaccion();

                t.tipoTransaccion = Transaccion.Tipos.ENVASES_DEVOLUCION;
                t.cantidad = art.cantidadEnvasesDevueltos;
                t.articulo = art.nombreDelArticulo;
                t.cliente_id= art.clienteId;
                t.cliente = art.nombreCliente;

                ts.add( t);
            }

            if(art.cantidadEnvasesRelevados > 0){
                Transaccion t=new Transaccion();

                t.tipoTransaccion = Transaccion.Tipos.ENVASES_RELEVAMIENTO;
                t.cantidad = art.cantidadEnvasesRelevados;
                t.articulo = art.nombreDelArticulo;
                t.cliente_id= art.clienteId;
                t.cliente = art.nombreCliente;

                ts.add( t);
            }
        }

        for (ViewItemDeRecibo item : itemsDeRecibos){

            Transaccion t=new Transaccion();

            t.tipoTransaccion = Transaccion.Tipos.COBRO;
            t.precio = item.importe;
            t.articulo = item.getFormaDePago();
            t.cliente_id = item.clienteId;
            t.cliente = item.nombreCliente;
            ts.add(t);
        }

        for( ViewDevolucionArticulo dev: devolucionArticulos ){

            Transaccion t=new Transaccion();

            t.tipoTransaccion = Transaccion.Tipos.ARTICULO_DEVOLUCION;
            t.cantidad = dev.cantidad;
            t.cliente = dev.nombreCliente;
            t.cliente_id=dev.cliente_id;
            t.articulo = dev.nombreArticulo;

            ts.add( t);
        }

        return ts;
    }

    public List<Transaccion> ObtenerTransacciones(int clienteId) throws Exception {

        List<ViewArticuloEntregado> articulos = _dbHelper.getRTE_Dao_ViewArticuloEntregado()
                .queryForEq("clienteId", clienteId);

        List<ViewDevolucionArticulo> devolucionArticulos = _dbHelper.getRTE_Dao_ViewDevolucionArticulo()
                .queryForEq("cliente_id", clienteId);

        List<ViewItemDeRecibo> itemsDeRecibos = _dbHelper.getRTE_Dao_ViewItemDeRecibo()
                .queryForEq("clienteId", clienteId);

        return ObtenerTrasacciones(articulos, devolucionArticulos, itemsDeRecibos);
    }

    public List<Transaccion> ObtenerTransacciones(){

        List<ViewDevolucionArticulo> devolucionArticulos = _dbHelper.getRTE_Dao_ViewDevolucionArticulo().queryForAll();
        List<ViewArticuloEntregado> articuloEntregados = _dbHelper.getRTE_Dao_ViewArticuloEntregado().queryForAll();
        List<ViewItemDeRecibo> itemsDeRecibos = _dbHelper.getRTE_Dao_ViewItemDeRecibo().queryForAll();

        List<Transaccion> transacciones= ObtenerTrasacciones(articuloEntregados, devolucionArticulos, itemsDeRecibos);

        Collections.sort(transacciones, new Comparator<Transaccion>() {
            @Override
            public int compare(Transaccion left, Transaccion right) {
                return String.valueOf(left.cliente_id).compareToIgnoreCase(String.valueOf(right.cliente_id));
            }
        });

        return transacciones;
    }

    public List<ArticuloEntregado> obtenerArticulosEntregados(Cliente cliente){

        List<ArticuloEntregado> articuloVendidos = _dbHelper.getRTE_Dao_ArticuloEntregado()
                .queryForEq("clienteId", cliente.cliente_id);

        return articuloVendidos;
    }

    public List<DevolucionArticulo> obtenerDevolucionesDeArticulos(Cliente cliente){

        List<DevolucionArticulo> devoluciones = _dbHelper.getRTE_Dao_DevolucionArticulo()
                .queryForEq("cliente_id", cliente.cliente_id);

        return devoluciones;
    }

    public void registrarFirmaRemito(Cliente cliente, byte[] firmaRemito, String firmante, boolean esServicioTecnico){

        String sql =  "INSERT INTO FirmasRemitos (cliente_id, firmaRemito, firmante, fechaHora, esServicioTecnico) " +
                      " VALUES (?,?,?,?,"+ (esServicioTecnico? "1" : "0") +")";
        SQLiteStatement insertStmt = _dbHelper.getWritableDatabase().compileStatement(sql);

        insertStmt.clearBindings();
        insertStmt.bindString(1, Integer.toString(cliente.cliente_id));
        insertStmt.bindBlob(2, firmaRemito);
        insertStmt.bindString(3, firmante);
        insertStmt.bindString(4, Utiles.Get_Fecha());

        insertStmt.executeInsert();
    }

    public FirmaRemito obtenerFirmaDeCliente(Cliente cliente){

        List<FirmaRemito> firma = _dbHelper.getRTE_Dao_FirmaRemito()
                .queryForEq("cliente_id", cliente.cliente_id);

        return firma.size()>0 ? firma.get(0) : null;
    }

    private static boolean enviandoVisitas = false;
    public void syncVisitasCerradas(){

            if(enviandoVisitas) return;

            enviandoVisitas = true;

            try{

            Service_Clientes srvClientes = new Service_Clientes(CurrentContext);

            List<Cliente> clientes = srvClientes.obtenerClientesCerrados(true);
            HojaDeRuta hojaDeRuta = new Service_HojasDeRutas(CurrentContext).obtenerHojaDeRutaActual();
            String codigoDeMovil = Utiles.Get_Config_Value( "codigo_identificacion_movil", CurrentContext);

            for(Cliente clt : clientes){

                try{

                    syncVisitaCliente(clt, hojaDeRuta, codigoDeMovil);

                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }

        enviandoVisitas = false;
    }

    private void syncVisitaCliente(final Cliente cliente, HojaDeRuta hojaDeRuta, String codigoDeMovil) throws Exception{

        SyncMobile srvSync = new SyncMobile(CurrentContext);

        VisitaClienteMobile visita = new VisitaClienteMobile();

        Service_Recibos srvRecibos = new Service_Recibos(CurrentContext);
        String firmaBase64=null;

        Recibo recibo = srvRecibos.obtenerReciboDeCliente(cliente.cliente_id, true, true, true);
        List<ArticuloEntregado> articulosEntregados = obtenerArticulosEntregados(cliente);
        List<DevolucionArticulo> articulosDevueltos = obtenerArticulosDevueltos(cliente);
        List<ComprobanteEntregado> comprobantesEntregados = obtenerComprobantesEntregados(cliente);
        List<ComprobanteFisicoUtilizado> comprobantesUtilizados = obtenerComprobantesUtilizados(cliente);
        FirmaRemito firma = obtenerFirmaDeCliente(cliente);

        visita.ArticulosEntregados = new ArrayList<>();
        visita.Recibos = new ArrayList<>();
        visita.ItemsDeRecibos = new ArrayList<>();
        visita.Imputaciones = new ArrayList<>();
        visita.FirmasRemito = new ArrayList<>();
        visita.ComprobantesEntregados = new ArrayList<>();
        visita.ComprobantesFisicosUtilizados = new ArrayList<>();
        visita.DevolucionesArticulos = new ArrayList<>();

        visita.ClienteVisitado = obtenerClienteMobile(cliente);

        for (ComprobanteEntregado comp: comprobantesEntregados){

            ComprobanteEntregadoMobile cm = new ComprobanteEntregadoMobile();
            cm.id = comp.id;
            cm.cliente_id = comp.cliente_id;
            cm.tipoComprobanteInterno_id = comp.tipoComprobanteInterno_id;
            cm.idEntidad = comp.idEntidad;

            visita.ComprobantesEntregados.add(cm);
        }

        for (ComprobanteFisicoUtilizado comp: comprobantesUtilizados){

            ComprobanteFisicoUtilizadoMobile cfum = new ComprobanteFisicoUtilizadoMobile();
            cfum.id = comp.id;
            cfum.cliente_id = comp.cliente_id;
            cfum.nroComprobante = comp.nroComprobante;
            cfum.nroPrefijo = comp.nroPrefijo;
            cfum.tipoDeComprobanteFisico_ids = comp.tipoDeComprobanteFisico_ids;

            visita.ComprobantesFisicosUtilizados.add(cfum);
        }

        for (DevolucionArticulo art: articulosDevueltos){

            DevolucionArticuloMobile dev = new DevolucionArticuloMobile();
            dev.articulo_id = art.articulo_id;
            dev.cantidad = art.cantidad;
            dev.cliente_id = art.cliente_id;
            dev.esCambioDirecto = art.esCambioDirecto;
            dev.esReutilizable = art.esReutilizable;
            dev.fecha = Utiles.Get_Fecha(Calendar.getInstance().getTime());
            dev.motivoDolucion_ids = art.motivoDevolucion_ids;
            dev.id = art.id;

            visita.DevolucionesArticulos.add(dev);
        }

        for (ArticuloEntregado art: articulosEntregados){

            ArticuloEntregadoMobile am = new ArticuloEntregadoMobile();
            am.articulo_id = art.articulo_id;
            am.cantidadEntregada = art.cantidadEntregada;
            am.cantidadEnvasesDevueltos = (int) art.cantidadEnvasesDevueltos;
            am.cantidadEnvasesPrestados = (int) art.cantidadEnvasesPrestados;
            am.cantidadEnvasesRelevados = (int) art.cantidadEnvasesRelevados;
            am.clienteId = art.clienteId;
            am.descuentoManual = art.descuentoManual;
            am.descuentoPorCantidad = art.descuentoPorCantidad;
            am.motivoPrestamoDevolucionId = art.motivoPrestamoDevolucionId;
            am.precioUnitario = art.precioUnitario;
            am.id = art.id;

            visita.ArticulosEntregados.add(am);
        }

        if(recibo!=null)
        {
            ReciboMobile rm = new ReciboMobile();
            rm.cerrado = recibo.cerrado;
            rm.clienteFacturaId = recibo.clienteFacturaId;
            rm.clienteId = recibo.clienteId;
            rm.comprobanteFisicoNro = recibo.comprobanteFisicoNro;
            rm.comprobanteFisicoPrefijo = recibo.comprobanteFisicoPrefijo;
            rm.fechaHora = Utiles.Get_Fecha(recibo.fechaHora);
            rm.id = recibo.id;

            visita.Recibos.add(rm);

            for (ItemDeRecibo item: recibo.items) {
                ItemDeReciboMobile im = new ItemDeReciboMobile();

                im.id = item.id;
                im.importe = item.importe;
                im.reciboId = item.reciboId;
                im.formaDePagoId = item.formaDePagoId;
                im.chequeId = item.chequeId;
                im.retencionId = item.retencionId;
                im.tarjetaDeCreditoId = item.tarjetaDeCreditoId;
                im.tarjetaDeDebitoId = item.tarjetaDeDebitoId;

                visita.ItemsDeRecibos.add(im);
            }

            for (Factura imp : recibo.imputaciones){

                ImputacionDeReciboMobile im = new ImputacionDeReciboMobile();

                im.cliente_id = imp.cliente_id;
                im.factura_id = imp.id;
                im.imputado = imp.imputado;
                im.recibo_id = recibo.id;

                visita.Imputaciones.add(im);
            }

            visita.Cheques = recibo.cheques;
            visita.TarjetaDeDebito = recibo.tarjetasDeDebito;
            visita.TarjetasDeCredito = recibo.tarjetasDeCredito;
            visita.Retenciones = recibo.retenciones;

        }

        if(firma!=null){
            FirmaRemitoMobile f = new FirmaRemitoMobile();
            f.cliente_id = firma.cliente_id;
            f.firmante = firma.firmante;
            f.fechaHora = firma.fechaHora;

            firmaBase64 = Utiles.encodeImage(firma.firmaRemito);

            visita.FirmasRemito.add(f);
        }

        srvSync.registrarVisitaClienteMobile(hojaDeRuta.id, codigoDeMovil, firmaBase64, visita,
            new ApiCallback<ResponseBase>() {
                @Override
                public void onSuccess(ResponseBase response) {

                    cliente.fechaHoraTransferido = Calendar.getInstance().getTime();
                    cliente.comunicacion3 = null;
                    _dbHelper.getRTE_Dao_Cliente().update(cliente);
                }

                @Override
                public void onError(String message, int errorCode) {

                    if(message!=null && message.contains("mismo cliente")){
                        cliente.fechaHoraTransferido = Calendar.getInstance().getTime();
                        cliente.comunicacion3 = null;
                    }else{
                        cliente.comunicacion3 = message;
                    }

                    _dbHelper.getRTE_Dao_Cliente().update(cliente);
                }
            });

    }

    private ClienteMobile obtenerClienteMobile(Cliente cliente) {

        ClienteMobile cl = new ClienteMobile();

        cl.cliente_id = cliente.cliente_id;
        cl.visitado = cliente.visitado;
        cl.ausente = cliente.ausente;
        cl.ventaEntrega = cliente.ventaEntrega;
        cl.cobroConsumo = cliente.cobroConsumo;
        cl.cobroFactura = cliente.cobroFactura;
        cl.devolucionEnvases = cliente.devolucionEnvases;
        cl.prestamoEnvases = cliente.prestamoEnvases;
        cl.devolucionArticulo = cliente.devolucionArticulo;
        cl.visita_altitud = cliente.visita_altitud;
        cl.visita_longitud = cliente.visita_longitud;
        cl.visita_fechaHora = cliente.visita_fechaHora;
        cl.latitud = cliente.altitud;
        cl.longitud = cliente.longitud;
        cl.descargado = cliente.descargado;
        cl.relevamientoCoordenadas_latitud = cliente.relevamientoCoordenadas_latitud;
        cl.relevamientoCoordenadas_longitud = cliente.relevamientoCoordenadas_longitud;


        return cl;
    }

    private List<ComprobanteFisicoUtilizado> obtenerComprobantesUtilizados(Cliente cliente) {

        List<ComprobanteFisicoUtilizado> list = _dbHelper.getRTE_Dao_ComprobanteFisicoUtilizado()
                .queryForEq("cliente_id", cliente.cliente_id);

        return list;
    }

    private List<ComprobanteEntregado> obtenerComprobantesEntregados(Cliente cliente) {

        List<ComprobanteEntregado> list = _dbHelper.getRTE_Dao_ComprobanteEntregado()
                .queryForEq("cliente_id", cliente.cliente_id);

        return list;

    }

    public void validarVenta(VentaActual venta, Cliente cliente, List<ArticuloDeLista> articulos)throws Exception{

        if(venta.esContadoEfectivo && venta.importeEfectivo > 0)
            throw new Exception("No es posible marcar de contado y registrar efectivo a la vez");

        if(venta.importeEfectivo < 0)
            throw new Exception("El importe en efectivo debe ser igual o superior a $0");

        if(venta.articulosEntregados == null || venta.articulosEntregados.size()==0)
            throw new Exception("Debe agregar al menos un artículo.");

        Service_Clientes srvCliente = new Service_Clientes(this.CurrentContext);
        List<AlertaDeCliente > alertasDeCliente = srvCliente.obtenerAlertasDeClientes(venta.clienteId);

        Service_Recibos srvRecibos = new Service_Recibos(CurrentContext);

        boolean bloquearCuenta = obtenerConfiguracion("BLOQUEO_DE_CUENTA", false);
        boolean permiteRelevamiento = obtenerConfiguracion("PERMITE_RELEVAMIENTOS", false);
        boolean permiteDescuentos = obtenerConfiguracion("PERMITE_DESCUENTOS", false);

        for (ArticuloEntregado art: venta.articulosEntregados) {

            if(!permiteDescuentos && art.descuentoManual > 0)
                throw new Exception("Los descuentos no están permitidos");

            if(!permiteRelevamiento && art.cantidadEnvasesRelevados > 0)
                throw new Exception("Los relevamientos no están permitidos");

            if(art.cantidadEnvasesPrestados > art.cantidadEntregada)
                throw new Exception("No puede prestar más envases de los que entrega");

            ArticuloDeLista artLista = Service_Articulos.obtenerArticulo(art.articulo_id, articulos);

            if(!artLista.permiteDecimales && art.cantidadEntregada >0 && (int) art.cantidadEntregada != art.cantidadEntregada)  {
                throw new Exception("El artículo "+art.nombreDelArticulo+" no permite decimales");
            }
        }

        Recibo recibo = srvRecibos.obtenerReciboDeCliente(venta.clienteId, true, true, false);

        if(recibo != null && recibo.obtenerTotal() > 0 && (venta.esContadoEfectivo || venta.importeEfectivo > 0 ) )
            throw new Exception("Si existe un recibo registrado, no puede seleccionar cobro de contado, ni agregar efectivo");

        if(bloquearCuenta && venta.tieneEntregas() && (cliente.desbloqueado == null || cliente.desbloqueado == false)){

            for (AlertaDeCliente alerta : alertasDeCliente ) {

                if(alerta.tipoDeAlerta_ids == 1 ){ //Límite de saldo superado

                    if(recibo == null || (venta.obtenerTotal() > recibo.obtenerTotal() &&
                            !venta.esContadoEfectivo &&
                            venta.obtenerTotal() > venta.importeEfectivo))
                        throw new Exception("Cuenta bloqueada por límite saldo superado. " +
                                "Debe registrar un cobro igual o mayor que la venta actual");

                }else if (alerta.tipoDeAlerta_ids == 2){ //Límite de facturas superado

                    Service_Facturas srvFacturas = new Service_Facturas(CurrentContext);

                    List<ViewFactura> facturas = srvFacturas.obtenerFacturasDeCliente(venta.clienteId);

                    int cantidadFacturasConDeuda = 0;

                    for (ViewFactura factura: facturas) {
                        if(factura.saldoFinal>0) cantidadFacturasConDeuda++;
                    }

                    LimiteDeCliente limites = srvCliente.obtenerLimiteDeCliente(venta.clienteId);

                    if(limites != null && limites.validaLimiteFacturas &&
                            cantidadFacturasConDeuda >= limites.limiteDeFacturas)
                        throw new Exception("Cuenta bloqueada por límite de facturas adeudadas superado. " +
                                "Debe registrar e imputar el cobro de las facturas");
                }
            }
        }

    }

    public void confirmarVenta(VentaActual ventaActual, Cliente cliente,
                               ComprobanteFisicoUtilizado comprobanteFisico, FirmaRemito firmaRemito) throws Exception{

        if(cliente.requiereRemito && firmaRemito == null && comprobanteFisico == null)
            throw new Exception("El cliente requiere un remito");

        guardarArticulosEntregados(cliente, ventaActual.articulosEntregados);

        cliente.devolucionEnvases = ventaActual.tieneDevolucionesEnvases();
        cliente.prestamoEnvases = ventaActual.tienePrestamosEnvases();
        cliente.ventaEntrega = ventaActual.tieneEntregas();
        cliente.saldoConsumos += ventaActual.obtenerTotal();
        cliente.totalEntregadoActual = ventaActual.obtenerTotal();

        if( (ventaActual.esContadoEfectivo && ventaActual.obtenerTotal() > 0) ||
                ventaActual.importeEfectivo > 0){

            Service_Recibos srvRecibo = new Service_Recibos(this.CurrentContext);

            double importeCobradoEfectivo = ventaActual.esContadoEfectivo?
                    ventaActual.obtenerTotal():
                    ventaActual.importeEfectivo;

            srvRecibo.guardarItemReciboEfectivo(importeCobradoEfectivo, cliente);

            Recibo recibo = srvRecibo.obtenerReciboDeCliente(cliente.cliente_id, false,
                    false, false);
            recibo.cerrado = true;
            cliente.saldoFacturacion -= importeCobradoEfectivo;

            _dbHelper.getRTE_Dao_Recibo().update(recibo);
        }

        if(firmaRemito!=null){
            registrarFirmaRemito(cliente, firmaRemito.firmaRemito, firmaRemito.firmante, false);
        }

        if(comprobanteFisico != null){
            Service_ComprobantesFisicos srvComprobantes = new Service_ComprobantesFisicos(this.CurrentContext);
            srvComprobantes.registrarComprobanteFisico(comprobanteFisico.tipoDeComprobanteFisico_ids,
                    comprobanteFisico.nroPrefijo, comprobanteFisico.nroComprobante, cliente.cliente_id);
        }

        GuardarVisita(cliente);
    }

    public void imprimirComprobante(Cliente cliente, Activity activity) throws Exception{

        Printer printer = new Printer(activity, null);

        List<ArticuloEntregado> articulos = obtenerArticulosEntregados(cliente);

        List<ArticuloEntregado>
                articulosEntregados = new ArrayList<>(),
                envasesRelevados = new ArrayList<>(),
                envasesPrestados = new ArrayList<>(),
                envasesDevueltos = new ArrayList<>();

        double total = 0;

        for (ArticuloEntregado art : articulos) {

            total += art.subtotal;

            if(art.cantidadEntregada>0)
                articulosEntregados.add(art);

            if(art.cantidadEnvasesDevueltos>0)
                envasesDevueltos.add(art);

            if(art.cantidadEnvasesPrestados>0)
                envasesPrestados.add(art);

            if(art.cantidadEnvasesRelevados>0)
                envasesRelevados.add(art);
        }

        Service_HojasDeRutas srvHojaDeRuta = new Service_HojasDeRutas(CurrentContext);
        HojaDeRuta hr = srvHojaDeRuta.obtenerHojaDeRutaActual();

        Service_General srvGeneral = new Service_General(CurrentContext);
        String nombreFantasia =  srvGeneral.obtenerConfiguracion("NOMBRE_FANTASIA","Sistema Water Service");
        String telefonos =  srvGeneral.obtenerConfiguracion("TELEFONO","");
        String domicilio =  srvGeneral.obtenerConfiguracion("WEB_EMPRESA","");

        FirmaRemito firma = null;

        try{
            firma = obtenerFirmaDeCliente(cliente);
        }catch (Exception ex){
            ex.printStackTrace();
        }

        printer.iniciarSalida();

        printer.agregarEspacio();
        printer.agregarSeparador();
        printer.agregarLinea("    "+nombreFantasia.toUpperCase()+"    ");
        printer.agregarLinea(domicilio);
        printer.agregarLinea("Tel: "+telefonos);
        printer.agregarSeparador();
        printer.agregarLinea("Entregas y envases");
        printer.agregarLinea("Cliente: "+ cliente.nombreCliente + " ("+String.valueOf(cliente.cliente_id)+")");
        printer.agregarLinea("Fecha: " + Utiles.Get_Fecha());
        printer.agregarLinea("Hoja de ruta: #" + hr.nombreReparto);
        printer.agregarLinea("Comprobante #" + String.valueOf(hr.id)+"-"+String.valueOf(cliente.cliente_id));

        printer.agregarSeparador();

        printer.agregarEspacio();

        if(articulosEntregados.size()>0){

            printer.agregarLinea("---------- Productos ----------");
            printer.agregarEspacio();

            for (ArticuloEntregado item : articulosEntregados) {

                if(item.cantidadDisponibles>0){

                    double cantidadEntregadaSinCosto = item.cantidadEntregada > item.cantidadDisponibles?
                            item.cantidadDisponibles : item.cantidadEntregada;

                    printer.agregarLinea(String.format("> $0 (X %1$s)  %2$s",
                            String.valueOf(cantidadEntregadaSinCosto),
                            item.nombreDelArticulo));

                    if(item.cantidadEntregada - item.cantidadDisponibles > 0)
                    {
                        printer.agregarLinea(String.format("> $%1$s (X %2$s)  %3$s",
                                item.subtotal,
                                String.valueOf(item.cantidadEntregada - item.cantidadDisponibles ),
                                item.nombreDelArticulo));
                    }

                }else{

                    printer.agregarLinea(String.format("> $%1$s (X %2$s)  %3$s",
                            item.subtotal,
                            String.valueOf(item.cantidadEntregada),
                            item.nombreDelArticulo));
                }
            }
        }

        printer.agregarLinea(String.format("TOTAL: $%1$s", Utiles.Round(total)));

        printer.agregarEspacio();

        if(envasesPrestados.size()>0){

            printer.agregarLinea("------ Envases prestados ------");
            printer.agregarEspacio();

            for (ArticuloEntregado item : envasesPrestados) {

                printer.agregarLinea(String.format("> (X %1$s)  %2$s",
                        String.valueOf(item.cantidadEnvasesPrestados),
                        item.nombreDelArticulo));
            }

            printer.agregarEspacio();
        }

        if(envasesDevueltos.size()>0){

            printer.agregarLinea("------ Envases devueltos ------");
            printer.agregarEspacio();

            for (ArticuloEntregado item : envasesDevueltos) {

                printer.agregarLinea(String.format("> (X %1$s)  %2$s",
                        String.valueOf(item.cantidadEnvasesDevueltos),
                        item.nombreDelArticulo));
            }

            printer.agregarEspacio();
        }

        if(envasesRelevados.size()>0){
            printer.agregarLinea("------ Envases relevados ------");
            printer.agregarEspacio();

            for (ArticuloEntregado item : envasesRelevados) {

                printer.agregarLinea(String.format("> (X %1$s)  %2$s",
                        String.valueOf(item.cantidadEnvasesRelevados),
                        item.nombreDelArticulo));
            }

            printer.agregarEspacio();
        }

        printer.agregarLinea("------ Saldo del cliente ------");
        printer.agregarLinea(String.format("Saldo consumos: $%1$s",Utiles.Round(cliente.saldoConsumos)));
        printer.agregarLinea(String.format("Saldo facturación: $%1$s",Utiles.Round(cliente.saldoFacturacion)));
        printer.agregarLinea(String.format("Saldo al momento: $%1$s",Utiles.Round(cliente.obtenerSaldoAlDia())));

        if(firma!=null){
            printer.agregarLinea("------ Firma de receptor ------");
            printer.agregarLinea("Receptor: "+firma.firmante );
        }

        printer.findBT();
        printer.openBT();
        printer.imprimirSalida();

        if(firma!=null){
            double rezise = 0.8;
            Bitmap bmp = BitmapFactory.decodeByteArray(firma.firmaRemito, 0, firma.firmaRemito.length);
            Bitmap bmp2 = Bitmap.createScaledBitmap(bmp,380, 380, true);
            printer.print_image(bmp2);
        }
        printer.iniciarSalida();
        printer.agregarSeparador();
        printer.imprimirSalida();
        printer.closeBT();
    }

    public void imprimirComprobanteDeDevolucion(Cliente cliente, Activity context) throws Exception {

        List<DevolucionArticulo> devoluciones = obtenerDevolucionesDeArticulos(cliente);
        if(devoluciones==null || devoluciones.size() == 0)
            return;

        Printer printer = new Printer(context, null);

        Service_HojasDeRutas srvHojaDeRuta = new Service_HojasDeRutas(CurrentContext);
        HojaDeRuta hr = srvHojaDeRuta.obtenerHojaDeRutaActual();

        Service_General srvGeneral = new Service_General(CurrentContext);
        String nombreFantasia =  srvGeneral.obtenerConfiguracion("NOMBRE_FANTASIA","Sistema Water Service");
        String telefonos =  srvGeneral.obtenerConfiguracion("TELEFONO","");
        String domicilio =  srvGeneral.obtenerConfiguracion("WEB_EMPRESA","");

        printer.iniciarSalida();

        printer.agregarEspacio();
        printer.agregarSeparador();
        printer.agregarLinea("    "+nombreFantasia.toUpperCase()+"    ");
        printer.agregarLinea(domicilio);
        printer.agregarLinea("Tel: "+telefonos);
        printer.agregarSeparador();
        printer.agregarLinea("   COMPROBANTE DE DEVOLUCIONES    ");
        printer.agregarLinea("Cliente: "+ cliente.nombreCliente + " ("+String.valueOf(cliente.cliente_id)+")");
        printer.agregarLinea("Fecha: " + Utiles.Get_Fecha());
        printer.agregarLinea("Hoja de ruta: #" + hr.nombreReparto);
        printer.agregarEspacio();

        printer.agregarLinea("--------- Devoluciones --------");
        for(DevolucionArticulo item: devoluciones){

            printer.agregarLinea(String.format("> (X %1$s) %2$s. %3$s. "+(item.esCambioDirecto? " Cambio directo":"")  ,
                    String.valueOf(item.cantidad),
                    item.nombreArticulo,
                    item.motivoDevolucion));
        }

        printer.agregarEspacio();
        printer.agregarLinea("----------- Mensaje -----------");
        printer.agregarLinea("Estas devoluciones están pedientes de ser procesadas en su cuenta");

        printer.agregarEspacio();
        printer.agregarSeparador();
        //printer.agregarLinea("------ Saldo del cliente ------");
        //printer.agregarEspacio();
        //printer.agregarLinea(String.format("Saldo consumos: $%1$s",Utiles.Round(cliente.saldoConsumos)));
        //printer.agregarLinea(String.format("Saldo facturación: $%1$s",Utiles.Round(cliente.saldoFacturacion)));
        //printer.agregarLinea(String.format("Saldo al momento: $%1$s",Utiles.Round(cliente.obtenerSaldoAlDia())));
        //printer.agregarSeparador();

        printer.findBT();
        printer.openBT();
        printer.imprimirSalida();
        printer.closeBT();

    }

    public void imprimirCuentaBloqueada(Cliente cliente, Activity activity) throws Exception{

        Printer printer = new Printer(activity, null);

        Service_HojasDeRutas srvHojaDeRuta = new Service_HojasDeRutas(CurrentContext);
        HojaDeRuta hr = srvHojaDeRuta.obtenerHojaDeRutaActual();

        Service_General srvGeneral = new Service_General(CurrentContext);
        String nombreFantasia =  srvGeneral.obtenerConfiguracion("NOMBRE_FANTASIA","Sistema Water Service");
        String telefonos =  srvGeneral.obtenerConfiguracion("TELEFONO","");
        String domicilio =  srvGeneral.obtenerConfiguracion("WEB_EMPRESA","");

        printer.iniciarSalida();

        printer.agregarEspacio();
        printer.agregarSeparador();
        printer.agregarLinea("    "+nombreFantasia.toUpperCase()+"    ");
        printer.agregarLinea(domicilio);
        printer.agregarLinea("Tel: "+telefonos);
        printer.agregarSeparador();
        printer.agregarLinea("Entregas y envases");
        printer.agregarLinea("Cliente: "+ cliente.nombreCliente + " ("+String.valueOf(cliente.cliente_id)+")");
        printer.agregarLinea("Fecha: " + Utiles.Get_Fecha());
        printer.agregarLinea("Hoja de ruta: #" + hr.nombreReparto);
        printer.agregarLinea("Comprobante #" + String.valueOf(hr.id)+"-"+String.valueOf(cliente.cliente_id));

        printer.agregarSeparador();

        printer.agregarEspacio();

        printer.agregarLinea("------ Cuenta suspendida ------");
        printer.agregarEspacio();

        printer.agregarLinea("Estimado cliente le informamos que la cuenta se encuentra suspendida " +
                "por falta de pago. " +
                "Le solicitamos regularizar la situacion para continuar con el servicio.");

        printer.agregarEspacio();
        printer.agregarSeparador();

        printer.agregarLinea("------ Saldo del cliente ------");
        printer.agregarLinea(String.format("Saldo consumos: $%1$s",Utiles.Round(cliente.saldoConsumos)));
        printer.agregarLinea(String.format("Saldo facturación: $%1$s",Utiles.Round(cliente.saldoFacturacion)));
        printer.agregarLinea(String.format("Saldo al momento: $%1$s",Utiles.Round(cliente.obtenerSaldoAlDia())));


        printer.findBT();
        printer.openBT();
        printer.imprimirSalida();
        printer.closeBT();
    }

}
