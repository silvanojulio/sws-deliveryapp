package waterservice.mobile.services;

import android.content.Context;

import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.List;

import waterservice.mobile.clases.Cheque;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.ItemDeRecibo;
import waterservice.mobile.clases.TarjetaDeDebito;

public class Service_TarjetaDeDebito extends Service_Base {
    public Service_TarjetaDeDebito(Context context){
        super(context);
    }


    public void guardarTarjetaDeDebito(TarjetaDeDebito tarjetaDeDebito, int reciboId) throws Exception {

        if(tarjetaDeDebito.id == 0){

            _dbHelper.getRTE_Dao_TarjetaDeDebito().create(tarjetaDeDebito);

            ItemDeRecibo item = new ItemDeRecibo();
            item.formaDePagoId = Constantes.FORMADEPAGO_TARJETADEBITO;
            item.importe = tarjetaDeDebito.importe;
            item.reciboId = reciboId;
            item.tarjetaDeDebitoId = tarjetaDeDebito.id;
            item.descripcion = tarjetaDeDebito.descripcionResumen;

            _dbHelper.getRTE_Dao_ItemDeRecibo().create(item);

        }else{

            ItemDeRecibo item = obtenerItemDeReciboPorTarjetaDeDebitoId(tarjetaDeDebito.id, reciboId);
            if(item == null) throw new Exception("Debe crear nuevamente el cobro");

            item.importe = tarjetaDeDebito.importe;
            item.descripcion = tarjetaDeDebito.descripcionResumen;
            item.formaDePagoId = Constantes.FORMADEPAGO_TARJETADEBITO;

            _dbHelper.getRTE_Dao_TarjetaDeDebito().update(tarjetaDeDebito);
            _dbHelper.getRTE_Dao_ItemDeRecibo().update(item);

        }
    }

    public ItemDeRecibo obtenerItemDeReciboPorTarjetaDeDebitoId(int tarjetaDeDebitoId, int reciboId) throws SQLException {

        String select="SELECT * FROM ItemsDeRecibos WHERE reciboId= "+String.valueOf(reciboId) +
                " and tarjetaDeDebitoId = " + String.valueOf(tarjetaDeDebitoId);

        GenericRawResults<ItemDeRecibo> result=_dbHelper.getRTE_Dao_ItemDeRecibo()
                .queryRaw(select, _dbHelper.getRTE_Dao_ItemDeRecibo().getRawRowMapper());

        List<ItemDeRecibo> list = result.getResults();

        ItemDeRecibo item = list.size()>0? list.get(0) : null;

        return item;
    }
}
