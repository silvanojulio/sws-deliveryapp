package waterservice.mobile.services;


import android.app.Activity;
import android.content.Context;

import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import waterservice.mobile.clases.AlertaDeCliente;
import waterservice.mobile.clases.ArticuloDeAbono;
import waterservice.mobile.clases.ArticuloDeComodato;
import waterservice.mobile.clases.ArticuloDeLista;
import waterservice.mobile.clases.ArticuloEntregado;
import waterservice.mobile.clases.Cheque;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.ComprobanteFisicoUtilizado;
import waterservice.mobile.clases.Deposito;
import waterservice.mobile.clases.Dispenser;
import waterservice.mobile.clases.DispenserAsociado;
import waterservice.mobile.clases.Factura;
import waterservice.mobile.clases.FirmaRemito;
import waterservice.mobile.clases.HojaDeRuta;
import waterservice.mobile.clases.LimiteDeCliente;
import waterservice.mobile.clases.OrdenDeTrabajo;
import waterservice.mobile.clases.PrecioEspecial;
import waterservice.mobile.clases.ResumenCliente;
import waterservice.mobile.clases.StockDeCliente;
import waterservice.mobile.utiles.Printer;
import waterservice.mobile.utiles.Utiles;

public class Service_Clientes extends  Service_Base{

    public Service_Clientes(Context context){
        super(context);
    }

    public List<Cliente> obtenerClientes(int estadoVisita, String textoCliente, HojaDeRuta hojaDeRuta, int tipoDeBusqueda)throws Exception
    {
        String select="SELECT * FROM Clientes " +
                "WHERE nombreCliente LIKE '%"+textoCliente+"%' ";

        //estadoVisita
        //1: no visitado
        //2: visitado
        //3: pendiente
        //4: pendiente de sync

        switch (estadoVisita){
            case 1: select +=" AND visitado = 0"; break;
            case 2: select +=" AND visitado = 1"; break;
            case 3: select +=" AND visitado = 0 AND esRepaso = 1"; break;
            case 4: select +=" AND visitado = 1 AND fechaHoraTransferido IS NULL"; break;
            case 5: select +=" AND tipoDeVisitaId = 4"; break;
            case 6: select +=" AND esSync = 1"; break;
        }

        select+= " ORDER BY orden, cliente_id";

        GenericRawResults<Cliente> result=_dbHelper.getRTE_Dao_Cliente()
                .queryRaw(select, _dbHelper.getRTE_Dao_Cliente().getRawRowMapper());

        List<Cliente> list = result.getResults();

        List<AlertaDeCliente> alertas = _dbHelper.getDao_AlertaDeCliente().queryForAll();

        List<Integer> idsClientes = new ArrayList<Integer>();
        for (Cliente clt: list) {
            idsClientes.add(clt.cliente_id);
            List<AlertaDeCliente> alertasDeCliente = AlertaDeCliente.filtrarPorCliente(alertas, clt.cliente_id);
            clt.esCuentaBloqueada = AlertaDeCliente.tieneBloqueantes(alertasDeCliente);
            clt.esCuentaPreventiva = AlertaDeCliente.tienePreventivo(alertasDeCliente);
        }

        if((tipoDeBusqueda != 2 && tipoDeBusqueda != 3) || textoCliente == null || textoCliente.trim().equals("") || textoCliente.length()<= 2) return list;

        select="SELECT * FROM ResumenClientes " +
                "WHERE (nombreCliente LIKE '%"+textoCliente+"%')  " +
                (tipoDeBusqueda==3? "" : " AND repartoId = " + hojaDeRuta.reparto_id) +
                " ORDER BY nombreCliente LIMIT 30";

        List<ResumenCliente> resultResumenClientePorNombre=_dbHelper.getRTE_Dao_ResumenCliente()
                .queryRaw(select, _dbHelper.getRTE_Dao_ResumenCliente().getRawRowMapper()).getResults();

        select="SELECT * FROM ResumenClientes " +
                " WHERE (domicilioCompleto LIKE '%"+textoCliente+"%')  " +
                (tipoDeBusqueda==3? "" : " AND repartoId  = " + hojaDeRuta.reparto_id) +
                " ORDER BY domicilioCompleto  LIMIT 30";

        List<ResumenCliente> resultResumenClientePorDomicilio=_dbHelper.getRTE_Dao_ResumenCliente()
                .queryRaw(select, _dbHelper.getRTE_Dao_ResumenCliente().getRawRowMapper()).getResults();

        for (ResumenCliente clt: resultResumenClientePorNombre) {

            if(idsClientes.contains(clt.clienteId)) continue;
            idsClientes.add(clt.clienteId);

            Cliente cliente = new Cliente();
            cliente.esResumenDeCliente = true;
            cliente.resumenDeClienteResultadoBusquedaTipoId = 1;
            cliente.cliente_id = clt.clienteId;
            cliente.nombreCliente = clt.nombreCliente;
            cliente.altitud = clt.latitud;
            cliente.longitud = clt.longitud;
            cliente.domicilioCompleto = clt.domicilioCompleto;
            cliente.reparto = clt.reparto;
            cliente.tipoCliente = clt.tipoCliente;
            list.add(cliente);
        }

        for (ResumenCliente clt: resultResumenClientePorDomicilio) {

            if(idsClientes.contains(clt.clienteId)) continue;
            idsClientes.add(clt.clienteId);

            Cliente cliente = new Cliente();
            cliente.esResumenDeCliente = true;
            cliente.resumenDeClienteResultadoBusquedaTipoId = 2;
            cliente.cliente_id = clt.clienteId;
            cliente.nombreCliente = clt.nombreCliente;
            cliente.altitud = clt.latitud;
            cliente.longitud = clt.longitud;
            cliente.domicilioCompleto = clt.domicilioCompleto;
            cliente.reparto = clt.reparto;
            cliente.tipoCliente = clt.tipoCliente;
            list.add(cliente);
        }

        return list;
    }

    public Cliente obtenerCliente(int clienteId)throws Exception
    {
        String select="SELECT * FROM Clientes WHERE cliente_id = " +  String.valueOf(clienteId) ;

        GenericRawResults<Cliente> result=_dbHelper.getRTE_Dao_Cliente()
                .queryRaw(select, _dbHelper.getRTE_Dao_Cliente().getRawRowMapper());

        List<Cliente> list=result.getResults();

        return list.size()>0? list.get(0):null;
    }

    public void agregarCliente(Cliente cliente,
                               List<ArticuloDeAbono> articuloDeAbono,
                               List<ArticuloDeComodato> comodatos,
                               List<PrecioEspecial> preciosEspeciales,
                               List<Factura> facturas,
                               List<AlertaDeCliente> alertas,
                               List<Dispenser> dispensers,
                               List<OrdenDeTrabajo> ordenesDeTrabajo,
                               List<DispenserAsociado> dispenserAsociadosAServicioTecnico,
                               List<StockDeCliente> stockDeEnvases) throws Exception {

        _dbHelper.getDao_Cliente().create(cliente);

        GuardarArticulosDeAbonoDeCliente(articuloDeAbono);
        GuardarComodatosDeCliente(comodatos);
        GuardarPreciosEspecialesDeCliente(preciosEspeciales);
        GuardarFacturasDeCliente(facturas);
        GuardarStockDeEnvases(stockDeEnvases);
        GuardarAlertasDeCliente(alertas);
        GuardarDispensersDeCliente(dispensers);
        GuardarOrdenesDeTrabajoDeCliente(ordenesDeTrabajo);
        GuardarDispensersDeOT(dispenserAsociadosAServicioTecnico);
    }

    private void GuardarArticulosDeAbonoDeCliente(List<ArticuloDeAbono> articuloDeAbono) throws SQLException {
        if(articuloDeAbono==null) return;
        for (ArticuloDeAbono item: articuloDeAbono) {
            try{
                _dbHelper.getDao_ArticuloDeAbono().create(item);

            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    private void GuardarComodatosDeCliente(List<ArticuloDeComodato> comodatos)throws SQLException{
        if(comodatos==null) return;
        for (ArticuloDeComodato item: comodatos) {
            try{
                _dbHelper.getDao_ArticuloDeComodato().create(item);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    private void GuardarPreciosEspecialesDeCliente(List<PrecioEspecial> preciosEspeciales)throws SQLException{
        if(preciosEspeciales==null) return;
        for (PrecioEspecial item: preciosEspeciales) {
            try{
                _dbHelper.getRTE_Dao_PrecioEspecial().create(item);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    private void GuardarFacturasDeCliente(List<Factura> facturas)throws SQLException{
        if(facturas==null) return;
        for (Factura item: facturas) {
            try{
                _dbHelper.getDao_Factura().create(item);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    private void GuardarStockDeEnvases(List<StockDeCliente> stocks)throws SQLException{
        if(stocks==null) return;
        for (StockDeCliente item: stocks) {
            try{
                _dbHelper.getRTE_Dao_StockDeCliente().create(item);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    private void GuardarAlertasDeCliente(List<AlertaDeCliente> alertas)throws SQLException{
        if(alertas==null) return;
        for (AlertaDeCliente item: alertas) {
            try{
                _dbHelper.getDao_AlertaDeCliente().create(item);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    public void GuardarDispensersDeCliente(List<Dispenser> dispensers)throws SQLException{
        if(dispensers==null) return;
        for (Dispenser item: dispensers) {
            try{
                _dbHelper.getDao_Dispenser().create(item);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    public void GuardarOrdenesDeTrabajoDeCliente(List<OrdenDeTrabajo> ordenesDeTrabajo)throws SQLException{
        if(ordenesDeTrabajo==null) return;
        for (OrdenDeTrabajo item: ordenesDeTrabajo) {
            try{
                _dbHelper.getDao_OrdenDeTrabajo().create(item);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    public void GuardarDispensersDeOT(List<DispenserAsociado> dispenserAsociadosAServicioTecnico)throws SQLException{
        if(dispenserAsociadosAServicioTecnico==null) return;
        for (DispenserAsociado item: dispenserAsociadosAServicioTecnico) {
            try{
                _dbHelper.getDao_DispenserAsociado().create(item);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    public List<Factura> obtenerFacturasDeCliente(int cliente_id) throws Exception {

        String select="SELECT * FROM Facturas WHERE cliente_id= "+String.valueOf(cliente_id);

        GenericRawResults<Factura> result=_dbHelper.getRTE_Dao_Factura()
                .queryRaw(select, _dbHelper.getRTE_Dao_Factura().getRawRowMapper());

        List<Factura> list=result.getResults();

        return list;
    }

    public FirmaRemito obtenerFirmaRemito(int cliente_id, boolean esServicioTecnico) throws SQLException {

        List<FirmaRemito> list=_dbHelper.getRTE_Dao_FirmaRemito()
                .queryForEq("cliente_id",String.valueOf(cliente_id));

        if(list==null || list.size()==0) return null;

        for (FirmaRemito f: list) {
            if(esServicioTecnico == (f.esServicioTecnico == 1)) return f;
        }
        return null;
    }

    public ComprobanteFisicoUtilizado obtenerComprobanteFisicoUtilizado(int cliente_id, int tipoId) throws SQLException {

        List<ComprobanteFisicoUtilizado> list=_dbHelper.getRTE_Dao_ComprobanteFisicoUtilizado()
                .queryForEq("cliente_id",String.valueOf(cliente_id));

        if(list==null || list.size()==0) return null;

        for (ComprobanteFisicoUtilizado item: list) {
            if( item.tipoDeComprobanteFisico_ids == tipoId) return item;
        }
        return null;
    }

    public List<Cheque> obtenerChequesDisponibles(int cliente_id) throws SQLException {

        List<Cheque> list=_dbHelper.getRTE_Dao_Cheque().queryForEq("cliente_id",String.valueOf(cliente_id));

        return list;

    }

    public List<Deposito> obtenerDepositosDisponibles(int cliente_id) throws SQLException {

        List<Deposito> list=_dbHelper.getRTE_Dao_Deposito().queryForEq("cliente_id",String.valueOf(cliente_id));

        return list;

    }

    public List<AlertaDeCliente> obtenerAlertasDeClientes(int cliente_id) throws SQLException {

        List<AlertaDeCliente> list=_dbHelper.getDao_AlertaDeCliente()
                .queryForEq("cliente_id",String.valueOf(cliente_id));

        return list;

    }

    public void registrarCoordenadas( String latitud, String longitud, Cliente cliente){

        cliente.relevamientoCoordenadas_longitud = longitud;
        cliente.relevamientoCoordenadas_latitud = latitud;

        _dbHelper.getRTE_Dao_Cliente().update(cliente);
    }

    public void registrarAlertasVistas( Cliente _cliente){


        //Cliente cliente = _dbHelper.getRTE_Dao_Cliente().queryForId(_cliente.cliente_id);

        //cliente.haVistoAlertas = true;
        _cliente.haVistoAlertas = true;

        _dbHelper.getRTE_Dao_Cliente().update(_cliente);
    }

    private ArticuloDeLista obtenerArticulo(int articuloId, List<ArticuloDeLista> articulos){

        for (ArticuloDeLista art: articulos  ) {
          if(art.id==articuloId)
              return art;
        }
        return null;
    }

    private Boolean articuloYaAgregado(List<ArticuloEntregado> articulosEntregados, int articuloId){
        for (ArticuloEntregado art: articulosEntregados  ) {
            if(art.articulo_id == articuloId)
                return true;
        }
        return false;
    }

    public LimiteDeCliente obtenerLimiteDeCliente(int clienteId) throws SQLException {

        String select="SELECT * FROM LimitesDeClientes WHERE clienteId= "+String.valueOf(clienteId);

        GenericRawResults<LimiteDeCliente> result=_dbHelper.getRTE_Dao_LimiteDeCliente()
                .queryRaw(select, _dbHelper.getRTE_Dao_LimiteDeCliente().getRawRowMapper());

        List<LimiteDeCliente> list=result.getResults();

        return list.size()>0? list.get(0) : null;
    }

    public List<Cliente> obtenerClientesCerrados(boolean soloSinTransferir) throws Exception{

        String select="SELECT * FROM Clientes WHERE cerrado = 1 AND fechaHoraTransferido is NULL";

        GenericRawResults<Cliente> result=_dbHelper.getRTE_Dao_Cliente()
                .queryRaw(select, _dbHelper.getRTE_Dao_Cliente().getRawRowMapper());

        List<Cliente> list = result.getResults();

        return list;
    }

    public void confirmarClienteCerrado(Cliente cliente) {

        cliente.cerrado = true;
        _dbHelper.getRTE_Dao_Cliente().update(cliente);
    }

    public void ponerEnRepaso(Cliente cliente, boolean quitarRepaso) throws Exception{

        cliente.esRepaso = quitarRepaso? false : true;
        _dbHelper.getRTE_Dao_Cliente().update(cliente);
    }

    public void imprimirComprobanteDeVisita(Cliente cliente, Activity context) throws Exception {

        Printer printer = new Printer(context, null);

        Service_HojasDeRutas srvHojaDeRuta = new Service_HojasDeRutas(CurrentContext);
        HojaDeRuta hr = srvHojaDeRuta.obtenerHojaDeRutaActual();

        Service_General srvGeneral = new Service_General(CurrentContext);
        String nombreFantasia =  srvGeneral.obtenerConfiguracion("NOMBRE_FANTASIA","Sistema Water Service");
        String telefonos =  srvGeneral.obtenerConfiguracion("TELEFONO","");
        String domicilio =  srvGeneral.obtenerConfiguracion("WEB_EMPRESA","");
        String mensajeDeVisita =  srvGeneral.obtenerConfiguracion("MENSAJE_VISITA",
                "Estimado cliente: Visitamos su\n" +
                        "domicilio, pero nadie nos \n" +
                        "atendió. En caso de necesitar \n" +
                        "alguno de nuestros productos,\n" +
                        "por favor contáctenos. ");

        printer.iniciarSalida();

        printer.agregarEspacio();
        printer.agregarSeparador();
        printer.agregarLinea("    "+nombreFantasia.toUpperCase()+"    ");
        printer.agregarLinea(domicilio);
        printer.agregarLinea("Tel: "+telefonos);
        printer.agregarSeparador();
        printer.agregarLinea("   COMPROBANTE DE VISITA    ");
        printer.agregarLinea("Cliente: "+ cliente.nombreCliente + " ("+String.valueOf(cliente.cliente_id)+")");
        printer.agregarLinea("Fecha: " + Utiles.Get_Fecha());
        printer.agregarLinea("Hoja de ruta: #" + hr.nombreReparto);

        printer.agregarEspacio();
        printer.agregarLinea("----------- Mensaje -----------");
        printer.agregarLinea(mensajeDeVisita);

        printer.agregarEspacio();

        printer.agregarLinea("------ Saldo del cliente ------");
        printer.agregarEspacio();

        printer.agregarLinea(String.format("Saldo consumos: $%1$s",Utiles.Round(cliente.saldoConsumos)));
        printer.agregarLinea(String.format("Saldo facturación: $%1$s",Utiles.Round(cliente.saldoFacturacion)));
        printer.agregarLinea(String.format("Saldo al momento: $%1$s",Utiles.Round(cliente.obtenerSaldoAlDia())));

        printer.agregarSeparador();

        printer.findBT();
        printer.openBT();
        printer.imprimirSalida();
        printer.closeBT();

    }

    public void desbloquearCliente(Cliente cliente, String codigoDesbloqueo) throws Exception {

        Service_HojasDeRutas srvHojaDeRuta = new Service_HojasDeRutas(CurrentContext);
        HojaDeRuta hr = srvHojaDeRuta.obtenerHojaDeRutaActual();

        int clienteId = nroDeClienteDeCodigoDeDesbloqueo(codigoDesbloqueo,hr);

        if(cliente.cliente_id != clienteId)
            throw new Exception("Código inválido");

        cliente.desbloqueado = true;

        _dbHelper.getRTE_Dao_Cliente().update(cliente);
    }

    private int nroDeClienteDeCodigoDeDesbloqueo(String codigoDeDesbloqueo, HojaDeRuta hr){

        String fecha = hr.fechaReparto.split(" ")[0];

        int dia = Integer.valueOf(fecha.split("-")[2]);
        int mes = Integer.valueOf(fecha.split("-")[1]);
        int diaMes = Integer.valueOf(String.valueOf(dia) + String.valueOf(mes));

        String codigo = codigoDeDesbloqueo.substring(0, codigoDeDesbloqueo.length()-3);
        int random = Integer.valueOf(codigoDeDesbloqueo.substring(codigoDeDesbloqueo.length()-3));

        int codigoNum = Integer.valueOf(codigo);
        int codigoCliente = codigoNum - diaMes - random;

        return codigoCliente;
    }

}
