package waterservice.mobile.services;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import waterservice.mobile.Session.AppSession;
import waterservice.mobile.clases.ArticuloControlDePlaya;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.ControlDePlaya;
import waterservice.mobile.clases.Dispenser;
import waterservice.mobile.clases.PlanificacionDeCarga;
import waterservice.mobile.clases.Usuario;
import waterservice.mobile.utiles.CurrentApp;
import waterservice.mobile.utiles.HttpHeaderValue;
import waterservice.mobile.utiles.HttpJsonClient;
import waterservice.mobile.utiles.Utiles;

public class Service_ControlDePlayaOnLine extends Service_Base {

    HttpJsonClient _apiClient;
    String urlRoot;
    AppSession appSession;

    public Service_ControlDePlayaOnLine(Context context){
        super(context);
        appSession = new AppSession(context);
        _apiClient=new HttpJsonClient();
        urlRoot= Utiles.Get_Config_Value("servidor_web",CurrentContext);
    }

    public Usuario obtenerUsuarioControlador(String username, String password) throws Exception {

        String path=appSession.getCurrentUrlForLocal()+"/Session/IniciarSesionControladorPlaya";

        JSONObject request = new JSONObject();
        request.put("username",username);
        request.put("password", password);

        JSONObject response = _apiClient.postJson(path,request, null);

        int error=response.getInt("error");

        if(error!=0){
            throw new Exception(response.getString("message"));
        }

        Usuario usuario=new Usuario();
        usuario.token=response.getString("tokenValido");
        usuario.session_id=response.getLong("session_id");

        JSONObject jUsuario= response.getJSONObject("usuario");

        usuario.nombreApellido=jUsuario.getString("nombreApellido");
        usuario.nombreUsuario=jUsuario.getString("userName");
        usuario.usuario_id=jUsuario.getInt("usuario_id");

        return usuario;
    }

    public void guardarControlDePlaya(ControlDePlaya controlDePlaya, String token) throws Exception {

        List<Integer> idsDispensers = new ArrayList<>();
        List<Dispenser> dispensers = new ArrayList<>();
        JSONArray jIdsDispensers = new JSONArray();

        List<ArticuloControlDePlaya> articulosRepuestos = new ArrayList<>();

        for (ArticuloControlDePlaya item: controlDePlaya.articulos) {

            if(item.dispenser!=null){
                idsDispensers.add(item.dispenser_id);
                jIdsDispensers.put(item.dispenser_id);
                dispensers.add(item.dispenser);
            }else{
                articulosRepuestos.add(item);
            }
        }
        controlDePlaya.dispensersIds = idsDispensers;

        String path= appSession.getCurrentUrlForLocal()+"/ControlesDePlaya/GuardarNuevoControlMobile";

        JSONObject request = new JSONObject();
        request.put("hojaDeRutaId", controlDePlaya.hojaDeRutaId);
        request.put("usuarioControlaId", controlDePlaya.usuarioControlaId);
        request.put("tipoEventoId", controlDePlaya.tipoEventoId);
        request.put("kilometros", controlDePlaya.kilometros);
        request.put("idsDispensers", jIdsDispensers);

        JSONArray jArticulos = new JSONArray();

        for(ArticuloControlDePlaya art:articulosRepuestos){

            JSONObject jArt = new JSONObject();
            jArt.put("articulo_id",art.articulo.id);
            jArt.put("cantidadVacios",art.cantidadVacios);
            jArt.put("cantidadLlenos",art.cantidadLlenos);
            jArt.put("cantidadFallados",art.cantidadFallados);

            jArticulos.put(jArt);
        }

        request.put("articulos",jArticulos);

        try{
            JSONObject response = _apiClient.postJson(path,request, getHeaderForToken(token));

            int error=response.getInt("error");

            if(error!=0){
                throw new Exception(response.getString("message"));
            }else{
                if(controlDePlaya.tipoEventoId== 1) { //Si es salida

                    try {
                        cargarDispensersDeFabrica(dispensers);
                    }catch (Exception ex){
                        CrearLog("No se ha podido guardar el listado de dispensers que han salido de en ruta."+
                                " Error: "+ex.getMessage(),"Control De Playa",null,null);
                    }
                }
            }
        }catch (Exception ex){
            throw new Exception("No es posible registrar el control de playa");
        }

    }

    private void cargarDispensersDeFabrica(List<Dispenser> dispensers) throws SQLException {

        String delete = "DELETE FROM Dispensers WHERE cliente_id = 0";
        _dbHelper.getWritableDatabase().execSQL(delete);

        for (Dispenser d : dispensers) {
            d.cliente_id = 0;
            _dbHelper.getDao_Dispenser().create(d);
        }

    }

    public static List<HttpHeaderValue> getHeaderForToken(String token){

        List headers=new ArrayList<HttpHeaderValue>();

        HttpHeaderValue httpHeaderValue = new HttpHeaderValue();
        httpHeaderValue.key="CURRENTTOKENVALUE";
        httpHeaderValue.value=token;

        headers.add(httpHeaderValue);

        return headers;
    }

    public PlanificacionDeCarga obtenerPlanificacionDeCarga(long hojaDeRutaId) throws Exception {

        String path=appSession.getCurrentUrlForLocal()+"/PlanificacionDeCarga/ObtenerPlanificacionDeCargaPorHojaDeRuta" +
                "?hojaDeRutaId="+String.valueOf(hojaDeRutaId);

        JSONObject response = _apiClient.getJson(path, null);

        int error=response.getInt("error");

        if(error!=0){
            throw new Exception(response.getString("message"));
        }

        JSONObject jPlanificacion= response.getJSONObject("planificacion");

        PlanificacionDeCarga planificacion = new PlanificacionDeCarga(jPlanificacion);

        return planificacion;
    }

}
