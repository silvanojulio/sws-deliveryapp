package waterservice.mobile.services;


import android.content.Context;

import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.List;

import waterservice.mobile.clases.ValorSatelite;

public class Service_General extends  Service_Base {

    public Service_General(Context context){
        super(context);
    }

    public List<ValorSatelite> obtenerValoresSatelites(int tablaId) throws SQLException {

        String select="SELECT * FROM ValoresSatelites where tabla_id = "+ String.valueOf(tablaId);

        GenericRawResults<ValorSatelite> result=_dbHelper.getRTE_Dao_ValorSatelite()
                .queryRaw(select, _dbHelper.getRTE_Dao_ValorSatelite().getRawRowMapper());

        List<ValorSatelite> list=result.getResults();

        return list;

    }

    public String[] ObtenerTextosDeValoresSatelites(List<ValorSatelite> motivos){

        String[] result= new String[motivos.size()];

        for (int i=0; i<motivos.size(); i++){
            result[i]=motivos.get(i).valor_texto;
        }

        return result;

    }


}
