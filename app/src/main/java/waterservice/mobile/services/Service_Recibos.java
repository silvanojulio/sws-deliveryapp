package waterservice.mobile.services;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import waterservice.mobile.clases.Cheque;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.ComprobanteFisicoUtilizado;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.Factura;
import waterservice.mobile.clases.HojaDeRuta;
import waterservice.mobile.clases.ItemDeRecibo;
import waterservice.mobile.clases.Recibo;
import waterservice.mobile.clases.Retencion;
import waterservice.mobile.clases.TarjetaDeCredito;
import waterservice.mobile.clases.TarjetaDeDebito;
import waterservice.mobile.utiles.Printer;
import waterservice.mobile.utiles.Utiles;

public class Service_Recibos extends  Service_Base{

    public Service_Recibos(Context context){
        super(context);
    }

    public Recibo obtenerOCrearRecibo(Cliente cliente) throws SQLException {

        Recibo recibo = obtenerReciboDeCliente(cliente.cliente_id, false, false, false);

        if(recibo != null){
            return recibo;
        }
        else{
            recibo =  new Recibo();

            recibo.fechaHora = Calendar.getInstance().getTime();
            recibo.clienteId = cliente.cliente_id;
            recibo.clienteFacturaId = cliente.clienteFactura_id == 0 ? cliente.cliente_id : cliente.clienteFactura_id;
            recibo.nombreCliente = cliente.nombreCliente;

            this._dbHelper.getRTE_Dao_Recibo().create(recibo);

            return recibo;
        }
    }

    public Recibo obtenerReciboDeCliente(int clienteId, boolean incluirItems,
                                         boolean incluirImputaciones, boolean incluirDetalleDeItem) throws SQLException {

        String select="SELECT * FROM Recibos WHERE clienteId= "+String.valueOf(clienteId);

        GenericRawResults<Recibo> result=_dbHelper.getRTE_Dao_Recibo()
                .queryRaw(select, _dbHelper.getRTE_Dao_Recibo().getRawRowMapper());

        List<Recibo> list=result.getResults();

        Recibo recibo = list.size()>0? list.get(0) : null;

        if(recibo!=null && incluirItems){
            recibo.items = obtenerItemsDeRecibo(recibo.id);
        }

        if(recibo!=null && incluirImputaciones){
            recibo.imputaciones = obtenerImputacionesDeRecibo(recibo.clienteId);
        }

        if(recibo!=null && incluirImputaciones && incluirDetalleDeItem){

            List<Integer> idsCheques = new ArrayList<>(),
                    idsTarjetasDeCredito = new ArrayList<>(),
                    idsTarjetasDeDebito = new ArrayList<>(),
                    idsRetenciones = new ArrayList<>();

            for (ItemDeRecibo item: recibo.items) {

                if(item.formaDePagoId == Constantes.FORMADEPAGO_CHEQUE)
                    idsCheques.add(item.chequeId);
                else if(item.formaDePagoId == Constantes.FORMADEPAGO_TARJETACREDITO)
                    idsTarjetasDeCredito.add(item.tarjetaDeCreditoId);
                else if(item.formaDePagoId == Constantes.FORMADEPAGO_TARJETADEBITO)
                    idsTarjetasDeDebito.add(item.tarjetaDeDebitoId);
                else if(item.formaDePagoId == Constantes.FORMADEPAGO_RETENCIONES)
                    idsRetenciones.add(item.retencionId);
            }

            recibo.cheques = obtenerChequesPorIds(idsCheques);
            recibo.tarjetasDeCredito = obtenerTarjetasDeCreditoPorIds(idsTarjetasDeCredito);
            recibo.tarjetasDeDebito = obtenerTarjetasDeDebitoPorIds(idsTarjetasDeDebito);
            recibo.retenciones = obtenerRetencionesPorIds(idsRetenciones);

        }

        return recibo;
    }

    public List<ItemDeRecibo> obtenerItemsDeRecibo(int reciboId) throws SQLException {

        String select="SELECT * FROM ItemsDeRecibos WHERE reciboId= "+String.valueOf(reciboId);

        GenericRawResults<ItemDeRecibo> result=_dbHelper.getRTE_Dao_ItemDeRecibo()
                .queryRaw(select, _dbHelper.getRTE_Dao_ItemDeRecibo().getRawRowMapper());

        return result.getResults();
    }

    public List<ItemDeRecibo> obtenerItemsDeReciboPorClienteId(int clienteId) throws SQLException {

        Recibo recibo = obtenerReciboDeCliente(clienteId, false, false, false);

        if(recibo==null){
            return null;
        }else{
            return obtenerItemsDeRecibo(recibo.id);
        }

    }

    public List<ItemDeRecibo> obtenerTodosLosItemsDeRecibos() throws SQLException {

        String select="SELECT * FROM ItemsDeRecibos";

        GenericRawResults<ItemDeRecibo> result=_dbHelper.getRTE_Dao_ItemDeRecibo()
                .queryRaw(select, _dbHelper.getRTE_Dao_ItemDeRecibo().getRawRowMapper());

        return result.getResults();

    }

    public List<Factura> obtenerImputacionesDeRecibo(int clienteId) throws SQLException {

        String select="SELECT * FROM Facturas WHERE cliente_id= "+String.valueOf(clienteId) +
                " AND imputado > 0";

        GenericRawResults<Factura> result=_dbHelper.getRTE_Dao_Factura()
                .queryRaw(select, _dbHelper.getRTE_Dao_Factura().getRawRowMapper());

        return result.getResults();
    }

    public List<Cheque> obtenerChequesPorIds(List<Integer> ids) throws SQLException {

        if(ids==null || ids.size()==0) return new ArrayList<>();

        String select="SELECT * FROM Cheques WHERE id in " + obtenerListaDeIds(ids);

        GenericRawResults<Cheque> result=_dbHelper.getRTE_Dao_Cheque()
                .queryRaw(select, _dbHelper.getRTE_Dao_Cheque().getRawRowMapper());

        return result.getResults();
    }

    public List<TarjetaDeCredito> obtenerTarjetasDeCreditoPorIds(List<Integer> ids) throws SQLException {

        if(ids==null || ids.size()==0) return new ArrayList<>();

        String select="SELECT * FROM TarjetasDeCredito WHERE id in " + obtenerListaDeIds(ids);

        GenericRawResults<TarjetaDeCredito> result=_dbHelper.getRTE_Dao_TarjetaDeCredito()
                .queryRaw(select, _dbHelper.getRTE_Dao_TarjetaDeCredito().getRawRowMapper());

        return result.getResults();
    }

    public List<TarjetaDeDebito> obtenerTarjetasDeDebitoPorIds(List<Integer> ids) throws SQLException {

        if(ids==null || ids.size()==0) return new ArrayList<>();

        String select="SELECT * FROM TarjetasDeDebito WHERE id in " + obtenerListaDeIds(ids);

        GenericRawResults<TarjetaDeDebito> result=_dbHelper.getRTE_Dao_TarjetaDeDebito()
                .queryRaw(select, _dbHelper.getRTE_Dao_TarjetaDeDebito().getRawRowMapper());

        return result.getResults();
    }

    public List<Retencion> obtenerRetencionesPorIds(List<Integer> ids) throws SQLException {

        if(ids==null || ids.size()==0) return new ArrayList<>();

        String select="SELECT * FROM Retenciones WHERE id in " + obtenerListaDeIds(ids);

        GenericRawResults<Retencion> result=_dbHelper.getRTE_Dao_Retencion()
                .queryRaw(select, _dbHelper.getRTE_Dao_Retencion().getRawRowMapper());

        return result.getResults();
    }

    public void guardarItemReciboEfectivo(double importe, Cliente cliente) throws Exception {

        Recibo recibo = obtenerOCrearRecibo(cliente);

        if(tieneItemEfectivoYaCargado(recibo.id)){
            throw new Exception("Ya posee registrado un cobro en efectivo.");
        }

        ItemDeRecibo item = new ItemDeRecibo();
        item.formaDePagoId = Constantes.FORMADEPAGO_EFECTIVO;
        item.importe = importe;
        item.reciboId = recibo.id;

        _dbHelper.getRTE_Dao_ItemDeRecibo().create(item);
    }

    public void guardarItemReciboCheque(Cheque cheque, Cliente cliente) throws Exception {

        Recibo recibo = obtenerOCrearRecibo(cliente);

        Service_Cheques srvCheques = new Service_Cheques(this.CurrentContext);

        srvCheques.guardarCheque(cheque, recibo.id);
    }

    public void guardarItemReciboTarjetaDeDebito(TarjetaDeDebito tarjetaDeDebito, Cliente cliente) throws Exception {

        Recibo recibo = obtenerOCrearRecibo(cliente);

        Service_TarjetaDeDebito srvCheques = new Service_TarjetaDeDebito(this.CurrentContext);

        srvCheques.guardarTarjetaDeDebito(tarjetaDeDebito, recibo.id);
    }

    public void guardarItemReciboTarjetaDeCredito(TarjetaDeCredito tarjetaDeCredito, Cliente cliente) throws Exception {

        Recibo recibo = obtenerOCrearRecibo(cliente);

        Service_TarjetaDeCredito srv = new Service_TarjetaDeCredito(this.CurrentContext);

        srv.guardarTarjetaDeCredito(tarjetaDeCredito, recibo.id);
    }

    public void guardarItemReciboRetencion(Retencion retencion, Cliente cliente) throws Exception {

        Recibo recibo = obtenerOCrearRecibo(cliente);

        Service_Retenciones srv = new Service_Retenciones(this.CurrentContext);

        srv.guardarRetencion(retencion, recibo.id);
    }

    public void eliminarItemDeRecibo(ItemDeRecibo item) throws Exception {

        String deleteItem = "DELETE FROM ItemsDeRecibos WHERE id = "+item.id;
        String instrumento = "";
        int idInstrumento = 0;

        if(item.formaDePagoId == Constantes.FORMADEPAGO_RETENCIONES){
            instrumento = "Retenciones";
            idInstrumento = item.retencionId;
        }else if(item.formaDePagoId == Constantes.FORMADEPAGO_CHEQUE){
            instrumento = "Cheques";
            idInstrumento = item.chequeId;
        }else if(item.formaDePagoId == Constantes.FORMADEPAGO_TARJETACREDITO){
            instrumento = "TarjetasDeCredito";
            idInstrumento = item.tarjetaDeCreditoId;
        }else if(item.formaDePagoId == Constantes.FORMADEPAGO_TARJETADEBITO){
            instrumento = "TarjetasDeDebito";
            idInstrumento = item.tarjetaDeDebitoId;
        }

        String deleteInstrumento = idInstrumento > 0 ? "DELETE FROM "+instrumento+" WHERE id = "+String.valueOf(idInstrumento) : null ;

        SQLiteDatabase db = _dbHelper.getWritableDatabase();

        try{
            db.execSQL(deleteItem);

            if(deleteInstrumento!=null)
                db.execSQL(deleteInstrumento);
        }catch (Exception ex){
            throw ex;
        }finally {

        }

    }

    public void confirmarImputacionesDeRecibo(Cliente cliente, List<Factura> facturas,
                                              ComprobanteFisicoUtilizado comprobanteFisico) throws Exception {

        Recibo recibo = obtenerReciboDeCliente(cliente.cliente_id, true, false, false);

        if(facturas==null)
            facturas = new ArrayList<>();

        if(recibo==null)
            throw new Exception("No existe ningún recibo generado");

        if(recibo.obtenerTotalImptuado(facturas) > recibo.obtenerTotal())
            throw new Exception("El importe imputado en las facturas es superior al monto del recibo");

        for (Factura f: facturas) {
            if(f.imputado > f.saldoActual)
                throw new Exception("Hay al menos una factura cuyo importe imputado es superior al saldo");

            if(f.imputado < 0)
                throw new Exception("No es posible imputar montos negativos");
        }

        recibo.cerrado = true;

        for (Factura f: facturas) {
            if(f.imputado>0){
                _dbHelper.getRTE_Dao_Factura().update(f);
            }
        }

        if(comprobanteFisico != null){
            Service_ComprobantesFisicos srvComprobantes = new Service_ComprobantesFisicos(this.CurrentContext);
            srvComprobantes.registrarComprobanteFisico(comprobanteFisico.tipoDeComprobanteFisico_ids,
                    comprobanteFisico.nroPrefijo, comprobanteFisico.nroComprobante, cliente.cliente_id);

            recibo.comprobanteFisicoNro = comprobanteFisico.nroComprobante;
            recibo.comprobanteFisicoPrefijo = comprobanteFisico.nroPrefijo;
        }

        cliente.saldoFacturacion = cliente.saldoFacturacion - recibo.obtenerTotal();

        _dbHelper.getRTE_Dao_Recibo().update(recibo);

        cliente.cobroFactura = true;

        try{
            new Service_Alertas(this.CurrentContext).DesbloquearClientePorPago(cliente.cliente_id);
        }catch (Exception ex){}

        new Service_Ventas(this.CurrentContext).GuardarVisita(cliente);
    }

    public String imprimirReciboDeCliente(Cliente cliente, Activity activity) throws Exception{

        Printer printer = new Printer(activity, null);
        Recibo recibo = obtenerReciboDeCliente(cliente.cliente_id, true, true, false);

        if(!recibo.cerrado) throw new Exception("El recibo debe estar cerrado");

        Service_HojasDeRutas srvHojaDeRuta = new Service_HojasDeRutas(CurrentContext);
        HojaDeRuta hr = srvHojaDeRuta.obtenerHojaDeRutaActual();

        Service_General srvGeneral = new Service_General(CurrentContext);
        String nombreFantasia =  srvGeneral.obtenerConfiguracion("NOMBRE_FANTASIA","Sistema Water Service");
        String telefonos =  srvGeneral.obtenerConfiguracion("TELEFONO","");
        String domicilio =  srvGeneral.obtenerConfiguracion("WEB_EMPRESA","");

        printer.iniciarSalida();

        printer.agregarEspacio();
        printer.agregarSeparador();
        printer.agregarLinea("    "+nombreFantasia.toUpperCase()+"    ");
        printer.agregarLinea(domicilio);
        printer.agregarLinea("Tel: "+telefonos);
        printer.agregarSeparador();
        printer.agregarLinea("       COMPROBANTE DE PAGO");
        printer.agregarLinea("Cliente: "+ cliente.nombreCliente + " ("+String.valueOf(cliente.cliente_id)+")");
        printer.agregarLinea("Fecha: " + Utiles.Get_Fecha());
        printer.agregarLinea("Hoja de ruta: #" + hr.nombreReparto);
        printer.agregarLinea("Comprobante #" + String.valueOf(hr.id)+"-"+String.valueOf(cliente.cliente_id));

        printer.agregarSeparador();
        printer.agregarLinea(String.format("Saldo previo: $%1$s", Utiles.Round(cliente.obtenerSaldoAlDia() + recibo.obtenerTotal() - cliente.obtenerTotalVentaActual() )));
        printer.agregarLinea(String.format("Consumos del día: $%1$s", Utiles.Round(cliente.obtenerTotalVentaActual())));
        printer.agregarLinea(String.format("Saldo al día: $%1$s", Utiles.Round(cliente.obtenerSaldoAlDia() + recibo.obtenerTotal() )));
        printer.agregarLinea("-------------------------------");

        printer.agregarEspacio();
        printer.agregarLinea("     VALORES RECIBIDOS ");
        printer.agregarEspacio();

        for (ItemDeRecibo item : recibo.items) {
            printer.agregarLinea(String.format("> $%1$s %2$s  %3$s",
                    item.importe,
                    item.getFormaDePago(),
                    item.descripcion == null? "": "("+item.descripcion+")"));
        }

        printer.agregarEspacio();


        printer.agregarLinea("-------------------------------");
        printer.agregarLinea("TOTAL RECIBO: $" + Utiles.Round(recibo.obtenerTotal()));
        printer.agregarLinea("-------------------------------");
        printer.agregarLinea(String.format("Saldo consumos no fc: $%1$s",Utiles.Round(cliente.saldoConsumos)));
        printer.agregarLinea(String.format("Saldo facturación: $%1$s",Utiles.Round(cliente.saldoFacturacion)));
        printer.agregarLinea(String.format("Saldo total: $%1$s",Utiles.Round(cliente.obtenerSaldoAlDia())));
        printer.agregarEspacio();

        if(recibo.comprobanteFisicoNro!=null && !recibo.comprobanteFisicoNro.equals("")){
            printer.agregarLinea(String.format("Recibo nro: %1$s-%2$s",
                    recibo.comprobanteFisicoPrefijo,
                    recibo.comprobanteFisicoNro));
        }

        printer.agregarEspacio();
        printer.agregarSeparador();

        String salida = printer.obtenerContenido();

        printer.findBT();
        printer.openBT();
        printer.imprimirSalida();
        printer.closeBT();

        return salida;
    }

    public boolean tieneItemEfectivoYaCargado(int reciboId) throws Exception {

        String select="SELECT * FROM ItemsDeRecibos WHERE reciboId= "+String.valueOf(reciboId) + " and formaDePagoId = 1";

        GenericRawResults<ItemDeRecibo> result=_dbHelper.getRTE_Dao_ItemDeRecibo()
                .queryRaw(select, _dbHelper.getRTE_Dao_ItemDeRecibo().getRawRowMapper());

        List<ItemDeRecibo> list=result.getResults();

       return list.size()>0;

    }

}
