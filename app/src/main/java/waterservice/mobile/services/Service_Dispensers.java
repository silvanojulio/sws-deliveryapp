package waterservice.mobile.services;

import android.content.Context;

import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.field.types.StringBytesType;

import org.json.JSONObject;

import java.util.List;

import waterservice.mobile.clases.Dispenser;
import waterservice.mobile.clases.PlanificacionDeCarga;
import waterservice.mobile.clases.ValorSatelite;

/**
 * Created by Silvano on 08/03/2017.
 */

public class Service_Dispensers extends  Service_Base {

    public Service_Dispensers(Context context) {
        super(context);
    }

    public Dispenser obtenerDispenserPorNro(String nroDispenser, Integer cliente_id) throws Exception{

        String select="SELECT * FROM Dispensers WHERE nroDispenser='" +nroDispenser + "' ";

        if(cliente_id!=null){
            select += " AND cliente_id = "+ String.valueOf(cliente_id);
        }

        GenericRawResults<Dispenser> result=_dbHelper.getDao_Dispenser()
                .queryRaw(select, _dbHelper.getDao_Dispenser().getRawRowMapper());

        List<Dispenser> list=result.getResults();

        return list.size()>0? list.get(0):null;
    }

    public Dispenser obtenerDispenserParaControlDePlaya(String nroDispenser) throws Exception{

        String path=urlRoot+"/Dispensers/ObtenerDispenserPorNumeroInterno?" +
                "numero="+nroDispenser;

        JSONObject response = _apiClient.getJson(path, null);

        int error=response.getInt("error");

        if(error!=0){
            throw new Exception(response.getString("message"));
        }

        JSONObject jDispenser= response.getJSONObject("dispenser");

        Dispenser d= new Dispenser();

        d.id = jDispenser.getInt("id");
        d.color  = jDispenser.getString("color");
        d.nroDispenser  = jDispenser.getString("numeroInterno");
        d.marca  = jDispenser.getString("marcaDispenser");
        d.tipo  = jDispenser.getString("tipoDispenser");

        return d;
    }

}
