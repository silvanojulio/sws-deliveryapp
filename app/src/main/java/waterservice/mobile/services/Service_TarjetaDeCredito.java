package waterservice.mobile.services;

import android.content.Context;

import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.List;

import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.ItemDeRecibo;
import waterservice.mobile.clases.TarjetaDeCredito;

public class Service_TarjetaDeCredito extends Service_Base {
    public Service_TarjetaDeCredito(Context context){
        super(context);
    }


    public void guardarTarjetaDeCredito(TarjetaDeCredito tarjetaDeCredito, int reciboId) throws Exception {

        if(tarjetaDeCredito.id == 0){

            _dbHelper.getRTE_Dao_TarjetaDeCredito().create(tarjetaDeCredito);

            ItemDeRecibo item = new ItemDeRecibo();
            item.formaDePagoId = Constantes.FORMADEPAGO_TARJETACREDITO;
            item.importe = tarjetaDeCredito.importe;
            item.reciboId = reciboId;
            item.tarjetaDeCreditoId = tarjetaDeCredito.id;
            item.descripcion = tarjetaDeCredito.descripcionResumen;

            _dbHelper.getRTE_Dao_ItemDeRecibo().create(item);

        }else{

            ItemDeRecibo item = obtenerItemDeReciboPorTarjetaDeCreditoId(tarjetaDeCredito.id, reciboId);
            if(item == null) throw new Exception("Debe crear nuevamente el cobro");

            item.importe = tarjetaDeCredito.importe;
            item.descripcion = tarjetaDeCredito.descripcionResumen;
            item.formaDePagoId = Constantes.FORMADEPAGO_TARJETACREDITO;

            _dbHelper.getRTE_Dao_TarjetaDeCredito().update(tarjetaDeCredito);
            _dbHelper.getRTE_Dao_ItemDeRecibo().update(item);

        }
    }

    public ItemDeRecibo obtenerItemDeReciboPorTarjetaDeCreditoId(int tarjetaDeCreditoId, int reciboId) throws SQLException {

        String select="SELECT * FROM ItemsDeRecibos WHERE reciboId= "+String.valueOf(reciboId) +
                " and tarjetaDeCreditoId = " + String.valueOf(tarjetaDeCreditoId);

        GenericRawResults<ItemDeRecibo> result=_dbHelper.getRTE_Dao_ItemDeRecibo()
                .queryRaw(select, _dbHelper.getRTE_Dao_ItemDeRecibo().getRawRowMapper());

        List<ItemDeRecibo> list = result.getResults();

        ItemDeRecibo item = list.size()>0? list.get(0) : null;

        return item;
    }
}
