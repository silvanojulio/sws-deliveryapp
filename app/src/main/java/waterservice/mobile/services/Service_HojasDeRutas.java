package waterservice.mobile.services;


import android.content.Context;

import com.j256.ormlite.dao.GenericRawResults;

import java.util.List;

import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.GastoDeReparto;
import waterservice.mobile.clases.HojaDeRuta;
import waterservice.mobile.clases.UsuarioApp;

public class Service_HojasDeRutas extends  Service_Base{

    public Service_HojasDeRutas(Context context){
        super(context);
    }

    public HojaDeRuta obtenerHojaDeRutaActual()throws Exception {
        String select="SELECT * FROM HojasDeRuta" ;

        GenericRawResults<HojaDeRuta> result=_dbHelper.getRTE_Dao_HojaDeRuta()
                .queryRaw(select, _dbHelper.getRTE_Dao_HojaDeRuta().getRawRowMapper());

        List<HojaDeRuta> list=result.getResults();

        return list.get(0);
    }

    public void cerrarHojaDeRuta(double montoIngresado, String username, String password, HojaDeRuta hojaDeRuta) throws Exception {

        UsuarioApp user = obtenerUsuario(username,password);

        hojaDeRuta.usuarioRepartidor_id= user.usuario_id;
        hojaDeRuta.montoDeclarado=montoIngresado;
        hojaDeRuta.cerrada=true;

        _dbHelper.getRTE_Dao_HojaDeRuta().update(hojaDeRuta);
    }

    public UsuarioApp obtenerUsuario(String username, String password) throws Exception{

        String select="SELECT * FROM UsuariosApp WHERE username='"+username +"' AND password='"+password+"'";

        GenericRawResults<UsuarioApp> result=_dbHelper.getRTE_Dao_UsuarioApp()
                .queryRaw(select, _dbHelper.getRTE_Dao_UsuarioApp().getRawRowMapper());

        List<UsuarioApp> list=result.getResults();

        if(list.size()==0)
            throw new Exception("Error de usuario y contraseña");

        UsuarioApp user =  list.get(0);

        if(!user.password.equals(password))
            throw new Exception("Error de usuario y contraseña");

        return user;
    }

    public UsuarioApp obtenerUsuarioPorId(int usuarioId) throws Exception{

        String select="SELECT * FROM UsuariosApp WHERE usuario_id="+usuarioId;

        GenericRawResults<UsuarioApp> result=_dbHelper.getRTE_Dao_UsuarioApp()
                .queryRaw(select, _dbHelper.getRTE_Dao_UsuarioApp().getRawRowMapper());

        List<UsuarioApp> list=result.getResults();

        if(list.size()==0)
            throw new Exception("No se puede encontrar ningún usuario con el ID "+String.valueOf(usuarioId));

        return list.get(0);
    }

    public void guardarGasto(GastoDeReparto gasto) throws  Exception{


        if( gasto.montoGasto ==0)
            throw new Exception("El importe debe ser superior a 0");

        if(gasto.descripcion.equalsIgnoreCase(""))
            throw new Exception("Debe contener una descripcion");

        _dbHelper.getRTE_Dao_GastoDeReparto().create(gasto);
    }

    public int obtenerCantidadVisitasSinSincronizar() throws Exception {
        List<Cliente> clientes = new Service_Clientes(CurrentContext).obtenerClientes(4,"", appSession.getHojaDeRuta(), 1);
        return clientes.size();
    }
}
