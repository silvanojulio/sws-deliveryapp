package waterservice.mobile.services;

import android.content.Context;

import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.DeclaracionEfectivo;
import waterservice.mobile.clases.Factura;
import waterservice.mobile.clases.GastoDeReparto;
import waterservice.mobile.clases.HojaDeRuta;
import waterservice.mobile.clases.ItemDeRecibo;
import waterservice.mobile.clases.ResumenDeHojaDeRuta;
import waterservice.mobile.clases.Usuario;
import waterservice.mobile.clases.UsuarioApp;
import waterservice.mobile.clases.apiContracts.ResponseBase;
import waterservice.mobile.procesos.ServiceSync;
import waterservice.mobile.server.ApiCallback;
import waterservice.mobile.server.SyncMobile;
import waterservice.mobile.utiles.Utiles;

public class Service_DeclaracionesEfectivo extends Service_Base {

    public Service_DeclaracionesEfectivo(Context context){
        super(context);
    }

    public List<DeclaracionEfectivo> obtenerDeclaracionesEfectivo()throws Exception{

        List<DeclaracionEfectivo> list= _dbHelper.getRTE_Dao_DeclaracionEfectivo().queryForAll();

        return list;
    }

    public double obtenerTotalDeclaracionesEfectivo() throws Exception{

        List<DeclaracionEfectivo> list= obtenerDeclaracionesEfectivo();

        double total = 0;

        for (DeclaracionEfectivo decl: list) {

            total += Double.valueOf(decl.cantidad) * decl.valorNominal;
        }

        return total;
    }

    public double obtenerTotalDeGastos() throws Exception{

        List<GastoDeReparto> list= _dbHelper.getRTE_Dao_GastoDeReparto().queryForAll();

        double total = 0;

        for (GastoDeReparto gasto: list) {

            total += gasto.montoGasto;
        }

        return total;
    }

    public List<DeclaracionEfectivo> obtenerDeclaracionesEfectivo(boolean esMoneda) throws SQLException {

        String select="SELECT * FROM DeclaracionesEfectivo WHERE esMoneda= "+(esMoneda?"'Y'":"'N'") +
                " ORDER BY valorNominal DESC";

        GenericRawResults<DeclaracionEfectivo> result=_dbHelper.getRTE_Dao_DeclaracionEfectivo()
                .queryRaw(select, _dbHelper.getRTE_Dao_DeclaracionEfectivo().getRawRowMapper());

        List<DeclaracionEfectivo> list=result.getResults();

        return list;
    }

    public void guardarCantidades(int declaracion_id, int cantidad ){

        DeclaracionEfectivo declaracion = _dbHelper.getRTE_Dao_DeclaracionEfectivo().queryForId(declaracion_id);
        declaracion.cantidad = cantidad;
        _dbHelper.getRTE_Dao_DeclaracionEfectivo().update(declaracion);
    }

    public ResumenDeHojaDeRuta obtenerResumeDeHojaDeRuta() throws Exception {

        ResumenDeHojaDeRuta resumen = new ResumenDeHojaDeRuta();

        Service_Recibos srvRecibos = new Service_Recibos(this.CurrentContext);

        List<ItemDeRecibo> items = srvRecibos.obtenerTodosLosItemsDeRecibos();

        resumen.efectivo = obtenerTotalDeItems(items, Constantes.FORMADEPAGO_EFECTIVO);
        resumen.efectivo.totalDeclarado = obtenerTotalDeclaracionesEfectivo();

        resumen.cheques = obtenerTotalDeItems(items, Constantes.FORMADEPAGO_CHEQUE);
        resumen.retenciones = obtenerTotalDeItems(items, Constantes.FORMADEPAGO_RETENCIONES);
        resumen.tarjetasDeDebito = obtenerTotalDeItems(items, Constantes.FORMADEPAGO_TARJETADEBITO);
        resumen.tarjetasDeCredito = obtenerTotalDeItems(items, Constantes.FORMADEPAGO_TARJETACREDITO);

        resumen.totalGastos = obtenerTotalDeGastos();

        return resumen;
    }

    public void confirmarCierreDeRuta(final UsuarioApp usuario, final OnServerSimpleResponse onServerSimpleResponse) throws Exception {

        final ResumenDeHojaDeRuta resumen = obtenerResumeDeHojaDeRuta();
        List<DeclaracionEfectivo> declaracionEfectivo = obtenerDeclaracionesEfectivo();

        final Service_HojasDeRutas _srvHojaDeRuta = new Service_HojasDeRutas(this.CurrentContext);
        final HojaDeRuta hojaDeRuta = _srvHojaDeRuta.obtenerHojaDeRutaActual();

        SyncMobile srvSync = new SyncMobile(CurrentContext);
        String codigoMovil = Utiles.Get_Config_Value("codigo_identificacion_movil", CurrentContext);

        List<GastoDeReparto> gastos = _dbHelper.getRTE_Dao_GastoDeReparto().queryForAll();

        if(gastos==null) gastos = new ArrayList<>();

        srvSync.cerrarHojaDeRutaMobile(hojaDeRuta.id, codigoMovil, usuario.usuario_id, gastos, declaracionEfectivo,
                new ApiCallback<ResponseBase>() {
                    @Override
                    public void onSuccess(ResponseBase response) {

                        try {
                            _srvHojaDeRuta.cerrarHojaDeRuta(resumen.efectivo.totalDeclarado,
                                    usuario.username,usuario.password, hojaDeRuta);

                            onServerSimpleResponse.onSuccess();
                        } catch (Exception e) {
                        onServerSimpleResponse.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String message, int errorCode) {
                        onServerSimpleResponse.onError(message);
                    }
                });


    }

    private ResumenDeHojaDeRuta.TotalFormaDePago obtenerTotalDeItems(List<ItemDeRecibo> items, int formaDePagoId){

        double total = 0;
        int cantidad = 0;

        for (ItemDeRecibo item: items) {
            if(item.formaDePagoId == formaDePagoId)
            {
                total +=item.importe;
                cantidad ++;
            }
        }

        ResumenDeHojaDeRuta.TotalFormaDePago _total = new ResumenDeHojaDeRuta.TotalFormaDePago();
        _total.totalRegistrado = total;
        _total.cantidadItems = cantidad;

        return _total;
    }

    public static interface OnServerSimpleResponse{
        void onSuccess();
        void onError(String message);
    }
}
