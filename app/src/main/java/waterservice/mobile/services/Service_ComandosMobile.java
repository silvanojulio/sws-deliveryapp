package waterservice.mobile.services;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.apiContracts.Sync.ComandoMobile;
import waterservice.mobile.clases.apiContracts.Sync.ObtenerComandosParaEjecutarResponse;
import waterservice.mobile.server.ApiCallback;
import waterservice.mobile.server.SyncMobile;
import waterservice.mobile.utiles.Utiles;

public class Service_ComandosMobile extends  Service_Base{

    Service_Clientes srvClientes;
    SyncMobile srvSyncMobile;

    public Service_ComandosMobile(Context context) {
        super(context);
        srvClientes = new Service_Clientes(context);
        srvSyncMobile = new SyncMobile(context);
    }

    public void obtenerYProcesarNuevosComandos(){

        String identificadorMovil= Utiles.Get_Config_Value("codigo_identificacion_movil", CurrentContext);

        srvSyncMobile.obtenerNuevosComandos(identificadorMovil,
                new ApiCallback<ObtenerComandosParaEjecutarResponse>() {
                    @Override
                    public void onSuccess(ObtenerComandosParaEjecutarResponse response) {
                        procesarComandos(response.comandos);
                    }

                    @Override
                    public void onError(String message, int errorCode) {
                       // Toast.makeText(CurrentContext, "Comandos: Error al conectar al servidor: "+message, Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void procesarComandos(List<ComandoMobile> comandos) {

        List<String> idsComandosEjecutados = new ArrayList<>();

        for (ComandoMobile comando: comandos) {

            try{
                if(comando.comando.equals( "EliminarAusenteNoCompra")){
                    ejecutarEliminarAusenteNoCompra(comando);
                }
                idsComandosEjecutados.add(comando._id);
            }catch (Exception ex){}
        }

        confirmarEjecucion(idsComandosEjecutados);
    }

    private void ejecutarEliminarAusenteNoCompra(ComandoMobile comando) throws Exception {

        Cliente cliente = srvClientes.obtenerCliente(Integer.valueOf(String.valueOf(comando.valor1)));
        if(!cliente.visitado ) return;

        cliente.ausente=false;
        cliente.visitado=false;
        cliente.visita_longitud= null;
        cliente.visita_altitud= null;

        _dbHelper.getRTE_Dao_Cliente().update(cliente);
    }

    private void confirmarEjecucion( List<String> idsComandosEjecutados){


    }
}
