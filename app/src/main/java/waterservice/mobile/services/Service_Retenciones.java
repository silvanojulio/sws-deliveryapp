package waterservice.mobile.services;

import android.content.Context;

import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.List;

import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.ItemDeRecibo;
import waterservice.mobile.clases.Retencion;

public class Service_Retenciones extends Service_Base {
    public Service_Retenciones(Context context){
        super(context);
    }


    public void guardarRetencion(Retencion Retencion, int reciboId) throws Exception {

        if(Retencion.id == 0){

            _dbHelper.getRTE_Dao_Retencion().create(Retencion);

            ItemDeRecibo item = new ItemDeRecibo();
            item.formaDePagoId = Constantes.FORMADEPAGO_RETENCIONES;
            item.importe = Retencion.importe;
            item.reciboId = reciboId;
            item.retencionId = Retencion.id;
            item.descripcion = Retencion.descripcionResumen;

            _dbHelper.getRTE_Dao_ItemDeRecibo().create(item);

        }else{

            ItemDeRecibo item = obtenerItemDeReciboPorRetencionId(Retencion.id, reciboId);
            if(item == null) throw new Exception("Debe crear nuevamente el cobro");

            item.importe = Retencion.importe;
            item.descripcion = Retencion.descripcionResumen;
            item.formaDePagoId = Constantes.FORMADEPAGO_RETENCIONES;

            _dbHelper.getRTE_Dao_Retencion().update(Retencion);
            _dbHelper.getRTE_Dao_ItemDeRecibo().update(item);

        }
    }

    public ItemDeRecibo obtenerItemDeReciboPorRetencionId(int retencionId, int reciboId) throws SQLException {

        String select="SELECT * FROM ItemsDeRecibos WHERE reciboId= "+String.valueOf(reciboId) +
                " and retencionId = " + String.valueOf(retencionId);

        GenericRawResults<ItemDeRecibo> result=_dbHelper.getRTE_Dao_ItemDeRecibo()
                .queryRaw(select, _dbHelper.getRTE_Dao_ItemDeRecibo().getRawRowMapper());

        List<ItemDeRecibo> list = result.getResults();

        ItemDeRecibo item = list.size()>0? list.get(0) : null;

        return item;
    }
}
