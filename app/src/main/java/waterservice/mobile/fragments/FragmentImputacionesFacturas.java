package waterservice.mobile.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.adapters.ImputacionFacturaAdapter;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.Factura;
import waterservice.mobile.clases.ImputacionDeRecibo;
import waterservice.mobile.clases.Recibo;
import waterservice.mobile.services.Service_Clientes;
import waterservice.mobile.services.Service_General;
import waterservice.mobile.services.Service_Recibos;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class FragmentImputacionesFacturas extends Fragment {

    public FragmentImputacionesFacturas.Acciones acciones;
    private Button btn_confirmarImputaciones;
    public List<Factura> facturas;

    private Cliente cliente;
    private AppSession appSession;

    private RecyclerView lst_facturas;

    public FragmentImputacionesFacturas() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_imputaciones_facturas, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {
        appSession = new AppSession(getContext());
        cliente = appSession.getClienteActual();

        btn_confirmarImputaciones = (Button) this.getView().findViewById(R.id.btn_confirmarImputaciones);
        btn_confirmarImputaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmarOnClick();
            }
        });

        lst_facturas = (RecyclerView) this.getView().findViewById(R.id.lst_facturas);

        if(facturas==null){
            facturas = new Service_Clientes(getContext()).obtenerFacturasDeCliente(cliente.cliente_id);
        }

        Utiles.prepareRecyclerView(lst_facturas, getContext());

        Recibo recibo = new Service_Recibos(this.getContext()).obtenerReciboDeCliente(
                cliente.cliente_id, false, false, false);

        ImputacionFacturaAdapter adapter = new ImputacionFacturaAdapter(facturas, getContext(), new ImputacionFacturaAdapter.Acciones() {
            @Override
            public void onActualizarTotal() {
                if(acciones!=null)
                    acciones.onActualizarTotales();
            }
        }, recibo == null || !recibo.cerrado);

        lst_facturas.setAdapter(adapter);
    }

    public double obtenerTotalImputado(){

        if(facturas==null) return 0;

        double total=0;

        for (Factura f:facturas) {
            total += f.imputado;
        }

        return total;
    }

    public void confirmarOnClick(){

        if(acciones!=null)
            acciones.onConfirmarImputaciones();
    }

    public interface Acciones{
        void onConfirmarImputaciones();
        void onActualizarTotales();
    }
}
