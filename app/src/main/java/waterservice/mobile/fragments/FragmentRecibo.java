package waterservice.mobile.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.sql.SQLException;
import java.util.List;

import ar.com.waterservice.newapp.R;
import waterservice.mobile.Session.AppSession;
import waterservice.mobile.activities.formasDePago.ActivityFormaDePagoCheque;
import waterservice.mobile.activities.formasDePago.ActivityFormaDePagoEfectivo;
import waterservice.mobile.activities.formasDePago.ActivityFormaDePagoRetencion;
import waterservice.mobile.activities.formasDePago.ActivityFormaDePagoTarjetaDeCredito;
import waterservice.mobile.activities.formasDePago.ActivityFormaDePagoTarjetaDeDebito;
import waterservice.mobile.activities.formasDePago.ActivitySeleccionarFormaDePago;
import waterservice.mobile.adapters.ItemReciboAdapter;
import waterservice.mobile.clases.Cliente;
import waterservice.mobile.clases.Constantes;
import waterservice.mobile.clases.ItemDeRecibo;
import waterservice.mobile.clases.Recibo;
import waterservice.mobile.services.Service_Recibos;
import waterservice.mobile.utiles.Dialog;
import waterservice.mobile.utiles.SessionKeys;
import waterservice.mobile.utiles.SessionVars;
import waterservice.mobile.utiles.Utiles;

public class FragmentRecibo extends Fragment {

    FloatingActionButton btn_agregarItem;
    public Acciones acciones;
    private Cliente cliente;
    private RecyclerView lst_itemsRecibo;
    public List<ItemDeRecibo> items;
    private AppSession appSession;

    private Service_Recibos srvRecibos;

    public FragmentRecibo() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recibo, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {

        appSession = new AppSession(this.getContext());
        srvRecibos = new Service_Recibos(this.getContext());
        cliente = appSession.getClienteActual();

        btn_agregarItem = (FloatingActionButton) this.getView().findViewById(R.id.btn_agregarItem);
        btn_agregarItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAgregarItemClick();
            }
        });
        lst_itemsRecibo = (RecyclerView) this.getView().findViewById(R.id.lst_itemsRecibo);
        Utiles.prepareRecyclerView(lst_itemsRecibo, getContext());

        try {
            cargarItemsDeRecibo();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void cargarItemsDeRecibo() throws SQLException {

        items = srvRecibos.obtenerItemsDeReciboPorClienteId(cliente.cliente_id);

        Recibo recibo = srvRecibos.obtenerReciboDeCliente(cliente.cliente_id, false, false, false);

        ItemReciboAdapter adapter = new ItemReciboAdapter(items, this.getContext(),
            new ItemReciboAdapter.Acciones() {
            @Override
            public void onEliminar(final ItemDeRecibo item) {

                Dialog.confirm("Eliminar","Confirma eliminar este cobro?",new Dialog.OnConfirm(){
                    @Override
                    public void confirm() {
                        onEliminarItemRecibo(item);
                    }
                }, getContext());
            }

            @Override
            public void onEditar(ItemDeRecibo item) {
                onEditarItemRecibo(item);
            }
        }, recibo == null || !recibo.cerrado);

        lst_itemsRecibo.setAdapter(adapter);

        if(acciones!=null)acciones.onActualizarTotal();
    }

    private void onEliminarItemRecibo(ItemDeRecibo item) {

        try{

            srvRecibos.eliminarItemDeRecibo(item);

            if(acciones==null)
                acciones.onItemAgregado();

            try {
                cargarItemsDeRecibo();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }catch (Exception ex){
            Dialog.error("Error", ex.getMessage(),this.getContext());
        }
    }

    private void onEditarItemRecibo(ItemDeRecibo item) {

    }

    private void onAgregarItemClick(){

       try{
           Recibo recibo = srvRecibos.obtenerReciboDeCliente(cliente.cliente_id, false, false, false);

           if(recibo!=null && recibo.cerrado)
               throw new Exception("El recibo está cerrado, no se permite edición");

           Intent intent = new Intent(this.getContext(), ActivitySeleccionarFormaDePago.class);
           startActivityForResult(intent, Constantes.ACTIVITY_SELECCIONAR_FORMA_DE_PAGO);

       }catch (Exception ex){
            Dialog.error("Error", ex.getMessage(),getContext());
       }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case Constantes.ACTIVITY_SELECCIONAR_FORMA_DE_PAGO:
                onSeleccionarFormaDePagoResult(resultCode);
                break;
            case Constantes.ACTIVITY_AGREGAR_ITEM_RECIBO:
                onAgregarItemResult(resultCode);
                break;
        }
    }

    private void onAgregarItemResult(int resultCode){

        if(resultCode != Activity.RESULT_OK) return;

        if(acciones==null)
            acciones.onItemAgregado();

        try {
            cargarItemsDeRecibo();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void onSeleccionarFormaDePagoResult(int resultCode) {

        if(resultCode != Activity.RESULT_OK) return;

        Intent intent = null;

        int formaDePagoId = (Integer) SessionVars.Get_Session_Var(SessionKeys.FORMA_DE_PAGO_SELECTED);

        if(formaDePagoId == Constantes.FORMADEPAGO_EFECTIVO)
            intent = new Intent(this.getContext(), ActivityFormaDePagoEfectivo.class);
        else if(formaDePagoId == Constantes.FORMADEPAGO_CHEQUE)
            intent = new Intent(this.getContext(), ActivityFormaDePagoCheque.class);
        else if(formaDePagoId == Constantes.FORMADEPAGO_RETENCIONES)
            intent = new Intent(this.getContext(), ActivityFormaDePagoRetencion.class);
        else if(formaDePagoId == Constantes.FORMADEPAGO_TARJETACREDITO)
            intent = new Intent(this.getContext(), ActivityFormaDePagoTarjetaDeCredito.class);
        else if(formaDePagoId == Constantes.FORMADEPAGO_TARJETADEBITO)
            intent = new Intent(this.getContext(), ActivityFormaDePagoTarjetaDeDebito.class);
        else
            intent = new Intent(this.getContext(), ActivityFormaDePagoEfectivo.class);

        startActivityForResult(intent, Constantes.ACTIVITY_AGREGAR_ITEM_RECIBO);
    }

    public double obtenerTotal(){

        if(items == null) return 0;

        double totalRecibo = 0;

        for (ItemDeRecibo item: items) {
            totalRecibo += item.importe;
        }

        return totalRecibo;
    }

    public interface Acciones{
        void onItemAgregado();
        void onActualizarTotal();
    }
}
